﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("Atlas Report")] 
[assembly: AssemblyDescription("Atlas Report")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en-GB", System.Resources.UltimateResourceFallbackLocation.MainAssembly)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None,          // where theme specific resource dictionaries are located 
                                                               //    (used if a resource is not found in the page, or application resource dictionaries)
                     ResourceDictionaryLocation.SourceAssembly // where the generic resource dictionary is located
                                                               //    (used if a resource is not found in the page, app, or any theme specific resource dictionaries)
)]

/*--------------------------------------------------------------------
  Assembly product attributes 
  *** IMPORTANT: DO NOT MODIFY THESE VALUES
--------------------------------------------------------------------*/

[assembly: AssemblyProduct("Atlas Report")]               // Used to determine data directories (..\AtlasLink\...) and display titles (ATLAS LINK)
[assembly: AssemblyVersion("4.0.0.1")]                  // CLR Binding Version 
[assembly: AssemblyFileVersion("1.0.0.0")]              // File Version (Product Version + Build Number)
[assembly: AssemblyInformationalVersion("1.0.0-DEV")]   // Product version (Product Version + Build Number + Development Phase)