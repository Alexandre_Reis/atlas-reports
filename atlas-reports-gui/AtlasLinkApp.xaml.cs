﻿namespace Atlas.Link
{
    /// <summary>
    /// The AtlasLink Application implementation.
    /// </summary>
    public partial class AtlasLinkApp : Atlas.Presentation.AtlasApplication { }
}