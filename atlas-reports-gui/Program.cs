﻿using System;
using System.Windows.Forms;

namespace Atlas.Link
{
    /// <summary>
    /// The class the exposes the static entry point for the application.
    /// </summary>
    class Startup
    {
        #region Main Entry Point

        private static Common.Logging.ILog Logger = null;

        [STAThread()]
        static void Main()
        {
            try
            {
                
                // initialize the entry-point logger with a SafeLogger to ensure fatal exceptions are logged
                Logger = new Common.Logging.Simple.ConsoleOutLogger("Console", Common.Logging.LogLevel.All, true, true, false, "G");

                Logger.Debug("Set the AppDomain.CurrentDomain.UnhandledException");

                // wire-up domain level events (import when dynamically instantiating types)
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                Logger.Debug("Set the ILoggerFactoryAdapter");
                // IOC initialization
                ServiceLocator.Register(typeof(Common.Logging.ILoggerFactoryAdapter), typeof(Atlas.Link.Logging.AtlasNLogFactory));
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    Logger.Debug("Set the ITranslationProvider ,DebugTranslationWriter");
                    ServiceLocator.Register(typeof (Atlas.Link.Localization.ITranslationProvider), typeof (Atlas.Link.Localization.DebugTranslationWriter));
                }
                else
                {
                    Logger.Debug("Set the ITranslationProvider ,RuntimeTranslationProvider");
                    ServiceLocator.Register(typeof (Atlas.Link.Localization.ITranslationProvider), typeof (Atlas.Link.Localization.RuntimeTranslationProvider));
                }

                Logger.Debug("Set the IPresentationManager");
                ServiceLocator.Register(typeof(Atlas.Link.Presentation.IPresentationManager), typeof(Atlas.Link.AtlasLinkApp));

                Logger.Debug("Set the ISettingsStorageProvider");
                ServiceLocator.Register(typeof(Atlas.Link.Settings.ISettingsStorageProvider), typeof(Atlas.Link.Settings.FileSettingsProvider));

                Logger.Debug("Set the IApplicationSettings");
                ServiceLocator.Register(typeof(Atlas.Link.Settings.IApplicationSettings), typeof(Atlas.Presentation.Settings.ApplicationSettingsProvider));
                
                //IMPORTANT: initialize cross-cutting concerns
                Logger = ServiceLocator.Get<Common.Logging.ILoggerFactoryAdapter>().GetLogger(typeof(Startup)); // if this fails, the SafeLogger is still enabled

                Logger.Debug("Logger craeted");

                // create the presentation manager (application)
                var app = ServiceLocator.Get<Atlas.Link.Presentation.IPresentationManager>();

                Logger.Debug("get app service locater");


                Logger.Debug("app.start()");
                app.Start();
            }
            catch (RuntimeException ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
                Logger.Fatal("Main", ex);
            }
            catch (Exception ex)
            {
                //TODO: localize a meaningful error message
                System.Windows.MessageBox.Show("A fatal runtime error occurred. The application must shut down.\n\n" + ex.Message);

                Logger.Fatal("Main", ex);
            }
            finally
            {
                ServiceLocator.Release();
            }
        }

        #endregion

        #region Domain Events

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if ((e.ExceptionObject != null) && (e.ExceptionObject is Exception))
            {
                Exception ex = (e.ExceptionObject as Exception);
                Logger.Error("Unhandled domain exception", ex);
            }
        }

        #endregion

        #region Alternate Domain Entry Point

        //[STAThread()]
        //static void Main()
        //{
        //    AppDomain domain =
        //    AppDomain.CreateDomain(
        //    "another domain");
        //    CrossAppDomainDelegate action = () =>
        //    {
        //        App app = new App();
        //        app.MainWindow = new Window1();
        //        app.MainWindow.Show();
        //        app.Run();
        //    };
        //    domain.DoCallBack(action);
        //}

        #endregion
    }
}