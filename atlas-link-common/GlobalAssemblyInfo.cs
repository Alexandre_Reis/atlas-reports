﻿using System.Reflection;

/*--------------------------------------------------------------------
  Shared company attributes
--------------------------------------------------------------------*/

[assembly: AssemblyCompany("BRITech")]
[assembly: AssemblyCopyright("Copyright © BRITech 2014")]
[assembly: AssemblyTrademark("BRITech")]
[assembly: AssemblyCulture("")]

/*--------------------------------------------------------------------
  Shared build configuration attributes
--------------------------------------------------------------------*/

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif