﻿using System.Reflection;
using System.Runtime.InteropServices;

/*--------------------------------------------------------------------
  Assembly project attributes
--------------------------------------------------------------------*/

[assembly: AssemblyTitle("atlas-bus-core")]
[assembly: AssemblyDescription("atlas-bus-core")]
[assembly: ComVisible(false)]
[assembly: Guid("54e8548e-900e-4b80-899c-5ef1132d409a")]

/*--------------------------------------------------------------------
  Assembly product attributes
--------------------------------------------------------------------*/

[assembly: AssemblyVersion("4.0.0.1")]     // CLR Binding Version (API version: modified manually)
[assembly: AssemblyFileVersion("1.0.0.0")] // Win32 File version (modified by build system)