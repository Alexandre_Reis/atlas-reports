﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Defines a Model Entity.
    /// </summary>
    public interface IEntity
    {
        String ID { get; }
    }
}
