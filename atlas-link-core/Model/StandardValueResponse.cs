﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "response", Namespace = "")]
    public class StandardValueResponse : StandardResponse
    {
        #region Serializable Properties

        [XmlElement(ElementName = "value")]
        public List<String> Values { get; set; }

        #endregion
    }
}