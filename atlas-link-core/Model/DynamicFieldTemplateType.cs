﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Model
{
    public enum DynamicFieldTemplateType
    {
        Text,
        CheckBox,
        ComboBox,
        DatePicker
    }
}