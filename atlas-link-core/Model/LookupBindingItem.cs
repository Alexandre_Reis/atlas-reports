﻿using System;

namespace Atlas.Link.Model
{
    /// <summary>
    /// A generic container for lookup value binding (like Combo Boxes).
    /// This allows custom formatting for lookup items display members.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    /// <typeparam name="TDisplay">The type of the display.</typeparam>
    public class LookupBindingItem<TValue, TDisplay>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LookupBindingItem{TValue, TDisplay}"/> class.
        /// </summary>
        /// <param name="valueMember">The value member.</param>
        /// <param name="displayMember">The display member.</param>
        public LookupBindingItem(TValue valueMember, TDisplay displayMember)
        {
            this.ValueMember = valueMember;
            this.DisplayMember = displayMember;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value member.
        /// </summary>
        /// <value>
        /// The value member.
        /// </value>
        public TValue ValueMember { get; private set; }
        /// <summary>
        /// Gets the display member.
        /// </summary>
        /// <value>
        /// The display member.
        /// </value>
        public TDisplay DisplayMember { get; private set; }

        #endregion
    }
}