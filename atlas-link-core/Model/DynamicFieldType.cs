﻿using System;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Defines the data type of the field
    /// </summary>
    public enum DynamicFieldType : byte
    {
        #region Values

        String = 0,
        Number = 1,
        Boolean = 2,
        DateTime = 3,
        Enum = 4,
        Object = 5

        #endregion
    }
}