﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Defines a row of dynamic data
    /// </summary>
    public class DynamicEntityRow : IDynamicEntityRow, INotifyPropertyChanged
    {
        #region Construction

        private static Int32 GlobalEntityID = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicEntityRow" /> class.
        /// </summary>
        /// <param name="fieldValues">The field values.</param>
        public DynamicEntityRow(IDictionary<String, Object> fieldValues)
        {
            _entityID = System.Threading.Interlocked.Increment(ref GlobalEntityID);
            _currFieldValues = fieldValues;
        }

        #endregion

        #region Fields

        private Int32 _entityID;
        private IDictionary<String, Object> _currFieldValues;
        private IDictionary<String, Object> _origFieldValues;

        #endregion

        #region IDynamicEntity

        /// <summary>
        /// Determines whether the field exists.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>
        /// flag indicating whether it exists
        /// </returns>
        public Boolean ContainsField(String fieldName)
        {
            return _currFieldValues.ContainsKey(fieldName);
        }
        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public Object GetFieldValue(String fieldName)
        {
            Object result = null;

            if (_currFieldValues.TryGetValue(fieldName, out result))
                return result;

            throw new Exception(String.Format("Field [{0}] was not found", fieldName));
        }
        /// <summary>
        /// Sets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="newValue">The value.</param>
        public void SetFieldValue(String fieldName, Object newValue)
        {
            if (_origFieldValues == null)
                _origFieldValues = new Dictionary<String, Object>();

            Object curValue = null;

            if (_currFieldValues.TryGetValue(fieldName, out curValue))
            {
                if (_origFieldValues.ContainsKey(fieldName) == false)
                    _origFieldValues.Add(fieldName, curValue);

                _currFieldValues[fieldName] = newValue;
            }
            else
            {
                _currFieldValues.Add(fieldName, newValue);
            }

            RaisePropertyChanged(fieldName);
        }
        /// <summary>
        /// Resets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        public void ResetFieldValue(String fieldName)
        {
            if (_origFieldValues != null) 
            {
                Object origValue = null;

                if (_origFieldValues.TryGetValue(fieldName, out origValue))
                {
                    _currFieldValues[fieldName] = origValue; // field must exists here due to existence in original
                }            
            }
        }
        /// <summary>
        /// Resets all field values.
        /// </summary>
        public void ResetAllFieldValues()
        {
            if (_origFieldValues != null)
            {
                foreach (String fieldName in _origFieldValues.Keys)
                {
                    _currFieldValues[fieldName] = _origFieldValues[fieldName];
                }
            }
        }
        /// <summary>
        /// Indicated whether any values have been modified
        /// </summary>
        /// <value>
        /// The modified flag.
        /// </value>
        public Boolean IsModified
        {
            get
            {
                if (_origFieldValues != null)
                {
                    foreach (String fieldName in _origFieldValues.Keys)
                    {
                        // this is necessary because a field can get assigned its original value (then it has not changed)
                        if (_currFieldValues[fieldName] != _origFieldValues[fieldName])
                            return true;
                    }
                }

                return false;
            }
        }

        #endregion

        #region INotifyPropertyChanged

        /// <summary>
        /// Occurs when [property changed].
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Overrides

        public override int GetHashCode()
        {
            return _entityID;
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType().Equals(this.GetType()) &&
                   obj.GetHashCode().Equals(this.GetHashCode());
        }
        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (KeyValuePair<String, Object> kvp in _currFieldValues)
            {
                if (sb.Length == 0)
                    sb.AppendFormat("{0}={1}", kvp.Key, kvp.Value);
                else
                    sb.AppendFormat(", {0}={1}", kvp.Key, kvp.Value);
            }

            return sb.ToString();
        }

        #endregion
    }
}