﻿
namespace Atlas.Link.Messages
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandMessage
    {
        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public CommandType Command { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// The insert
        /// </summary>
        Insert,
        /// <summary>
        /// The edit
        /// </summary>
        Edit,
        /// <summary>
        /// The delete
        /// </summary>
        Delete,
        /// <summary>
        /// The commit
        /// </summary>
        Commit,
        /// <summary>
        /// The refresh
        /// </summary>
        Refresh
    }

}
