﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    [Serializable]
    [XmlRoot("user_settings_directory")]
    public class UserSettingsDirectory
    {
        public String Key { get; set; }
        public String Value { get; set; }
    }
}
