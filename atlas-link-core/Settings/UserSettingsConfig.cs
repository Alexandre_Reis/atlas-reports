﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    [Serializable]
    [XmlRoot("user_settings")]
    public class UserSettingsConfig : IUserSettingsConfig
    {
        [XmlArray(ElementName = "user_settings_directories")]
        [XmlArrayItem(ElementName = "user_settings_directory")]
        public List<UserSettingsDirectory> DialogDirectories { get; set; }

        [XmlArray(ElementName = "user_settings_show_dialogs")]
        [XmlArrayItem(ElementName = "user_settings_show_dialog")]
        public List<UserSettingsShowDialog> ShowDialogs { get; set; }


    }
}
