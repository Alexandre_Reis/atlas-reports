﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Atlas.Link.Settings
{
    public class ViewProviderElement : ConfigurationElement
    {
        public ViewProviderElement() { }

        [ConfigurationProperty("type", IsRequired = true, DefaultValue = "")]
        //[StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\", MinLength = 1, MaxLength = 256)]
        public String Type
        {
            get { return (String)this["type"]; }
            set { this["type"] = value; }
        }
    }
}