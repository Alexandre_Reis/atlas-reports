﻿using System;
using System.Collections.Generic;
using DevExpress.Data.Filtering.Helpers;

namespace Atlas.Link.Settings
{

    /// <summary>
    /// 
    /// </summary>
    public interface IApplicationConfig
    {
        /// <summary>
        /// Gets the UI locale.
        /// </summary>
        /// <value>
        /// The UI locale.
        /// </value>
        String UiLocale { get; }

        /// <summary>
        /// Gets the XLS folder.
        /// </summary>
        /// <value>
        /// The XLS folder.
        /// </value>
        String XlsFolder { get; }

        List<DbConfig> Databases { get; }

    }
}