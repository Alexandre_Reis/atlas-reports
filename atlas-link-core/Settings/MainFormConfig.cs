﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    [Serializable]
    [XmlRoot("main_form")]
    public class MainFormConfig
    {
        [XmlElement("dummy_val")]
        public String DummyVal { get; set; }
    }
}