﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Settings
{
    public interface IApplicationSettings
    {
        /// <summary>
        /// Determines whether the application configuration file exists.
        /// </summary>
        /// <value>
        /// The application configuration exists.
        /// </value>
        Boolean ApplicationConfigExists { get; }

        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <returns></returns>
        ApplicationConfig GetApplicationConfig();

        /// <summary>
        /// Saves the application configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        void SetApplicationConfig(ApplicationConfig config);

        /// <summary>
        /// Gets the main form configuration.
        /// </summary>
        /// <returns></returns>
        MainFormConfig GetMainFormConfig();

        /// <summary>
        /// Saves the main form configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        void SetMainFormConfig(MainFormConfig config);

        /// <summary>
        /// Gets the view configuration.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        ViewConfig GetViewConfig(String viewName);

        /// <summary>
        /// Saves the view configuration.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="config">The configuration.</param>
        void SetViewConfig(String viewName, ViewConfig config);

        UserSettingsConfig GetUserSettingsConfig();

        void SetUserSettingsConfig(UserSettingsConfig config);

    }
}