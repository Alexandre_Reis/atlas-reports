﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Atlas.Link.Settings
{
    public class ViewProviderSection : ConfigurationSection
    {
        public static ViewProviderSection Settings
        {
            get 
            { 
                if (_settings == null)
                {
                    try
                    {
                        _settings = (ConfigurationManager.GetSection("viewProvider") as ViewProviderSection);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        
                        throw;
                    }
                }
                return _settings; 
            }
        }
        private static ViewProviderSection _settings = null;

        [ConfigurationProperty("views", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ViewProviderCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public ViewProviderCollection Views
        {
            get { return this["views"] as ViewProviderCollection; }
        }
    }
}