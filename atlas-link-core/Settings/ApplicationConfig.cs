﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("application")]
    public class ApplicationConfig : IApplicationConfig
    {
        [XmlElement("user_interface_locale")]
        public string UiLocale { get;  set; }

        [XmlElement("xls_folder")]
        public string XlsFolder { get; set; }

        [XmlArray("databases")]
        [XmlArrayItem("database")]
        public List<DbConfig> Databases { get; set; }
    }

    [Serializable]
    [XmlRoot("database")]
    public class DbConfig 
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("provider_name")]
        public string ProviderName { get; set; }

        [XmlAttribute("server_name")]
        public string ServerName { get; set; }

        [XmlAttribute("database_name")]
        public string DatabaseName { get; set; }

        [XmlAttribute("user_id")]
        public string UseriD { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }
    }
}