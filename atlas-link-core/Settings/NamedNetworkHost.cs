﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    [Serializable]
    [XmlRoot("network_service_host")]
    public class NamedNetworkHost
    {
        [XmlAttribute("service_name")]
        public String ServiceName { get; set; }

        [XmlAttribute("network_host_name")]
        public String HostName { get; set; }

        [XmlAttribute("network_host_port")]
        public Int32 HostPort { get; set; }
    }
}