﻿using System;
using System.Collections.Generic;

namespace Atlas.Link.Settings
{
    public interface IUserSettingsConfig
    {
        List<UserSettingsDirectory> DialogDirectories { get; }
        List<UserSettingsShowDialog> ShowDialogs { get; }
    }
}
