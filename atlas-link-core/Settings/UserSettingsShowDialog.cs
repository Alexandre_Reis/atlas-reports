﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    [Serializable]
    [XmlRoot("user_settings_show_dialog")]
    public class UserSettingsShowDialog
    {
        public String Key { get; set; }
        public Boolean ShowDialog { get; set; }
    }
}
