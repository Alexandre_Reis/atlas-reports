﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Atlas.Link.Windows
{
    /// <summary>
    /// Interaction logic for TriggerAnimationWindow.xaml
    /// </summary>
    public partial class TriggerAnimationWindow : Window
    {
        public static void ShowAnimationWindow()
        {
            TriggerAnimationWindow window = new TriggerAnimationWindow();
            window.ShowDialog();
        }

        public TriggerAnimationWindow()
        {
            InitializeComponent();
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            Close();
        }

    }
}
