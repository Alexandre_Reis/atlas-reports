﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Atlas.Link.Localization;

namespace Atlas.Link.Presentation
{
    public enum ApplicationLogLevel
    {
        Info, Warn, Error
    }

    public class ApplicationLogData : INotifyPropertyChanged, IDisposable
    {
        private static PropertyChangedEventArgs LevelTextEventArgs = new PropertyChangedEventArgs("LevelText");
        private static PropertyChangedEventArgs MessageTextEventArgs = new PropertyChangedEventArgs("MessageText");

        #region Construction

        public static ApplicationLogData Info(LocalizedString message, params Object[] args)
        {
            return new ApplicationLogData(ApplicationLogLevel.Info, message, args);
        }
        public static ApplicationLogData Warn(LocalizedString message, params Object[] args)
        {
            return new ApplicationLogData(ApplicationLogLevel.Warn, message, args);
        }
        public static ApplicationLogData Error(LocalizedString message, params Object[] args)
        {
            return new ApplicationLogData(ApplicationLogLevel.Error, message, args);
        }

        private ApplicationLogData(ApplicationLogLevel level, LocalizedString message, params Object[] args)
        {
            _messageString = message;
            _args = args;

            this.Level = level;
            this.Time = DateTime.Now;

            switch (level)
            {
                case ApplicationLogLevel.Info:
                    this.LevelText = Localization.StringConst.Data.Info;
                    break;
                case ApplicationLogLevel.Warn:
                    this.LevelText = Localization.StringConst.Data.Warn;
                    break;
                case ApplicationLogLevel.Error:
                    this.LevelText = Localization.StringConst.Data.Error;
                    break;
                default:
                    this.LevelText = Localization.StringConst.Data.Info;
                    break;
            }

            _messageString = message;
            _args = args;

            UpdateMessageText();

            _messageString.PropertyChanged += new PropertyChangedEventHandler(MessageString_PropertyChanged);
        }

        #endregion

        #region Fields

        private LocalizedString _messageString;
        private Object[] _args;

        #endregion

        #region Properties

        public ApplicationLogLevel Level { get; private set; }
        public DateTime Time { get; private set; }
        public LocalizedString LevelText { get; private set; }
        public String MessageText { get; private set; }
        public String LevelTextString { get { return this.Level.ToString(); } } // xaml dont recognize localizedstring on databinds of styles
        #endregion

        #region Utility Members

        private void MessageString_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMessageText();
        }

        private void UpdateMessageText()
        {
            this.MessageText = LocalizedString.Format(_messageString, _args);

            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, LevelTextEventArgs);
                handler(this, MessageTextEventArgs);
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _messageString.PropertyChanged -= new PropertyChangedEventHandler(MessageString_PropertyChanged);
        }

        #endregion
    }
}