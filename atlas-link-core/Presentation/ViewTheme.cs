﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link.Localization;
using System.Windows.Media.Imaging;

namespace Atlas.Link.Presentation
{
    public class ViewTheme
    {
        #region Properties

        public String ID { get; set; }
        public LocalizedString Name { get; set; }
        public BitmapSource Image { get; set; }

        #endregion

        #region Properties

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType() == this.GetType() &&
                   obj.GetHashCode() == this.GetHashCode();
        }
        public override string ToString()
        {
            return this.Name.Text;
        }

        #endregion
    }
}