﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Presentation
{
    public static class PresentationManager
    {
        private static IPresentationManager _current;

        public static IPresentationManager Current
        {
            get 
            {
                // REALLY DONT LIKE THIS PATTERN
                // FIX IT
                if (_current == null)
                    _current = ServiceLocator.Get<IPresentationManager>();

                return _current;
            }
            set 
            {
                _current = value;
            }
        }

    }
}
