﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link.Localization;
using System.ComponentModel;

namespace Atlas.Link.Presentation
{
    public class AuditTrailEventArgs: EventArgs
    {
        #region Construction

        public AuditTrailEventArgs(String key)
        {
            this.Key = key;
        }

        #endregion

        #region Properties

        public String Key { get; private set; }

        #endregion
    }

    public class SettlementEventArgs : EventArgs
    {
        #region Construction

        public SettlementEventArgs(String key)
        {
            this.Key = key;
        }

        #endregion

        #region Properties

        public String Key { get; private set; }

        #endregion
    }


    public class ApplicationLogEventArgs : EventArgs
    {
        #region Construction

        public ApplicationLogEventArgs(ApplicationLogData logData)
        {
            this.LogData = logData;
        }

        #endregion

        #region Properties

        public ApplicationLogData LogData { get; private set; }

        #endregion
    }
}