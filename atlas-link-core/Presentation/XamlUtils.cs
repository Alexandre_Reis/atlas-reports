﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Atlas.Link.Presentation
{
    /// <summary>
    /// Utilities for processing XAML elements
    /// </summary>
    public static class XamlUtils
    {
        private static Boolean InternalProcessChildren(DependencyObject parent, Func<DependencyObject, Boolean> handler)
        {
            Int32 childCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childCount; i++)
            {
                // get the child object
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);

                // process the child object
                if (handler(child) == false)
                    return false;

                // process the child's children
                if (InternalProcessChildren(child, handler) == false)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Processes all child objects of the requested parent
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="handler">The handler.</param>
        public static void ProcessChildren(DependencyObject parent, Func<DependencyObject, Boolean> handler)
        {
            System.Diagnostics.Contracts.Contract.Requires(parent != null, "parent is mandatory");
            System.Diagnostics.Contracts.Contract.Requires(handler != null, "handler is mandatory");

            InternalProcessChildren(parent, handler);
        }


        public static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;

            while (parent != null)
            {
                T correctlyTyped = parent as T;

                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;

            }
            return null;
        }

        public static T FindChild<T>(DependencyObject parent) where T : DependencyObject
        {
            if (parent == null) return null;

            T childElement = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                T childType = child as T; if (childType == null)
                {
                    childElement = FindChild<T>(child);
                    if (childElement != null) break;
                }
                else
                {
                    childElement = (T)child; break;
                }
            }
            return childElement;
        }

        //public static DependencyObject FindChildWithName(DependencyObject parent, String name) 
        //{
        //    if (parent == null) return null;

        //    int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

        //    for (int i = 0; i < childrenCount; i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(parent, i);

        //        if (child.na)


        //        T childType = child as T; if (childType == null)
        //        {
        //            childElement = FindChild<T>(child);
        //            if (childElement != null) break;
        //        }
        //        else
        //        {
        //            childElement = (T)child; break;
        //        }
        //    }
        //    return childElement;
        //}


    }
}
