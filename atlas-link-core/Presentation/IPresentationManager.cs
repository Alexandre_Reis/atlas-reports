﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link.Localization;
using Atlas.Link.Wpf.Control;

namespace Atlas.Link.Presentation
{
    public enum PresentationState
    {
        Initializing,
        Authenticating,
        Running,
        Stopping,
        Stopped,
    }

    public enum WaitDialogType
    {
        Spin,
        Clipboard,
    }
    
    /// <summary>
    /// The UI presentation manager for the application
    /// </summary>
    public interface IPresentationManager
    {
        #region Application Management

        /// <summary>
        /// Gets the state of the presentation layer.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        PresentationState State { get; }

        /// <summary>
        /// Starts the presentation manager (UI).
        /// </summary>
        void Start();
        /// <summary>
        /// Stops the presentation manager (UI).
        /// </summary>
        void Stop();

        #endregion

        #region Window Management

        /// <summary>
        /// Gets the dispatcher for the main window.
        /// </summary>
        /// <value>
        /// The dispatcher.
        /// </value>
        System.Windows.Threading.Dispatcher Dispatcher { get; }

        /// <summary>
        /// Gets the open child windows.
        /// </summary>
        /// <value>
        /// The open child windows.
        /// </value>
        IList<Object> OpenChildWindows { get; }


        #endregion

        #region Dialogs

        Boolean MessageBoxYesNo(LocalizedString text, params Object[] args);
        Boolean MessageBoxYesNo(LocalizedString text, string key, params Object[] args);

        Boolean MessageBoxOkCancel(LocalizedString text, params Object[] args);
        Boolean MessageBoxOkCancel(LocalizedString text, string key, params Object[] args);
        Boolean CustomMessageBox(MessageBoxExInfo msgBoxInfo);

        void MessageBoxInfo(LocalizedString text, params Object[] args);
        void MessageBoxInfo(LocalizedString text, string key, params Object[] args);
        void MessageBoxInfo(string text, params Object[] args);

        void MessageBoxWarn(LocalizedString text, params Object[] args);
        void MessageBoxWarn(LocalizedString text, string key, params Object[] args);

        void MessageBoxError(LocalizedString text, params Object[] args);
        void MessageBoxError(LocalizedString text, string key, params Object[] args);
        

        Boolean FileOpenDialog(LocalizedString title, String defaultExt, String filter, ref String fileName);
        Boolean FileSaveDialog(LocalizedString title, String defaultExt, String filter, ref String fileName);
        Boolean DirectoryOpenDialog(LocalizedString title, ref String selectedPath, string key);

        void WaitDialog(WaitDialogType dialogType);

        #endregion

        #region Service Handling

        /// <summary>
        /// Gets a list of available styles.
        /// </summary>
        /// <returns></returns>
        IList<ViewTheme> GetThemes();

        /// <summary>
        /// Gets and sets the active theme
        /// </summary>
        /// <value>
        /// The current theme.
        /// </value>
        /// <param name="style">The style.</param>
        ViewTheme CurrentTheme { get; set; }

        /// <summary>
        /// Sets the audit trail.
        /// </summary>
        /// <param name="key">The key.</param>
        void SetAuditTrail(String key);

        /// <summary>
        /// Allows Audit Trail service handlers to respond to requests.
        /// </summary>
        event EventHandler<AuditTrailEventArgs> HandleAuditTrailRequest;

        /// <summary>
        /// Writes to the application log.
        /// </summary>
        /// <param name="logData">The log data.</param>
        void WriteApplicationLog(ApplicationLogData logData);

        /// <summary>
        /// Allows Application Log service handlers to respond to requests.
        /// </summary>
        event EventHandler<ApplicationLogEventArgs> HandleApplicationLogRequest;

        #endregion
    }
}