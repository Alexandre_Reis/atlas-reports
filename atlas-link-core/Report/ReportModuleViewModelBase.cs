﻿using System;
using DevExpress.Xpf.Printing;
using DevExpress.XtraReports;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;
using System.ComponentModel;

namespace Atlas.Link.Report
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ReportModuleViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// The report preview model
        /// </summary>
        private XtraReportPreviewModel reportPreviewModel;

        /// <summary>
        /// Gets the report preview model.
        /// </summary>
        /// <value>
        /// The report preview model.
        /// </value>
        public XtraReportPreviewModel ReportPreviewModel
        {
            get { return reportPreviewModel; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportModuleViewModelBase"/> class.
        /// </summary>
        public ReportModuleViewModelBase()
        {
            CreateReportCore(delegate(IReport report) { reportPreviewModel = CreateReportPreviewModel(report); });
        }

        /// <summary>
        /// Updates the report.
        /// </summary>
        public void UpdateReport()
        {
            CreateReportCore(delegate(IReport report)
            {
                if (ReportPreviewModel.Report != null)
                    ReportPreviewModel.Report.PrintingSystemBase.CreateDocumentException -= OnCreateDocumentException;
                ReportPreviewModel.Report = report;
                if (report != null)
                    ReportPreviewModel.Report.PrintingSystemBase.CreateDocumentException += OnCreateDocumentException;
            });
            ReportPreviewModel.Report.CreateDocument(true);
        }

        private void OnCreateDocumentException(object sender, DevExpress.XtraPrinting.ExceptionEventArgs args)
        {
            args.Handled = true;
            ReportPreviewModel.Report.StopPageBuilding();
            ReportPreviewModel.DialogService.ShowError(args.Exception.Message, PrintingLocalizer.GetString(PrintingStringId.Error));
        }

        private void CreateReportCore(Action<IReport> assignReport)
        {
            IReport report = CreateReport();
            assignReport(report);
        }

        /// <summary>
        /// Resets the data adapters.
        /// </summary>
        /// <param name="report">The report.</param>
        private void ResetDataAdapters(DevExpress.XtraReports.UI.XtraReportBase report)
        {
            if (report == null)
                return;
            NestedComponentEnumerator enumerator = new NestedComponentEnumerator(new object[] {report});
            while (enumerator.MoveNext())
            {
                IDataContainer dataContainer = enumerator.Current as IDataContainer;
                if (dataContainer != null)
                    dataContainer.DataAdapter = null;
                XRSubreport subreport = enumerator.Current as XRSubreport;
                if (subreport != null)
                    ResetDataAdapters(subreport.ReportSource as XtraReportBase);
            }
        }

        /// <summary>
        /// Creates the report.
        /// </summary>
        /// <returns></returns>
        protected abstract IReport CreateReport();

        /// <summary>
        /// Creates the report preview model.
        /// </summary>
        /// <param name="report">The report.</param>
        /// <returns></returns>
        protected virtual XtraReportPreviewModel CreateReportPreviewModel(IReport report)
        {
            return new XtraReportPreviewModel(report) {ZoomMode = new ZoomFitModeItem(ZoomFitMode.PageWidth)};
        }

        #region INotifyPropertyChanged Members

#pragma warning disable 67
        public event PropertyChangedEventHandler PropertyChanged; // INotifyPropertyChanged to avoid memory leaks
#pragma warning restore 67

        #endregion
    }
}