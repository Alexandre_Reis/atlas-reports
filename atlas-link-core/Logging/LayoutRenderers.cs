﻿using System.Text;
using NLog;
using NLog.LayoutRenderers;

namespace Atlas.Link.Logging
{
    /// <summary>
    /// The AppEnvironment log path.
    /// </summary>
    [LayoutRenderer("brit-logpath")]
    public class LogPathLayoutRenderer : LayoutRenderer
    {
        /// <summary>
        /// Renders the specified Log Path for the AppEnvironment and appends it to the specified <see cref="StringBuilder" />.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> to append the rendered data to.</param>
        /// <param name="logEvent">Logging event.</param>
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(AppEnvironment.AppDataLogPath);
        }
    }
}