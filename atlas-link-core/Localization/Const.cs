﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Application global localized string constants.
    /// </summary>
    public class StringConst
    {
        public class Data
        {
            #region Localized String Constants

            public static readonly LocalizedString Info = new LocalizedLiteralString("Data.Info", "Info");
            public static readonly LocalizedString Warn = new LocalizedLiteralString("Data.Warn", "Warn");
            public static readonly LocalizedString Error = new LocalizedLiteralString("Data.Error", "Error");
            public static readonly LocalizedString Yes = new LocalizedLiteralString("Data.Yes", "Yes");
            public static readonly LocalizedString No = new LocalizedLiteralString("Data.No", "No");

            public static readonly LocalizedString EntityNameDefault = new LocalizedLiteralString("Data.EntityNameDefault", "Record");

            public static readonly LocalizedString EntityInsertErrorFormat = new LocalizedLiteralString("Data.EntityInsertErrorFormat", "An unexpected error occurred while inserting the {0}");
            public static readonly LocalizedString EntityUpdateErrorFormat = new LocalizedLiteralString("Data.EntityUpdateErrorFormat", "An unexpected error occurred while updating the {0}");
            public static readonly LocalizedString EntityPermissionWarnFormat = new LocalizedLiteralString("Data.EntityPermissionWarnFormat", "Permission error: {0}");
            public static readonly LocalizedString EntityDeleteErrorFormat = new LocalizedLiteralString("Data.EntityDeleteErrorFormat", "An unexpected error occurred while deleting the {0}");
            public static readonly LocalizedString EntityTriggerErrorFormat = new LocalizedLiteralString("Data.EntityTriggerErrorFormat", "An unexpected error occurred while triggering the {0}");
            public static readonly LocalizedString EntityReadErrorFormat = new LocalizedLiteralString("Data.EntityReadErrorFormat", "An unexpected error occurred while reading the {0}");
            
            public static readonly LocalizedString EntityNameEndpoint = new LocalizedLiteralString("Data.EntityNameEndpoint", "Endpoint");
            public static readonly LocalizedString EntityNameMessage = new LocalizedLiteralString("Data.EntityNameMessage", "Message");
            public static readonly LocalizedString EntityNameReconciliation = new LocalizedLiteralString("Data.EntityNameReconciliation", "Reconciliation");
            public static readonly LocalizedString EntityNameTransformation = new LocalizedLiteralString("Data.EntityNameTransformation", "Transformation");
            public static readonly LocalizedString EntityNameTransformationRoute = new LocalizedLiteralString("Data.EntityNameTransformationRoute", "Transformation Route");
            public static readonly LocalizedString EntityNameXmlDataRoute = new LocalizedLiteralString("Data.EntityNameXmlDataRoute", "XML Data Route");
            public static readonly LocalizedString EntityNameXmlDataSchema = new LocalizedLiteralString("Data.EntityNameXmlDataSchema", "XML Data Schema");
            public static readonly LocalizedString EntityNameTransformationComponent = new LocalizedLiteralString("Data.EntityNameTransformationComponent", "Component");

            public static readonly LocalizedString EntityNameUser = new LocalizedLiteralString("Data.EntityNameUser", "User");
            public static readonly LocalizedString EntityNameUserGroup = new LocalizedLiteralString("Data.EntityNameUserGroup", "User Group");
            public static readonly LocalizedString EntityNameRights = new LocalizedLiteralString("Data.EntityNameRights", "Rights");

            public static readonly LocalizedString ReconDuplicates = new LocalizedLiteralString("Data.ReconDuplicates", "Duplicate Keys");
            public static readonly LocalizedString ReconMissing = new LocalizedLiteralString("Data.ReconMissing", "Missing Keys");
            public static readonly LocalizedString ReconUnmatched = new LocalizedLiteralString("Data.ReconUnmatched", "Unmatched Data");
            public static readonly LocalizedString ReconWithinVar = new LocalizedLiteralString("Data.ReconWithinVar", "Within Variance");
            public static readonly LocalizedString ReconMatched = new LocalizedLiteralString("Data.ReconMatched", "Matched Data");
            public static readonly LocalizedString ReconTotal = new LocalizedLiteralString("Data.ReconTotal", "Total Rows");

            public static readonly LocalizedString AuditTrailError = new LocalizedLiteralString("Data.AuditTrailError", "Audit Trail Error");

            public static readonly LocalizedString DocumentReadError = new LocalizedLiteralString("Data.DocumentReadError", "An error occurred while opening the document");
            public static readonly LocalizedString ReconciliationReadError = new LocalizedLiteralString("Data.ReconciliationReadError", "An error occurred while opening the reconciliation");

            public static readonly LocalizedString FileUploadSucceedFormat = new LocalizedLiteralString("Data.FileUploadSucceedFormat", "The file [{0}] uploaded successfully");
            public static readonly LocalizedString FileUploadFailedFormat = new LocalizedLiteralString("Data.FileUploadFailedFormat", "The file [{0}] failed to uploaded successfully");

            #endregion
        }

        public class Validation
        {
            #region Localized String Constants

            public static readonly LocalizedString PropertyRequiredError = new LocalizedLiteralString("Validation.PropertyRequiredError", "Please enter a value for this field");
            public static readonly LocalizedString NumberNotLessThanZero = new LocalizedLiteralString("Validation.NumberNotLessThanZero", "May not be less than zero");
            public static readonly LocalizedString DuplicateValueError = new LocalizedLiteralString("Validation.DuplicateValueError", "Please enter a unique value for this field");

            public static readonly LocalizedString ValueLessThanZeroError = new LocalizedLiteralString("Validation.ValueLessThanZeroError", "Value can not be less than zero");
            public static readonly LocalizedString ValueMustBeZeroError = new LocalizedLiteralString("Validation.ValueMustBeZeroError", "Value must be zero");
            public static readonly LocalizedString ValueMayNotBeZeroError = new LocalizedLiteralString("Validation.ValueMayNotBeZeroError", "Value may not be zero");
            public static readonly LocalizedString ValueMustBeGreaterThanZeroError = new LocalizedLiteralString("Validation.ValueMustBeGreaterThanZeroError", "Value must be greater than zero");

            public static readonly LocalizedString SpacesNotAllowedError = new LocalizedLiteralString("Validation.SpacesNotAllowedError", "Spaces are not allowed for this field");
            public static readonly LocalizedString UserGroupNameAlreadyExists = new LocalizedLiteralString("Validation.UserGroupNameAlreadyExists", "This user group name already exists");
            public static readonly LocalizedString UserNameAlreadyExists = new LocalizedLiteralString("Validation.UserNameAlreadyExists", "This user name already exists");
            public static readonly LocalizedString InvalidEmailAddress = new LocalizedLiteralString("Validation.InvalidEmailAddress", "Invalid email address");
            public static readonly LocalizedString InvalidTimeRange = new LocalizedLiteralString("Validation.InvalidTimeRange", "Start Time is after End Time");
            public static readonly LocalizedString ScheduleOverlap = new LocalizedLiteralString("Validation.ScheduleOverlap", "The Schedule overlaps another Schedule");

            #endregion
        }

        /// <summary>
        /// Window and dialog captions (title).
        /// </summary>
        public class Caption
        {
            #region Localized String Constants

            public static readonly LocalizedString Confirm = new LocalizedLiteralString("Caption.Confirm", "Confirm");
            public static readonly LocalizedString Information = new LocalizedLiteralString("Caption.Information", "Information");
            public static readonly LocalizedString Warning = new LocalizedLiteralString("Caption.Warning", "Warning");
            public static readonly LocalizedString Error = new LocalizedLiteralString("Caption.Error", "Error");

            public static readonly LocalizedString TitleNewRecordFormat = new LocalizedLiteralString("Caption.TitleNewRecordFormat", "New - {0}");
            public static readonly LocalizedString TitleNewBrokerAllegement = new LocalizedLiteralString("Caption.NewBrokerAllegement", "Novo Broker Allegement");
            public static readonly LocalizedString TitleComplementTrade = new LocalizedLiteralString("Caption.ComplementTrade", "Complementar Operacao");
            public static readonly LocalizedString ComplementReason = new LocalizedLiteralString("Caption.ComplementReason", "Complementacao do Motivo");

            public static readonly LocalizedString TitleEditRecordFormat = new LocalizedLiteralString("Caption.TitleEditRecordFormat", "Edit - {0}");
            public static readonly LocalizedString TitleDeleteRecordFormat = new LocalizedLiteralString("Caption.TitleDeleteRecordFormat", "Delete - {0}");

            public static readonly LocalizedString TitleNewRoute = new LocalizedLiteralString("Caption.TitleNewRoute", "Create new Transformation Route");
            public static readonly LocalizedString TitleEditRoute = new LocalizedLiteralString("Caption.TitleEditRoute", "Modify Transformation Route");
            public static readonly LocalizedString TitleNewEndpoint = new LocalizedLiteralString("Caption.TitleNewEndpoint", "New Endpoint");
            public static readonly LocalizedString TitleEditEndpoint = new LocalizedLiteralString("Caption.TitleEditEndpoint", "Edit Endpoint");
            public static readonly LocalizedString TitleEditReconSource = new LocalizedLiteralString("Caption.TitleEditReconSource", "Edit Reconciliation Source");
            public static readonly LocalizedString TitleNewReconSource = new LocalizedLiteralString("Caption.TitleNewReconSource", "New Reconciliation Source");
            public static readonly LocalizedString TitleEditReconTarget = new LocalizedLiteralString("Caption.TitleEditReconTarget", "Edit Reconciliation Target");
            public static readonly LocalizedString TitleNewReconTarget = new LocalizedLiteralString("Caption.TitleNewReconTarget", "New Reconciliation Target");
            public static readonly LocalizedString TitleSchedule = new LocalizedLiteralString("Caption.TitleSchedule", "Schedule");

            public static readonly LocalizedString TitleViewMessageDocument = new LocalizedLiteralString("Caption.TitleViewMessageDocument", "View Document");
            public static readonly LocalizedString TitleViewReconciliationResult = new LocalizedLiteralString("Caption.TitleViewReconciliationResult", "View Reconciliation Results");

            public static readonly LocalizedString TitleDefineAuditReconciliation = new LocalizedLiteralString("Caption.TitleDefineAuditReconciliation", "Define Reconciliation Parameters");

            public static readonly LocalizedString TitleSelectJarFile = new LocalizedLiteralString("Caption.TitleSelectJarFile", "Select a JAR File");

            public static readonly LocalizedString TitleSelectFile = new LocalizedLiteralString("Caption.TitleSelectFile", "Select a File");
            public static readonly LocalizedString TitleSelectDirectory = new LocalizedLiteralString("Caption.TitleSelectDirectory", "Select a directory");
            public static readonly LocalizedString DontShowMessageAgain = new LocalizedLiteralString("Caption.DontShowMessageAgain", "Don't show this message again");
            
            #endregion
        }

        /// <summary>
        /// Dialog content (MessageBox)
        /// </summary>
        public class Dialog
        {
            #region Localized String Constants

            public static readonly LocalizedString UnexpectedNetworkErrorOccurred = new LocalizedLiteralString("Dialog.UnexpectedNetworkErrorOccurred", "Unable to connect to server");
            public static readonly LocalizedString UnexpectedErrorOccurred = new LocalizedLiteralString("Dialog.UnexpectedError", "An unexpected error has occurred");
            public static readonly LocalizedString TextCopiedToClipboard = new LocalizedLiteralString("Dialog.TextCopiedToClipboard", "Text was copied to clipboard");
            public static readonly LocalizedString SelectExportFile = new LocalizedLiteralString("Dialog.SelectExportFile", "Select file to export to");
            public static readonly LocalizedString ConfirmDeleteEndpoint = new LocalizedLiteralString("Dialog.ConfirmDeleteEndpoint", "Are you sure you want to delete this Endpoint");
            public static readonly LocalizedString ConfirmDeleteRoute = new LocalizedLiteralString("Dialog.ConfirmDeleteRoute", "Are you sure you want to delete this Route");
            public static readonly LocalizedString ConfirmResetPassword = new LocalizedLiteralString("Dialog.ConfirmResetUserPassword", "Are you sure you want to reset Password the {0}");
            public static readonly LocalizedString ConfirmDeleteRecordFormat = new LocalizedLiteralString("Dialog.ConfirmDelete", "Are you sure you want to delete the {0}");
            public static readonly LocalizedString ConfirmExitApplication = new LocalizedLiteralString("Dialog.ConfirmExitApplication", "Are you sure you want to exit?");
            public static readonly LocalizedString DeleteRecordErrorFormat = new LocalizedLiteralString("Dialog.DeleteRecordErrorFormat", "An error occurred while deleting the {0}. Ensure that there are no dependencies to the {0}");
            public static readonly LocalizedString ExitApplication = new LocalizedLiteralString("Dialog.ExitApplication", "Exit Application");
            public static readonly LocalizedString FileConfirmOverwrite = new LocalizedLiteralString("Dialog.FileConfirmOverwrite", "The file already exists. Are you sure you want to overwrite it");
            public static readonly LocalizedString InvalidAccount = new LocalizedLiteralString("Dialog.InvalidAccount", "Invalid Account");
            public static readonly LocalizedString InvalidPassword = new LocalizedLiteralString("Dialog.InvalidPassword", "Invalid Password");
            public static readonly LocalizedString AccountDisabled = new LocalizedLiteralString("Dialog.AccountDisabled", "Account Disabled");
            public static readonly LocalizedString PasswordExpired = new LocalizedLiteralString("Dialog.PasswordExpired", "Password Expired");
            public static readonly LocalizedString TriggerRecordErrorFormat = new LocalizedLiteralString("Dialog.TriggerRecordErrorFormat", "An error occurred while Triggering {0}.");
            public static readonly LocalizedString MessageDocumentNotFound = new LocalizedLiteralString("Dialog.MessageDocumentNotFound", "The requested Document was not found");
            public static readonly LocalizedString ReconDetailNotFound = new LocalizedLiteralString("Dialog.ReconDetailNotFound", "The requested Recon Detail was not found");
            public static readonly LocalizedString ErrorRecordsWillBeDiscarded = new LocalizedLiteralString("Dialog.ErrorRecordsWillBeDiscarded", "The records with errors will be discarded.\r\nWould you like proceed.");
            public static readonly LocalizedString PleaseSpecifyValidUserName = new LocalizedLiteralString("Dialog.PleaseSpecifyValidUserName", "Please specify a valid user name.");
            public static readonly LocalizedString MessageEmailWithPasswordToUser = new LocalizedLiteralString("Dialog.MessageEmailWithPasswordToUser", "An email containing a new password will be sent to the user.\n\nAre You sure?");
            public static readonly LocalizedString PauseRoutesAreYouSure = new LocalizedLiteralString("Dialog.PauseRoutesAreYouSure", "This will stop all currently enabled routes.\n\nAre You sure?");
            public static readonly LocalizedString JarIsDisallowedFileType = new LocalizedLiteralString("Dialog.JarIsDisallowedFileType", "JAR files may not be uploaded as a Component Resource");
            public static readonly LocalizedString SaveWorkspace = new LocalizedLiteralString("Dialog.SaveWorkspace", "Save Workspace");
            public static readonly LocalizedString PasswordHasExpiredPleaseSpecifyNewPassword = new LocalizedLiteralString("Dialog.PasswordHasExpiredPleaseSpecifyNewPassword", "Password has expired, please specify new password");
            public static readonly LocalizedString LeftFieldNotFoundInRight = new LocalizedLiteralString("Dialog.LeftFieldNotFoundInRight", "The field '{0}' not found in right side.");
            public static readonly LocalizedString RightFieldNotFoundInLeft = new LocalizedLiteralString("Dialog.RightFieldNotFoundInLeft", "The field '{0}' not found in left side.");

            #endregion
        }
    }
}