﻿using System;
using System.Windows;

namespace Atlas.Link.Localization
{
    public class LanguageChangedEventManager : WeakEventManager
    {
        public static void AddListener(TranslationManager source, IWeakEventListener listener)
        {
            LanguageChangedEventManager currmanager = LanguageChangedEventManager.CurrentManager;

            if (currmanager != null)
                currmanager.ProtectedAddListener(source, listener);
        }

        public static void RemoveListener(TranslationManager source, IWeakEventListener listener)
        {
            // workaround for errors raised during shutdown
            LanguageChangedEventManager currmanager = LanguageChangedEventManager.CurrentManager;

            if (currmanager != null)
                currmanager.ProtectedRemoveListener(source, listener);
        }

        private void OnLanguageChanged(object sender, EventArgs e)
        {
            DeliverEvent(sender, e);
        }

        protected override void StartListening(object source)
        {
            var manager = (TranslationManager)source;
            manager.LanguageChanged += OnLanguageChanged;
        }

        protected override void StopListening(Object source)
        {
            var manager = (TranslationManager)source;
            manager.LanguageChanged -= OnLanguageChanged;
        }

        private static LanguageChangedEventManager CurrentManager
        {
            get
            {
                Type managerType = typeof(LanguageChangedEventManager);
                LanguageChangedEventManager manager = null;

                try
                {
                    manager = (LanguageChangedEventManager)GetCurrentManager(managerType);

                    if (manager == null)
                    {
                        manager = new LanguageChangedEventManager();
                        SetCurrentManager(managerType, manager);
                    }
                }
                catch (Exception)
                {
                    //FIX THIS IMPLEMENTATION
                }

                return manager;
            }
        }
    }
}