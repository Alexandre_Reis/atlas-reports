﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Localizes an enumerations values.
    /// </summary>
    /// <typeparam name="TEnum">The type of the enum.</typeparam>
    public class LocalizedEnum<TEnum>
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedEnum{TEnum}"/> class.
        /// </summary>
        public LocalizedEnum()
        {
            System.Diagnostics.Contracts.Contract.Requires(typeof(TEnum).IsEnum, "TEnum must be an enumeration type");

            List<LocalizedEnumItem<TEnum>> list = new List<LocalizedEnumItem<TEnum>>();

            String keyPrefix = String.Format("Enum.{0}", typeof(TEnum).FullName);

            foreach (TEnum item in Enum.GetValues(typeof(TEnum)))
            {
                String defaultText = item.ToString();
                String key = String.Format("{0}.{1}", keyPrefix, defaultText);
                LocalizedEnumItem<TEnum> localizedItem = new LocalizedEnumItem<TEnum>(item, key, defaultText);
                list.Add(localizedItem);
            }

            this.LocalizedItems = list;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the localized items for this enumeration type;
        /// </summary>
        /// <value>
        /// The localized items.
        /// </value>
        public IList<LocalizedEnumItem<TEnum>> LocalizedItems { get; private set; }

        #endregion
    }


    /// <summary>
    /// Defines a localized value of an enumeration.
    /// </summary>
    public class LocalizedEnumItem<TEnum> : INotifyPropertyChanged, IDisposable
    {
        private static PropertyChangedEventArgs TextChangedEventArgs = new PropertyChangedEventArgs("Text");

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        public LocalizedEnumItem(TEnum item, String key, String defaultText)
        {
            _key = key;
            _defaultText = defaultText;
            _value = item;

            this.EnumText = _defaultText;

            if (AppEnvironment.IsDesignMode == false)
            {
                this.Text = TranslationManager.Instance.Translate(_key, _defaultText);
                TranslationManager.Instance.LanguageChanged += new EventHandler(LanguageChangedEvent);
            }
            else
            {
                this.Text = _defaultText;
            }
        }

        #endregion

        #region Fields

        private String _key;
        private String _defaultText;
        private TEnum _value;

        #endregion

        #region Events

        private void LanguageChangedEvent(object sender, EventArgs e)
        {
            this.Text = TranslationManager.Instance.Translate(_key, _defaultText);

            if (PropertyChanged != null)
                PropertyChanged(this, TextChangedEventArgs);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public String Key { get { return _key; } }

        /// <summary>
        /// Gets the translated text.
        /// </summary>
        /// <value>
        /// The translated text.
        /// </value>
        public String Text { get; private set; }

        /// <summary>
        /// Gets the enum text.
        /// </summary>
        /// <value>
        /// The enum text.
        /// </value>
        public String EnumText { get; private set; }

        /// <summary>
        /// Gets the enumeration item value;
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public TEnum EnumValue { get { return _value; } }

        #endregion

        #region Override

        public override string ToString()
        {
            return this.Text;
        }
        public override int GetHashCode()
        {
            return _key.GetHashCode();
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (AppEnvironment.IsDesignMode == false)
            {
                TranslationManager.Instance.LanguageChanged -= new EventHandler(LanguageChangedEvent);
            }
        }

        #endregion
    }
}