﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Globalization;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// 
    /// </summary>
    public class ResxTranslationProvider : ITranslationProvider
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="ResxTranslationProvider"/> class.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        /// <param name="assembly">The assembly.</param>
        public ResxTranslationProvider(string baseName, Assembly assembly)
        {
            _resourceManager = new ResourceManager(baseName, assembly);
        }

        #endregion

        #region Fields

        private readonly ResourceManager _resourceManager;

        #endregion

        #region ITranslationProvider Members

        /// <summary>
        /// See <see cref="ITranslationProvider.Translate" />
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <returns></returns>
        public String Translate(string key, string defaultText)
        {
            return _resourceManager.GetString(key);
        }
        /// <summary>
        /// See <see cref="ITranslationProvider.AvailableLanguages" />
        /// </summary>
        public IEnumerable<TranslationLanguage> Languages
        {
            get
            {
                return TranslationStore.DefaultLanguages;
            }
        }

        #endregion
    }
}