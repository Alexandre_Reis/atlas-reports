﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Threading;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Manages translation-providers for the application.
    /// This also handles design-time invocation.
    /// </summary>
    public class TranslationManager
    {
        public static Int32 GetCultureHash(CultureInfo culture)
        {
            return culture.IetfLanguageTag.ToLowerInvariant().GetHashCode();
        }

        #region Construction

        private static TranslationManager _translationManager;

        public static TranslationManager Instance
        {
            get
            {
                if (_translationManager == null)
                    _translationManager = new TranslationManager();

                return _translationManager;
            }
        }
        public TranslationManager()
        {
            if (AppEnvironment.IsDesignMode == false)
                this.TranslationProvider = ServiceLocator.Get<ITranslationProvider>();
        }

        #endregion

        #region Events

        public event EventHandler LanguageChanged;

        private void OnLanguageChanged()
        {
            if (LanguageChanged != null)
            {
                LanguageChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current language.
        /// </summary>
        /// <value>
        /// The current language.
        /// </value>
        public CultureInfo CurrentLanguage
        {
            get { return Thread.CurrentThread.CurrentUICulture; }
            set
            {
                if ((value != null) && (value != Thread.CurrentThread.CurrentUICulture))
                {
                    Thread.CurrentThread.CurrentUICulture = value;
                    Thread.CurrentThread.CurrentCulture = value;
                    _currentLanguageHash = TranslationManager.GetCultureHash(value);
                    OnLanguageChanged();
                }
            }
        }
        /// <summary>
        /// Gets the current language hash.
        /// </summary>
        /// <value>
        /// The current language hash.
        /// </value>
        public Int32 CurrentLanguageHash
        {
            get 
            {
                if (_currentLanguageHash == 0)
                    _currentLanguageHash = TranslationManager.GetCultureHash(this.CurrentLanguage);

                return _currentLanguageHash;
            }
        }
        private Int32 _currentLanguageHash = 0;

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <value>
        /// The languages.
        /// </value>
        public IEnumerable<TranslationLanguage> Languages
        {
            get
            {
                if (this.TranslationProvider != null)
                {
                    return this.TranslationProvider.Languages;
                }
                return Enumerable.Empty<TranslationLanguage>();
            }
        }
        /// <summary>
        /// Gets or sets the translation provider.
        /// </summary>
        /// <value>
        /// The translation provider.
        /// </value>
        public ITranslationProvider TranslationProvider { get; set; }

        #endregion

        #region Public Methods

        public String Translate(string key, string defaultText)
        {
            // runtime assertions
            if (AppEnvironment.IsDesignMode == false)
            {
                System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(key) == false, "TranslationExtension Key must be specified");
                System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(defaultText) == false, "TranslationExtension DefaultText must be specified");

                if (this.TranslationProvider != null)
                {
                    String translatedValue = this.TranslationProvider.Translate(key, defaultText);

                    System.Diagnostics.Contracts.Contract.Ensures(String.IsNullOrWhiteSpace(translatedValue) == false, "TranslationProvider returned empty text");

                    return translatedValue;
                }
            }

            // runtime and design-time fallback
            if (String.IsNullOrWhiteSpace(defaultText) == false)
                return string.Format("{0}", defaultText);

            else if (String.IsNullOrWhiteSpace(key) == false)
                return string.Format("*{0}*", key);

            else
                throw new Exception("TranslationExtension requires a key value");
        }

        #endregion
    }
}