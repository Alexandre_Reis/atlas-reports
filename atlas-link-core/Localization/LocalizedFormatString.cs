﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// A String format instance of Localized Strings
    /// </summary>
    public class LocalizedFormatString : LocalizedString
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public LocalizedFormatString(LocalizedString message, params Object[] args)
        {
            _message = message;
            _args = args;
        }

        #endregion

        #region Fields

        private LocalizedString _message;
        private Object[] _args;

        #endregion

        #region Override

        protected override string DoGetText()
        {
            return LocalizedString.Format(_message, _args);
        }

        #endregion
    }
}