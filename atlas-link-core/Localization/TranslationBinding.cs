﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;

namespace Atlas.Link.Localization
{
    public class TranslationBinding : IWeakEventListener, INotifyPropertyChanged
    {
        #region Private Members

        private string _key;
        private string _defaultText;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationBinding"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        public TranslationBinding(string key, string defaultText)
        {
            _key = key;
            _defaultText = defaultText;
            LanguageChangedEventManager.AddListener(TranslationManager.Instance, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="TranslationBinding"/> is reclaimed by garbage collection.
        /// </summary>
        ~TranslationBinding()
        {
            LanguageChangedEventManager.RemoveListener(TranslationManager.Instance, this);
        }

        /// <summary>
        /// Gets the text value.
        /// This is the actual property that is bound to the UIElement.
        /// The onLanguageChanged event triggers an OnPropertyChanged event
        /// which causes the UI to reread this value.
        /// </summary>
        /// <value>
        /// A string value.
        /// </value>
        public object Value
        {
            get { return TranslationManager.Instance.Translate(_key, _defaultText); }
        }

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(LanguageChangedEventManager))
            {
                OnLanguageChanged(sender, e);
                return true;
            }
            return false;
        }

        private void OnLanguageChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Value"));
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}