﻿using System;
using System.Windows.Data;
using System.Windows.Markup;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// The Translate Markup extension returns a binding to a TranslationData
    /// that provides a translated resource of the specified key
    /// </summary>
    [MarkupExtensionReturnType(typeof(String))]
    public class TranslateExtension : MarkupExtension
    {
        #region Private Members

        private string _key = "";
        private string _defaultText = "";

        #endregion

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateExtension"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        //public TranslateExtension(string key)
        //{
        //    _key = key;
        //}

        #endregion

        //[ConstructorArgument("key")]
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        public string DefaultText
        {
            get { return _defaultText; }
            set { _defaultText = value; }
        }

        /// <summary>
        /// See <see cref="MarkupExtension.ProvideValue" />
        /// </summary>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            Binding binding = new Binding("Value") // bind to TranslationData.Value property
            {
                Source = new TranslationBinding(_key, _defaultText)
            };
            return binding.ProvideValue(serviceProvider);
        }
    }
}