﻿using System;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Provides localized text version of exception. 
    /// This is used where the intention is to display the details to the end user.
    /// </summary>
    public class LocalizedException : Exception
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public LocalizedException(LocalizedString message)
            : base(message.Text)
        {
            this.LocalizedMessage = message;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public LocalizedException(LocalizedString message, Exception innerException)
            : base(message.Text, innerException)
        {
            this.LocalizedMessage = message;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the localized message.
        /// </summary>
        /// <value>
        /// The localized message.
        /// </value>
        public LocalizedString LocalizedMessage { get; private set; }

        #endregion
    }
}