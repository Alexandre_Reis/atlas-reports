﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Provides translation text during runtime.
    /// </summary>
    public class RuntimeTranslationProvider : ITranslationProvider
    {
        /// <summary>
        /// The name of the runtime translations file.
        /// </summary>
        public static readonly String StorageFilename = "translations.xml";

        private static readonly Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof(RuntimeTranslationProvider));

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugTranslationWriter" /> class.
        /// </summary>
        public RuntimeTranslationProvider()
        {
            try
            {
                var entryAssembly = Assembly.GetEntryAssembly();
                var resourceName = entryAssembly.GetManifestResourceNames().FirstOrDefault(x => x.Contains(StorageFilename));

                if (String.IsNullOrWhiteSpace(resourceName) == false)
                {
                    var resStream = entryAssembly.GetManifestResourceStream(resourceName);

                    if (resStream != null)
                        _storage = LocalStorage.ReadStream<TranslationStore>(resStream);
                }
                else
                {
                    Logger.ErrorFormat("Unable to find the {0} localization resource", StorageFilename);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Constructor", ex);
            }

            if (_storage == null)
                _storage = new TranslationStore();

            // define default cultures
            _languages = TranslationStore.DefaultLanguages;
        }

        #endregion

        #region Fields

        private readonly TranslationStore _storage;
        private readonly IList<TranslationLanguage> _languages;

        #endregion

        #region ITranslationProvider Members

        /// <summary>
        /// See <see cref="ITranslationProvider.Translate" />
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <returns></returns>
        public String Translate(string key, string defaultText)
        {
            String resultText = defaultText;

            // attempt to find the text key in the item Store
            TranslationItem item = _storage.TryFindTranslationItem(key);
            if (item != null)
            {
                // use the default item value
                resultText = item.DefaultText;

                // if we are using the non-default culture then attempt to find a translation
                if (TranslationManager.Instance.CurrentLanguage.IetfLanguageTag != _storage.DefaultCultureKey)
                {
                    TranslationValue value = item.TryFindTranslationValue(TranslationManager.Instance.CurrentLanguage.IetfLanguageTag);
                    if (value != null)
                    {
                        resultText = value.TranslatedText;
                    }
                }
            }

            return resultText;
        }

        /// <summary>
        /// Gets the available languages.
        /// </summary>
        /// <value>
        /// The available languages.
        /// </value>
        public IEnumerable<TranslationLanguage> Languages
        {
            get { return _languages; }
        }

        #endregion
    }
}