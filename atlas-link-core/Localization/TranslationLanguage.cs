﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Xml.Serialization;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Defines a language to be translated.
    /// </summary>
    [Serializable]
    public class TranslationLanguage
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationStore" /> class.
        /// </summary>
        public TranslationLanguage()
        {
            this.CultureKey = "";
            this.ImageUri = "";
        }

        #endregion

        #region Properties

        [XmlAttribute(AttributeName = "culture")]
        public String CultureKey { get; set; }

        [XmlAttribute(AttributeName = "image")]
        public String ImageUri { get; set; }

        [XmlIgnore]
        public CultureInfo Culture
        {
            get
            {
                if (_culture == null)
                {
                    _culture = new CultureInfo(this.CultureKey);
                }
                return _culture;
            }
        }
        private CultureInfo _culture;

        #endregion

        #region Overrides

        public override int GetHashCode()
        {
            return this.Culture.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return (obj != null && obj.GetType().Equals(this.GetType()) && obj.GetHashCode().Equals(this.GetHashCode()));
        }
        public override string ToString()
        {
            return this.Culture.EnglishName;
        }

        #endregion
    }
}