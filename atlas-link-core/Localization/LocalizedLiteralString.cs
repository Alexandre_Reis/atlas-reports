﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// A literal instance of the Localized String
    /// </summary>
    public class LocalizedLiteralString : LocalizedString
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString" /> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        public LocalizedLiteralString(String key, String defaultText)
        {
            _key = key;
            _defaultText = defaultText;
        }

        #endregion

        #region Fields

        private String _key;
        private String _defaultText;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public String Key { get { return _key; } }

        #endregion

        #region Override

        protected override string DoGetText()
        {
            return TranslationManager.Instance.Translate(_key, _defaultText);
        }
        public override int GetHashCode()
        {
            return _key.GetHashCode();
        }

        #endregion
    }
}