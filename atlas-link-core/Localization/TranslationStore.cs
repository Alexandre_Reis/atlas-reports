﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Xml.Serialization;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// A storage container for translated text.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "translation_store")]
    public class TranslationStore
    {
        #region Static Utility Members

        /// <summary>
        /// Gets the default languages.
        /// </summary>
        /// <value>
        /// The default languages.
        /// </value>
        public static IList<TranslationLanguage> DefaultLanguages
        {
            get
            {
                if (_defaultLanguages == null)
                {
                    _defaultLanguages = new List<TranslationLanguage>
                    {
                        new TranslationLanguage { CultureKey="en-GB", ImageUri="en-GB.png" }, //(new CultureInfo("en-GB"), new Uri("en-GB.png")),
                        new TranslationLanguage { CultureKey="pt-BR", ImageUri="pt-BR.png" }, //new LocalizedCultureInfo(new CultureInfo("pt-BR"),new Uri("pt-BR.png")),
                    };
                }
                return _defaultLanguages;
            }
        }
        private static IList<TranslationLanguage> _defaultLanguages;


        public static Int32 GenerateHash(String textKey)
        {
            return textKey.ToLowerInvariant().GetHashCode();
        }

        #endregion

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationStore" /> class.
        /// </summary>
        public TranslationStore()
        {
            this.DefaultCultureKey = "en-GB";
            this.Languages = new List<TranslationLanguage>();
            this.TranslationItems = new List<TranslationItem>();
        }

        #endregion

        #region Properties

        [XmlElement(ElementName = "default_language")]
        public String DefaultCultureKey { get; set; }

        [XmlArray(ElementName = "languages")]
        [XmlArrayItem(ElementName = "language")]
        public List<TranslationLanguage> Languages { get; set; }

        [XmlArray(ElementName = "translation_items")]
        [XmlArrayItem(ElementName = "text")]
        public List<TranslationItem> TranslationItems { get; set; }

        #endregion

        #region Utility Methods

        public TranslationItem TryFindTranslationItem(String key)
        {
            Int32 keyhash = TranslationStore.GenerateHash(key);
            return this.TranslationItems.FirstOrDefault(x => x.GetHashCode() == keyhash);
        }

        #endregion
    }
}