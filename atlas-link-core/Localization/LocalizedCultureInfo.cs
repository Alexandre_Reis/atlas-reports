﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Media.Imaging;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// A wrapper around the CultureInfo type.
    /// </summary>
    public class LocalizedCultureInfo : LocalizedString
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString" /> class.
        /// </summary>
        /// <param name="language">The language.</param>
        public LocalizedCultureInfo(TranslationLanguage language)
        {
            _language = language;
        }

        #endregion

        #region Fields

        private TranslationLanguage _language;

        #endregion

        #region Utility Members

        private BitmapImage LoadImage()
        {
            return null;
            //Image i = new Image();
            //BitmapImage src = new BitmapImage();

            //src.BeginInit();
            //src.UriSource = new Uri("picture.jpg", UriKind.Relative);
            //src.EndInit();

            //src.BeginInit();
            //src.UriSource = new Uri("picture.jpg", UriKind.Relative);
            //src.CacheOption = BitmapCacheOption.OnLoad;
            //src.EndInit();

            //i.Source = src;
            //i.Stretch = Stretch.Uniform;
            ////int q = src.PixelHeight;        // Image loads here
            //sp.Children.Add(i);
        }

        //public static BitmapImage LoadBitmapFromResource(string pathInApplication, Assembly assembly = null)
        //{
        //    if (assembly == null)
        //    {
        //        assembly = Assembly.GetCallingAssembly();
        //    }

        //    if (pathInApplication[0] == '/')
        //    {
        //        pathInApplication = pathInApplication.Substring(1);
        //    }
        //    return new BitmapImage(new Uri(@"pack://application:,,,/" + assembly.GetName().Name + ";component/" + pathInApplication, UriKind.Absolute));

        //    //this.Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
        //    //             + Assembly.GetExecutingAssembly().GetName().Name
        //    //             + ";component/"
        //    //             + "Icons/Freq.png", UriKind.Absolute)); 
        //}


        #endregion

        #region Properties

        public CultureInfo Culture
        {
            get { return _language.Culture; }
        }

        public BitmapImage Image
        {
            get
            {
                if (_image == null)
                {
                    _image = LoadImage();
                }
                return _image;
            }
        }
        private BitmapImage _image;

        #endregion

        #region Override

        protected override string DoGetText()
        {
            return _language.Culture.DisplayName; // IF THIS DOES NOT WORK THEN JUST LOCALIZE THE IETF TAG
        }
        public override int GetHashCode()
        {
            return _language.Culture.GetHashCode();
        }

        #endregion
    }
}