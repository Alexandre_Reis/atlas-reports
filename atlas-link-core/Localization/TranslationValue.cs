﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// An actual translated text.
    /// </summary>
    [Serializable]
    public class TranslationValue
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationValue" /> class.
        /// </summary>
        public TranslationValue()
        {
            this.CultureKey = "";
            this.TranslatedText = "";
            this.TranslatedTime = DateTime.MinValue;
        }

        #endregion

        #region Properties

        [XmlAttribute(AttributeName = "culture")]
        public String CultureKey { get; set; }

        [XmlAttribute(AttributeName = "text")]
        public String TranslatedText { get; set; }

        [XmlAttribute(AttributeName = "translated")]
        public DateTime TranslatedTime { get; set; }

        #endregion

        #region Overides

        public override string ToString()
        {
            return this.CultureKey;
        }
        public override int GetHashCode()
        {
            return TranslationStore.GenerateHash(this.CultureKey);
        }
        public override bool Equals(object obj)
        {
            return (obj != null && obj.GetType().Equals(this.GetType()) && obj.GetHashCode().Equals(this.GetHashCode()));
        }

        #endregion
    }
}