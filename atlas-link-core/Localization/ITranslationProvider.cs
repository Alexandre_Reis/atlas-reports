﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Atlas.Link.Localization
{
    public interface ITranslationProvider
    {
        /// <summary>
        /// Translates the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <returns></returns>
        String Translate(string key, string defaultText);

        /// <summary>
        /// Gets the available languages.
        /// </summary>
        /// <value>The available languages.</value>
        IEnumerable<TranslationLanguage> Languages { get; }
    }
}