﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Provides translation text during debug and testing.
    /// </summary>
    public class DebugTranslationWriter : ITranslationProvider, IDisposable
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugTranslationWriter" /> class.
        /// </summary>
        public DebugTranslationWriter()
        {
            String dirName = AppEnvironment.AppDataConfigPath;

            if (System.Diagnostics.Debugger.IsAttached)
            {
                Int32 binPos = AppEnvironment.RuntimePath.LastIndexOf(@"\bin\");

                if (binPos > 0)
                {
                    dirName = System.IO.Path.Combine(AppEnvironment.RuntimePath.Substring(0, binPos), "Resources");
                    AppEnvironment.ForceDirectory(dirName);
                }
            }

            _fileName = System.IO.Path.Combine(dirName, RuntimeTranslationProvider.StorageFilename);
            _storage = LocalStorage.ReadOrCreateXml<TranslationStore>(_fileName, true);
            _languages = new List<TranslationLanguage>();
            _nonDefaultcultureKeys = new List<String>();

            // ensure the TranslationStorage has the bare minimum cultures 
            // (takes care of first-time file creation and changes to default list)
            foreach (var language in TranslationStore.DefaultLanguages)
            {
                if (_storage.Languages.Contains(language) == false)
                {
                    _storage.Languages.Add(language);
                    _isModified = true;
                }
            }

            // sync the ITranslationProvider Culture List with the TranslationStorage
            foreach (var language in _storage.Languages)
            {
                _languages.Add(language);

                if (language.CultureKey != _storage.DefaultCultureKey)
                    _nonDefaultcultureKeys.Add(language.CultureKey);
            }
        }

        #endregion

        #region Fields

        private Boolean _isModified = false;
        private String _fileName;
        private TranslationStore _storage;
        private IList<TranslationLanguage> _languages;
        private IList<String> _nonDefaultcultureKeys;

        #endregion

        #region ITranslationProvider Members

        /// <summary>
        /// See <see cref="ITranslationProvider.Translate" />
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <returns></returns>
        public String Translate(string key, string defaultText)
        {
            String resultText = defaultText;
            TranslationItem item = _storage.TryFindTranslationItem(key);

            if (item == null)
            {
                // create the Translation Item the first time it is requested
                item = new TranslationItem { TextKey = key, DefaultText = defaultText };
                _storage.TranslationItems.Add(item);
                _isModified = true;

                resultText = defaultText;
            }
            else
            {
                // the default text may have been modified, so get it
                resultText = item.DefaultText;
            }

            // create references for all non-default cultures
            foreach (String cultureKey in _nonDefaultcultureKeys)
            {
                TranslationValue value = item.TryFindTranslationValue(cultureKey);

                if (value == null)
                {
                    // create entries for all non-default cultures (this is a place-holder for items to translate)
                    item.Translations.Add(new TranslationValue { CultureKey = cultureKey, TranslatedText = resultText });
                    _isModified = true;
                }
                else if (value.CultureKey == TranslationManager.Instance.CurrentLanguage.IetfLanguageTag)
                {
                    // in case we are using the non-default culture at runtime
                    resultText = value.TranslatedText;
                }
            }

            return resultText;
        }
        /// <summary>
        /// See <see cref="ITranslationProvider.AvailableLanguages" />
        /// </summary>
        /// <value>
        /// The available languages.
        /// </value>
        public IEnumerable<TranslationLanguage> Languages
        {
            get { return _languages; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                if ((_storage != null) && (_isModified))
                {
                    LocalStorage.SaveXml<TranslationStore>(_fileName, _storage);
                    Console.WriteLine("DebugTranslationWriter: Translation data was modified");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DebugTranslationWriter: UNABLE TO WRITE Translation data on dispose");
            }
        }

        #endregion
    }
}