﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// A piece of text that needs to be translated.
    /// </summary>
    [Serializable]
    public class TranslationItem
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationItem" /> class.
        /// </summary>
        public TranslationItem()
        {
            this.TextKey = "";
            this.DefaultText = "";
            this.Translations = new List<TranslationValue>();
            this.CreatedTime = DateTime.UtcNow;
        }

        #endregion

        #region Properties

        [XmlAttribute(AttributeName = "key")]
        public String TextKey { get; set; }

        [XmlAttribute(AttributeName = "default")]
        public String DefaultText { get; set; }

        [XmlAttribute(AttributeName = "created")]
        public DateTime CreatedTime { get; set; }

        [XmlArray(ElementName = "translation_values")]
        [XmlArrayItem(ElementName = "translation")]
        public List<TranslationValue> Translations { get; set; }

        #endregion

        #region Utility Members

        public TranslationValue TryFindTranslationValue(String cultureKey)
        {
            Int32 keyhash = TranslationStore.GenerateHash(cultureKey);
            return this.Translations.FirstOrDefault(x => x.GetHashCode() == keyhash);
        }

        #endregion

        #region Overides

        private Int32 _hash = 0;

        public override string ToString()
        {
            return String.Format("[{0}] [{1}]", this.TextKey, this.DefaultText);
        }
        public override int GetHashCode()
        {
            return TranslationStore.GenerateHash(this.TextKey);
        }
        public override bool Equals(object obj)
        {
            return (obj != null && obj.GetType().Equals(this.GetType()) && obj.GetHashCode().Equals(this.GetHashCode()));
        }

        #endregion
    }
}