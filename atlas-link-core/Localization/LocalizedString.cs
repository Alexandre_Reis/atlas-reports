﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Atlas.Link.Localization
{
    /// <summary>
    /// Defines a localized piece of text based on a localization key.
    /// Define string literals using this class in order to be displayed
    /// correctly by the localization sub-system.
    /// </summary>
    public abstract class LocalizedString : INotifyPropertyChanged, IDisposable
    {
        #region Static Members

        private static PropertyChangedEventArgs TextChangedEventArgs = new PropertyChangedEventArgs("Text");

        /// <summary>
        /// Formats the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        public static String Format(LocalizedString message, params Object[] args)
        {
            try
            {
                if ((args == null) || (args.Length == 0))
                    return message.Text;
                else
                    return String.Format(message.Text, args);
            }
            catch (Exception)
            {
                return message.Text;
            }
        }

        #endregion

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString" /> class.
        /// </summary>
        public LocalizedString()
        {
            if (AppEnvironment.IsDesignMode == false)
            {
                TranslationManager.Instance.LanguageChanged += new EventHandler(LanguageChangedEvent);
            }
        }

        #endregion

        #region Events

        private void LanguageChangedEvent(object sender, EventArgs e)
        {
            // invalidate the text
            _text = null;

            // notify subscribers that the text has changed
            RaisePropertyChanged(TextChangedEventArgs);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the translated text.
        /// </summary>
        /// <value>
        /// The translated text.
        /// </value>
        public String Text
        {
            get
            {
                // this will happen the first time the Text property is called.
                // important because we don't want to call descendant DoGetText in ctor.
                if (_text == null)
                    _text = DoGetText();

                return _text;
            }
        }
        private String _text = null;

        protected abstract String DoGetText();

        #endregion

        #region Override

        public override string ToString()
        {
            return this.Text;
        }

        #endregion

        #region INotifyPropertyChanged

        protected void RaisePropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
                handler(this, args);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (AppEnvironment.IsDesignMode == false)
            {
                TranslationManager.Instance.LanguageChanged -= new EventHandler(LanguageChangedEvent);
            }
        }

        #endregion
    }
}