﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Http.Converters.Xml;
using System.Xml;
using System.Xml.Serialization;

namespace Atlas.Link.Serialization
{
    /// <summary>
    /// Extends XmlSerializableHttpMessageConverter in order to strip off XML namespaces.
    /// </summary>
    public class LeanXmlSerializableHttpMessageConverter : XmlSerializableHttpMessageConverter
    {
        #region overrides

        /// <summary>
        /// Abstract template method that writes the actual body using a <see cref="XmlWriter"/>. Invoked from <see cref="M:WriteInternal"/>.
        /// </summary>
        /// <param name="xmlWriter">The XmlWriter to use.</param>
        /// <param name="content">The object to write to the HTTP message.</param>
        protected override void WriteXml(XmlWriter xmlWriter, object content)
        {
            //Create our own namespaces for the output
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            XmlSerializer serializer = this.GetSerializer(content.GetType());
            serializer.Serialize(xmlWriter, content, ns);
        }

        #endregion
    }
}