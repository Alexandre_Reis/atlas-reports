﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link.Model;

namespace Atlas.Link.Client
{
    public interface IUpdateClient<TEntity> where TEntity : BaseAuditableEntity
    {
        TEntity Save(TEntity entity);

        StandardResponse Delete(String entityID);
    }
}