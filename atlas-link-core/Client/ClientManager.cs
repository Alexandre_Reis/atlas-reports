﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Atlas.Link.Client
{
    public static class ClientManager
    {
        #region Factory Members

        private static IClientFactory _factory;

        public static IClientFactory Factory
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new ClientFactory();
                }
                return _factory;
            }
            set { }
        }

        #endregion
    }
}