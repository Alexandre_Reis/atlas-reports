﻿using System;
using System.Collections.Generic;
using Atlas.Link.Model;

namespace Atlas.Link.Client
{
    public interface IPollClient<TEntity> where TEntity : BaseAuditableEntity
    {
        IList<TEntity> FetchAll();
        IList<TEntity> FetchUpdates(DateTime lastUpdatedDate);
    }
}