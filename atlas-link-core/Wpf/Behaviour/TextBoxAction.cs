﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Atlas.Link.Wpf.Behaviour
{
    /// <summary>
    /// Attached Property behaviours for TextBox elements
    /// </summary>
    public static class TextBoxAction
    {
        #region TextSelectionController

        /// <summary>
        /// Attached Property for Text Selection from ViewModels that implement ITextBoxSelectionController.
        /// </summary>
        public static readonly DependencyProperty TextSelectionControllerProperty = DependencyProperty.RegisterAttached("TextSelectionController",
                                                                                                                         typeof(ITextBoxSelectionController),
                                                                                                                         typeof(TextBoxAction),
                                                                                                                         new FrameworkPropertyMetadata(null, OnTextSelectionControllerChanged));

        /// <summary>
        /// Sets the text selection controller.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetTextSelectionController(UIElement element, ITextBoxSelectionController value)
        {
            element.SetValue(TextSelectionControllerProperty, value);
        }
        /// <summary>
        /// Gets the text selection controller.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static ITextBoxSelectionController GetTextSelectionController(UIElement element)
        {
            return (ITextBoxSelectionController)element.GetValue(TextSelectionControllerProperty);
        }

        private static readonly Dictionary<ITextBoxSelectionController, TextBox> _textSelectionControllers = new Dictionary<ITextBoxSelectionController, TextBox>();

        private static void OnTextSelectionControllerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as TextBox;
            if (element == null)
                throw new ArgumentNullException("d");

            var oldController = e.OldValue as ITextBoxSelectionController;
            if (oldController != null)
            {
                _textSelectionControllers.Remove(oldController);
                oldController.SelectAll -= SelectAll;
                oldController.SelectText -= SelectText;

                element.LostKeyboardFocus -= new System.Windows.Input.KeyboardFocusChangedEventHandler(TextBox_LostKeyboardFocus);
                element.LostFocus -= new RoutedEventHandler(TextBox_LostFocus);
            }

            var newController = e.NewValue as ITextBoxSelectionController;
            if (newController != null)
            {
                _textSelectionControllers.Add(newController, element);
                newController.SelectAll += SelectAll;
                newController.SelectText += SelectText;


                element.LostKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(TextBox_LostKeyboardFocus);
                element.LostFocus += new RoutedEventHandler(TextBox_LostFocus);
                element.Focus();
            }
        }
        private static void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }
        private static void TextBox_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            e.Handled = true;
        }
        private static void SelectAll(ITextBoxSelectionController sender)
        {
            TextBox element;
            if (!_textSelectionControllers.TryGetValue(sender, out element))
                throw new ArgumentException("sender");

            // element.Focus();
            element.SelectAll();
        }
        private static void SelectText(ITextBoxSelectionController sender, SelectionEventArgs args)
        {
            TextBox element;
            if (!_textSelectionControllers.TryGetValue(sender, out element))
                throw new ArgumentException("sender");

            // element.Focus();
            element.Select(args.Start, args.Length);
        }

        #endregion
    }
}