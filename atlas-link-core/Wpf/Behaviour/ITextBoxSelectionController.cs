﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Wpf.Behaviour
{
    /// <summary>
    /// Defines behaviour that a ViewModel can implement to control Views.
    /// </summary>
    public interface ITextBoxSelectionController
    {
        /// <summary>
        /// Occurs when selecting all text.
        /// </summary>
        event SelecAllEventHandler SelectAll;
        /// <summary>
        /// Occurs when selecting specific text.
        /// </summary>
        event SelectTextEventHandler SelectText;
    }

    /// <summary>
    /// Text selection delegate.
    /// </summary>
    /// <param name="sender">The sender.</param>
    public delegate void SelecAllEventHandler(ITextBoxSelectionController sender);

    /// <summary>
    /// Text selection delegate.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="SelectionEventArgs" /> instance containing the event data.</param>
    public delegate void SelectTextEventHandler(ITextBoxSelectionController sender, SelectionEventArgs args);

    /// <summary>
    /// Text selection delegate argument.
    /// </summary>
    public class SelectionEventArgs : EventArgs
    {
        #region Construction

        public SelectionEventArgs(Int32 start, Int32 length)
        {
            this.Start = start;
            this.Length = length;
        }

        #endregion

        #region Properties

        public Int32 Start { get; private set; }
        public Int32 Length { get; private set; }

        #endregion
    }
}