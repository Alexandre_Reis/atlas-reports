﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Wpf.Behaviour
{
    public interface IDataFocusReceiver
    {
        void PositionChanged(Int32 sourceRowNbr, String sourceFieldName);
    }
}