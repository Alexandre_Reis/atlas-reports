﻿using System;
using System.Collections.Generic;
using System.Windows;
using DevExpress.Xpf.Grid;

namespace Atlas.Link.Wpf.Behaviour
{
    /// <summary>
    /// Attached Property behaviours for DXGrid elements
    /// </summary>
    public static class DxGridAction
    {
        #region IDataFocusReceiver Implementation

        /// <summary>
        /// Attached Property to bind DX Grid navigation to IDataFocusReceiver implementations.
        /// </summary>
        public static readonly DependencyProperty DataFocusReceiverProperty = DependencyProperty.RegisterAttached("DataFocusReceiver",
                                                                                                                   typeof(IDataFocusReceiver),
                                                                                                                   typeof(DxGridAction),
                                                                                                                   new FrameworkPropertyMetadata(null, OnDataFocusReceiverChanged));

        /// <summary>
        /// Sets the data focus receiver.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetDataFocusReceiver(UIElement element, IDataFocusReceiver value)
        {
            element.SetValue(DataFocusReceiverProperty, value);
        }
        /// <summary>
        /// Gets the data focus receiver.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static IDataFocusReceiver GetDataFocusReceiver(UIElement element)
        {
            return (IDataFocusReceiver)element.GetValue(DataFocusReceiverProperty);
        }

        // dictionary to store instance references.
        private static readonly Dictionary<GridControl, IDataFocusReceiver> _focusReceivers = new Dictionary<GridControl, IDataFocusReceiver>();

        /// <summary>
        /// Called when data focus receiver changed.
        /// </summary>
        /// <param name="d">The dependency object (Grid).</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        /// <exception cref="System.ArgumentNullException">d</exception>
        private static void OnDataFocusReceiverChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GridControl grid = (d as GridControl);
            if (grid == null)
                throw new ArgumentNullException("d");

            IDataFocusReceiver oldReceiver = (e.OldValue as IDataFocusReceiver);
            if (oldReceiver != null)
            {
                _focusReceivers.Remove(grid);
                grid.View.DataControl.CurrentColumnChanged -= new CurrentColumnChangedEventHandler(DataControl_CurrentColumnChanged);
                grid.View.DataControl.CurrentItemChanged -= new CurrentItemChangedEventHandler(DataControl_CurrentItemChanged);
            }

            IDataFocusReceiver newReceiver = (e.NewValue as IDataFocusReceiver);
            if (newReceiver != null)
            {
                _focusReceivers.Add(grid, newReceiver);
                grid.View.DataControl.CurrentColumnChanged += new CurrentColumnChangedEventHandler(DataControl_CurrentColumnChanged);
                grid.View.DataControl.CurrentItemChanged += new CurrentItemChangedEventHandler(DataControl_CurrentItemChanged);
            }
        }
        private static void DataControl_CurrentItemChanged(object sender, CurrentItemChangedEventArgs e)
        {
            GridControl grid = (sender as GridControl);
            if (grid == null)
                throw new ArgumentNullException("sender");

            UpdateDataFocusReceiver(grid);
        }
        private static void DataControl_CurrentColumnChanged(object sender, CurrentColumnChangedEventArgs e)
        {
            GridControl grid = (sender as GridControl);
            if (grid == null)
                throw new ArgumentNullException("sender");

            UpdateDataFocusReceiver(grid);
        }
        private static void UpdateDataFocusReceiver(GridControl grid)
        {
            Int32 listIndex = grid.GetListIndexByRowHandle(grid.View.FocusedRowHandle);
            String fieldName = grid.CurrentColumn.FieldName;

            if (_focusReceivers.ContainsKey(grid))
                _focusReceivers[grid].PositionChanged(listIndex, fieldName);
        }

        #endregion

        #region IBatchedDataUpdate Implementation

        /// <summary>
        /// Attached Property to bind DX Grid batched updates to IBatchedDataUpdate implementations.
        /// </summary>
        public static readonly DependencyProperty BatchedDataUpdateProperty = DependencyProperty.RegisterAttached("BatchedDataUpdate",
                                                                                                                   typeof(IBatchedDataUpdate),
                                                                                                                   typeof(DxGridAction),
                                                                                                                   new FrameworkPropertyMetadata(null, OnBatchedDataUpdateChanged));

        /// <summary>
        /// Sets the data focus receiver.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetBatchedDataUpdate(UIElement element, IBatchedDataUpdate value)
        {
            element.SetValue(BatchedDataUpdateProperty, value);
        }
        /// <summary>
        /// Gets the data focus receiver.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static IBatchedDataUpdate GetBatchedDataUpdate(UIElement element)
        {
            return (IBatchedDataUpdate)element.GetValue(BatchedDataUpdateProperty);
        }

        // dictionary to store instance references.
        private static readonly Dictionary<GridControl, IBatchedDataUpdate> _dataBatchUpdaters = new Dictionary<GridControl, IBatchedDataUpdate>();

        /// <summary>
        /// Called when data focus receiver changed.
        /// </summary>
        /// <param name="d">The dependency object (Grid).</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        /// <exception cref="System.ArgumentNullException">d</exception>
        private static void OnBatchedDataUpdateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GridControl grid = (d as GridControl);
            if (grid == null)
                throw new ArgumentNullException("d");

            IBatchedDataUpdate oldBatchUpdater = (e.OldValue as IBatchedDataUpdate);
            if (oldBatchUpdater != null)
            {
                lock (_dataBatchUpdaters)
                {
                    _dataBatchUpdaters.Remove(grid);
                }
                oldBatchUpdater.DataUpdateStarted -= BatchUpdater_DataUpdateStarted;
                oldBatchUpdater.DataUpdateFinished -= BatchUpdater_DataUpdateFinished;
            }

            IBatchedDataUpdate newBatchUpdater = (e.NewValue as IBatchedDataUpdate);
            if (newBatchUpdater != null)
            {
                lock (_dataBatchUpdaters)
                {
                    _dataBatchUpdaters.Add(grid, newBatchUpdater);
                }
                newBatchUpdater.DataUpdateStarted += BatchUpdater_DataUpdateStarted;
                newBatchUpdater.DataUpdateFinished += BatchUpdater_DataUpdateFinished;
            }
        }
        private static void BatchUpdater_DataUpdateStarted(object sender, EventArgs e)
        {
            IBatchedDataUpdate batchUpdater = (sender as IBatchedDataUpdate);
            if (batchUpdater != null)
            {
                lock (_dataBatchUpdaters)
                {
                    foreach (KeyValuePair<GridControl, IBatchedDataUpdate> kvp in _dataBatchUpdaters)
                    {
                        if (kvp.Value == batchUpdater)
                            Application.Current.Dispatcher.Invoke((Action)(() => kvp.Key.BeginDataUpdate()));
                    }
                }
            }
        }
        private static void BatchUpdater_DataUpdateFinished(object sender, EventArgs e)
        {
            IBatchedDataUpdate batchUpdater = (sender as IBatchedDataUpdate);
            if (batchUpdater != null)
            {
                lock (_dataBatchUpdaters)
                {
                    foreach (KeyValuePair<GridControl, IBatchedDataUpdate> kvp in _dataBatchUpdaters)
                    {
                        if (kvp.Value == batchUpdater)
                            Application.Current.Dispatcher.Invoke((Action)(() => kvp.Key.EndDataUpdate()));
                    }
                }
            }
        }

        #endregion
    }
}