﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using DevExpress.Xpf.Editors;

namespace Atlas.Link.Wpf.Behaviour
{
    /// <summary>
    /// Attached Property behaviours for DXEdit elements
    /// </summary>
    public class DxEditAction: DependencyObject
    {
        #region Utility Members

        private static void SelectAllDelayed(BaseEdit edit)
        {
            if (edit != null)
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                {
                    edit.SelectAll();
                }));
            }
        }

        #endregion

        #region FocusedOnLoad Attached Property

        /// <summary>
        /// Instructs the BaseView to set focus to this control when loading has completed.
        /// </summary>
        public static readonly DependencyProperty FocusedOnLoadProperty = DependencyProperty.RegisterAttached("FocusedOnLoad", 
                                                                                                              typeof(Boolean), 
                                                                                                              typeof(DxEditAction), 
                                                                                                              new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.None));
        /// <summary>
        /// Sets the focused on load.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetFocusedOnLoad(UIElement element, Boolean value)
        {
            element.SetValue(FocusedOnLoadProperty, value);
        }
        /// <summary>
        /// Gets the focused on load.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static Boolean GetFocusedOnLoad(UIElement element)
        {
            return (Boolean)element.GetValue(FocusedOnLoadProperty);
        }
        
        #endregion

        #region SelectAllOnGotFocus Attached Property

        /// <summary>
        /// Configures the editor to select all the text when it receives focus.
        /// </summary>
        public static readonly DependencyProperty SelectAllOnGotFocusProperty = DependencyProperty.RegisterAttached("SelectAllOnGotFocus", 
                                                                                                                    typeof(Boolean), 
                                                                                                                    typeof(DxEditAction), 
                                                                                                                    new UIPropertyMetadata(false, new PropertyChangedCallback(OnSelectAllOnGotFocusChanged)));
        /// <summary>
        /// Gets the select all on got focus.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static Boolean GetSelectAllOnGotFocus(DependencyObject target)
        {
            return (Boolean)target.GetValue(SelectAllOnGotFocusProperty);
        }
        /// <summary>
        /// Sets the select all on got focus.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetSelectAllOnGotFocus(DependencyObject target, Boolean value)
        {
            target.SetValue(SelectAllOnGotFocusProperty, value);
        }

        private static void OnSelectAllOnGotFocusChanged(DependencyObject depobj, DependencyPropertyChangedEventArgs e)
        {
            OnSelectAllOnGotFocusChanged(depobj, (Boolean)e.OldValue, (Boolean)e.NewValue);
        }
        private static void OnSelectAllOnGotFocusChanged(DependencyObject depobj, Boolean oldValue, Boolean newValue)
        {
            BaseEdit edit = (depobj as BaseEdit);

            //TODO: check the old and new values here and act accordingly

            edit.PreviewMouseLeftButtonUp += BaseEdit_PreviewMouseLeftButtonUpHandler;
            edit.GotFocus += BaseEdit_GotFocusHandler;
        }

        private static void BaseEdit_GotFocusHandler(object sender, RoutedEventArgs e)
        {
            BaseEdit edit = (sender as BaseEdit);

            if ((edit != null) && (Mouse.LeftButton == MouseButtonState.Pressed))
                SetNeedSelectAllOnMouseUp(edit, true);
        }
        private static void BaseEdit_PreviewMouseLeftButtonUpHandler(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BaseEdit edit = (sender as BaseEdit);

            if ((edit != null) && GetNeedSelectAllOnMouseUp(edit))
            {
                SetNeedSelectAllOnMouseUp(edit, false);
                SelectAllDelayed(edit);
            }
        }

        #endregion

        #region NeedSelectAllOnMouseUp Attached Property

        /// <summary>
        /// The need select all on mouse up property.
        /// </summary>
        public static readonly DependencyProperty NeedSelectAllOnMouseUpProperty = DependencyProperty.RegisterAttached("NeedSelectAllOnMouseUp", 
                                                                                                                       typeof(Boolean), 
                                                                                                                       typeof(DxEditAction), 
                                                                                                                       new UIPropertyMetadata(false, new PropertyChangedCallback(OnNeedSelectAllOnMouseUpChanged)));
        /// <summary>
        /// Gets the need select all on mouse up.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static bool GetNeedSelectAllOnMouseUp(DependencyObject target)
        {
            return (bool)target.GetValue(NeedSelectAllOnMouseUpProperty);
        }
        /// <summary>
        /// Sets the need select all on mouse up.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetNeedSelectAllOnMouseUp(DependencyObject target, bool value)
        {
            target.SetValue(NeedSelectAllOnMouseUpProperty, value);
        }

        private static void OnNeedSelectAllOnMouseUpChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            OnNeedSelectAllOnMouseUpChanged(o, (bool)e.OldValue, (bool)e.NewValue);
        }
        private static void OnNeedSelectAllOnMouseUpChanged(DependencyObject o, bool oldValue, bool newValue)
        {
            // TODO: Add your property changed side-effects. Descendants can override as well.
        }

        #endregion        
    }
}