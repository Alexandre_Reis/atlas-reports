﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Atlas.Link.Wpf.DataTemplate
{
    public class DataColumn
    {
        public String FieldName { get; set; }
        public String Description { get; set; }
        public DataColumnType ColumnType { get; set; }
        public Boolean ReadOnly { get; set; }
        public IList Source { get; set; }
    }

    public class DataColumnList : List<DataColumn> { }
}