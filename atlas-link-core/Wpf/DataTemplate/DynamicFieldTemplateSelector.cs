﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Atlas.Link.Model;

namespace Atlas.Link.Wpf.DataTemplate
{
    public class DynamicFieldTemplateSelector : DataTemplateSelector
    {
        public String TemplateNameFormat
        {
            get { return _templateNameFormat; }
            set { _templateNameFormat = value; }
        }
        private String _templateNameFormat = "DynamicField{0}Template";

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            DynamicFieldDescriptor fieldDesc = null;

            if (item is DynamicFieldDescriptor)
                fieldDesc = (item as DynamicFieldDescriptor);
            else if (item is DevExpress.Xpf.Grid.ColumnGeneratorItemContext)
                fieldDesc = (((DevExpress.Xpf.Grid.ColumnGeneratorItemContext)item).PropertyDescriptor as DynamicFieldDescriptor);

            if (fieldDesc == null)
                throw new ArgumentException("Unknown item type for DynamicFieldTemplateSelector");

            String templateName = String.Format(TemplateNameFormat, fieldDesc.TemplateType);
            System.Windows.DataTemplate template = (((System.Windows.Controls.Control)container).FindResource(templateName) as System.Windows.DataTemplate);

            if (template == null)
                throw new ArgumentException(String.Format("Unable to find data template [{0}] for DynamicFieldTemplateSelector", templateName));

            return template;
        }
    }
}