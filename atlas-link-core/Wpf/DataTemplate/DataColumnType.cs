﻿using System;

namespace Atlas.Link.Wpf.DataTemplate
{
    public enum DataColumnType
    {
        Text,
        CheckBox,
        ComboBox,
        DateTime
    }
}