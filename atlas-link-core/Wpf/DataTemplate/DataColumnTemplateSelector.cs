﻿using System;
using System.Windows.Controls;

namespace Atlas.Link.Wpf.DataTemplate
{
    public class DataColumnTemplateSelector : DataTemplateSelector
    {
        public System.Windows.DataTemplate TextDataTemplate { get; set; }
        public System.Windows.DataTemplate CheckBoxDataTemplate { get; set; }
        public System.Windows.DataTemplate ComboBoxDataTemplate { get; set; }
        public System.Windows.DataTemplate DateTimeDataTemplate { get; set; }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            DataColumn column = (DataColumn)item;

            switch (column.ColumnType)
            {
                case DataColumnType.CheckBox:
                    return CheckBoxDataTemplate;

                case DataColumnType.ComboBox:
                    return ComboBoxDataTemplate;

                case DataColumnType.DateTime:
                    return DateTimeDataTemplate;

                default:
                    return TextDataTemplate;
            }
        }
    }
}