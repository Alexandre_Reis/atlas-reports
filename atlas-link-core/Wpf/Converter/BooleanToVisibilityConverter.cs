﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{
    /// <summary>
    /// Extended Boolean to Visibility converter.
    /// </summary>
    /// <remarks>
    /// Thanks to Kent Boogaart: http://kentb.blogspot.co.uk/2011/02/booleantovisibilityconverter.html
    /// </remarks>
    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        #region Properties

        // Collapsed:    Do not display the element, and do not reserve space for it in layout.
        // Hidden:       Do not display the element, but reserve space for the element in layout.
        // Visible:      Display the element.

        /// <summary>
        /// Gets or sets a value indicating whether this Boolean Value is reversed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is reversed; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsReversed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use Hidden or Collapsed enumeration value.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use hidden]; otherwise, <c>false</c>.
        /// </value>
        public Boolean UseHidden { get; set; }

        #endregion

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Boolean boolValue = System.Convert.ToBoolean(value, CultureInfo.InvariantCulture);

            if (this.IsReversed)
            {
                boolValue = !boolValue;
            }
            if (boolValue)
            {
                return Visibility.Visible;
            }

            return this.UseHidden ? Visibility.Hidden : Visibility.Collapsed;
        }
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}