﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{
    /// <summary>
    /// Provides a DEBUG mechanism for inspecting XAML bindings at runtime.
    /// </summary>
    public class DebugConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}