﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{
    /// <summary>
    /// Converting full path to file name.
    /// </summary>
    public class PathToFileNameConverter : IValueConverter
    {
        /// <summary>
        /// Converts a full path to file name.
        /// </summary>
        /// <param name="value">The value produced by the binding source. Value should be a full path.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Path.GetFileName(value.ToString());
        }

        /// <summary>
        /// Converts back a value from file name and directory path provided by parameter.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target. Value should be a valid file name.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use. Value should be a valid directory path.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = Path.Combine(parameter.ToString(), value.ToString());
            return str;
        }
    }
}
