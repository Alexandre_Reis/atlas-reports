﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace Atlas.Link.Wpf.Converter
{
    public class StatusToBoolean : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string status = (string)value;

                if (status == "ENABLED")
                    return false;
                else
                    return true;
            }
            return true;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
