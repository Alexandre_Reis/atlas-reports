﻿using System;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{

    public class DateToNullConverter : IValueConverter
    {
        public DateTime Null { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (Null.Equals(value))
                return null;
            return value;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Null;
            return value;
        }
    }
}
