﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{
    /// <summary>
    /// Converts the <see cref="DateTime"/> value to bool value depends on <see cref="TimeSpan"/> passed as Converter Parameter.
    /// </summary>
    public class DateTimeToBoolConverter : IValueConverter
    {
        /// <summary>
        /// Converts a date and time value to bool value.
        /// </summary>
        /// <param name="value">The value produced by the binding source. Should be value of <see cref="DateTime"/> or <see cref="DateTime"/> parsable string.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use. Should be value of <see cref="TimeSpan"/> or <see cref="TimeSpan"/> parsable string.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. True if value is in parameter time span (or default 500ms) from current time.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime date;
            TimeSpan timeSpan;

            if (value is DateTime)
            {
                date = (DateTime)value;
            }
            else if (!DateTime.TryParse(value.ToString(), out date))
            {
                return false;
            }

            if (parameter is TimeSpan)
            {
                timeSpan = (TimeSpan)parameter;
            }
            else if (!TimeSpan.TryParse(parameter.ToString(), out timeSpan))
            {
                timeSpan = new TimeSpan(0, 0, 0, 0, 500);
            }

            bool ret = date + timeSpan >= DateTime.Now;

            return ret;
        }

        /// <summary>
        /// Converts back a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// Current date and time.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}