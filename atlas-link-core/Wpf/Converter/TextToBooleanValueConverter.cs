﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Atlas.Link.Wpf.Converter
{
    public class TextToBooleanValueConverter : IValueConverter
    {
        #region IValueConverter Members

        private String[] ParseParameter(Object parameter)
        {
            String[] boolText = { "NO", "YES" };

            if ((parameter != null) && (parameter is String))
            {

            }

            return boolText;
        }

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            String[] boolText = ParseParameter(parameter);

            if (value == null)
                throw new Exception("value can not be empty");
            if ((value is String) == false)
                throw new Exception("value must be of type String");

            String strValue = value.ToString();

            if (strValue.Equals(boolText[0], StringComparison.InvariantCultureIgnoreCase))
                return false;
            else if (strValue.Equals(boolText[1], StringComparison.InvariantCultureIgnoreCase))
                return true;
            else
                throw new Exception("Invalid boolean text: " + strValue);
        }
        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            String[] boolText = ParseParameter(parameter);

            if (value == null)
                throw new Exception("value can not be empty");
            if ((value is Boolean) == false)
                throw new Exception("value must be of type Boolean");

            Boolean boolValue = (Boolean)value;

            if (boolValue == false)
                return boolText[0];
            else
                return boolText[1];
        }

        #endregion
    }
}