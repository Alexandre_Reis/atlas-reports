﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Atlas.Link.Wpf.Converter
{
    public class TextToBooleanTypeConverter : TypeConverter
    {
        #region Construction

        public TextToBooleanTypeConverter()
        {
            this.FalseText = "FALSE";
            this.TrueText = "TRUE";
        }

        #endregion

        #region Properties

        public String FalseText { get; set; }
        public String TrueText { get; set; }

        #endregion

        #region TypeConverter Members

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(String))
                return true;

            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(String))
                return true;

            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, Object value)
        {
            if (value is String)
            {
                try
                {
                    return Boolean.Parse(value as String);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Cannot convert '{0}' ({1}) because {2}", value, value.GetType(), ex.Message), ex);
                }
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, Object value, Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException("destinationType");

            Boolean boolValue = (Boolean)value;

            if (this.CanConvertTo(context, destinationType))
                return boolValue.ToString();

            return base.ConvertTo(context, culture, value, destinationType);
        }

        #endregion
    }
}