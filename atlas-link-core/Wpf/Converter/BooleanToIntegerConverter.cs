﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Atlas.Link.Wpf.Converter
{
    /// <summary>
    /// Boolean to Integer converter.
    /// </summary>
    public sealed class BooleanToIntegerConverter : IValueConverter
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="BooleanToIntegerConverter"/> class.
        /// </summary>
        public BooleanToIntegerConverter()
        {
            this.FalseValue = 0;
            this.TrueValue = 1;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the "false" value.
        /// </summary>
        /// <value>
        /// The false value.
        /// </value>
        public Int32 FalseValue { get; set; }

        /// <summary>
        /// Gets or sets the "true" value.
        /// </summary>
        /// <value>
        /// The true value.
        /// </value>
        public Int32 TrueValue { get; set; }

        #endregion

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Boolean boolValue = System.Convert.ToBoolean(value, CultureInfo.InvariantCulture);

            return boolValue ? this.TrueValue : this.FalseValue;
        }
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}