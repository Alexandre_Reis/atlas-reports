﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Atlas.Link.Wpf.Control
{
    /// <summary>
    /// Interaction logic for TwoStatesButton.xaml
    /// </summary>
    public partial class TwoStatesButton : UserControl, INotifyPropertyChanged
    {
        public TwoStatesButton()
        {
            this.InitializeComponent();

            this.DataContext = this;

            this.MouseLeftButtonUp += (sender, e) =>
            {
                IsLeftValue = !IsLeftValue;

                if (IsLeftValue)
                    this.BeginStoryboard((Storyboard)this.Resources["MoveRight"]);
                else
                    this.BeginStoryboard((Storyboard)this.Resources["MoveLeft"]);
            };

            this.borderLeft.Loaded += (sender, e) => { RefreshWidth(); };
            this.borderRight.Loaded += (sender, e) => { RefreshWidth(); };
        }

        private void RefreshWidth()
        {
            if (!Double.IsNaN(this.borderLeft.ActualWidth) && !Double.IsNaN(this.borderRight.ActualWidth))
            {
                this.Width = Math.Max(this.borderLeft.ActualWidth, this.borderRight.ActualWidth) + 28;
                this.BeginStoryboard((Storyboard)this.Resources["MoveRight"]);
            }
        }

        private Boolean _isLeftValue = true;

        public Boolean IsLeftValue
        {
            get
            {
                return this._isLeftValue;
            }
            private set
            {
                if (value != this._isLeftValue)
                {
                    this._isLeftValue = value;
                    NotifyPropertyChanged("IsLeftValue");
                }
            }
        }

        #region Displayed values

        public String LeftValue
        {
            get { return (String)GetValue(LeftValueProperty); }
            set { SetValue(LeftValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftValueProperty =
            DependencyProperty.Register("LeftValue", typeof(String), typeof(TwoStatesButton), new UIPropertyMetadata("ON"));

        public String RightValue
        {
            get { return (String)GetValue(RightValueProperty); }
            set { SetValue(RightValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightValueProperty =
            DependencyProperty.Register("RightValue", typeof(String), typeof(TwoStatesButton), new UIPropertyMetadata("OFF"));

        #endregion

        #region FirstColumnWidth & LastColumnWidth

        private GridLength FirstColumnWidth
        {
            get { return (GridLength)GetValue(FirstColumnWidthProperty); }
            set { SetValue(FirstColumnWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstColumnWidth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty FirstColumnWidthProperty =
            DependencyProperty.Register("FirstColumnWidth", typeof(GridLength), typeof(TwoStatesButton), new UIPropertyMetadata(new GridLength(1, GridUnitType.Star)));

        private GridLength LastColumnWidth
        {
            get { return (GridLength)GetValue(LastColumnWidthProperty); }
            set { SetValue(LastColumnWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstColumnWidth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty LastColumnWidthProperty =
            DependencyProperty.Register("LastColumnWidth", typeof(GridLength), typeof(TwoStatesButton), new UIPropertyMetadata(new GridLength(1, GridUnitType.Star)));

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion

    }
}
