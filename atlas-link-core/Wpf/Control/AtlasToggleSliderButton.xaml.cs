﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms.VisualStyles;
using System.Windows.Ink;
using DevExpress.Data.XtraReports.ServiceModel.DataContracts;
using DevExpress.Xpf.Core.HandleDecorator;

namespace Atlas.Link.Wpf.Control
{
    /// <summary>
    /// Interaction logic for MyToggleButton2.xaml
    /// </summary>
    public partial class AtlasToggleSliderButton : ToggleButton
    {
        public AtlasToggleSliderButton()
        {
            InitializeComponent();
        }

        public string sStatus { get; set; }

        #region Displayed values

        public String Left
        {
            get { return (String)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register("Left", typeof(String), typeof(AtlasToggleSliderButton), new UIPropertyMetadata("ON"));

        public String Right
        {
            get { return (String)GetValue(RightProperty); }
            set { SetValue(RightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightProperty =
            DependencyProperty.Register("Right", typeof(String), typeof(AtlasToggleSliderButton), new UIPropertyMetadata("OFF"));



        public String Status
        {
            get { return (String)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Status.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register("Status", typeof(String), typeof(AtlasToggleSliderButton), new UIPropertyMetadata("ENABLED", new PropertyChangedCallback(OnStatusChanged)));

        private static void OnStatusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var obj = o as AtlasToggleSliderButton;
            obj.MyIsEnabled = true;
            obj.SpinVisibility = Visibility.Collapsed;
        }

        public static readonly DependencyProperty MyIsEnabledProperty = DependencyProperty.Register(
            "MyIsEnabled", typeof(bool), typeof(AtlasToggleSliderButton), new PropertyMetadata(true));

        public bool MyIsEnabled
        {
            get { return (bool)GetValue(MyIsEnabledProperty); }
            set { SetValue(MyIsEnabledProperty, value); }
        }

        public static readonly DependencyProperty SpinVisibilityProperty = DependencyProperty.Register(
            "SpinVisibility", typeof(Visibility), typeof(AtlasToggleSliderButton), new PropertyMetadata(Visibility.Collapsed));

        public Visibility SpinVisibility
        {
            get { return (Visibility)GetValue(SpinVisibilityProperty); }
            set { SetValue(SpinVisibilityProperty, value); }
        }

        #endregion

        private void Control_Click(object sender, RoutedEventArgs e)
        {

            System.Threading.Tasks.Task.Factory.StartNew(() => { }).ContinueWith((t) =>
            {
                Dispatcher.BeginInvoke(new Action(() => { MyIsEnabled = false; SpinVisibility = Visibility.Visible; }));
            });


        }
    }
}
