﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Atlas.Link.Wpf.Control
{
    /// <summary>
    /// Interaction logic for StatusLabel.xaml
    /// </summary>
    public partial class StatusLabel : UserControl
    {
        public StatusLabel()
        {
            InitializeComponent();
        }

        #region Displayed values

        
        public string LabelStatusText
        {
            get { return (string)GetValue(LabelStatusTextProperty); }
            set { SetValue(LabelStatusTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LabelStatusText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelStatusTextProperty =
            DependencyProperty.Register("LabelStatusText", typeof(string), typeof(StatusLabel), new UIPropertyMetadata(""));


        public string LabelStatus
        {
            get { return (string)GetValue(LabelStatusProperty); }
            set { SetValue(LabelStatusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LabelStatus.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelStatusProperty =
            DependencyProperty.Register("LabelStatus", typeof(string), typeof(StatusLabel), new UIPropertyMetadata(""));

        

        #endregion

    }
}
