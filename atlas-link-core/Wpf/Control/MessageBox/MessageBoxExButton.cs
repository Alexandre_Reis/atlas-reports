﻿using System;

namespace Atlas.Link.Wpf.Control
{
    public class MessageBoxExButton
    {
        public MessageBoxExButton(string i_result, string i_caption)
            : this(i_result, i_caption, null, null, false, true) { }

        public MessageBoxExButton(string i_result, string i_caption, bool? i_default)
            : this(i_result, i_caption, i_default, null, false, true) { }

        public MessageBoxExButton(string i_result, string i_caption, bool? i_default, Action i_callback)
            : this(i_result, i_caption, i_default, i_callback, false, true) { }

        public MessageBoxExButton(string i_result, string i_caption, bool? i_default, Action i_callback, bool i_strong)
            : this(i_result, i_caption, i_default, i_callback, i_strong, true) { }

        public MessageBoxExButton(string i_result, string i_caption, bool? i_default, Action i_callback, bool i_strong, bool i_visible)
        {
            Result = i_result;
            Caption = i_caption;
            Default = i_default;
            Callback = i_callback;
            Strong = i_strong;
            Visible = i_visible;
        }

        public string Result { get; private set; }
        public string Caption { get; private set; }
        public bool? Default { get; private set; }
        public Action Callback { get; private set; }
        public bool Strong { get; private set; }
        public bool Visible { get; private set; }
    }
}
