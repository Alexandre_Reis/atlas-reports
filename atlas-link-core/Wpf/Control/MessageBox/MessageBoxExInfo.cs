﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;

namespace Atlas.Link.Wpf.Control
{
    public class MessageBoxExInfo
    {
        #region Public Members

        public Window Owner { get; set; }
        public Form OwnerForm { get; set; }

        public string Text { get; set; }
        public string Title { get; set; }

        public IEnumerable<MessageBoxExButton> Buttons { get; set; }
        public ImageSource Icon { get; set; }
        public string IconUri { get; set; }

        public bool UseOptionCheck { get; set; }
        public bool? OptionCheckValue { get; set; }
        public string OptionCheckText { get; set; }

        #endregion
    }
}
