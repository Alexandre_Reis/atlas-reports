﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Atlas.Link.Wpf.Control
{
    public static class MessageBoxExButtons
    {
        #region Private Data

        private static readonly Dictionary<string, ReadOnlyCollection<MessageBoxExButton>> Presets;

        #endregion

        #region Constructor

        static MessageBoxExButtons()
        {
            Presets = new Dictionary<string, ReadOnlyCollection<MessageBoxExButton>>();

            List<MessageBoxExButton> buttons;

            buttons = new List<MessageBoxExButton>
                          {
                              new MessageBoxExButton(MessageBoxExResults.OK, "_OK", true)
                          };
            Presets["OK"] = new ReadOnlyCollection<MessageBoxExButton>(buttons);

            buttons = new List<MessageBoxExButton>
                          {
                              new MessageBoxExButton(MessageBoxExResults.OK, "_OK", true),
                              new MessageBoxExButton(MessageBoxExResults.Cancel, "_Cancel", false)
                          };
            Presets["OKCancel"] = new ReadOnlyCollection<MessageBoxExButton>(buttons);

            buttons = new List<MessageBoxExButton>
                          {
                              new MessageBoxExButton(MessageBoxExResults.Yes, "_Yes", true),
                              new MessageBoxExButton(MessageBoxExResults.No, "_No", false)
                          };
            Presets["YesNo"] = new ReadOnlyCollection<MessageBoxExButton>(buttons);

            buttons = new List<MessageBoxExButton>
                          {
                              new MessageBoxExButton(MessageBoxExResults.Yes, "_Yes", true),
                              new MessageBoxExButton(MessageBoxExResults.No, "_No", null),
                              new MessageBoxExButton(MessageBoxExResults.Cancel, "_Cancel", false)
                          };
            Presets["YesNoCancel"] = new ReadOnlyCollection<MessageBoxExButton>(buttons);
        }

        #endregion

        #region Public Members

        public static IEnumerable<MessageBoxExButton> OK
        {
            get { return Presets["OK"]; }
        }

        public static IEnumerable<MessageBoxExButton> OKCancel
        {
            get { return Presets["OKCancel"]; }
        }

        public static IEnumerable<MessageBoxExButton> YesNo
        {
            get { return Presets["YesNo"]; }
        }

        public static IEnumerable<MessageBoxExButton> YesNoCancel
        {
            get { return Presets["YesNoCancel"]; }
        }

        #endregion
    }
}
