﻿namespace Atlas.Link.Wpf.Control
{
    public class MessageBoxExResults
    {
        #region Public Members

        public static string OK
        {
            get { return "OK"; }
        }

        public static string Cancel
        {
            get { return "Cancel"; }
        }

        public static string Yes
        {
            get { return "Yes"; }
        }

        public static string No
        {
            get { return "No"; }
        }

        #endregion
    }
}
