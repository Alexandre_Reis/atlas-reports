﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Atlas.Link.Wpf.Control
{
    public static class MessageBoxExIcons
    {
        #region Private Data

        private static readonly Dictionary<string, ImageSource> SystemIcons;

        private const string ApplicationIconID = "SystemIcons.Application";
        private const string AsteriskIconID = "SystemIcons.Asterisk";
        private const string ErrorIconID = "SystemIcons.Error";
        private const string ExclamationIconID = "SystemIcons.Exclamation";
        private const string HandIconID = "SystemIcons.Hand";
        private const string InformationIconID = "SystemIcons.Information";
        private const string QuestionIconID = "SystemIcons.Question";
        private const string ShieldIconID = "SystemIcons.Shield";
        private const string WarningIconID = "SystemIcons.Warning";
        private const string WinLogoIconID = "SystemIcons.WinLogo";

        #endregion

        #region Constructor

        static MessageBoxExIcons()
        {
            SystemIcons = new Dictionary<string, ImageSource>();

            var systemIcons = GetSystemIcons();
            foreach (var key in systemIcons.Keys)
            {
                SystemIcons[key] = Imaging.CreateBitmapSourceFromHIcon(systemIcons[key].Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
        }

        #endregion

        #region Public Members

        public static ImageSource Application
        {
            get { return SystemIcons[ApplicationIconID]; }
        }

        public static ImageSource Asterisk
        {
            get { return SystemIcons[AsteriskIconID]; }
        }

        public static ImageSource Error
        {
            get { return SystemIcons[ErrorIconID]; }
        }

        public static ImageSource Exclamation
        {
            get { return SystemIcons[ExclamationIconID]; }
        }

        public static ImageSource Hand
        {
            get { return SystemIcons[HandIconID]; }
        }

        public static ImageSource Information
        {
            get { return SystemIcons[InformationIconID]; }
        }

        public static ImageSource Question
        {
            get { return SystemIcons[QuestionIconID]; }
        }

        public static ImageSource Shield
        {
            get { return SystemIcons[ShieldIconID]; }
        }

        public static ImageSource Warning
        {
            get { return SystemIcons[WarningIconID]; }
        }

        public static ImageSource WinLogo
        {
            get { return SystemIcons[WinLogoIconID]; }
        }

        #endregion

        #region Private Implementation

        public static Dictionary<string, Icon> GetSystemIcons()
        {
            var systemIcons = new Dictionary<string, Icon>();
            systemIcons[ApplicationIconID] = System.Drawing.SystemIcons.Application;
            systemIcons[AsteriskIconID] = System.Drawing.SystemIcons.Asterisk;
            systemIcons[ErrorIconID] = System.Drawing.SystemIcons.Error;
            systemIcons[ExclamationIconID] = System.Drawing.SystemIcons.Exclamation;
            systemIcons[HandIconID] = System.Drawing.SystemIcons.Hand;
            systemIcons[InformationIconID] = System.Drawing.SystemIcons.Information;
            systemIcons[QuestionIconID] = System.Drawing.SystemIcons.Question;
            systemIcons[ShieldIconID] = System.Drawing.SystemIcons.Shield;
            systemIcons[WarningIconID] = System.Drawing.SystemIcons.Warning;
            systemIcons[WinLogoIconID] = System.Drawing.SystemIcons.WinLogo;
            return systemIcons;
        }

        #endregion
    }
}
