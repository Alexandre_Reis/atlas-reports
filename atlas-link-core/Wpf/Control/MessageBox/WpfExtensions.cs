﻿using System;
using System.Windows;
using System.Windows.Interop;

namespace Atlas.Link.Wpf.Control
{
    public static class WpfExtensions
    {
        #region Public Methods

        public static void RemoveIcon(this Window i_window)
        {
            IntPtr hwnd = new WindowInteropHelper(i_window).Handle;

            int exStyle = NativeMethods.GetWindowLong(hwnd, NativeMethods.GWL_EXSTYLE);
            exStyle |= NativeMethods.WS_EX_DLGMODALFRAME;
            NativeMethods.SetWindowLong(hwnd, NativeMethods.GWL_EXSTYLE, exStyle);

            NativeMethods.SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0,
                NativeMethods.SWP_NOMOVE | NativeMethods.SWP_NOSIZE | NativeMethods.SWP_NOZORDER | NativeMethods.SWP_FRAMECHANGED);

            NativeMethods.SendMessage(hwnd, NativeMethods.WM_SETICON, IntPtr.Zero, IntPtr.Zero);
        }

        #endregion
    }
}
