﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Atlas.Link.Wpf.Control
{
    /// <summary>
    /// Interaction logic for StatusButton.xaml
    /// </summary>
    public partial class StatusButton : UserControl, INotifyPropertyChanged
    {
        public StatusButton()
        {
            this.InitializeComponent();
        }


        #region Displayed values


        public string ImageStatus
        {
            get { return (string)GetValue(ImageStatusProperty); }
            set { SetValue(ImageStatusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageStatus.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageStatusProperty =
            DependencyProperty.Register("ImageStatus", typeof(string), typeof(StatusButton), new UIPropertyMetadata(""));

        


        public string StatusText
        {
            get { return (string)GetValue(StatusTextProperty); }
            set { SetValue(StatusTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusTextProperty =
            DependencyProperty.Register("StatusText", typeof(string), typeof(StatusButton), new UIPropertyMetadata(""));

        

        public string Status
        {
            get { return (string)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Status.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register("Status", typeof(string), typeof(StatusButton), new UIPropertyMetadata("ENABLED"));

        

        public String LeftValue
        {
            get { return (String)GetValue(LeftValueProperty); }
            set { SetValue(LeftValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftValueProperty =
            DependencyProperty.Register("LeftValue", typeof(String), typeof(TwoStatesButton), new UIPropertyMetadata("ON"));

        public String RightValue
        {
            get { return (String)GetValue(RightValueProperty); }
            set { SetValue(RightValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightValueProperty =
            DependencyProperty.Register("RightValue", typeof(String), typeof(TwoStatesButton), new UIPropertyMetadata("OFF"));

        #endregion

        #region FirstColumnWidth & LastColumnWidth

        private GridLength FirstColumnWidth
        {
            get { return (GridLength)GetValue(FirstColumnWidthProperty); }
            set { SetValue(FirstColumnWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstColumnWidth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty FirstColumnWidthProperty =
            DependencyProperty.Register("FirstColumnWidth", typeof(GridLength), typeof(TwoStatesButton), new UIPropertyMetadata(new GridLength(1, GridUnitType.Star)));

        private GridLength LastColumnWidth
        {
            get { return (GridLength)GetValue(LastColumnWidthProperty); }
            set { SetValue(LastColumnWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstColumnWidth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty LastColumnWidthProperty =
            DependencyProperty.Register("LastColumnWidth", typeof(GridLength), typeof(TwoStatesButton), new UIPropertyMetadata(new GridLength(1, GridUnitType.Star)));

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion

    }
}
