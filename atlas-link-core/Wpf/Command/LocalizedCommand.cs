﻿using System;
using System.Windows.Input;
using Atlas.Link.Localization;

namespace Atlas.Link.Wpf.Command
{
    /// <summary>
    /// A WPF command with localized text.
    /// </summary>
    public class LocalizedCommand : LocalizedString, ICommand
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedCommand" /> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="canExecute">The can execute.</param>
        public LocalizedCommand(String key, String defaultText, Action handler, Boolean canExecute = true)
        {
            _key = key;
            _defaultText = defaultText;
            _handler = handler;
            _canExecute = canExecute;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedCommand" /> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultText">The default text.</param>
        /// <param name="parameterizedHandler">The parameterized handler.</param>
        /// <param name="canExecute">The can execute.</param>
        public LocalizedCommand(String key, String defaultText, Action<Object> parameterizedHandler, Boolean canExecute = true)
        {
            _key = key;
            _defaultText = defaultText;
            _parameterizedHandler = parameterizedHandler;
            _canExecute = canExecute;
        }

        #endregion

        #region Fields

        private Object _syncObject = new Object();
        private String _key;
        private String _defaultText;
        private Action _handler;
        private Action<Object> _parameterizedHandler;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the value of the Execute flag.
        /// </summary>
        /// <value>
        /// The can execute.
        /// </value>
        public Boolean CanExecute
        {
            get { return _canExecute; }
            set
            {
                if (value != _canExecute)
                {
                    _canExecute = value;
                    RaiseCanExecuteChanged();
                }
            }
        }
        private Boolean _canExecute;

        public Boolean IsDefault
        {
            get { return _isDefault; }
            set
            {
                if (value != _isDefault)
                {
                    _isDefault = value;
                    RaisePropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("IsDefault"));
                }
            }
        }
        private Boolean _isDefault;

        public Boolean IsCancel
        {
            get { return _isCancel; }
            set
            {
                if (value != _isCancel)
                {
                    _isCancel = value;
                    RaisePropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("IsCancel"));
                }
            }
        }
        private Boolean _isCancel;



        #endregion
        
        #region ICommand

        Boolean ICommand.CanExecute(object parameter)
        {
            return _canExecute;
        }

        private void RaiseCanExecuteChanged()
        {
            EventHandler handler = _canExecutEvent;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private event EventHandler _canExecutEvent;

        event EventHandler ICommand.CanExecuteChanged
        {
            add
            {
                lock (_syncObject)
                {
                    _canExecutEvent += value;
                }
            }
            remove
            {
                lock (_syncObject)
                {
                    _canExecutEvent -= value;
                }
            }
        }
        void ICommand.Execute(object parameter)
        {
            if (_handler != null)
                _handler();
            else
                _parameterizedHandler(parameter);
        }

        #endregion

        #region Override

        /// <summary>
        /// Returns the localized text.
        /// </summary>
        /// <returns></returns>
        protected override string DoGetText()
        {
            return TranslationManager.Instance.Translate(_key, _defaultText);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return _key.GetHashCode();
        }

        #endregion
    }
}