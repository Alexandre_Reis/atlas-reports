﻿using System;
using System.Linq.Expressions;

namespace Atlas.Link.Reflection
{
    /// <summary>
    /// Reflection Utilities
    /// </summary>
    public static class PropertyUtils
    {
        /// <summary>
        /// Extracts the name of the property from an expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpresssion">The property expresssion.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">propertyExpresssion</exception>
        /// <exception cref="System.ArgumentException">Lambda must return a property.</exception>
        public static String ExtractPropertyName<T>(Expression<Func<T>> propertyExpresssion)
        {
            if (propertyExpresssion == null)
                throw new ArgumentNullException("propertyExpresssion");

            MemberExpression body = (propertyExpresssion.Body as MemberExpression);
            if (body == null)
                throw new ArgumentException("Lambda must return a property.");

            return body.Member.Name;

            //var vmExpression = body.Expression as ConstantExpression;
            //if (vmExpression != null)
            //{
            //    LambdaExpression lambda = System.Linq.Expressions.Expression.Lambda(vmExpression);
            //    Delegate vmFunc = lambda.Compile();
            //    object sender = vmFunc.DynamicInvoke();

            //    if (handler != null)
            //    {
            //        handler(sender, new PropertyChangedEventArgs(body.Member.Name));
            //    }
            //}

            //field = value;
            //return true;
        }
    }
}