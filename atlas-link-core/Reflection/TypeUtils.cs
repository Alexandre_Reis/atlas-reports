﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Reflection
{
    /// <summary>
    /// Utility methods for System.Type.
    /// </summary>
    public static class TypeUtils
    {
        /// <summary>
        /// Standardizes type names, specifically generic types.
        /// </summary>
        /// <param name="typeref">A Typ ereference.</param>
        /// <returns></returns>
        public static String GetCleanTypeName(Type typeref)
        {
            String typeName = typeref.FullName;

            if (typeref.IsGenericType && !typeref.IsGenericTypeDefinition)
            {
                //Atlas.Link.Client.PollingDataSource`1[[Atlas.Link.Model.Message, AtlasBus.Model, Version=4.0.0.0, Culture=neutral, PublicKeyToken=null]]
                Int32 offset = typeref.FullName.IndexOf('`');

                if (offset > 0)
                {
                    System.Text.StringBuilder output = new System.Text.StringBuilder();
                    output.Append(typeName.Substring(0, offset));
                    output.Append("(");

                    Type[] typeParameters = typeref.GetGenericArguments();

                    for (int i = 0; i < typeParameters.Length; i++)
                    {
                        if (i > 0)
                            output.Append(", ");
                        output.Append(GetCleanTypeName(typeParameters[i]));
                    }

                    output.Append(")");
                    typeName = output.ToString();
                }
            }

            return typeName;
        }
    }
}
