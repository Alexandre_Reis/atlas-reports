﻿using System;

namespace Atlas.Link.Views
{
    /// <summary>
    /// Defines where the view is bound
    /// </summary>
    public enum ViewTypeBinding : byte
    {
        #region Values

        MessageProvider = 0,
        ToolboxProvider = 1,
        PropertyProvider = 2,
        MainDocument = 3,
        FixedModalDocument = 4,
        FixedFloatingDocument = 5,
        ResizeableModalDocument = 6,
        ResizeableFloatingDocument = 7,

        #endregion
    }
}