﻿using System;

namespace Atlas.Link.Views
{
    /// <summary>
    /// Defines the menu bindings for the View
    /// </summary>
    public enum RootMenuBinding : byte
    {
        #region Values

        None = 0,
        File = 1,
        Edit = 2,
        Tool = 3,
        View = 4,
        Help = 5,
        Log = 6,

        #endregion
    }
}