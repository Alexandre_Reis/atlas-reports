﻿using System;
using System.Collections.Generic;

namespace Atlas.Link
{
    public enum InstantiationMode
    {
        Singleton = 0,
        Multiple = 1,
    }

    /// <summary>
    /// A temporary poor-mans IoC container.
    /// Replace this eventually with Sprint.NET or Castle Windsor or the like.
    /// </summary>
    public static class ServiceLocator 
    {
        #region Types

        internal class ServiceMetaData
        {
            public Type ConcreteType;
            public Object Singleton;
            public InstantiationMode Mode;
        }

        #endregion

        #region Factory members

        private static Dictionary<Type, ServiceMetaData> _typeDictionary = new Dictionary<Type, ServiceMetaData>();

        /// <summary>
        /// Registers types to be constructed by the ServiceLocator.
        /// </summary>
        /// <param name="baseType">Type of the base.</param>
        /// <param name="concreteType">Type of the concrete.</param>
        /// <param name="mode">The mode.</param>
        /// <exception cref="System.ArgumentNullException">baseType</exception>
        /// <exception cref="System.Exception"></exception>
        public static void Register(Type baseType, Type concreteType, InstantiationMode mode = InstantiationMode.Singleton)
        {
            if (baseType == null) throw new ArgumentNullException("baseType");
            if (concreteType == null) throw new ArgumentNullException("concreteType");

            lock (_typeDictionary)
            {
                if (_typeDictionary.ContainsKey(baseType))
                    throw new Exception(String.Format("The baseType [{0}] has already been registered with ServiceLocator", baseType.FullName));

                if (baseType.IsAssignableFrom(concreteType) == false)
                    throw new Exception(String.Format("The concreteType [{0}] does not implement Base type [{1}]", concreteType.FullName, baseType.FullName));

                if (concreteType.IsInterface)
                    throw new Exception(String.Format("The concreteType [{0}] can not be an interface type", concreteType.FullName));

                if (concreteType.IsAbstract)
                    throw new Exception(String.Format("The concreteType [{0}] can not be an abstract type", concreteType.FullName));

                if (concreteType.IsByRef)
                    throw new Exception(String.Format("The concreteType [{0}] can not be a reference type", concreteType.FullName));

                if (concreteType.IsEnum)
                    throw new Exception(String.Format("The concreteType [{0}] can not be an enumeration type", concreteType.FullName));

                if (concreteType.IsPrimitive)
                    throw new Exception(String.Format("The concreteType [{0}] can not be a primitive type", concreteType.FullName));

                if (concreteType.IsValueType)
                    throw new Exception(String.Format("The concreteType [{0}] can not be a value type", concreteType.FullName));



                _typeDictionary.Add(baseType, new ServiceMetaData { ConcreteType = concreteType, Singleton = null, Mode = mode });
            }
        }

        /// <summary>
        /// Get a concrete instance for the supplied generic type (usually an interface or abstract type).
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static TService Get<TService>(Boolean allowNull = false) where TService: class
        {
            Type baseType = typeof(TService);
            ServiceMetaData metaData = null;

            lock (_typeDictionary)
            {
                if (_typeDictionary.TryGetValue(baseType, out metaData) == false)
                {
                    if (allowNull)
                        return null;
                    else
                        throw new Exception(String.Format("Type [{0}] has not been registered with ServiceLocator", baseType.FullName));
                }

                if ((metaData.Mode == InstantiationMode.Singleton) && (metaData.Singleton == null))
                {
                    metaData.Singleton = System.Activator.CreateInstance(metaData.ConcreteType);
                }
            }

            if (metaData.Mode == InstantiationMode.Singleton)
                return (TService)metaData.Singleton;
            else
                return (TService)System.Activator.CreateInstance(metaData.ConcreteType);
        }

        /// <summary>
        /// Releases all known instances.
        /// </summary>
        public static void Release()
        {
            lock (_typeDictionary)
            {
                foreach (var metaData in _typeDictionary.Values)
                {
                    if (metaData.Singleton != null)
                    {
                        if (metaData.Singleton is IDisposable)
                            ((IDisposable)metaData.Singleton).Dispose();

                        metaData.Singleton = null;
                    }
                }

                GC.Collect();
            }
        }

        #endregion
    }

    //TODO: implement attribute for dynamic ServiceLocator

    //[AttributeUsage(AttributeTargets.Assembly, AllowMultiple=false, Inherited=false)]
    //public class TestAttribute: Attribute
    //{
    //    //
    //}

}