﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link
{
    public class MethodMetric: IDisposable
    {
        public MethodMetric(Common.Logging.ILog logger, String methodName)
        {
            _logger = logger;
            _methodName = methodName;
            _stopwatch = System.Diagnostics.Stopwatch.StartNew();

            _logger.TraceFormat("{0} entered.", _methodName);
        }

        private Common.Logging.ILog _logger;
        private String _methodName;
        private System.Diagnostics.Stopwatch _stopwatch;

        public void Dispose()
        {
            _stopwatch.Stop();
            _logger.TraceFormat("{0} exited. Duration = {1}", _methodName, _stopwatch.Elapsed);
        }
    }
}
