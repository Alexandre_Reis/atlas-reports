﻿using System;
using System.Diagnostics;

namespace Atlas.Link
{
    /// <summary>
    /// The singleton class that contains the runtime Application Environment values.
    /// </summary>
    public static class AppEnvironment
    {
        #region Static Constructors

        static AppEnvironment()
        {
            // determine whether the ..core.dll has been loaded in the XAML designer (
            AppEnvironment.IsDesignMode = System.ComponentModel.DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject());

            if (AppEnvironment.IsDesignMode == false)
            {
                // get the entry point assembly/executable
                var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();

                // loads the win32 FileInfo structure for the entry point executable.
                System.Diagnostics.FileVersionInfo fileInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(entryAssembly.Location);
                Version buildVersion = Version.Parse(String.Format("{0}.{1}.{2}.{3}", fileInfo.FileMajorPart, fileInfo.FileMinorPart, fileInfo.FileBuildPart, fileInfo.FilePrivatePart));
                String buildMajorMinor = String.Format("{0}.{1}", buildVersion.Major, buildVersion.Minor);

                // store exe info
                AppEnvironment.RuntimeExe = entryAssembly.Location;
                AppEnvironment.RuntimePath = System.IO.Path.GetDirectoryName(AppEnvironment.RuntimeExe);

                // store version numbers
                AppEnvironment.RuntimeVersion = entryAssembly.GetName().Version.ToString();
                AppEnvironment.CompileVersion = fileInfo.FileVersion;
                AppEnvironment.ProductVersion = fileInfo.ProductVersion;
                AppEnvironment.CompanyName = String.IsNullOrWhiteSpace(fileInfo.CompanyName) ? "BRITech" : fileInfo.CompanyName;
                AppEnvironment.CompanyCode = AppEnvironment.CompanyName.Replace(" ", "");
                AppEnvironment.ProductName = String.IsNullOrWhiteSpace(fileInfo.ProductName) ? "Atlas" : fileInfo.ProductName;
                AppEnvironment.ProductCode = AppEnvironment.ProductName.Replace(" ", "");

                String productStage = "1.0"; // "DEV";

                //TODO: implement this properly

                //if (AppEnvironment.ProductVersion.EndsWith("-UAT"))
                //    productStage = "UAT";
                //else if (AppEnvironment.ProductVersion.EndsWith("-REL"))
                //    productStage = "REL";

                // create environment paths

                String configPath = Atlas.Link.CommandLine.KeyExists("c") ? Atlas.Link.CommandLine.GetValue("c") : "";

                if (String.IsNullOrWhiteSpace(configPath))
                {
                    AppEnvironment.AppDataPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), AppEnvironment.CompanyCode, AppEnvironment.ProductCode, productStage);
                    AppEnvironment.UserDataPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), AppEnvironment.CompanyCode, AppEnvironment.ProductCode, productStage);
                }
                else
                {
                    
                    configPath = configPath.TrimEnd('"');
                    AppEnvironment.AppDataPath = configPath;
                    AppEnvironment.UserDataPath = configPath;
                }

                AppEnvironment.AppDataConfigPath = System.IO.Path.Combine(AppEnvironment.AppDataPath, "Config");
                AppEnvironment.AppDataLogPath = System.IO.Path.Combine(AppEnvironment.AppDataPath, "Log");
                
                AppEnvironment.UserDataSettingsPath = System.IO.Path.Combine(AppEnvironment.UserDataPath, "Settings");
                AppEnvironment.UserDataLayoutPath = System.IO.Path.Combine(AppEnvironment.AppDataPath, "Layout");

                if (String.IsNullOrWhiteSpace(Properties.Settings.Default.LogViewerPath))
                {
                    Debug.Assert(RuntimePath != null, "RuntimePath != null");
                    LogViewerPath = System.IO.Path.Combine(RuntimePath, "Atlas.Logviewer.exe");
                }
                else
                {
                    LogViewerPath = Properties.Settings.Default.LogViewerPath;
                }

                // ensure the directories exist
                ForceDirectory(AppEnvironment.AppDataPath);
                ForceDirectory(AppEnvironment.AppDataConfigPath);
                ForceDirectory(AppEnvironment.AppDataLogPath);
                ForceDirectory(AppEnvironment.UserDataPath);
                ForceDirectory(AppEnvironment.UserDataSettingsPath);
                ForceDirectory(AppEnvironment.UserDataLayoutPath);
            }
        }

        #endregion

        #region Static Properties

        public static Boolean IsDesignMode { get; private set; }

        /// <summary>
        /// Gets the full name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        public static String CompanyName { get; private set; }
        /// <summary>
        /// Gets the company code used for path names.
        /// </summary>
        /// <value>
        /// The company code.
        /// </value>
        public static String CompanyCode { get; private set; }
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public static String ProductName { get; private set; }
        /// <summary>
        /// Gets the product code used for path names.
        /// </summary>
        /// <value>
        /// The product code.
        /// </value>
        public static String ProductCode { get; private set; }
        /// <summary>
        /// Gets the full path of the entry point assembly.
        /// </summary>
        /// <value>
        /// Gets the full path of the entry point assembly.
        /// </value>
        public static String RuntimeExe { get; private set; }
        /// <summary>
        /// Gets the directory of the entry point assembly.
        /// </summary>
        /// <value>
        /// Gets the directory of the entry point assembly.
        /// </value>
        public static String RuntimePath { get; private set; }
        /// <summary>
        /// Gets the runtime version.
        /// Used by the VLR for assembly binding.
        /// </summary>
        /// <value>
        /// The assembly version for the entry point exe.
        /// </value>
        public static String RuntimeVersion { get; private set; }
        /// <summary>
        /// Gets the compile version.
        /// Used by the build system to indicate build information.
        /// </summary>
        /// <value>
        /// The win32 file version for the entry point exe.
        /// </value>
        public static String CompileVersion { get; private set; }
        /// <summary>
        /// Gets the product version.
        /// Used to document the product for end-user display.
        /// </summary>
        /// <value>
        /// The product version.
        /// </value>
        public static String ProductVersion { get; private set; }
        /// <summary>
        /// Gets the root application data path.
        /// Application data is shared between user accounts.
        /// </summary>
        /// <value>
        /// The root application data path.
        /// </value>
        public static String AppDataPath { get; private set; }
        /// <summary>
        /// Gets the application configuration path.
        /// Configuration files are stored here.
        /// </summary>
        /// <value>
        /// The application configuration path.
        /// </value>
        public static String AppDataConfigPath { get; private set; }
        /// <summary>
        /// Gets the application log path.
        /// Log files are written here.
        /// </summary>
        /// <value>
        /// The application log path.
        /// </value>
        public static String AppDataLogPath { get; private set; }
        /// <summary>
        /// Gets the root user data path.
        /// User data is private for a user account.
        /// </summary>
        /// <value>
        /// The root user data path.
        /// </value>
        public static String UserDataPath { get; private set; }
        /// <summary>
        /// Gets the user settings path.
        /// User settings are stored here.
        /// </summary>
        /// <value>
        /// The user settings path.
        /// </value>
        public static String UserDataSettingsPath { get; private set; }
        /// <summary>
        /// Provides a full path to the LogViewer. This can be overridden in config.
        /// </summary>
        /// <value>
        /// The log viewer path.
        /// </value>
        public static String LogViewerPath { get; private set; }


        public static String UserDataLayoutPath { get; private set; }

        #endregion

        #region Utility Members

        public static void ForceDirectory(String directoryPath)
        {
            if (System.IO.Directory.Exists(directoryPath) == false)
                System.IO.Directory.CreateDirectory(directoryPath);
        }

        #endregion
    }
}