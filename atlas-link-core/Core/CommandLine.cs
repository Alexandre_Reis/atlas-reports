﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link
{
    /// <summary>
    /// A more intelligent command line parser than system default.
    /// </summary>
    public static class CommandLine
    {
        private static Dictionary<String, String> _commands;
        private static Char[] QuoteChars = { '"', '\'' };

        static CommandLine()
        {
            _commands = new Dictionary<String, String>();

            String curKey = "";

            foreach (String word in System.Environment.GetCommandLineArgs())
            {
                if (word.StartsWith("/") || word.StartsWith("-"))
                {
                    String newKey = word.Substring(1);
                    String newVal = "";

                    if ((newKey.Length > 2) && (newKey[1] == ':'))
                    {
                        newVal = newKey.Substring(2);
                        newKey = newKey.Substring(0, 1);
                    }

                    curKey = newKey.ToLower();

                    _commands.Add(curKey, newVal);
                }
                else if (curKey.Length > 0)
                {                   
                    _commands[curKey] = word.TrimStart(QuoteChars).TrimEnd(QuoteChars);
                }
            }
        }

        /// <summary>
        /// Determines whether a key exists.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static Boolean KeyExists(String key)
        {
            return _commands.ContainsKey(key.ToLower());
        }

        /// <summary>
        /// Gets the value for the given key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static String GetValue(String key)
        {
            return _commands[key.ToLower()];
        }
    }
}