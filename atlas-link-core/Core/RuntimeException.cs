﻿using System;

// ReSharper disable once CheckNamespace
namespace Atlas.Link
{
    /// <summary>
    /// Provides a localized runtime exception that displays specific messages to the user for fatal exceptions.
    /// </summary>
    public class RuntimeException: Exception
    {
        private static String Localize(String key, String message)
        {
            try
            {
                return Localization.TranslationManager.Instance.Translate(key, message);
            }
            catch (Exception)
            {
                return message;
            }            
        }
        private static String Localize(String key, String message, params Object[] args)
        {
            try
            {
                return String.Format(Localization.TranslationManager.Instance.Translate(key, message), args);
            }
            catch (Exception)
            {
                return message;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeException"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="message">The message.</param>
        public RuntimeException(String key, String message): base(Localize(key, message))
        {
            //
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeException"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        public RuntimeException(String key, String message, params Object[] args) : base(Localize(key, message, args))
        {
            //
        }
    }
}