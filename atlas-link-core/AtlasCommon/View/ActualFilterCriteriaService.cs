using DevExpress.Data.Filtering;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Editors.Filtering;

namespace Atlas.Link.AtlasCommon.View
{
    public class ActualFilterCriteriaService : ServiceBase
    {
        private FilterControl FilterControl
        {
            get { return ((FilterControl) AssociatedObject); }
        }

        public CriteriaOperator ActualFilterCriteria
        {
            get { return FilterControl.ActualFilterCriteria; }
        }
    }
}
