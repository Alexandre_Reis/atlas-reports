using System;
using System.Globalization;
using System.Windows.Data;

namespace Atlas.Link.AtlasCommon.View
{
    /// <summary>
    /// 
    /// </summary>
    public class TextSingleLineConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = value as string;
            return text == null ? null : text.Replace(Environment.NewLine, " ");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
