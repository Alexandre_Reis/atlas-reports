using System;
using System.Globalization;
using System.Windows.Data;

namespace Atlas.Link.AtlasCommon.View
{
    public class DecimalToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal? d = value as decimal?;
            return d == null ? null : (double?) d.Value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
