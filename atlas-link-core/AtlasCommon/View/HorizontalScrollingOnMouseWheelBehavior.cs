using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using DevExpress.Mvvm.UI.Interactivity;
using DevExpress.Xpf.Core.Native;

namespace Atlas.Link.AtlasCommon.View
{
    public class HorizontalScrollingOnMouseWheelBehavior : Behavior<ListView>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseWheel += View_PreviewMouseWheel;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.PreviewMouseWheel -= View_PreviewMouseWheel;
        }

        private void View_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var scrollBar = (ScrollBar) LayoutHelper.FindElementByName(AssociatedObject, "PART_HorizontalScrollBar");
            if (e.Delta > 0)
                ScrollBar.LineLeftCommand.Execute(null, scrollBar);
            else if (e.Delta < 0)
            {
                ScrollBar.LineRightCommand.Execute(null, scrollBar);
            }
        }
    }
}
