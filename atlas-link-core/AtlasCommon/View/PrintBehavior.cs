using System;
using System.Printing;
using System.Windows;
using DevExpress.Mvvm.UI.Interactivity;
using DevExpress.Xpf.Printing;
using DevExpress.XtraReports;
using DevExpress.XtraReports.Native;
using DevExpress.XtraReports.UI;

namespace Atlas.Link.AtlasCommon.View
{
    /// <summary>
    /// 
    /// </summary>
    public class PrintBehavior : Behavior<DocumentViewer>
    {
        /// <summary>
        /// Called when [attached].
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Model = XtraReportPreviewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PrintBehavior"/> class.
        /// </summary>
        public PrintBehavior()
        {
            CreateReportCore(delegate(IReport report) { XtraReportPreviewModel = CreateReportPreviewModel(report); });
        }

        /// <summary>
        /// Updates the report.
        /// </summary>
        public void UpdateReport()
        {
            CreateReportCore(delegate(IReport report) { XtraReportPreviewModel.Report = report; });
            if (Report == null)
            {
                AssociatedObject.Model = null;
                return;
            }
            if (AssociatedObject != null && AssociatedObject.Model == null)
                AssociatedObject.Model = XtraReportPreviewModel;
            XtraReportPreviewModel.Report.PrintingSystemBase.ClearContent();
            XtraReportPreviewModel.Report.CreateDocument(true);
        }

        private void CreateReportCore(Action<IReport> assignReport)
        {
            IReport report = CreateReport();
            assignReport(report);
        }

        private void ResetDataAdapters(DevExpress.XtraReports.UI.XtraReportBase report)
        {
            if (report == null)
                return;
            NestedComponentEnumerator enumerator = new NestedComponentEnumerator(new object[] {report});
            while (enumerator.MoveNext())
            {
                IDataContainer dataContainer = enumerator.Current as IDataContainer;
                if (dataContainer != null)
                    dataContainer.DataAdapter = null;
                XRSubreport subreport = enumerator.Current as XRSubreport;
                if (subreport != null)
                    ResetDataAdapters(subreport.ReportSource as XtraReportBase);
            }
        }

        protected IReport CreateReport()
        {
            return Report;
        }

        public IReport Report
        {
            get { return (IReport) GetValue(ReportProperty); }
            set { SetValue(ReportProperty, value); }
        }

        /// <summary>
        /// The report property
        /// </summary>
        public static readonly DependencyProperty ReportProperty = DependencyProperty.Register("Report", typeof (IReport), typeof (PrintBehavior), new PropertyMetadata(null, (d, e) => ((PrintBehavior) d).UpdateReport()));

        /// <summary>
        /// Gets or sets the xtra report preview model.
        /// </summary>
        /// <value>
        /// The xtra report preview model.
        /// </value>
        public XtraReportPreviewModel XtraReportPreviewModel
        {
            get { return (XtraReportPreviewModel) GetValue(XtraReportPreviewModelProperty); }
            set { SetValue(XtraReportPreviewModelProperty, value); }
        }

        /// <summary>
        /// The xtra report preview model property
        /// </summary>
        public static readonly DependencyProperty XtraReportPreviewModelProperty = DependencyProperty.Register("XtraReportPreviewModel", typeof (XtraReportPreviewModel), typeof (PrintBehavior), new PropertyMetadata(null, (d, e) => ((PrintBehavior) d).XtraReportPreviewModelPropertyChanged(e)));

        private void XtraReportPreviewModelPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null && e.NewValue == null)
                XtraReportPreviewModel = (XtraReportPreviewModel) e.OldValue;
        }

        /// <summary>
        /// Creates the report preview model.
        /// </summary>
        /// <param name="report">The report.</param>
        /// <returns></returns>
        protected virtual XtraReportPreviewModel CreateReportPreviewModel(IReport report)
        {
            return new PrintDirectXtraReportPreviewModel1(report) {ZoomMode = new ZoomFitModeItem(ZoomFitMode.PageWidth)};
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PrintDirectXtraReportPreviewModel1 : XtraReportPreviewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintDirectXtraReportPreviewModel1"/> class.
        /// </summary>
        /// <param name="report">An object implementing the <see cref="T:DevExpress.XtraReports.IReport" /> interface (typically, the <see cref="T:DevExpress.XtraReports.UI.XtraReport" /> class instance).</param>
        public PrintDirectXtraReportPreviewModel1(IReport report) : base(report)
        {
        }

        /// <summary>
        /// Prints the direct.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        protected override void PrintDirect(object parameter)
        {
            if (parameter is string)
                PrintDirect((string) parameter);
            else if (parameter is PrintQueue)
                PrintDirect((PrintQueue) parameter);
            else
                PrintDirect();
        }
    }
}
