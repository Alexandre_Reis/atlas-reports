using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Atlas.Link.AtlasCommon.View
{
    public class ColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color? color = value as Color?;
            return color == null ? null : new SolidColorBrush(color.Value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = value as SolidColorBrush;
            return brush == null ? Colors.Transparent : brush.Color;
        }
    }
}
