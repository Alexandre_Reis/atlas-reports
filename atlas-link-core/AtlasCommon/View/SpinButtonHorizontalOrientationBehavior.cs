using System.Windows;
using DevExpress.Mvvm.UI.Interactivity;
using DevExpress.Xpf.Editors;

namespace Atlas.Link.AtlasCommon.View
{
    /// <summary>
    /// 
    /// </summary>
    public class SpinButtonHorizontalOrientationBehavior : Behavior<SpinEdit>
    {
        /// <summary>
        /// Called when [attached].
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Loaded += SetSpinStyle;
        }

        /// <summary>
        /// Called when [detaching].
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Loaded -= SetSpinStyle;
        }

        private void SetSpinStyle(object sender, RoutedEventArgs e)
        {
            ((SpinButtonInfo) AssociatedObject.ActualButtons[0]).SpinStyle = SpinStyle.Horizontal;
        }
    }
}
