using System;

namespace Atlas.Link.AtlasCommon.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentCreateOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentCreateOptions"/> class.
        /// </summary>
        /// <param name="documentType">Type of the document.</param>
        /// <param name="viewModel">The view model.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="parentViewModel">The parent view model.</param>
        public DocumentCreateOptions(string documentType, object viewModel, Func<object> parameter, object parentViewModel)
        {
            DocumentType = documentType;
            ViewModel = viewModel;
            Parameter = parameter;
            ParentViewModel = parentViewModel;
        }

        /// <summary>
        /// Gets or sets the type of the document.
        /// </summary>
        /// <value>
        /// The type of the document.
        /// </value>
        public string DocumentType { get; set; }
        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        /// <value>
        /// The view model.
        /// </value>
        public object ViewModel { get; set; }
        /// <summary>
        /// Gets or sets the parent view model.
        /// </summary>
        /// <value>
        /// The parent view model.
        /// </value>
        public object ParentViewModel { get; set; }
        /// <summary>
        /// Gets or sets the parameter.
        /// </summary>
        /// <value>
        /// The parameter.
        /// </value>
        public Func<object> Parameter { get; set; }
    }
}
