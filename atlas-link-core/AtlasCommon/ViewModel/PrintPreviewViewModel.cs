using System.Collections.Generic;
using System.Linq;
using System.Printing;

namespace Atlas.Link.AtlasCommon.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PrintPreviewViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintPreviewViewModel"/> class.
        /// </summary>
        public PrintPreviewViewModel()
        {
            LocalPrintServer printServer = new LocalPrintServer();
            Printers = new List<PrinterViewModel>();
            foreach (var item in printServer.GetPrintQueues())
            {
                var printer = new PrinterViewModel();
                printer.Name = item.Name;
                if (!item.IsBusy)
                    printer.Status = "Ready";
                if (item.Name.ToLower().Contains("fax"))
                    printer.ImageUri = "pack://application:,,,/DevExpress.HybridApp.Wpf;component/Images/PrintItems/Preview/Fax_32x32.png";
                else if (item.Name.ToLower().Contains("note"))
                    printer.ImageUri = "pack://application:,,,/DevExpress.HybridApp.Wpf;component/Images/PrintItems/Preview/DefaultPrinter_32x32.png";
                else
                    printer.ImageUri = "pack://application:,,,/DevExpress.HybridApp.Wpf;component/Images/PrintItems/Preview/Printer_32x32.png";
                Printers.Add(printer);
            }
            SelectedPrinter = Printers.FirstOrDefault();
        }

        /// <summary>
        /// Gets or sets the selected printer.
        /// </summary>
        /// <value>
        /// The selected printer.
        /// </value>
        public PrinterViewModel SelectedPrinter { get; set; }
        /// <summary>
        /// Gets or sets the printers.
        /// </summary>
        /// <value>
        /// The printers.
        /// </value>
        public List<PrinterViewModel> Printers { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PrinterViewModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the image URI.
        /// </summary>
        /// <value>
        /// The image URI.
        /// </value>
        public string ImageUri { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }
    }
}
