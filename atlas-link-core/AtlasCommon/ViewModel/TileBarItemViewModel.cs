using System.Windows.Input;

namespace Atlas.Link.AtlasCommon.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class TileBarItemViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TileBarItemViewModel"/> class.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="imagePath">The image path.</param>
        /// <param name="count">The count.</param>
        /// <param name="command">The command.</param>
        /// <param name="parameter">The parameter.</param>
        public TileBarItemViewModel(string caption, string imagePath, int count, ICommand command, object parameter)
        {
            Caption = caption;
            ImagePath = imagePath;
            Count = count;
            Command = command;
            Parameter = parameter;
        }

        internal void SetCount(int value)
        {
            Count = value;
        }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption { get; set; }
        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public ICommand Command { get; set; }
        /// <summary>
        /// Gets or sets the parameter.
        /// </summary>
        /// <value>
        /// The parameter.
        /// </value>
        public object Parameter { get; set; }
        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count { get; set; }
        /// <summary>
        /// Gets or sets the image path.
        /// </summary>
        /// <value>
        /// The image path.
        /// </value>
        public string ImagePath { get; set; }

    }
}
