﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Security
{
    /// <summary>
    /// Manages authentication providers for the application
    /// </summary>
    public static class AuthenticationManager
    {
        /// <summary>
        /// Gets or sets the application authentication provider.
        /// </summary>
        /// <value>
        /// The provider.
        /// </value>
        public static IAuthenticationProvider Provider
        {
            get
            {
                if (_provider == null)
                    _provider = ServiceLocator.Get<IAuthenticationProvider>();

                return _provider;
            }
            set
            {
                if (_provider == null)
                    _provider = value;
            }
        }
        private static IAuthenticationProvider _provider;
    }
}