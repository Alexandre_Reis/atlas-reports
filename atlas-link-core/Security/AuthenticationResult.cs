﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Security
{
    /// <summary>
    /// Status of Authentication
    /// </summary>
    public enum AuthenticationStatus
    {
        Success,
        NetworkError,
        InvalidPassword,
        AccountDisabled,
        PasswordExpired,
    }

    // SUCCESS, ERROR, INVALID_PASSWORD, ACCOUNT_DISABLED, PASSWORD_EXPIRED

    /// <summary>
    /// Result of Authentication
    /// </summary>
    public class AuthenticationResult
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationResult" /> class.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="message">The message.</param>
        public AuthenticationResult(AuthenticationStatus status, String message)
        {
            this.Status = status;
            this.Message = message;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public AuthenticationStatus Status { get; private set; }
        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public String Message { get; private set; }

        #endregion
    }
}