﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Common.Logging;

namespace Atlas.Link.Security
{
    /// <summary>
    /// The Atlas security principal.
    /// </summary>
    public class Principal : IAtlasPrincipal, IIdentity
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="Principal"/> class.
        /// </summary>
        public Principal()
        {
            _groups = new Dictionary<String, String>();
            _permissions = new Dictionary<String, String>();

            AuthenticationType = "Custom";
            IsAuthenticated = false;

            Name = "";
            Login = "";
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Principal"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="login">The login.</param>
        /// <param name="groups">The groups.</param>
        /// <param name="permissions">The permissions.</param>
        public Principal(String name, String login, IEnumerable<string> groups, IEnumerable<string> permissions)
            : this()
        {
            IsAuthenticated = true;
            Name = name;
            Login = login;
            _logger = LogManager.Adapter.GetLogger(GetType());

            if (groups != null)
            {
                foreach (String grp in groups)
                    _groups.Add(grp.ToUpper(), grp);
            }

            if (permissions != null)
            {
                foreach (String prm in permissions)

                    if(_permissions.ContainsKey(prm))
                        _logger.Error(string.Format("the user {0} already have the {1} permission",login,prm));
                    else
                        _permissions.Add(prm.ToUpper(), prm);
            }
        }

        #endregion

        #region Fields
        private ILog _logger = null;
        private Dictionary<String, String> _groups;
        private Dictionary<String, String> _permissions;

        #endregion

        #region IAtlasPrincipal Members

        /// <summary>
        /// Gets the type of the authentication.
        /// </summary>
        /// <value>
        /// The type of the authentication.
        /// </value>
        public String AuthenticationType { get; private set; }

        /// <summary>
        /// Determines whether the principal is authenticated.
        /// </summary>
        /// <value>
        /// The is authenticated.
        /// </value>
        public bool IsAuthenticated { get; private set; }

        /// <summary>
        /// Gets the name of the principal
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the login of the principal
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        public String Login { get; private set; }

        /// <summary>
        /// Determines whether the current principal has a permission.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        public Boolean HasPermission(String code)
        {
            return _permissions.ContainsKey(code.ToUpper());
        }

        /// <summary>
        /// Determines whether the current principal has a set of permissions.
        /// </summary>
        /// <param name="codes">The codes.</param>
        /// <returns></returns>
        public Boolean HasPermissions(IEnumerable<String> codes)
        {
            return codes.All(HasPermission);
        }

        #endregion

        #region IPrincipal Members

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>The <see cref="T:System.Security.Principal.IIdentity" /> object associated with the current principal.</returns>
        public IIdentity Identity
        {
            get { return this; }
        }
        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <param name="role">The name of the role for which to check membership.</param>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        public Boolean IsInRole(string role)
        {
            return _groups.ContainsKey(role.ToUpper());
        }

        #endregion
    }
}