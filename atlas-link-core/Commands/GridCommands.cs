﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Atlas.Link.Commands
{
    public class GridCommands
    {
        #region Instance Fields

        public static readonly RoutedCommand CopyCellToClipboard = new RoutedCommand("CopyCellToClipboard", typeof(GridCommands));
        public static readonly RoutedCommand PrintGrid = new RoutedCommand("PrintGrid", typeof(GridCommands));
        public static readonly RoutedCommand ExportGrid = new RoutedCommand("ExportGrid", typeof(GridCommands));

        #endregion
    }
}