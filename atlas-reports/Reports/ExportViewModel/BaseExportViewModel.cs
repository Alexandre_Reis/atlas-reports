﻿using System.Collections.Generic;
using System.Data;

namespace Atlas.Reports.ExportViewModel
{
    public class BaseExportViewModel
    {

        #region Fields

        private List<DataRow> _dataRows;
        private DataTable _dataTable;

        #endregion

        #region constructor

        public BaseExportViewModel(List<DataRow> dataRows)
        {
            _dataRows = dataRows;
        }

        public BaseExportViewModel(DataTable dataTable)
        {
            _dataTable = dataTable;
        }
        #endregion

        #region properties

        public List<DataRow> DataRows
        {
            get { return _dataRows; }
        }

        public DataTable DataTable
        {
            get { return _dataTable; }
        }

        #endregion
    }
}
