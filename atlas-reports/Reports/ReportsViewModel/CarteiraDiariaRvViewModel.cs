﻿using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaRvViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totRv;

        #endregion

        #region constructor

        public CarteiraDiariaRvViewModel(DataRow modelentity, double totRv)
            : base(modelEntity: modelentity)
        {
            _totRv = totRv;
        }

        #endregion

        #region properties


        public string CodPap
        {
            get { return ModelEntity["CODPAP"].ToString(); }
        }
		public string CadPapNome
        {
            get { return ModelEntity["RV_CADPAP_NOME"].ToString(); }
        }
		public double QtDisponivel
        {
            get { return (double) ModelEntity["QT_DISPONIVEL"]; }
        }
		public double QtdeBloqueada
        {
            get { return (double) ModelEntity["QTDE_BLOQUEADA"]; }
        }
		public double VlMerc
        {
            get { return (double) ModelEntity["SAC_CL_PATRRV_VL_MERC"]; }
        }
		public double VlTotRv
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRV"]; }
        }
		public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }
		public string TipoEmp
        {
            get { return ModelEntity["TIPOEMP"].ToString(); }
        }
		public double VlLote
        {
            get { return (double) ModelEntity["VL_LOTE"]; }
        }
		public double QtTotal
        {
            get { return (double) ModelEntity["QT_TOTAL"]; }
        }
		public double VlCusto
        {
            get { return (double) ModelEntity["SAC_CL_PATRRV_VL_CUSTO"]; }
        }
		public double VlCustoFisc
        {
            get { return (double) ModelEntity["SAC_CL_PATRRV_VL_CUSTO_FISC"]; }
        }
		public string CadPapTipo
        {
            get { return ModelEntity["RV_CADPAP_TIPO"].ToString(); }
        }
		public double VlRvPatrVlCotacao
        {
            get { return (double)ModelEntity["VL_COTACAO"]; }
        }
		public double VlMercLiquido
        {
            get { return (double) ModelEntity["VL_MERC_LIQUIDO"]; }
        }
		public double VlIrrf
        {
            get { return (double) ModelEntity["VL_IRRF"]; }
        }
		public string CliBasTipo
        {
            get { return ModelEntity["CLI_BAS_TIPO"].ToString(); }
        }
		public string CdBolsa
        {
            get { return ModelEntity["CD_BOLSA"].ToString(); }
        }
		public string DsBolsa
        {
            get { return ModelEntity["DS_BOLSA"].ToString(); }
        }

        public double VlCustoMedioSemcorretagem
        {
            get { return VlCusto; }
        }

        public double vlCotacao
        {
            get
            {
                if (VlRvPatrVlCotacao == 0)
                {
                    if (QtTotal != 0)
                        return VlLote*VlMerc/QtTotal;
                }

                return VlRvPatrVlCotacao;
            }
        }

        public double VlCustoTotal
        {
            get { return VlCusto; }
        }

        public double VlLxP
        {
            get { return VlMerc - VlCustoTotal; }
        }

        public double PcSobreRv
        {
            get
            {
                if (_totRv != 0)
                    return VlMerc/_totRv;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlMerc / VlPatrLiqTot;

                return 0;
            }
        }

        public string TipoEmpGrupo
        {
            get
            {
                return TipoEmp == "P" ? "Empresas Privadas" : "Empresas Públicas";

            }
        }

        public string GrupoBolsa
        {

            get
            {
                return string.Format("{0} - {1}", CdBolsa.Trim(), DsBolsa.Trim());
            }
        }

        #endregion
    }
}