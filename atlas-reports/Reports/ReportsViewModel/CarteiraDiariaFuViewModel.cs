﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaFuViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totFu;

        #endregion

        #region constructor

        public CarteiraDiariaFuViewModel(DataRow modelentity, double totFu)
            : base(modelEntity: modelentity)
        {
            _totFu = totFu;
        }

        #endregion

        #region properties


        
		public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }

		public string FuAtvCd
        {
            get { return ModelEntity["FUATV_CD"].ToString(); }
        }
		public double vltotFut
        {
            get { return (double) ModelEntity["VL_TOTFUT"]; }
        }

		public string FuVctCd
        {
            get { return ModelEntity["FUVCT_CD"].ToString(); }
        }
		public string RvcorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }
		public double Qt
        {
            get { return (double) ModelEntity["QT"]; }
        }

		public double VlEqualizacao
        {
            get { return (double) ModelEntity["VL_EQUALIZACAO"]; }
        }

		public double VlValorizacao
        {
            get { return (double) ModelEntity["VL_VALORIZACAO"]; }
        }

		public double VlPrecoMerc
        {
            get { return (double) ModelEntity["VL_PRECOMERC"]; }
        }

		public double VlVarPonto
        {
            get { return (double)ModelEntity["VL_VARPONTO"]; }
        }


		public string Ic
        {
            get { return ModelEntity["IC"].ToString(); }
        }
		public string CdBolsa
        {
            get { return ModelEntity["CD_BOLSA"].ToString(); }
        }
		public string DsBolsa
        {
            get { return ModelEntity["DS_BOLSA"].ToString(); }
        }
		public string NmAtivoIngles
        {
            get { return ModelEntity["NM_ATIVO_INGLES"].ToString(); }
        }

        public double VlPrecoMercado
        {
            get { return VlPrecoMerc/Qt/VlVarPonto; }
        }

        public double PcSobreFu
        {
            get
            {
                if (_totFu != 0)
                    return VlPrecoMerc/_totFu;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlPrecoMerc / VlPatrLiqTot;

                return 0;
            }
        }

        
        public string GrupoBolsa
        {

            get
            {
                return string.Format("{0} - {1}", CdBolsa.Trim(), DsBolsa.Trim());
            }
        }

        #endregion
    }
}