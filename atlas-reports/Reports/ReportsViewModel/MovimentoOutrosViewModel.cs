﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class MovimentoOutrosViewModel : BaseReportViewModel<MovimentoOutrosModel>
    {
        #region constructor

        public MovimentoOutrosViewModel(MovimentoOutrosModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties


        public string Fundo
        {
            get { return ModelEntity.Fundo; }
        }

        public DateTime? Data
        {
            get { return ModelEntity.Data; }
        }

        public DateTime? DataAplicacao
        {
            get { return ModelEntity.DataAplicacao; }
            
        }

        public string IcAplicResg
        {
            get { return ModelEntity.IcAplicacaoResgate; }
        }

        public string IcTransf
        {
            get { return string.IsNullOrEmpty(ModelEntity.IcTransferencia) ? "N" : ModelEntity.IcTransferencia; }
        }

        public string Tipo
        {
            get
            {
                switch (IcAplicResg + IcTransf)
                {
                    case "AN":
                        return "Aplicação";
                    case "RN":
                        return "Resgate";
                    case "AS":
                        return "Depósito";
                    case "RS":
                        return "Retirada";
                    case "NN":
                        return "IRRF Ref. Lei 10.892/04";
                    default:
                        return IcAplicResg;

                }

            }
        }


        public DateTime? LiquidacaoFisica
        {
            get { return ModelEntity.LiquidacaoFisica; }
        }

        public DateTime? LiquidacaoFinanceira
        {
            get { return ModelEntity.LiquidacaoFinanceira; }
        }

        public string IcLiquidezDiaria
        {
            get { return ModelEntity.IcLiquidezDiaria; }
        }

        public double Vl
        {
            get { return (ModelEntity.Vl ?? 0); }
        }

        public double Valor
        {
            get
            {
                if (IcLiquidezDiaria == "S")
                    return Vl + Irrf + ( SgIofResgate == "V" ? 0 : Iof) + Emolumentos+ Corretagem;

                if(IcAplicResg== "A")
                    return Vl + Emolumentos+ Corretagem;

                return Vl;
                
            }
        }


        public double Corretagem
        {
            get { return (ModelEntity.VlCor ?? 0); }
        }

        public double Emolumentos
        {
            get { return (ModelEntity.VlEmol ?? 0); }
        }

        public string SgIofResgate
        {
            get { return ModelEntity.SgIofResgate; }
        }

        public double Iof
        {
            get
            {
                if (SgIofResgate == "V" || IcTransf == "S")
                    return 0;

                return (ModelEntity.VlIof ?? 0);

            }
        }


        public double Irrf
        {
            get { return ModelEntity.VlIrrf ?? 0; }
        }


        public double ValorLiquido
        {
            get
            {
                if (IcAplicResg == "N")
                    return 0;

                if (IcLiquidezDiaria == "S" || IcAplicResg=="A")
                    return Vl;

                return Vl - Irrf - Iof - Emolumentos - Corretagem;
                
            }
        }
        

        public double Quantidade
        {
            get { return ModelEntity.QtCotas ?? 0; }
        }

        #region calculate fields

        public double ValorAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Valor : 0; }

        }

        public double ValorResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Valor : 0; }
        }

        public double ValorDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Valor : 0; }
        }

        public double ValorRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Valor : 0; }
        }


        public double CorretagemAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Corretagem : 0; }

        }

        public double CorretagemResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Corretagem : 0; }
        }

        public double CorretagemDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Corretagem : 0; }
        }

        public double CorretagemRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Corretagem : 0; }
        }


        public double EmolumentosAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Emolumentos : 0; }

        }

        public double EmolumentosResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Emolumentos : 0; }
        }

        public double EmolumentosDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Emolumentos : 0; }
        }

        public double EmolumentosRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Emolumentos : 0; }
        }


        public double IofAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Iof : 0; }

        }

        public double IofResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Iof : 0; }
        }

        public double IofDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Iof : 0; }
        }

        public double IofRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Iof : 0; }
        }


        public double IrrfAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Irrf : 0; }

        }

        public double IrrfResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Irrf : 0; }
        }

        public double IrrfDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Irrf : 0; }
        }

        public double IrrfRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Irrf : 0; }
        }


        public double ValorLiquidoAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? ValorLiquido : 0; }

        }

        public double ValorLiquidoResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? ValorLiquido : 0; }
        }

        public double ValorLiquidoDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? ValorLiquido : 0; }
        }

        public double ValorLiquidoRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? ValorLiquido : 0; }
        }

        public double QuantidadeAplicacoes
        {
            get { return IcAplicResg == "A" && IcTransf == "N" ? Quantidade : 0; }

        }

        public double QuantidadeResgates
        {
            get { return IcAplicResg != "A" && IcTransf == "N" ? Quantidade : 0; }
        }

        public double QuantidadeDeposito
        {
            get { return IcAplicResg == "A" && IcTransf == "S" ? Quantidade : 0; }
        }

        public double QuantidadeRetirada
        {
            get { return IcAplicResg != "A" && IcTransf == "S" ? Quantidade : 0; }
        }

        #endregion

        #endregion
    }
}
