﻿using System;
using System.Data;
using System.ServiceModel;
using Atlas.Reports.BaseViewModel;
using DevExpress.XtraRichEdit.Layout;

namespace Atlas.Reports.ReportsViewModel
{
    public class PosicaoSwapViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public PosicaoSwapViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties

        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public string ClCliCd
        {
            get { return ModelEntity["CLCLI_CD"].ToString(); }
        }

        public string SwCadCd
        {
            get { return ModelEntity["SWCAD_CD"].ToString(); }
        }

        public double SwPosQt
        {
            get
            {
                return (double)ModelEntity["QT"]; 
            }
        }


        public double VlPassivo
        {
            get { return (double) ModelEntity["VL_PASSIVO"]; }
        }

        public double VlAtivo
        {
            get { return (double) ModelEntity["VL_ATIVO"]; }
        }

        public double VlFloor
        {
            get { return (double) ModelEntity["VL_FLOOR"]; }
        }

        public double VlApropBruto
        {
            get { return (double) ModelEntity["VL_APROP_BRUTO"]; }
        }

        public double VlApropIrrf
        {
            get { return (double) ModelEntity["VL_APROP_IRRF"]; }
        }

        public double VlApropLiq
        {
            get { return (double) ModelEntity["VL_APROP_LIQ"]; }
        }

        public double PuPassivo
        {
            get { return (double) ModelEntity["PU_PASSIVO"]; }
        }

        public double PuAtivo
        {
            get { return (double) ModelEntity["PU_ATIVO"]; }
        }

        public DateTime? DtVencimento
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO"]; }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public DateTime? DtPartida
        {
            get { return (DateTime?) ModelEntity["DT_PARTIDA"]; }
        }

        public double PcTaxaPassivo 
        {
            get { return (double) ModelEntity["PC_TAXA_PASSIVO"]; }
        }

        public string IddirCdPassivo
        {
            get { return ModelEntity["IDDIR_CD_PASSIVO"].ToString(); }
        }

        public string IcJurosPassivo
        {
            get { return ModelEntity["IC_JUROS_PASSIVO"].ToString(); }
        }

        public string IcApropPassivo
        {
            get { return ModelEntity["IC_APROP_PASSIVO"].ToString(); }
        }

        public string IcDiasPassivo
        {
            get { return ModelEntity["IC_DIAS_PASSIVO"].ToString(); }
        }

        public string IcIndicePassivo
        {
            get { return ModelEntity["IC_INDICE_PASSIVO"].ToString(); }
        }

        public double PcTaxaAtivo
        {
            get { return (double) ModelEntity["PC_TAXA_ATIVO"]; }
        }

        public string IddirCdAtivo
        {
            get { return ModelEntity["IDDIR_CD_ATIVO"].ToString(); }
        }

        public string IcJurosAtivo
        {
            get { return ModelEntity["IC_JUROS_ATIVO"].ToString(); }
        }

        public string IcApropAtivo
        {
            get { return ModelEntity["IC_APROP_ATIVO"].ToString(); }
        }

        public string IcDiasAtivo
        {
            get { return ModelEntity["IC_DIAS_ATIVO"].ToString(); }
        }

        public string IcIndiceAtivo
        {
            get { return ModelEntity["IC_INDICE_ATIVO"].ToString(); }
        }

        public string IcGarantia
        {
            get { return ModelEntity["IC_GARANTIA"].ToString(); }
        }

        public string RvCorCdContra
        {
            get { return ModelEntity["RVCOR_CD_CONTRA"].ToString(); }
        }

        public double PcFloor
        {
            get { return (double) ModelEntity["PC_FLOOR"]; }
        }

        public string IddirCdFloor
        {
            get { return ModelEntity["IDDIR_CD_FLOOR"].ToString(); }
        }

        public string IcJjurosFloor
        {
            get { return ModelEntity["IC_JUROS_FLOOR"].ToString(); }
        }

        public string IcApropFloor
        {
            get { return ModelEntity["IC_APROP_FLOOR"].ToString(); }
        }

        public string IcDiasFloor
        {
            get { return ModelEntity["IC_DIAS_FLOOR"].ToString(); }
        }

        public string IcIndiceFloor
        {
            get { return ModelEntity["IC_INDICE_FLOOR"].ToString(); }
        }

        public double VlApropAnt
        {
            get { return (double) ModelEntity["APROP_ANT"]; }
        }

        public double PuAtivoant
        {
            get { return (double) ModelEntity["PUA_ANT"]; }
        }

        public double PuPassivoAnt
        {
            get { return (double) ModelEntity["PUP_ANT"]; }
        }

        public string NomeInst
        {
            get { return ModelEntity["NOMEINST"].ToString(); }
        }

        public DateTime? DtIniValorizacao
        {
            get { return (DateTime?) ModelEntity["DT_INI_VALORIZACAO"]; }
        }

        public string CdLocalRegistro
        {
            get { return ModelEntity["CD_LOCAL_REGISTRO"].ToString(); }
        }

        public double VlTaxaPassivo
        {
            get { return (double) ModelEntity["VL_TAXA_PASSIVO"]; }
        }

        public double VlTaxaAtivo
        {
            get { return (double) ModelEntity["VL_TAXA_ATIVO"]; }
        }

        public double IddirPcAtivo
        {
            get { return (double)ModelEntity["IDDIR_PC_ATIVO"]; }
        }

        public double IddirPcPassivo
        {
            get { return (double)ModelEntity["IDDIR_PC_PASSIVO"]; }
        }

        public string IddirPcFloor
        {
            get { return ModelEntity["IDDIR_PC_FLOOR"].ToString(); }
        }

        public string IcTaxaCdiAtivo
        {
            get { return ModelEntity["IC_TAXA_CDI_ATIVO"].ToString(); }
        }

        public string IcTaxaCdiPassivo
        {
            get { return ModelEntity["IC_TAXA_CDI_PASSIVO"].ToString(); }
        }

        public string IcTaxaCdiFloor
        {
            get { return ModelEntity["IC_TAXA_CDI_FLOOR"].ToString(); }
        }

        public double VlApropriacaoDiaria
        {
            get { return ((PuAtivo - PuPassivo) - (PuAtivoant - PuPassivoAnt))*VlApropAnt; }
        }

        public double VlAjustePositivo
        {
            get
            {
                return VlApropBruto >= 0 ? VlApropBruto : 0;
            }
        }

        public double VlAjusteNegativo
        {
            get { return VlApropBruto >= 0 ? 0 : VlApropBruto; }
        }

        public string Garantia
        {
            get { return IcGarantia == "S" ? "*" : ""; }
        }

        public string JadPassivo
        {
            get { return string.Format("{0} {1} {2}", IcJurosPassivo, IcApropPassivo, IcDiasPassivo); }
        }

        public string JadAtivo
        {
            get { return string.Format("{0} {1} {2}", IcJurosAtivo, IcApropAtivo, IcDiasAtivo); }
        }

        public string IndexadorPassivo
        {
            get { return string.Format("{0} {1}", IddirCdPassivo, IddirPcPassivo != 100 && IcTaxaCdiPassivo != null ? string.Format("{0} {1}", IcTaxaCdiPassivo, IddirPcPassivo) : ""); }
        }


        public string IndexadorAtivo
        {
            get { return string.Format("{0} {1}", IddirCdAtivo, IddirPcAtivo != 100 && IcTaxaCdiAtivo != null ? string.Format("{0} {1}", IcTaxaCdiAtivo, IddirPcAtivo) : ""); }
        }

        public string IcGarantiaDescricao
        {
            get
            {
                switch (IcGarantia)
                {
                case "S":
                    return "Total de Contratos Com Garantia da Parte";
                    
                case "N":
                    return "Total de Contratos sem Garantia";
                    
                case "A":
                    return "Total de Contratos com Garantia  em Ambas as Partes";

                }
                return "Total de Contratos com Garantia  em Contraparte";
            }
        }

        #endregion
    }
}