﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Windows.Media.Animation;
using Atlas.Link.Localization;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.EntityModel;
using Atlas.Reports.ViewModels;
using DevExpress.Xpo.Helpers;
using DevExpress.XtraRichEdit.Layout;

namespace Atlas.Reports.ReportsViewModel
{
    public class RentabPatrimonialViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region Fields

        private double _vlEntrada;
        private double _vlSaidas;

        #endregion

        #region constructor

        public RentabPatrimonialViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {
            CalculateEntradaSaida();
        }

        #endregion

        #region properties


        public DateTime Dt
        {
            get { return (DateTime) ModelEntity["DT"]; }
        }

        public double VlCotasEmit
        {
            get { return ToDouble(ModelEntity["VL_COTAS_EMIT"]); }
        }

        public double VlCotasResg
        {
            get { return ToDouble(ModelEntity["VL_COTAS_RESG"]); }
        }

        public double VlPatriLiqtot
        {
            get { return ToDouble(ModelEntity["VL_PATRLIQTOT1"]); }
        }

        public double QtCotas
        {
            get { return ToDouble(ModelEntity["QT_COTAS1"]); }
        }

        public double VlCota
        {
            get { return ToDouble(ModelEntity["VL_COTA1"]); }
        }

        public double VlTotOutros
        {
            get { return ToDouble(ModelEntity["VL_TOTOUTROS"]); }
        }

        public double Qtcotas2
        {
            get { return ToDouble(ModelEntity["QT_COTAS2"]); }
        }

        public double VlCotaPfee
        {
            get { return ToDouble(ModelEntity["VL_COTAPFEE1"]); }
        }

        public double VlCotaPfee2
        {
            get { return ToDouble(ModelEntity["VL_COTAPFEE2"]); }
        }

        public double QtCotasPenal
        {
            get { return ToDouble(ModelEntity["QT_COTAS_PENAL"]); }
        }

        public string Moeda
        {
            get { return ModelEntity["MOEDA"].ToString().Trim(); }
        }

        public string Nivel
        {
            get { return ModelEntity["NIVEL"].ToString().Trim(); }
        }

        public double CotaInic
        {
            get { return ToDouble(ModelEntity["COTAINIC"]); }
        }

        public DateTime DataInic
        {
            get { return (DateTime) ModelEntity["DATAINIC"]; }
        }

        public double VlAport
        {
            get { return ToDouble(ModelEntity["VL_APORTE"]); }
        }

        public double VlRetirada
        {
            get { return ToDouble(ModelEntity["VL_RETIRADA"]); }
        }

        public double VlDia
        {
            get { return ToDouble(ModelEntity["PC_DIA_CT"]); }
        }
		public double VlMes
        {
            get { return ToDouble(ModelEntity["PC_MES_CT"]); }
        }
		public double VlAno
        {
            get { return ToDouble(ModelEntity["PC_ANO_CT"]); }
        }

        public double VlTotal
        {
            get { return (Dt >= DataInic && CotaInic > 0 && VlCota != 0) ? (VlCota/CotaInic) - 1 : 0; }
        }


        public double VlEntrada
        {
            get { return _vlEntrada; }
        }

        public double VlSaida
        {
            get { return _vlSaidas; }
        }
        #endregion

        #region Methods

        private void CalculateEntradaSaida()
        {
            using (var db = new SacEntities(DbContextFactory.ConnectString))
            {
                var clients = new List<string>();

                if (Nivel == "2")
                {

                    var clibasList = (from p in db.CLI_CON
                                      where p.CODCLICON.Equals(CodCli)
                                      select p).ToList();

                    clibasList.ForEach(x => clients.Add(x.CONSOLIDADO));

                }

                else
                {
                    clients.Add(CodCli);
                }

                var icInicioVigencia = db.SAC_CL_LIQUID2.Any(x => x.CLCLI_CD.Equals(CodCli) && x.DT_VALIDADE_INICIO == Dt);

                double vlEpf;
                double vlEpj;
                double vlSpf;
                double vlSpj;
                double vlIrpj;
                double vlIrpf;

                if (!icInicioVigencia)
                {
                    var entradas = (from cotas in db.SAC_CL_COTAS
                                  join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                  where (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) == Dt &&
                                        (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) &&
                                        cotas.SG_REGISTRO == "0" &&
                                        cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                        cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                  group cotas by cotas.CLCLI_CD
                                  into g
                                  select new
                                         {
                                             VlEpf = g.Sum(s => s.VL_EPF),
                                             VlEpj = g.Sum(s => s.VL_EPJ)
                                         }).FirstOrDefault();

                    var saidas = (from cotas in db.SAC_CL_COTAS
                                    join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                    where (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) == Dt &&
                                          (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) &&
                                          cotas.SG_REGISTRO == "0" &&
                                          cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                          cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                    group cotas by cotas.CLCLI_CD
                                        into g
                                        select new
                                        {
                                            VlSpf = g.Sum(s => s.VL_BRUTO_SPF),
                                            VlSpj = g.Sum(s => s.VL_BRUTO_SPJ)
                                        }).FirstOrDefault();

                    var resgir = (from cotas in db.SAC_CL_COTAS
                                  join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                  where (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) == Dt &&
                                        (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) &&
                                        cotas.SG_REGISTRO == "0" &&
                                        cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                        cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                  group cotas by cotas.CLCLI_CD
                                      into g
                                      select new
                                      {
                                          Vlirpf = g.Sum(s => s.VL_RESGIR_SPF),
                                          Vlirpj = g.Sum(s => s.VL_RESGIR_SPJ)
                                      }).FirstOrDefault();

                    vlEpf = entradas != null && entradas.VlEpf != null ? (double)entradas.VlEpf : 0;
                    vlEpj = entradas != null && entradas.VlEpj != null ? (double)entradas.VlEpj : 0;

                    vlSpf = saidas != null && saidas.VlSpf != null ? (double)saidas.VlSpf : 0;
                    vlSpj = saidas != null && saidas.VlSpj != null ? (double)saidas.VlSpj : 0;

                    vlIrpf = resgir != null && resgir.Vlirpf != null ? (double)resgir.Vlirpf : 0;
                    vlIrpj = resgir != null && resgir.Vlirpj != null ? (double)resgir.Vlirpj : 0;


                }
                else
                {
                    var entradas = (from cotas in db.SAC_CL_COTAS
                                  join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                  where (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) == Dt &&
                                        (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQECONV == null ? cotas.DT : cotas.DT_LIQECONV) &&
                                        cotas.SG_REGISTRO == "0" &&
                                        liq.DT_VALIDADE_INICIO == Dt &&
                                        cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                        cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                  group cotas by cotas.CLCLI_CD
                                  into g
                                  select new
                                         {
                                             VlEpf = g.Sum(s => s.VL_EPF),
                                             VlEpj = g.Sum(s => s.VL_EPJ)
                                         }).FirstOrDefault();

                    var saidas = (from cotas in db.SAC_CL_COTAS
                                  join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                  where (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) == Dt &&
                                        (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) &&
                                        cotas.SG_REGISTRO == "0" &&
                                        liq.DT_VALIDADE_INICIO == Dt &&
                                        cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                        cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                  group cotas by cotas.CLCLI_CD
                                      into g
                                      select new
                                      {
                                          VlSpf = g.Sum(s => s.VL_BRUTO_SPF),
                                          VlSpj = g.Sum(s => s.VL_BRUTO_SPJ)
                                      }).FirstOrDefault();

                    var resgir = (from cotas in db.SAC_CL_COTAS
                                  join liq in db.SAC_CL_LIQUID2 on cotas.CLCLI_CD equals liq.CLCLI_CD
                                  where (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) == Dt &&
                                        (liq.DT_VALIDADE_FIM == null ? (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) : liq.DT_VALIDADE_FIM) >= (cotas.DT_LIQSCONV == null ? cotas.DT : cotas.DT_LIQSCONV) &&
                                        cotas.SG_REGISTRO == "0" &&
                                        liq.DT_VALIDADE_INICIO == Dt &&
                                        cotas.DT >= liq.DT_VALIDADE_INICIO &&
                                        cotas.DT <= (liq.DT_VALIDADE_FIM == null ? cotas.DT : liq.DT_VALIDADE_FIM) &&
                                        clients.Contains(cotas.CLCLI_CD)
                                  group cotas by cotas.CLCLI_CD
                                      into g
                                      select new
                                      {
                                          Vlirpf = g.Sum(s => s.VL_RESGIR_SPF),
                                          Vlirpj = g.Sum(s => s.VL_RESGIR_SPJ)
                                      }).FirstOrDefault();


                    vlEpf = (entradas != null && entradas.VlEpf != null) ? (double)entradas.VlEpf : 0;
                    vlEpj = (entradas != null && entradas.VlEpj != null) ? (double)entradas.VlEpj : 0;

                    vlSpf = saidas != null && saidas.VlSpf != null ? (double)saidas.VlSpf : 0;
                    vlSpj = saidas != null && saidas.VlSpj != null ? (double)saidas.VlSpj : 0;

                    vlIrpf = resgir != null && resgir.Vlirpf != null ? (double)resgir.Vlirpf : 0;
                    vlIrpj = resgir != null && resgir.Vlirpj != null ? (double)resgir.Vlirpj : 0;
                }

                var estorno = (from e in db.SAC_CL_COTAS_ESTORNO
                               where e.DT == Dt && e.CLCLI_CD == CodCli
                               group e by e.CLCLI_CD
                               into g
                               select new
                                      {
                                          VlRpf = g.Sum(s => s.VL_RPF),
                                          VlRpj = g.Sum(s => s.VL_RPJ),
                                          VlIrpf = g.Sum(s => s.VL_IRRF_RPF),
                                          VlIrpj = g.Sum(s => s.VL_IRRF_RPJ)
                                      }).FirstOrDefault();

                double vlEstorno = ((estorno != null && estorno.VlRpf != null) ? (double) estorno.VlRpf : 0) +
                                   ((estorno != null && estorno.VlRpj != null) ? (double)estorno.VlRpj : 0) +
                                   ((estorno != null && estorno.VlIrpf != null) ? (double)estorno.VlIrpf : 0) +
                                   ((estorno != null && estorno.VlIrpj != null) ? (double)estorno.VlIrpj : 0);


                var estornoSaidas = (from e in db.SAC_CL_COTAS_ESTORNO
                               where e.DT == Dt && e.CLCLI_CD == CodCli
                               group e by e.CLCLI_CD
                                   into g
                                   select new
                                   {
                                       Vlapf = g.Sum(s => s.VL_APF),
                                       Vlapj = g.Sum(s => s.VL_APJ)
                                   }).FirstOrDefault();

                double vlEstornoSaidas = ((estornoSaidas != null && estornoSaidas.Vlapf != null) ? (double)estornoSaidas.Vlapf : 0) +
                                         ((estornoSaidas != null && estornoSaidas.Vlapj != null) ? (double)estornoSaidas.Vlapj : 0);

                _vlEntrada = vlEpf + vlEpj;
                _vlSaidas = vlSpf + vlSpj + vlIrpf + vlIrpj;

            }

        }


        #endregion
    }
}