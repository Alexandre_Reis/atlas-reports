﻿using System;
using System.Linq;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class OperacoesRendaFixaViewModel : BaseReportViewModel<OperacoesRendaFixaModel>
    {
        #region constructor

        public OperacoesRendaFixaViewModel(OperacoesRendaFixaModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties

        public DateTime? DtAquisicao
        {
            get { return ModelEntity.DtAquisicao; }
        }

        public DateTime? Data
        {
            get { return ModelEntity.Data; }
        }

        public string CodOperacao
        {
            get { return ModelEntity.CodOperacao; }
        }

        public string TipoOperacao
        {
            get
            {
                switch (ModelEntity.SgOperacao)
                {
                case "C":
                    if (ModelEntity.IdRegVendaTermo > 0 &&
                        (string.IsNullOrEmpty(ModelEntity.IcVendaTermo) || ModelEntity.IcVendaTermo == "N") &&
                        (string.IsNullOrEmpty(ModelEntity.IcRegVendaTermo) || ModelEntity.IcRegVendaTermo == "N"))
                        return "Depósito";


                    if (ModelEntity.IcOperTmo == "S" && (string.IsNullOrEmpty(ModelEntity.IcRegVendaTermo) || ModelEntity.IcRegVendaTermo == "N") &&
                        (string.IsNullOrEmpty(ModelEntity.IcVendaTermo) || ModelEntity.IcVendaTermo == "N") &&
                        ModelEntity.IdRegVendaTermo == 0)
                        return "Compra a Termo";

                    return "Compra";

                case "V":
                    if (ModelEntity.Data == ModelEntity.DtVencimento)
                        return "Resgate";

                    if (ModelEntity.IcOperTmo == "N")
                        return "Venda";

                    if (ModelEntity.IcOperTmo == "S" && (string.IsNullOrEmpty(ModelEntity.IcRegVendaTermo) || ModelEntity.IcRegVendaTermo == "N") &&
                        (string.IsNullOrEmpty(ModelEntity.IcVendaTermo) || ModelEntity.IcVendaTermo == "N") &&
                        ModelEntity.IdRegVendaTermo == 0)
                        return "Liq. Compra Termo";

                    if ((string.IsNullOrEmpty(ModelEntity.IcRegVendaTermo) || ModelEntity.IcRegVendaTermo == "N") && ModelEntity.IcVendaTermo == "S")
                        return "Liquidação Venda";

                    return "Venda a Termo";

                case "P":
                    return "Prêmio";
                case "I":
                    return "Principal";
                case "J":
                    return "Juros";
                case "M":
                    return "CM";
                case "R":
                    return "Repactuação";

                    default:
                    return ModelEntity.SgOperacao;

                }
            }
        }

        public string TipoTitulo
        {
            get { return ModelEntity.TipoTitulo; }
        }

        public string CodLastro
        {
            get { return ModelEntity.CodLastro; }
        }

        public string CodCetipSelic
        {
            get { return ModelEntity.CodCetipSelic; }
        }

        public string CodEmissor
        {
            get { return ModelEntity.CodEmissor; }
        }

        public DateTime? DtEmissao
        {
            get { return ModelEntity.DtEmissao; }
        }

        public DateTime? DtVencimento
        {
            get { return ModelEntity.DtVencimento; }
        }

        public string CdIndex
        {
            get { return ModelEntity.CdIndex; }
        }

        public double VlJurosOper
        {
            get { return (ModelEntity.VlJurosOper ?? 0); }
        }

        public double Quantidade
        {
            get { return (ModelEntity.Quantidade ?? 0); }
        }

        public double? VlNominal
        {
            get { return (ModelEntity.VlPuemPrinc ?? 0) * (ModelEntity.Quantidade ?? 0); }
        }

        public double PuOperacao
        {
            get { return (ModelEntity.PuOperacao ?? 0) ; }
        }

        public double PuAbertura
        {
            get
            {
                if (ModelEntity.RfPosPuMtm == 0 || ModelEntity.RfPosPuMtm==null)
                    return (ModelEntity.RfPosPu ?? 0);

                return (ModelEntity.RfPosPuMtm ?? 0);
                
            }
        }

        public double Cm
        {
            get { return Quantidade * (ModelEntity.VlPuemCm ?? 0); }
        }

        public double Valorizacao
        {
            get
            {
                if (ModelEntity.Data == DtAquisicao)
                    return 0;

                if (ModelEntity.SgOperacao == "J" || ModelEntity.SgOperacao == "I" || ModelEntity.SgOperacao == "P")
                    return 0;

                if (ModelEntity.SgOperacao == "V")
                    return (PuAbertura - (ModelEntity.PuAq ?? 0))*Quantidade;

                return (PuAbertura - (ModelEntity.PuAquisicao ?? 0))*Quantidade;
            }
        }

        public double VlLp
        {
            get
            {
                return 0;

                double qtAnt = 0;
                double vlBrutoAnt = 0;

                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    try
                    {

                        var maxDate = (from rfPos in db.SAC_RF_POS
                                       where rfPos.DT < Data && rfPos.RFOP_CD == CodOperacao
                                       select rfPos.DT).Max();

                        var posAnt = (from rfPos in db.SAC_RF_POS
                                      where rfPos.DT == maxDate && rfPos.RFOP_CD == CodOperacao
                                      select new
                                             {
                                                 VlBrutoAnt = rfPos.VL_BRUTOPOS,
                                                 QtAnt = rfPos.QT_DISPONIVEL
                                             }).FirstOrDefault();

                        if (posAnt == null)
                        {
                            var movAnt = (from rfMov in db.SAC_RF_MOV
                                          where rfMov.DT == Data && rfMov.RFOP_CD == CodOperacao
                                          select new
                                                 {
                                                     VlBrutoAnt = rfMov.VL_BRUTO,
                                                     QtAnt = rfMov.QT
                                                 }).FirstOrDefault();

                            if (movAnt != null)
                            {
                                vlBrutoAnt = movAnt.VlBrutoAnt ?? 0;
                                qtAnt = movAnt.QtAnt ?? 0;
                            }
                        }
                        else
                        {
                            vlBrutoAnt = posAnt.VlBrutoAnt ?? 0;
                            qtAnt = posAnt.QtAnt ?? 0;
                        }


                        if (qtAnt != Quantidade && qtAnt != 0)
                            return VlBruto - (vlBrutoAnt*(Quantidade/qtAnt));

                        return VlBruto - vlBrutoAnt;

                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }


            }
        }

        public double VlBruto
        {
            get { return ModelEntity.VlBruto ?? 0; }
        }

        public double VlJuros
        {
            get { return ModelEntity.VlJuros ?? 0; }
        }

        public string TipoOperacaoDescricao
        {
            get { return ModelEntity.IcOperTmo == "S" ? "Operações a Termo" : "Operações a Vista"; }
        }

        public String Clearing
        {
            get { return string.IsNullOrEmpty(ModelEntity.Clearing) ? "Nao Informada" :  ModelEntity.Clearing.Trim(); }
        }

        public String IcOperTermo
        {
            get { return ModelEntity.IcOperTmo; }
        }

        public double? Resultado
        {
            get
            {
                if (DtAquisicao == Data && ModelEntity.SgOperacao == "C")
                    return 0;

                if (ModelEntity.SgOperacao == "J" || ModelEntity.SgOperacao == "I" || ModelEntity.SgOperacao == "P" || ModelEntity.SgOperacao == "M")
                    return 0;


                if (VlLp != 0)
                    return VlLp;

                return (PuOperacao - (ModelEntity.VlPuCurva ?? 0))*Quantidade;

            }
        }


        public double VlPremio
        {
            get { return ModelEntity.VlPremio ?? 0; }
        }

        public double Agio
        {
            get
            {
                if(ModelEntity.SgOperacao=="C" || ModelEntity.SgOperacao == "V" && (ModelEntity.VlAgio ?? 0) != 0)
                    if (ModelEntity.VlPuAgio != 0)
                        return Quantidade* (ModelEntity.VlPuAgio ?? 0);
                    else
                        if((ModelEntity.VlFatorAgio ?? 0) !=0)
                            return Quantidade* (ModelEntity.VlFatorAgio ?? 0);

                return 0;
            }
        }

        public double Impostos
        {
            get
            {
                if (DtAquisicao == Data)
                    return 0;

                return (ModelEntity.VlIrrf ?? 0) + (ModelEntity.VlIof ?? 0);
            }

        }

        public double Aquisicao
        {
            get { return Quantidade*(ModelEntity.PuAquisicao ?? 0); }
        }

        public double VlLiquido
        {
            get { return VlBruto - ImpostosCalc; }
        }

        public double ImpostosCalc
        {
            get
            {
                if (DtAquisicao == Data)
                    return 0;

                return (ModelEntity.IcIrrfVirtual == "S" ? 0 : (ModelEntity.VlIrrf ?? 0)) + (ModelEntity.IcIofResgateVirtual == "S" ? 0 : (ModelEntity.VlIof ?? 0));
                
            }
        }

        

        #endregion
    }
}
