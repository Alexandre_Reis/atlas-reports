﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class PosicaoRendaFixaViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public PosicaoRendaFixaViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {
        }

        #endregion

        #region properties

        public string IcContaInvestimento
        {
            get { return ModelEntity["IC_CONTA_INVESTIMENTO"].ToString(); }
        }


        public string ClCliCd
        {
            get { return ModelEntity["CLCLI_CD"].ToString(); }
        }

        public string Cd
        {
            get { return ModelEntity["CD"].ToString(); }
        }

        public string RfLasCd
        {
            get { return ModelEntity["RFLAS_CD"].ToString(); }
        }

        public string RfTpCd
        {
            get { return ModelEntity["RFTP_CD"].ToString(); }
        }

        public string BaInsCd
        {
            get { return ModelEntity["BAINS_CD"].ToString(); }
        }

        public DateTime? DtEmissao
        {
            get { return ModelEntity.Field<DateTime?>("DT_EMISSAO"); //ModelEntity.Field<DateTime?>("DT_EMISSAO"); }
            }
        }

        public DateTime? DtAquisicao
        {
            get { return (DateTime?) ModelEntity["DT_AQUISICAO"]; }
        }


        public DateTime? DtVencimento
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO"]; }
        }

        public string IddirCd
        {
            get { return ModelEntity["IDDIR_CD"].ToString(); }
        }

        public double VlJurosEmnissao
        {
            get { return (double) ModelEntity["VL_JUROS_EMISSAO"]; }
        }

        public double VlJurosOper
        {
            get { return (double) ModelEntity["VL_JUROSOPER"]; }
        }

        public double QtDisponivel
        {
            get { return (double) ModelEntity["QT_DISPONIVEL"]; }
        }

        public double QtBloqueada
        {
            get { return (double) ModelEntity["QT_BLOQUEADA"]; }
        }

        public double QtTotal
        {
            get { return (double) ModelEntity["QT_TOTAL"]; }
        }

        public double VlPuAquisicao
        {
            get { return (double) ModelEntity["VL_PU_AQUISICAO"]; }
        }

        public double VlPu
        {
            get { return (double) ModelEntity["VL_PU"]; }
        }

        public double SacRfPosVlPrincipal
        {
            get { return (double) ModelEntity["VL_PRINCIPAL"]; }
        }

        public double SacRfPosVlCm
        {
            get { return (double) ModelEntity["VL_CM"]; }
        }

        public double SacRfPosVlJuros
        {
            get { return (double) ModelEntity["VL_JUROS"]; }
        }

        public double SacRfPosVlPremio
        {
            get { return (double) ModelEntity["VL_PREMIO"]; }
        }

        public double SacRfPosVlAgio
        {
            get { return (double) ModelEntity["VL_AGIO"]; }
        }

        public double VlBrutoPos
        {
            get { return (double) ModelEntity["VL_BRUTOPOS"]; }
        }

        public double VlProvIrrf
        {
            get { return (double) ModelEntity["VL_PROV_IRRF"]; }
        }

        public double VlProvIof
        {
            get { return (double) ModelEntity["VL_PROV_IOF"]; }
        }

        public double VlLiqPos
        {
            get { return (double) ModelEntity["VL_LIQ_POS"]; }
        }

        public double SacRfPosVlAjuste
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_AJUSTE"]; }
        }

        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public
            double VlLiquidGer
        {
            get { return (double) ModelEntity["VL_LIQUID_GER"]; }
        }

        public
            double VlBrutoGer
        {
            get { return (double) ModelEntity["VL_BRUTO_GER"]; }
        }

        public
            string Nm
        {
            get { return ModelEntity["NM"].ToString(); }
        }

        public
            double VlAp
        {
            get { return (double) ModelEntity["VL_AP"]; }
        }

        public
            string DsDescricao
        {
            get { return ModelEntity["DS_DESCRICAO"].ToString(); }
        }

        public
            double VlDuration
        {
            get { return (double) ModelEntity["VL_DURATION"]; }
        }

        public
            string Nome
        {
            get { return ModelEntity["NOME"].ToString(); }
        }

        public
            string IcPrePos
        {
            get { return ModelEntity["IC_PRE_POS"].ToString(); }
        }

        public
            string IddirCd2
        {
            get { return ModelEntity["IDDIR_CD_2"].ToString(); }
        }

        public
            DateTime? SacRfEventoDt
        {
            get { return (DateTime?) ModelEntity["SAC_RF_EVENTO.DT"]; }
        }

        public
            string CdAgencia4
        {
            get { return ModelEntity["CD_AGENCIA_4"].ToString(); }
        }

        public
            string NoCc4
        {
            get { return ModelEntity["NO_CC_4"].ToString(); }
        }

        public
            string CdDv4
        {
            get { return ModelEntity["CD_DV_4"].ToString(); }
        }

        public
            string CdCetip
        {
            get { return ModelEntity["CD_CETIP"].ToString(); }
        }

        public
            string CdSelic
        {
            get { return ModelEntity["CD_SELIC"].ToString(); }
        }

        public
            double VlPuVenc
        {
            get { return (double) ModelEntity["VL_PU_VENC"]; }
        }

        public
            string CdCetipSelic
        {
            get { return ModelEntity["CD_CETIP_SELIC"].ToString(); }
        }

        public
            string CdInterface
        {
            get { return ModelEntity["CD_INTERFACE"].ToString(); }
        }

        public
            double IddirPc
        {
            get { return (double) ModelEntity["IDDIR_PC"]; }
        }

        public
            string IcAbertIndex
        {
            get { return ModelEntity["IC_ABERT_INDEX"].ToString(); }
        }

        public
            string NomeIndex
        {
            get { return ModelEntity["NOME_INDEX"].ToString(); }
        }


        public
            string CdBanco
        {
            get { return ModelEntity["CD_BANCO"].ToString(); }
        }

        public
            string CdAgencia
        {
            get { return ModelEntity["CD_AGENCIA"].ToString(); }
        }

        public
            string CdContaCorrente
        {
            get { return ModelEntity["CD_CONTA_CORRENTE"].ToString(); }
        }

        public
            double VlTir
        {
            get { return (double) ModelEntity["VL_TIR"]; }
        }

        public
            string IcOperTermo
        {
            get { return ModelEntity["IC_OPER_TMO"].ToString(); }
        }

        public string SacRfTipoNm
        {
            get { return ModelEntity["SAC_RF_TIPO_NM"].ToString(); }
        }

        public
            string SgCondResgate
        {
            get { return ModelEntity["SG_COND_RESGATE"].ToString(); }
        }

        public
            string IcVendaTermo
        {
            get { return ModelEntity["IC_VENDA_TERMO"].ToString(); }
        }

        public
            double QtVendaTermo
        {
            get { return (double) ModelEntity["QT_VENDA_TERMO"]; }
        }


        public string LastroCodigoDescricao
        {
            get { return string.Format("{0}-{1}", RfLasCd.Trim(), DsDescricao.Trim()); }
        }

        public string IndexDescricao
        {
            get { return IddirCd; }
        }

        public double QtdeDisponivel
        {
            get { return QtDisponivel*VlAp; }
        }

        public double Qtdebloqueada
        {
            get { return QtBloqueada*VlAp; }
        }

        public double QtdeTotal
        {
            get { return QtTotal*VlAp; }
        }

        public double VlAjuste
        {
            get
            {
                if (IcOperTermo == "S" || IcVendaTermo == "S")
                    return VlAp*SacRfPosVlAjuste/QtTotal*QtVendaTermo;

                return VlAp*SacRfPosVlAjuste/QtTotal*(QtTotal*QtVendaTermo);
            }
        }


        public double VlPrincipal
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                {
                    if (IcPrePos != "A")
                        return (VlAp*SacRfPosVlPrincipal/QtTotal)*QtVendaTermo;

                    return QtVendaTermo*VlPuAquisicao;
                }

                if (IcPrePos != "A")
                    return VlAp*SacRfPosVlPrincipal/QtTotal - QtVendaTermo;

                return (QtDisponivel + QtBloqueada - QtVendaTermo)*VlPuAquisicao;
            }
        }


        public double VlCm
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                    return (VlAp*SacRfPosVlCm/QtTotal)*QtVendaTermo;

                return (VlAp*SacRfPosVlCm/QtTotal)*QtVendaTermo - QtVendaTermo;

            }
        }

        public double VlJuros
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                {
                    if (IcPrePos == "A" && DtAquisicao <= SacRfEventoDt)
                        return (VlPuVenc - VlPuAquisicao)*QtVendaTermo;

                    return (SacRfPosVlJuros/QtTotal)*QtVendaTermo*VlAp;
                }


                if (IcPrePos == "A" && DtAquisicao <= SacRfEventoDt)
                    return (VlPuVenc - VlPuAquisicao)*QtTotal - QtVendaTermo;
                return (SacRfPosVlJuros/QtTotal)*(QtTotal - QtVendaTermo)*VlAp;

            }
        }

        public double VlPremio
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                    return (VlAp*SacRfPosVlPremio/QtTotal)*QtVendaTermo;
                return (VlAp*SacRfPosVlPremio/QtTotal)*QtTotal - QtVendaTermo;
            }
        }

        public double VlAgio
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                    return (VlAp*SacRfPosVlAgio/QtTotal)*(QtVendaTermo);

                return (VlAp*SacRfPosVlAgio/QtdeTotal)*QtdeTotal - QtVendaTermo;
            }
        }

        public double Impostos
        {
            get
            {
                if (IcOperTermo == "S" && IcVendaTermo == "S")
                    return VlAp*(VlProvIrrf + VlProvIof)/QtTotal*QtVendaTermo;

                return VlAp*(VlProvIrrf + VlProvIof)/QtTotal*(QtTotal - QtVendaTermo);
            }
        }

        public string AberturaIndexada
        {
            get { return IcAbertIndex == "S" ? "(*)" : " "; }
        }

        public string GrupoConta
        {
            get { return string.Format("Banco: {0} Agencia: {1} Conta: {2}", CdBanco.Trim(), CdAgencia.Trim(), string.IsNullOrEmpty(CdContaCorrente) ? "Conta não cadastrada" : CdContaCorrente.Trim()); }
        }

        public string ContaInvestimento
        {
            get
            {
                if (string.IsNullOrEmpty(IcContaInvestimento))
                    return "Tipo da conta não cadastrada";

                return IcContaInvestimento == "S" ? "Conta Investimento" : "Conta Corrente";
            }

        }

        public string ClientAndCodeAndCdinterfce
        {
            get { return string.Format("{0} - {1}", ClientNameAndCodeString, CdInterface); }
        }

        public string ContaTitulo
        {
            get
            {
                return string.Format("Conta: {0}-{1}-{2}", CdAgencia4, NoCc4, CdDv4);
            }
        }

        public string OperacoesTermoDescricao
        {
            get { return IcOperTermo == "S" ? "Operações a Termo" : "Operações à Vista"; }
        }

        public string VendaTermoDescricao
        {
            get
            {
                if (IcOperTermo == "S")
                    return IcVendaTermo == "S" ? "Venda a Termo" : "Compra a Termo";

                return "";
            }
        }

        #endregion
    }
}