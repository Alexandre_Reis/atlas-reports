﻿using System;
using System.Data;
using System.ServiceModel;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Utils.Localization.Internal;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaCprViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totCpr;

        #endregion

        #region constructor

        public CarteiraDiariaCprViewModel(DataRow modelentity, double totCpr)
            : base(modelEntity: modelentity)
        {
            _totCpr = totCpr;
        }

        #endregion

        #region properties
        

        public string Descricao
        {
            get { return ModelEntity["DESCRICAO"].ToString(); }
        }
		public string MttpCd
        {
            get { return ModelEntity["MTTP_CD"].ToString(); }
        }
		public double Valor
        {
            get { return (double) ModelEntity["VALOR"]; }
        }
		public double PatrliqTot
        {
            get { return (double)ModelEntity["PATRLIQTOT"]; }
        }
		public string HistTraduzido
        {
            get { return ModelEntity["HIST_TRADUZIDO"].ToString(); }
        }
		public string CliBasMoeda
        {
            get { return ModelEntity["CLI_BAS_MOEDA"].ToString(); }
        }
		public string RvcadpapCodMoeda
        {
            get { return ModelEntity["RV_CADPAP_CODMOEDA"].ToString(); }
        }
		public double VlRateEa1
        {
            get { return (double) ModelEntity["VL_RATE_EA1"]; }
        }
		
		public double VlRateEa2
        {
            get { return (double) ModelEntity["VL_RATE_EA2"]; }
        }

        
        public double PcSobreCpr
        {
            get
            {
                if (_totCpr != 0)
                    return Valor / _totCpr;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (PatrliqTot != 0)
                    return Valor  / PatrliqTot;

                return 0;
            }
        }


        public double Vl
        {
            get
            {
                switch (MttpCd)
                {


                case "072":
                case "073":
                case "974":
                case "975":
                case "976":
                case "977":
                case "978":
                case "979":
                case "980":
                case "981":
                case "984":
                case "985":
                    return Valor/RateCarteiraAtivo;
                }

                return Valor;


            }
        }

        public
            double RateCarteiraAtivo
        {
            get
            {
                if (RvcadpapCodMoeda.Trim() != CliBasMoeda.Trim())
                    if (VlRateEa1 != 0)
                        return VlRateEa1;
                    else if (VlRateEa2 != 0)
                        return VlRateEa2;

                return 1;
            }
        }

        #endregion
    }
}