﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class CotPosicaoViewModel : BaseReportViewModel<CotPosicaoModel>
    {
        #region constructor

        public CotPosicaoViewModel(CotPosicaoModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties

        public DateTime? DtPosicao
        {
            get { return ModelEntity.DtPosicao; }
        }

        public string Fundo
        {
            get { return string.Format("{0} - {1}", ModelEntity.CdFundo, ModelEntity.Fundo); }
        }
        public double? Vlcota
        {
            get { return ModelEntity.Vlcota; }
        }
        public double? QtCotas
        {
            get { return ModelEntity.QtCotas; }
        }
        
        public DateTime? DtAplicacao
        {
            get { return ModelEntity.DtAplicacao; }
        }
        
        public double? VlIrrf
        {
            get { return ModelEntity.VlIrrf; }
        }
        public double? VlIof
        {
            get { return ModelEntity.VlIof; }
        }
        public double? VlLiq
        {
            get { return ModelEntity.VlLiq; }
        }
        public double? VlBruto
        {
            get { return ModelEntity.VlBruto; }
        }
        public int? Nota
        {
            get { return ModelEntity.Nota; }
        }

        #region calculate fields
        #endregion

        #endregion
    }
}
