﻿using Atlas.Reports.BaseViewModel;
using System;
using System.Data;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaRfViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totRf;

        #endregion

        #region constructor

        public CarteiraDiariaRfViewModel(DataRow modelentity, double totRf)
            : base(modelEntity: modelentity)
        {
            _totRf = totRf;
        }

        #endregion

        #region properties
        


	   public string RfOpCd
        {
            get { return ModelEntity["RFOP_CD"].ToString(); }
        }
       public string BaInsCd
        {
            get { return ModelEntity["BAINS_CD"].ToString(); }
        }
       public string RfTpCd
        {
            get { return ModelEntity["RFTP_CD"].ToString(); }
        }
       public double PatrVlTaxaAno
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_TAXA_ANO"]; }
        }
       public string IddirCd
        {
            get { return ModelEntity["IDDIR_CD"].ToString(); }
        }
       public double PatrVlPrazo
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_PRAZO"]; }
        }
       public DateTime? DtVemcto
        {
            get { return (DateTime?) ModelEntity["DT_VENCTO"]; }
        }
       public double ValorAplicacao
        {
            get { return (double) ModelEntity["VALOR_APLICACAO"]; }
        }
		public double PatrVlFinLiquido
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_LIQUIDO"]; }
        }
       public double PatrVltotrf1
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRF1"]; }
        }
       public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }
       public string RfTipoNM
        {
            get { return ModelEntity["SAC_RF_TIPO_NM"].ToString(); }
        }
       public DateTime? DtEmissao
        {
            get { return (DateTime?) ModelEntity["DT_EMISSAO"]; }
        }
       public double PatrVlFinBruto1
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_BRUTO1"]; }
        }
       public string IddirCdGEt
        {
            get { return ModelEntity["IDDIR_CD_GER"].ToString(); }
        }
       public DateTime? RfMovDt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }
       public double PatrvlFinLiq2
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_LIQUIDO2"]; }
        }
         public double PatrVlFinBruto2
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_BRUTO2"]; }
        }
       public double PatrVlTotrf2
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRF2"]; }
        }
       public double VlPatrliqtot2
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT2"]; }
        }
       public double PatrRfQt
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_QT"]; }
        }

        public string PagaIof
        {
            get { return ModelEntity["PAGAIOF"].ToString(); }
        }

        public double VlProvIrrf
        {
            get { return (double) ModelEntity["VL_PROV_IRRF"]; }
        }
        public double VlProvIof
        {
            get { return (double) ModelEntity["VL_PROV_IOF"]; }
        }
		public string RfLastroCd
        {
            get { return ModelEntity["CD"].ToString(); }
        }
        public double Cambio1
        {
            get { return (double) ModelEntity["CAMBIO1"]; }
        }
        public double Cambio2
        {
            get { return (double) ModelEntity["CAMBIO2"]; }
        }
       public double RfPosVlTir
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_TIR"]; }
        }
       public double RfPosVlCoupon
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_COUPON"] / 100; }
        }
       public double RfPosVlPu
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_PU"]; }
        }
		public string RfLastroDsDescricao
        {
            get { return ModelEntity["DS_DESCRICAO"].ToString(); }
        }
		public string BacenCd
        {
            get { return ModelEntity["BACEN_CD"].ToString(); }
        }
		public double LastroVlJurosEmissao
        {
            get { return (double) ModelEntity["SAC_RF_LASTRO_VL_JUROS_EMISSAO"]; }
        }
		public double PosVlDuration
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_DURATION"]; }
        }
		public string RftpcdAdelic
        {
            get { return ModelEntity["RFTP_CD_ADELIC"].ToString(); }
        }
		public DateTime? DtVencimentoAdelic
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO_ADELIC"]; }
        }
		public DateTime? DtEmissaoAdelic
        {
            get { return (DateTime?) ModelEntity["DT_EMISSAO_ADELIC"]; }
        }
		public double VlVoltaAdelic
        {
            get { return (double)ModelEntity["SAC_RF_OPER_VL_VOLTA_ADELIC"]; }
        }
		public double PcTaxaOver
        {
            get { return (double) ModelEntity["PC_TAXA_OVER"]/100; }
        }
		public string IcAdelic
        {
            get { return ModelEntity["IC_ADLIC"].ToString(); }
        }
        public double VlIrrf1
        {
            get { return (double) ModelEntity["VL_IRRF1"]; }
        }
        public double VlIof1
        {
            get { return (double) ModelEntity["VL_IOF1"]; }
        }
		public double IddirPc
        {
            get { return (double) ModelEntity["IDDIR_PC"]; }
        }
		public string IcTaxaCdi
        {
            get { return ModelEntity["IC_TAXA_CDI"].ToString(); }
        }
		public string IcOrigem
        {
            get { return ModelEntity["IC_ORIGEM"].ToString(); }
        }
		
		public string CdBanco
        {
            get { return ModelEntity["CD_BANCO"].ToString(); }
        }
		public string CdAgencia
        {
            get { return ModelEntity["CD_AGENCIA"].ToString(); }
        }
		public string CdContaCorrente
        {
            get { return ModelEntity["CD_CONTA_CORRENTE"].ToString(); }
        }
		public string Iccontainvestimento2
        {
            get { return ModelEntity["IC_CONTA_INVESTIMENTO2"].ToString(); }
        }
		public string IcOperTermo
        {
            get { return ModelEntity["IC_OPER_TMO"].ToString(); }
        }
		public string BamdaCd
        {
            get { return ModelEntity["BAMDA_CD"].ToString(); }
        }
       public string IcRegVendaTermo
        {
            get { return ModelEntity["IC_REG_VENDA_TERMO"].ToString(); }
        }
       public string IcAtivoPassivo
        {
            get { return ModelEntity["IC_ATIVO_PASSIVO"].ToString(); }
        }

       public string VendaTermoDescricao
       {
           get
           {
               if (IcOperTermo == "S")
                   return IcRegVendaTermo == "S" ? "Venda a Termo" : "Compra a Termo";

               return "";
           }
       }

       public string OperacoesTermoDescricao
       {
           get { return IcOperTermo == "S" ? "Operações a Termo" : "Operações à Vista"; }
       }


        public double PcSobreFI
        {
            get
            {
                if (_totRf != 0)
                    return PatrVlFinLiquido/ _totRf;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return PatrVlFinLiquido / VlPatrLiqTot;

                return 0;
            }
        }

        public string TipoMercadoGrupo
        {
            get { return IcOrigem == "I" ? "MERCADO INTERNACIONAL" : "MERCADO NACIONAL"; }
        }

        public double TaxaAno
        {
            get
            {
                return RfPosVlTir == 0 ? PatrVlTaxaAno / 100 : RfPosVlTir / 100;
            }
        }

        public string Index
        {
            get
            {
                return string.Format("{0} {1}", IddirCd, IddirPc != 100 ? string.Format("{0} {1}", IcTaxaCdi, IddirPc) : "");
            }
        }

        public double ValorBruto 
        {
            get
            {
                return IcAtivoPassivo == "P" ? PatrVlFinBruto1*-1 : PatrVlFinBruto1;
            }
        }

        public double Impostos
        {
            get { return VlIof1 + VlIrrf1; }
        }

        public double VlLiquido
        {
            get
            {
                return IcAtivoPassivo == "P" ? PatrVlFinLiquido*-1 : PatrVlFinLiquido;
            }
        }

        #endregion
    }
}