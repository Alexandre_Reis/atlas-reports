﻿using System;
using System.Data;
using System.ServiceModel;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Utils.Localization.Internal;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaFiViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totFi;

        #endregion

        #region constructor

        public CarteiraDiariaFiViewModel(DataRow modelentity, double totFi)
            : base(modelEntity: modelentity)
        {
            _totFi = totFi;
        }

        #endregion

        #region properties
        
        public string FiCadCd
        {
            get { return ModelEntity["FICAD_CD"].ToString(); }
        }
		public double QtCotas
        {
            get { return (double) ModelEntity["QT_COTAS"]; }
        }

		public double SacClPatrFiVlCota
        {
            get { return (double) ModelEntity["SAC_CL_PATRFI_VL_COTA"]; }
        }

		public double VlResgEmit
        {
            get { return (double) ModelEntity["SAC_CL_PATRFI_VL_RESG_EMIT"]; }
        }

		public double PatrFiVlFinanceiro
        {
            get { return (double) ModelEntity["SAC_CL_PATRFI_VL_FINANCEIRO"]; }
        }

		public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }

		public string BainsCd
        {
            get { return ModelEntity["BAINS_CD"].ToString(); }
        }
		public string NMfundo
        {
            get { return ModelEntity["NM"].ToString(); }
        }
		public double VlTotFi
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTFI"]; }
        }

		public double PatrFivlIrrf
        {
            get { return (double) ModelEntity["SAC_CL_PATRFI_VL_IRRF"]; }
        }

		public double VlFinanceiroLiq
        {
            get { return (double) ModelEntity["VL_FINANCEIRO_LIQ"]; }
        }

		public string TpFdoCd
        {
            get { return ModelEntity["TPFDO_CD"].ToString(); }
        }
		public string SgSegmento
        {
            get { return ModelEntity["SG_SEGMENTO"].ToString(); }
        }
		public double PatrFiVlIof
        {
            get { return (double) ModelEntity["SAC_CL_PATRFI_VL_IOF"]; }
        }

		public double QtBloqueada
        {
            get { return (double) ModelEntity["QT_BLOQUEADA"]; }
        }

		public double NoDecimaisQt
        {
            get { return (double) ModelEntity["NO_DECIMAIS_QT"]; }
        }

		public double NoDecimaisVl
        {
            get { return (double) ModelEntity["NO_DECIMAIS_VL"]; }
        }

		public string IcProvisionaIofResgate
        {
            get { return ModelEntity["IC_PROVISIONA_IOF_RESGATE"].ToString(); }
        }
		public string IcTipoSegmento
        {
            get { return ModelEntity["IC_TIPO_SEGMENTO"].ToString(); }
        }

        public string Segmento
        {
            get
            {
                return IcTipoSegmento == "F" ? "RV" : SgSegmento; 
            }
        }

        public string SegmentoGrupo
        {
            get
            {
                switch (Segmento)
                {
                case
                    "RF":
                    return "Fundos de Renda Fixa";
                case
                    "RV":
                    return "Fundos de Renda Variável";
                case
                    "IM":
                    return "Fundos Imobiliários";
                case
                    "EM":
                    return "Fundos Emergentes";
                case "OU":
                    return "Fundos Aplicação em Outros";

                }

                return "Fundos de Investimento";

            }
        }

        public double Impostos
        {
            get { return PatrFivlIrrf + IcProvisionaIofResgate == "N" ? 0 : PatrFiVlIof; }
        }


        public double PcSobreFI
        {
            get
            {
                if (_totFi != 0)
                    return VlFinanceiroLiq / _totFi;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlFinanceiroLiq / VlPatrLiqTot;

                return 0;
            }
        }

        #endregion
    }
}