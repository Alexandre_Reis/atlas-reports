﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class MovimentoFuturoViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public MovimentoFuturoViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties


        public string ClcliCd
        {
            get { return ModelEntity["CLCLI_CD"].ToString(); }
        }

        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public string OeCadCd
        {
            get { return ModelEntity["OECAD_CD"].ToString(); }
        }

        public string FuAtvCd
        {
            get { return ModelEntity["FUATV_CD"].ToString(); }
        }

        public string FuVctCd
        {
            get { return ModelEntity["FUVCT_CD"].ToString(); }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public double Qt
        {
            get { return (double) ModelEntity["QT"]; }
        }

        public double VlPreco
        {
            get { return (double) ModelEntity["VL_PRECO"]; }
        }

        public double Vlfinanceiro
        {
            get { return (double) ModelEntity["VL_FINANCEIRO"]; }
        }

        public double Corretagem
        {
            get { return (double) ModelEntity["CORRETAGEM"]; }
        }

        public string IcCv
        {
            get { return ModelEntity["IC_CV"].ToString(); }
        }

        public string IcDayTrade
        {
            get { return ModelEntity["IC_DAYTRADE"].ToString(); }
        }

        public double VlEmol
        {
            get { return (double) ModelEntity["VL_EMOLUMENTO"]; }
        }

        public double VlTaxaRegistro
        {
            get { return (double) ModelEntity["VL_TAXA_REGISTRO"]; }
        }

        public string RvcorCdAgente
        {
            get { return ModelEntity["RVCOR_CD_AGENTE"].ToString(); }
        }

        public double VlEqualizacao
        {
            get { return (double) ModelEntity["VL_EQUALIZACAO"]; }
        }

        public string Cdcustodiante
        {
            get { return ModelEntity["RVCOR_CD_CUSTODIANTE"].ToString(); }
        }

        public string FuAtivoIc
        {
            get { return ModelEntity["SAC_FU_ATIVO_IC"].ToString(); }
        }

        public string IsDayTrade
        {
            get { return IcDayTrade.Trim() == "S" ? "Sim" : "Não"; }
        }

        public double Quantidade
        {
            get
            {
                return IcCv.Trim() == "C" || IcCv.Trim() == "B" ? Qt : (Qt * -1);
            }
        }

        public double Financeiro
        {
            get
            {
                if (Math.Abs(VlEqualizacao) > 40000)
                    Console.WriteLine("a");

                if (FuAtivoIc.Trim() == "FUT")
                {
                    return (VlEqualizacao);
                    //if (IcCv == "C" || IcCv == "B")
                    //    //return (VlEqualizacao * -1);
                    //    return (VlEqualizacao);

                    //return Math.Abs(VlEqualizacao);
                }

                if (IcCv.Trim() == "C" || IcCv.Trim() == "B")
                    return (Vlfinanceiro * -1);
                    
                return Math.Abs(Vlfinanceiro);
            }
        }

        public double Total
        {
            get { return Financeiro - Corretagem - VlEmol - VlTaxaRegistro; }
        }

        public string DtMovGroup
        {
            get { return string.Format("Pregão: {0:dd/MM/yyyy}", Dt); }
        }

        #endregion
    }


}