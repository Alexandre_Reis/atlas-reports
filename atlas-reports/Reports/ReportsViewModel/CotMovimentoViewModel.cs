﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class CotMovimentoViewModel : BaseReportViewModel<MovimentoCotModel>
    {
        #region constructor

        public CotMovimentoViewModel(MovimentoCotModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties


        public string Fundo
        {
            get { return string.Format("{0}-{1}", ModelEntity.Fundo.Trim(), ModelEntity.CdFundo); }
        }

        public DateTime? Data
        {
            get { return ModelEntity.Data; }
        }

        public DateTime? DataAplicacao
        {
            get { return ModelEntity.DataAplicacao; }
            
        }

        public string IcAplicResg
        {
            get { return ModelEntity.IcAplicacaoResgate; }
        }

        public DateTime? LiquidacaoFisica
        {
            get { return ModelEntity.LiquidacaoFisica; }
        }

        public DateTime? LiquidacaoFinanceira
        {
            get { return ModelEntity.LiquidacaoFinanceira; }
        }

        public double ValorBruto
        {
            get { return ModelEntity.VlBruto ?? 0; }
        }

        public double Iof
        {
            get { return ModelEntity.VlIof?? 0; }
        }


        public double Irrf
        {
            get { return ModelEntity.VlIrrf ?? 0; }
        }


        public double ValorLiquido
        {
            get { return ModelEntity.VlLiquido ?? 0; }
        }
        

        public double Quantidade
        {
            get { return ModelEntity.QtCotas ?? 0; }
        }

        public double VlCota
        {
            get { return ModelEntity.VlCota ?? 0; }
        }

        #region calculate fields
        #endregion

        #endregion
    }
}
