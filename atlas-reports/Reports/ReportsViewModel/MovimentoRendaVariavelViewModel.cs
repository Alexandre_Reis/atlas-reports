﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class MovimentoRendaVariavelViewModel : BaseReportViewModel<MovimentoRendaVariavelModel>
    {
        #region constructor

        public MovimentoRendaVariavelViewModel(MovimentoRendaVariavelModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties

        public string CodBvsp
        {
            get
            {
                if (string.IsNullOrEmpty(ModelEntity.CodBvsp))
                    return "";

                return string.Format("[{0}]" ,ModelEntity.CodBvsp);
            }
        }


        public string Moeda
        {
            get { return ModelEntity.Moeda; }
        }

        public DateTime? DataMov
        {
            get { return ModelEntity.DataMov; }
        }

        public DateTime? DataLiq
        {
            get { return ModelEntity.DataLiq; }
        }

        public string CodPap
        {
            get { return ModelEntity.CodPap; }
        }

        public string CodCor
        {
            get { return ModelEntity.CodCor; }
        }

        public string RvCorCdFisica
        {
            get { return ModelEntity.RvCorCdFisica; }
        }

        public string Isin
        {
            get { return ModelEntity.Isin; }
        }

        public string Praca
        {
            get { return ModelEntity.Praca; }
        }

        public string TipoMov
        {
            get
            {
                switch (ModelEntity.CompraVenda)
                {
                case "C":

                    switch (ModelEntity.TipoMov)
                    {
                    case "0":

                        if (ModelEntity.Valor == 0)
                            return "Recebimento";
                        
                        return "Compra";


                    case "1":

                        if (ModelEntity.SgTipo == "B")
                            return "Bonificação";
                        
                        return "Cisão";

                    case "7":
                        return "Fusão";

                    case "2":
                        return "Dir.Subscr.";

                    case "6":
                        return "Exercício";

                    case "5":
                        return "Exercício";
                    default:
                        return ModelEntity.CompraVenda + ModelEntity.TipoMov;
                    }

                case "V":
                    return "Venda";

                case "S":
                    return "Ex.Subscr.";

                case "D":
                    return "Dividendo";

                case "I":

                    if (Quantidade/Preco >= 1)
                        return "Inplit";
                    
                    return "Split";

                case "T":
                    return "Transferência";

                case "R":
                    return "Recebimento";

                default:

                    if (ModelEntity.TipoMov == "6")
                        return "Exercício";
                    if (ModelEntity.TipoMov == "5")
                        return "Exercício";

                    return ModelEntity.CompraVenda + ModelEntity.TipoMov; 

                }

            }
        }

        public string DayTrade
        {
            get { return ModelEntity.DayTrade == "S" ? "Sim" : "Nao"; }
        }


        public double? Quantidade
        {
            get { return ModelEntity.TipoMov == "3" ? Math.Abs(ModelEntity.Quantidade ?? 0) : (ModelEntity.Quantidade ?? 0); }
        }

        public double? Preco
        {
            get { return ModelEntity.TipoMov=="1" || ModelEntity.TipoMov=="3" ? 0 : (ModelEntity.Preco ?? 0); }
        }

        public double? Valor
        {
            get
            {
                if (ModelEntity.CadPapTipo == "F")
                    return 0;

                if (ModelEntity.TipoMov == "1")
                    return 0;

                if (ModelEntity.CompraVenda == "C" || ModelEntity.CompraVenda == "R") 
                    return (ModelEntity.Valor ?? 0) * -1;

                return (ModelEntity.Valor ?? 0);

            }
        }

        public double? Equalizacao
        {
            get { return ModelEntity.CadPapTipo == "F" ? ModelEntity.Equalizacao : 0; }
        }

        public double? Corretagem
        {
            get { return ModelEntity.FlagCor == "N" ? 0 : ModelEntity.Corretagem; }
        }

        public double? Taxas
        {
            get
            {
                return ((ModelEntity.VlEmolumento ?? 0) + (ModelEntity.VlEmolumentoAgente ?? 0) + (ModelEntity.VlTxOpcoesAgente ?? 0) + (ModelEntity.VlTxOpcoes ?? 0) + (ModelEntity.VlEmolumentoFa ?? 0) + (ModelEntity.VlTxRegFa ?? 0) + (ModelEntity.VlTxRegFaAgente ?? 0) + (ModelEntity.VlTxRegFaAgente ?? 0)) * -1;
            }
        }

        public double? TaxaServico
        {
            get { return (ModelEntity.TaxaServico ?? 0) * -1; }
        }

        public double? Total
        {
            get { return Valor - Equalizacao - Corretagem - TaxaServico + Taxas; }
        }


        #endregion
    }
}
