﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaSwViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totSw;

        #endregion

        #region constructor

        public CarteiraDiariaSwViewModel(DataRow modelentity, double totSw)
            : base(modelEntity: modelentity)
        {
            _totSw = totSw;
        }

        #endregion

        #region properties

        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public string ClCliCd
        {
            get { return ModelEntity["CLCLI_CD"].ToString(); }
        }

        public string SwCadCd
        {
            get { return ModelEntity["SWCAD_CD"].ToString(); }
        }

        public double Principal
        {
            get { return (double) ModelEntity["PRINCIPAL"]; }
        }

        public double VlPassivo
        {
            get { return (double) ModelEntity["VL_PASSIVO"]; }
        }

        public double VlAtivo
        {
            get { return (double) ModelEntity["VL_ATIVO"]; }
        }

        public double VlFloow
        {
            get { return (double) ModelEntity["VL_FLOOR"]; }
        }

        public double VlApropBruto
        {
            get { return (double) ModelEntity["VL_APROP_BRUTO"]; }
        }

        public double VlApropIrrf
        {
            get { return (double) ModelEntity["VL_APROP_IRRF"]; }
        }

        public double VlapropLiquido
        {
            get { return (double) ModelEntity["VL_APROP_LIQUIDO"]; }
        }

        public DateTime? DtVencimentoTime
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO"]; }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public DateTime? DtPartida
        {
            get { return (DateTime?) ModelEntity["DT_PARTIDA"]; }
        }

        public double PcTaxaPassivo
        {
            get { return (double) ModelEntity["PC_TAXA_PASSIVO"] / 100; }
        }

        public string IddirCdPassivo
        {
            get { return ModelEntity["IDDIR_CD_PASSIVO"].ToString(); }
        }

        public string IcJurosPassivo
        {
            get { return ModelEntity["IC_JUROS_PASSIVO"].ToString(); }
        }

        public string IcApropPassivo
        {
            get { return ModelEntity["IC_APROP_PASSIVO"].ToString(); }
        }

        public string IcDiasPassivo
        {
            get { return ModelEntity["IC_DIAS_PASSIVO"].ToString(); }
        }

        public string IcIndicePassivo
        {
            get { return ModelEntity["IC_INDICE_PASSIVO"].ToString(); }
        }

        public double PcTaxaAtivo
        {
            get { return (double) ModelEntity["PC_TAXA_ATIVO"] / 100; }
        }

        public string IddirCdAtivo
        {
            get { return ModelEntity["IDDIR_CD_ATIVO"].ToString(); }
        }

        public string IcJurosAtivo
        {
            get { return ModelEntity["IC_JUROS_ATIVO"].ToString(); }
        }

        public string IcApropAtivo
        {
            get { return ModelEntity["IC_APROP_ATIVO"].ToString(); }
        }

        public string IcDiasAtivo
        {
            get { return ModelEntity["IC_DIAS_ATIVO"].ToString(); }
        }

        public string IcIndiceAtivo
        {
            get { return ModelEntity["IC_INDICE_ATIVO"].ToString(); }
        }

        public string IcGarantia
        {
            get { return ModelEntity["IC_GARANTIA"].ToString(); }
        }

        public string RvCorCdContra
        {
            get { return ModelEntity["RVCOR_CD_CONTRA"].ToString(); }
        }

        public double PcFloor
        {
            get { return (double) ModelEntity["PC_FLOOR"]; }
        }

        public string IddirCdFloor
        {
            get { return ModelEntity["IDDIR_CD_FLOOR"].ToString(); }
        }

        public string IcJurosFloor
        {
            get { return ModelEntity["IC_JUROS_FLOOR"].ToString(); }
        }

        public string IcApropFloor
        {
            get { return ModelEntity["IC_APROP_FLOOR"].ToString(); }
        }

        public string IcDiasFloor
        {
            get { return ModelEntity["IC_DIAS_FLOOR"].ToString(); }
        }

        public string IcIndiceFloor
        {
            get { return ModelEntity["IC_INDICE_FLOOR"].ToString(); }
        }

        public double TotSw
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTSW"]; }
        }

        public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }

        public double VlCorCambio
        {
            get { return (double) ModelEntity["SAC_SW2_POS_VL_COR_CAMBIO"]; }
        }

        public DateTime? DtValorizacao
        {
            get { return (DateTime?) ModelEntity["DT_VALORIZACAO"]; }
        }

        public double VlTaxaPassivo
        {
            get { return (double) ModelEntity["VL_TAXA_PASSIVO"]; }
        }

        public double VlTaxaAtivo
        {
            get { return (double) ModelEntity["VL_TAXA_ATIVO"]; }
        }

        public double PcTirPassivo
        {
            get { return (double) ModelEntity["PC_TIR_PASSIVO"]; }
        }

        public double PcTirAtivo
        {
            get { return (double) ModelEntity["PC_TIR_ATIVO"]; }
        }

        public double IddirPcAtivo
        {
            get { return (double)ModelEntity["IDDIR_PC_ATIVO"]; }
        }

        public double IddirPcPassivo
        {
            get { return (double) ModelEntity["IDDIR_PC_PASSIVO"]; }
        }

        public string IcTaxaCdiAtivo
        {
            get { return ModelEntity["IC_TAXA_CDI_ATIVO"].ToString(); }
        }

        public string IcTaxaCdiPassivo
        {
            get { return ModelEntity["IC_TAXA_CDI_PASSIVO"].ToString(); }
        }

        public string IcContaInvestimento
        {
            get { return ModelEntity["IC_CONTA_INVESTIMENTO"].ToString(); }
        }

        public string IcMtm
        {
            get { return ModelEntity["IC_MTM"].ToString(); }
        }

        public string SgMtmPassivo
        {
            get { return ModelEntity["SG_MTM_PASSIVO"].ToString(); }
        }

        public string SgMtmAtivo
        {
            get { return ModelEntity["SG_MTM_ATIVO"].ToString(); }
        }


        public double PcSobreSw
        {
            get
            {
                if (_totSw != 0)
                    return VlapropLiquido/_totSw;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlapropLiquido/VlPatrLiqTot;

                return 0;
            }
        }


        public string IndexadorPassivo
        {
            get { return string.Format("{0} {1}", IddirCdPassivo, IddirPcPassivo != 100 && IcTaxaCdiPassivo != null ? string.Format("{0} {1}", IcTaxaCdiPassivo, IddirPcPassivo) : ""); }
        }

        public double MtmPassivo
        {
            get
            {
                if(IcMtm=="1" || IcMtm=="S")
                    if (SgMtmPassivo != "N")
                        return PcTirPassivo / 100;

                return 0;
                    
            }
        }


        public string IndexadorAtivo
        {
            get { return string.Format("{0} {1}", IddirCdAtivo, IddirPcAtivo != 100 && IcTaxaCdiAtivo != null ? string.Format("{0} {1}", IcTaxaCdiAtivo, IddirPcAtivo) : ""); }
        }

        public double MtmAtivo
        {
            get
            {
                if (IcMtm == "1" || IcMtm == "S")
                    if (SgMtmAtivo != "N")
                        return PcTirAtivo / 100;

                return 0;

            }
        }

        public string ContaInvestimento
        {
            get
            {
                if (string.IsNullOrEmpty(IcContaInvestimento))
                    return "Tipo da conta não cadastrada";

                return IcContaInvestimento == "S" ? "Conta Investimento" : "Conta Corrente";
            }

        }

        #endregion
    }
}