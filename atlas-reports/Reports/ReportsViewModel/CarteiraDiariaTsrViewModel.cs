﻿using System;
using System.Data;
using System.ServiceModel;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Utils.Localization.Internal;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaTsrViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totTsr;

        #endregion

        #region constructor

        public CarteiraDiariaTsrViewModel(DataRow modelentity, double totTsr)
            : base(modelEntity: modelentity)
        {
            _totTsr = totTsr;
        }

        #endregion

        #region properties
        
        public string Descricao
        {
            get { return ModelEntity["DESCRICAO"].ToString(); }
        }
 		 public double Valor
        {
            get { return (double) ModelEntity["VALOR"]; }
        }
		 public double PatrliqTot
        {
            get { return (double)ModelEntity["PATRLIQTOT"]; }
        }
		 public string HistTraduzido
        {
            get { return ModelEntity["HIST_TRADUZIDO"].ToString(); }
        }

        public double PcSobreTsr
        {
            get
            {
                if (_totTsr != 0)
                    return Valor / _totTsr;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (PatrliqTot != 0)
                    return Valor  / PatrliqTot;

                return 0;
            }
        }



        #endregion
    }
}