﻿using System;
using System.Data;
using System.ServiceModel;
using Atlas.Reports.BaseViewModel;
using DevExpress.XtraRichEdit.Layout;

namespace Atlas.Reports.ReportsViewModel
{
    public class DemoCaixaViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public DemoCaixaViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties

        public DateTime? Data
        {
            get { return (DateTime?) ModelEntity["DATA"]; }
        }

        public string Hist
        {
            get { return ModelEntity["HIST"].ToString().Trim(); }
        }


        public double Valor
        {
            get { return (double) ModelEntity["VALOR"]; }
        }

        public string CodMoeda
        {
            get { return ModelEntity["CODMOEDA"].ToString(); }
        }

        public string CodPap
        {
            get { return ModelEntity["CODPAP"].ToString(); }
        }

        public double Saldo
        {
            get { return (double) ModelEntity["SALDO"]; }
        }

        
        public int Ordem
        {
            get { return (int) ModelEntity["ORDEM"]; }
        }

        public double VlEntrada
        {
            get { return Valor > 0 ? Valor : 0; }
        }

        public double VlSaida
        {
            get { return Valor < 0 ? Valor * -1 : 0; }
        }

        #endregion
    }
}