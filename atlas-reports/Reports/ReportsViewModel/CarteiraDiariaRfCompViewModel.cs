﻿using Atlas.Reports.BaseViewModel;
using System;
using System.Data;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaRfCompViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totRf;

        #endregion

        #region constructor

        public CarteiraDiariaRfCompViewModel(DataRow modelentity, double totRf)
            : base(modelEntity: modelentity)
        {
            _totRf = totRf;
        }

        #endregion

        #region properties
        


        public string RfOpCd
        {
            get { return ModelEntity["RFOP_CD"].ToString(); }
        }
		public string BaInsCD
        {
            get { return ModelEntity["BAINS_CD"].ToString(); }
        }
		public string RfTpCd
        {
            get { return ModelEntity["RFTP_CD"].ToString(); }
        }
		public double PatrVlTaxaAno
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_TAXA_ANO"] / 100; }
        }
		public string IdDirCd
        {
            get { return ModelEntity["IDDIR_CD"].ToString(); }
        }
		public double VlPrazo
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_PRAZO"]; }
        }
		public DateTime? DtVencto
        {
            get { return (DateTime?) ModelEntity["DT_VENCTO"]; }
        }
		public double ValorAplicacao
        {
            get { return (double) ModelEntity["VALOR_APLICACAO"]; }
        }
		public double PatrVlFinLiquido
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_LIQUIDO"]; }
        }
		public double VlTotrf1
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRF1"]; }
        }
		public double VlPatrliqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }
		public string RfTipoNm
        {
            get { return ModelEntity["SAC_RF_TIPO_NM"].ToString(); }
        }
		public DateTime? DtMissao
        {
            get { return (DateTime?) ModelEntity["DT_EMISSAO"]; }
        }
		public double IddirPc
        {
            get { return (double) ModelEntity["IDDIR_PC"]; }
        }
		public string IcTaxaCdi
        {
            get { return ModelEntity["IC_TAXA_CDI"].ToString(); }
        }
		public double PatrVlFinBruto
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_BRUTO1"]; }
        }

		public string IddirCdGer
        {
            get { return ModelEntity["IDDIR_CD_GER"].ToString(); }
        }
		public DateTime? RfMovDt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }
		public double PatrVlFinLiq2
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_LIQUIDO2"]; }
        }
		public double PatrVlFinBruto2
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_FIN_BRUTO2"]; }
        }
		public double VlTotrf2
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRF2"]; }
        }
		public double PatrVlPatrliqtot2
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT2"]; }
        }
		public double Qt
        {
            get { return (double) ModelEntity["QT"]; }
        }
		public string PagaIof
        {
            get { return ModelEntity["PAGAIOF"].ToString(); }
        }
		public double VlProvIrrf
        {
            get { return (double) ModelEntity["VL_PROV_IRRF"]; }
        }
		public string RfLastroCd
        {
            get { return ModelEntity["SAC_RF_LASTRO_CD"].ToString(); }
        }
		public double Cambio1
        {
            get { return (double) ModelEntity["CAMBIO1"]; }
        }
		public double Cambio2
        {
            get { return (double) ModelEntity["CAMBIO2"]; }
        }
		public double POsVlTir
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_TIR"]; }
        }
		public double PosVlcoupon
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_COUPON"] / 100; }
        }
		public DateTime? LastroDtVencimento
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO"]; }
        }

		public DateTime? DtVenctoAdelic
        {
            get { return (DateTime?) ModelEntity["DT_VENCIMENTO_ADELIC"]; }
        }
		public DateTime? DtEmissaoAdelic
        {
            get { return (DateTime?) ModelEntity["DT_EMISSAO_ADELIC"]; }
        }
		public double PuPartida
        {
            get { return (double) ModelEntity["SAC_CL_PATRRF_VL_PU_PARTIDA"]; }
        }
		public double VlJurosEmissao
        {
            get { return (double) ModelEntity["SAC_RF_LASTRO_VL_JUROS_EMISSAO"] / 100; }
        }
		public double VlPu
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_PU"]; }
        }
		public double Vlduration
        {
            get { return (double) ModelEntity["SAC_RF_POS_VL_DURATION"]; }
        }
		
		public double PcTaxaOver
        {
            get { return (double) ModelEntity["PC_TAXA_OVER"] / 100; }
        }
        public string RfTpCdAdelic
        {
            get { return ModelEntity["RFTP_CD_ADELIC"].ToString(); }
        }
		public double VlvoltaAdelic
        {
            get { return (double) ModelEntity["SAC_RF_OPER_VL_VOLTA_ADELIC"]; }
        }
		public double VlIrrf1
        {
            get { return (double) ModelEntity["VL_IRRF1"]; }
        }
		public double VlIof
        {
            get { return (double) ModelEntity["VL_IOF1"]; }
        }
		public string IcOrigem
        {
            get { return ModelEntity["IC_ORIGEM"].ToString(); }
        }


        public double PcSobreFI
        {
            get
            {
                if (_totRf != 0)
                    return PatrVlFinLiquido/ _totRf;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrliqTot != 0)
                    return PatrVlFinLiquido / VlPatrliqTot;

                return 0;
            }
        }

        public string TipoMercadoGrupo
        {
            get { return IcOrigem == "I" ? "MERCADO INTERNACIONAL" : "MERCADO NACIONAL"; }
        }

 
        public string Index
        {
            get
            {
                return string.Format("{0} {1}", IdDirCd, IddirPc != 100 ? string.Format("{0} {1}", IcTaxaCdi, IddirPc) : "");
            }
        }

        public double Impostos
        {
            get { return VlIof + VlIrrf1; }
        }


        #endregion
    }
}