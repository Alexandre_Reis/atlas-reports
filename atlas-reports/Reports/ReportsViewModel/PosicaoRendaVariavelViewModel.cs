﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;
using DevExpress.Mvvm.DataAnnotations;

namespace Atlas.Reports.ReportsViewModel
{
    public class PosicaoRendaVariavelViewModel : BaseReportViewModel<PosicaoRendaVariavelModel>
    {
        #region constructor

        public PosicaoRendaVariavelViewModel(PosicaoRendaVariavelModel modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties

        public DateTime? Data
        {
            get { return ModelEntity.Data; }
        }

        public string Papel
        {
            get { return string.Format("{0}-{1}", ModelEntity.Papel.Trim(), ModelEntity.codPap.Trim()); }
        }

        public string Isin
        {
            get { return ModelEntity.Isin; }
        }

        public string Corretora
        {
            get { return ModelEntity.Corretora; }
        }

        public string Praca
        {
            get { return ModelEntity.Praca; }
        }
        public double? Quantidade
        {
            get { return (ModelEntity.QtdeBloq == null ? 0 : ModelEntity.QtdeBloq) + (ModelEntity.QtdeDisp == null ? 0 : ModelEntity.QtdeDisp); }
        }

        public double? PrecoHistorico
        {
            get { return ModelEntity.PrecoHistorico ?? 0 ; }
        }
        public double? PrecoFiscal
        {
            get { return ModelEntity.PrecoFiscal ?? 0; }
        }

        public double? PercoMercado
        {
            get
            {
                if (ModelEntity.TipoRv == "M")
                    return ModelEntity.PrecoMercMedio;

                return ModelEntity.PrecoMercFech;
            }
        }

        public double? ValorHistorico
        {
            get
            {
                return PrecoHistorico * (Quantidade) / (ModelEntity.LoteCot ?? 0);
            }
        }

        public double? ValorFiscal
        {
            get { return PrecoFiscal * (Quantidade) / (ModelEntity.LoteCot ?? 0); }
        }

        public double? ValorMercadoBruto
        {
            get { return PercoMercado * (Quantidade) / (ModelEntity.LoteCot ?? 0); }
        }
        public double? Irrf { get { return ModelEntity.Irrf ?? 0; } }

        public double? ValorMercadoLiquido { get { return (ModelEntity.VlMercadoLiquido ?? 0); } }

        #endregion
    }
}
