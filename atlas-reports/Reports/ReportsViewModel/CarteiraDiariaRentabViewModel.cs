﻿using System;
using System.Data;
using System.ServiceModel;
using System.Windows.Navigation;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Utils.Localization.Internal;
using DevExpress.XtraCharts;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaRentabViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public CarteiraDiariaRentabViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {
        }

        #endregion

        #region properties
        

        public double VlrCota
        {
            get { return (double)ModelEntity["VLRCOTA"]; }
        }
		public double VlrPfee
        {
            get { return (double)ModelEntity["VLRPFEE"]; }
        }
		public double PatrliqTot
        {
            get { return (double)ModelEntity["PATRLIQTOT"]; }
        }
		public double Vc
        {
            get { return (double)ModelEntity["VC"]; }
        }
		public double Rcm
        {
            get { return (double)ModelEntity["RCM"]; }
        }
		public double Rc12
        {
            get { return (double)ModelEntity["RC12"]; }
        }
		public double RcUlt6
        {
            get { return (double)ModelEntity["RCULT6"]; }
        }
		public double RcUlt12
        {
            get { return (double) ModelEntity["RCULT12"]; }
        }
		public string IddirCdComp1
        {
            get { return ModelEntity["IDDIR_CD_COMP1"].ToString(); }
        }
		public string IddirCdComp2
        {
            get { return ModelEntity["IDDIR_CD_COMP2"].ToString(); }
        }
		public string IddirCdComp3
        {
            get { return ModelEntity["IDDIR_CD_COMP3"].ToString(); }
        }
		public string IddirCdComp4
        {
            get { return ModelEntity["IDDIR_CD_COMP4"].ToString(); }
        }
		public double PcDiai1
        {
            get { return (double) ModelEntity["PC_DIA_I1"]; }
        }
		public double PcDiai2
        {
            get { return (double) ModelEntity["PC_DIA_I2"]; }
        }
		public double PcDiai3
        {
            get { return (double) ModelEntity["PC_DIA_I3"]; }
        }
		public double PcDiai4
        {
            get { return (double) ModelEntity["PC_DIA_I4"]; }
        }
		public double PcMesi1
        {
            get { return (double) ModelEntity["PC_MES_I1"]; }
        }
		public double PcMesi2
        {
            get { return (double) ModelEntity["PC_MES_I2"]; }
        }
		public double PcMesi3
        {
            get { return (double)ModelEntity["PC_MES_I3"]; }
        }
		public double PcMesi4
        {
            get { return(double)  ModelEntity["PC_MES_I4"]; }
        }
		public double PcAnoi1
        {
            get { return (double) ModelEntity["PC_ANO_I1"]; }
        }
		public double PcAnoi2
        {
            get { return (double) ModelEntity["PC_ANO_I2"]; }
        }
		public double PcAnoi3
        {
            get { return (double) ModelEntity["PC_ANO_I3"]; }
        }
		public double PcAnoi4
        {
            get { return (double) ModelEntity["PC_ANO_I4"]; }
        }
		public double Pc06mI1
        {
            get { return (double)ModelEntity["PC_06M_I1"]; }
        }
		public double Pc06mI2
        {
            get { return (double)ModelEntity["PC_06M_I2"]; }
        }
		public double Pc06mI3
        {
            get { return (double)ModelEntity["PC_06M_I3"]; }
        }
		public double Pc06mI4
        {
            get { return (double)ModelEntity["PC_06M_I4"]; }
        }
		public double Pc12mI1
        {
            get { return (double)ModelEntity["PC_12M_I1"]; }
        }
		public double Pc12mI2
        {
            get { return (double)ModelEntity["PC_12M_I2"]; }
        }
		public double Pc12mI3
        {
            get { return (double)ModelEntity["PC_12M_I3"]; }
        }
		public double Pc12mI4
        {
            get { return (double)ModelEntity["PC_12M_I4"]; }
        }
		public double QtdCot
        {
            get { return (double)ModelEntity["QTDCOT"]; }
        }
		public string Tipo
        {
            get { return ModelEntity["TIPO"].ToString(); }
        }
		public string PagaIrrf
        {
            get { return ModelEntity["PAGAIRRF"].ToString(); }
        }
		public double VlTaxaAdministracao
        {
            get { return (double)ModelEntity["VL_TAXA_ADMINISTRACAO"]; }
        }

		public string IcCarteiraDiaria
        {
            get { return ModelEntity["IC_CARTEIRA_DIARIA"].ToString(); }
        }

		public DateTime? DtMesIni
        {
            get { return (DateTime?)ModelEntity["DT_MES_INI"]; }
        }
		public DateTime? DtMesFim
        {
            get { return (DateTime?)ModelEntity["DT_MES_FIM"]; }
        }
		public DateTime? DtAnoIni
        {
            get { return (DateTime?)ModelEntity["DT_ANO_INI"]; }
        }
		public DateTime? DtAnoFim
        {
            get { return (DateTime?)ModelEntity["DT_ANO_FIM"]; }
        }
		public DateTime? Dt06mIni
        {
            get { return (DateTime?)ModelEntity["DT_06M_INI"]; }
        }
		public DateTime? Dt06mFim
        {
            get { return (DateTime?)ModelEntity["DT_06M_FIM"]; }
        }
		public DateTime? Dt12mIni
        {
            get { return (DateTime?)ModelEntity["DT_12M_INI"]; }
        }
		public DateTime? Dt12mFim
        {
            get { return (DateTime?)ModelEntity["DT_12M_FIM"]; }
        }

        public double PcSobreTotal
        {
            get { return 1; }
        }


        public double BenchMark1
        {
            get
            {
                if (PcDiai1 == 0)
                    return 0;

                if ((Vc < 0 && PcDiai1 < 0 && Vc < PcDiai1) || (Vc < 0 && PcDiai1 > 0))
                    return ((((Math.Abs(Vc))/Math.Abs(PcDiai1)) * -1) *100);

                return ((Math.Abs(Vc)/Math.Abs(PcDiai1))*100);
            }
        }

        public double BenchMark2
        {
            get
            {
                if (PcDiai2 == 0)
                    return 0;

                if ((Vc < 0 && PcDiai2 < 0 && Vc < PcDiai2) || (Vc < 0 && PcDiai2 > 0))
                    return ((((Math.Abs(Vc)) / Math.Abs(PcDiai2)) * -1) * 100);

                return ((Math.Abs(Vc) / Math.Abs(PcDiai2)) * 100);
            }
        }

        public double BenchMark3
        {
            get
            {
                if (PcDiai3 == 0)
                    return 0;

                if ((Vc < 0 && PcDiai3 < 0 && Vc < PcDiai3) || (Vc < 0 && PcDiai3 > 0))
                    return ((((Math.Abs(Vc)) / Math.Abs(PcDiai3)) * -1) * 100);

                return ((Math.Abs(Vc) / Math.Abs(PcDiai3)) * 100);
            }
        }

        public double BenchMark4
        {
            get
            {
                if (PcDiai4 == 0)
                    return 0;

                if ((Vc < 0 && PcDiai4 < 0 && Vc < PcDiai4) || (Vc < 0 && PcDiai4 > 0))
                    return ((((Math.Abs(Vc)) / Math.Abs(PcDiai4)) * -1) * 100);

                return ((Math.Abs(Vc) / Math.Abs(PcDiai4)) * 100);

                
            }
        }

        public double RentabReal1
        {
            get { return ((((Vc/100) + 1)/((PcDiai1/100) + 1)) - 1)*100; }
        }

        public double RentabReal2
        {
            get { return ((((Vc / 100) + 1) / ((PcDiai2 / 100) + 1)) - 1) * 100; }
        }

        public double RentabReal3
        {
            get { return ((((Vc / 100) + 1) / ((PcDiai3 / 100) + 1)) - 1) * 100; }
        }

        public double RentabReal4
        {
            get { return ((((Vc / 100) + 1) / ((PcDiai4 / 100) + 1)) - 1) * 100; }
        }

        #endregion
    }
}