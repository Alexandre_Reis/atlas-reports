﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class MovimentoSwapViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public MovimentoSwapViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties

        public double SacSw2PosQt
        {
            get { return (double)ModelEntity["SAC_SW2_POS_QT"]; }
        }

        public DateTime? DtVencimento
        {
            get { return (DateTime?)ModelEntity["DT_VENCIMENTO"]; }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public DateTime? DtPartida
        {
            get { return (DateTime?)ModelEntity["DT_PARTIDA"]; }
        }

        public double VlApropBruto
        {
            get { return (double)ModelEntity["VL_APROP_BRUTO"]; }
        }

        public double VlApropIrrf
        {
            get { return (double)ModelEntity["VL_APROP_IRRF"]; }
        }

        public double VlApropLiq
        {
            get { return (double)ModelEntity["VL_APROP_LIQ"]; }
        }

        public double SacSw2MovQt
        {
            get { return (double)ModelEntity["SAC_SW2_MOV_QT"]; }
        }

        public string SwCadCd
        {
            get { return ModelEntity["SWCAD_CD"].ToString(); }
        }

        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public string CdLocalRegistro
        {
            get { return ModelEntity["CD_LOCAL_REGISTRO"].ToString(); }
        }

        public double VlEmol
        {
            get { return (double)ModelEntity["VL_EMOL"]; }
        }

        public double VlAtivo
        {
            get { return (double)ModelEntity["VL_ATIVO"]; }
        }

        public double VlAtivoCalc
        {
            get { return (double)ModelEntity["VL_ATIVO_CALC"]; }
        }

        public double VlPassivo
        {
            get { return (double)ModelEntity["VL_PASSIVO"]; }
        }

        public double VlPassivoCalc
        {
            get { return (double)ModelEntity["VL_PASSIVO_CALC"]; }
        }

        public DateTime? DtIniValorizacao
        {
            get { return (DateTime)ModelEntity["INI_VALORIZACAO"]; }
        }

        public double VlApropIrrfCalc
        {
            get { return (double)ModelEntity["VL_APROP_IRRF_CALC"]; }
        }

        public string CdBanco
        {
            get { return ModelEntity["CD_BANCO"].ToString(); }
        }

        public string CdAgencia
        {
            get { return ModelEntity["CD_AGENCIA"].ToString(); }
        }

        public string ContaCorrente
        {
            get { return ModelEntity["CONTACORRENTE"].ToString(); }
        }

        public string IcContaInvestimento
        {
            get { return ModelEntity["IC_CONTA_INVESTIMENTO"].ToString(); }
        }

        public string ContaInvestimento
        {
            get
            {
                if (string.IsNullOrEmpty(IcContaInvestimento))
                    return "Tipo da conta não cadastrada";

                return IcContaInvestimento == "S" ? "Conta Investimento" : "Conta Corrente";
            }

        }

        public string GrupoConta
        {
            get { return string.Format("Banco: {0} Agencia: {1} Conta: {2}", CdBanco.Trim(), CdAgencia.Trim(), string.IsNullOrEmpty(ContaCorrente) ? "Conta não cadastrada" : ContaCorrente.Trim()); }
        }

        public string CompraVenda
        {
            get
            {
                if (Dt == DtPartida)
                    return "Compra";

                if ( Dt  >  DtIniValorizacao && Dt <  DtVencimento)
                    return "Venda";

                if (Dt == DtVencimento)
                    return "Vencimento";

                return "";
            }
        }

        public double VlNetcalculado
        {
            get { return VlAtivoCalc - VlPassivoCalc; }
        }

        public double VlNetLiquidado
        {
            get { return VlAtivo - VlPassivo; }
        }

        public double VlIrrfCalc
        {
            get { return VlApropIrrfCalc > 0 ? VlApropIrrfCalc : 0; }
        }

        public double VlLucroPrejuizo
        {
            get { return VlNetLiquidado - VlNetcalculado; }
        }
        #endregion
    }
}