﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaOpcViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totRv;

        #endregion

        #region constructor

        public CarteiraDiariaOpcViewModel(DataRow modelentity, double totRv)
            : base(modelEntity: modelentity)
        {
            _totRv = totRv;
        }

        #endregion

        #region properties

		public string CodPap
        {
            get { return ModelEntity["CODPAP"].ToString(); }
        }
		public string NomePapel
        {
            get { return ModelEntity["RV_CADPAP_NOME"].ToString(); }
        }

        public double QtDisponivel
        {
            get { return (double) ModelEntity["QT_DISPONIVEL"]; }
        }

        public double VlTotalRv
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_TOTRV"]; }
        }
		public double VlPatrliqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }
		public string TipoEmp
        {
            get { return ModelEntity["TIPOEMP"].ToString(); }
        }
		public double VlLote
        {
            get { return (double) ModelEntity["VL_LOTE"]; }
        }
		public double QtTotal
        {
            get { return (double) ModelEntity["QT_TOTAL"]; }
        }
		public double PatrVlCusto
        {
            get { return (double) ModelEntity["SAC_CL_PATRRV_VL_CUSTO"]; }
        }
		public double PatrVlCustoFisc
        {
            get { return (double) ModelEntity["SAC_CL_PATRRV_VL_CUSTO_FISC"]; }
        }
		public string CadpapTipo
        {
            get { return ModelEntity["RV_CADPAP_TIPO"].ToString(); }
        }
		public double Exerc
        {
            get { return (double) ModelEntity["EXERC"]; }
        }

        public DateTime? DataVcto
        {
            get { return (DateTime)ModelEntity["DATAVCTO"]; }
        }

        public string RvpapCdOpcao
        {
            get { return ModelEntity["RVPAP_CD_OPCAO"].ToString(); }
        }
		public string FuVctCd
        {
            get { return ModelEntity["FUVCT_CD"].ToString(); }
        }

        public string IcOpcao
        {
            get { return ModelEntity["IC_OPCAO"].ToString(); }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }
		public string CdPraca
        {
            get { return ModelEntity["CD_PRACA"].ToString(); }
        }
		public double VlPatrCotacao
        {
            get { return (double) ModelEntity["VL_COTACAO"]; }
        }
		public string CdBolsa
        {
            get { return ModelEntity["CD_BOLSA"].ToString(); }
        }
		public string DsBolsa
        {
            get { return ModelEntity["DS_BOLSA"].ToString(); }
        }
		public double VlMerc
        {
            get { return (double) ModelEntity["VL_MERC"]; }
        }
		public double VlIrrf
        {
            get { return (double) ModelEntity["VL_IRRF"]; }
        }
		public double VlMercLiquido
        {
            get { return (double) ModelEntity["VL_MERC_LIQUIDO"]; }
        }

        public string TipoOpcaoDescricao
        {
            get
            {
                return CadpapTipo == "O" || CadpapTipo == "C" ? "CALL" : "PUT";
            }
        }


        public double VlCustoMedioSemcorretagem
        {
            get { return PatrVlCusto; }
        }

        public double VlCotacao
        {
            get
            {
                if (VlPatrCotacao == 0)
                {
                    if (QtTotal != 0)
                        return VlLote*VlMerc/QtTotal;
                }

                return VlPatrCotacao;
            }
        }

        public double VlCustoTotal
        {
            get { return PatrVlCusto; }
        }


        public double PcSobreRv
        {
            get
            {
                if (_totRv != 0)
                    return VlMerc/_totRv;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrliqTot != 0)
                    return VlMerc / VlPatrliqTot;

                return 0;
            }
        }

        public double Resultado
        {
            get
            {
                return VlMerc - PatrVlCusto;

            }
        }

        public string TipoEmpGrupo
        {
            get
            {
                return TipoEmp == "P" ? "Empresas Privadas" : "Empresas Públicas";

            }
        }

        public string GrupoBolsa
        {

            get
            {
                return string.Format("{0} - {1}", CdBolsa.Trim(), DsBolsa.Trim());
            }
        }

        #endregion
    }
}