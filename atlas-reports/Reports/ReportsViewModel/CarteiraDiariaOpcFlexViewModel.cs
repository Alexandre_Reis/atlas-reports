﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaOpcFlexViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totRv;

        #endregion

        #region constructor

        public CarteiraDiariaOpcFlexViewModel(DataRow modelentity, double totRv)
            : base(modelEntity: modelentity)
        {
            _totRv = totRv;
        }

        #endregion

        #region properties

        public DateTime? DtVencto
        {
            get { return (DateTime)ModelEntity["DATAVCTO"]; }
        }

        public string CodPap
        {
            get { return ModelEntity["CODPAP"].ToString(); }
        }


        public string CdBolsa
        {
            get { return ModelEntity["CD_BOLSA"].ToString(); }
        }


        public string DsBolsa
        {
            get { return ModelEntity["DS_BOLSA"].ToString(); }
        }


        public string CdOpcao
        {
            get { return ModelEntity["RVPAP_CD_OPCAO"].ToString(); }
        }


        public string RvpapTipo
        {
            get { return ModelEntity["RV_CADPAP_TIPO"].ToString(); }
        }


        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public double Exerc
        {
            get { return ToDouble(ModelEntity["EXERC"]); }
        }
        public double QtTotal
        {
            get { return ToDouble(ModelEntity["QT_TOTAL"]); }
        }
        public double Vlcusto
        {
            get { return ToDouble(ModelEntity["SAC_CL_PATRRV_VL_CUSTO"]); }
        }
        public double VlcustoFisc
        {
            get { return ToDouble(ModelEntity["SAC_CL_PATRRV_VL_CUSTO_FISC"]); }
        }
        public double VlMerc
        {
            get { return ToDouble(ModelEntity["SAC_CL_PATRRV_VL_MERC"]); }
        }

        public double VlLote
        {
            get { return ToDouble(ModelEntity["VL_LOTE"]); }
        }
        public double VlPatrCotacao
        {
            get { return ToDouble(ModelEntity["VL_COTACAO"]); }
        }
        public double VlMercLiquido
        {
            get { return ToDouble(ModelEntity["SAC_CL_PATRRV_VL_MERC_LIQUIDO"]); }
        }

        public string DsTipoOpcao
        {
            get { return RvpapTipo == "C" ? "CALL" : "PUT"; }
        }
        public double VlPatrLiqTot
        {
            get { return ToDouble(ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]); }
        }
        public double VlCustoMedio
        {
            get { return QtTotal == 0 ? 0 : VlLote * VlMerc / QtTotal; }
        }

        public double VlCotacao
        {
            get
            {
                if (VlPatrCotacao == 0)
                {
                    if (QtTotal != 0)
                        return VlLote*VlMerc/QtTotal;
                }

                return VlPatrCotacao;
            }
        }

        public double VlCustoTotal
        {
            get { return Vlcusto; }
        }

        public double Resultado
        {
            get { return VlMerc - VlCustoTotal; }
        }

        public double PcSobreRv
        {
            get
            {
                if (_totRv != 0)
                    return VlMerc / _totRv;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlMerc / VlPatrLiqTot;

                return 0;
            }
        }

        public string GrupoBolsa
        {

            get
            {
                return string.Format("{0} - {1}", CdBolsa.Trim(), DsBolsa.Trim());
            }
        }

        #endregion
    }
}