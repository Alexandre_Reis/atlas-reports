﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;
using DevExpress.Mvvm.DataAnnotations;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaFuOpcViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region Fields

        private double _totFu;

        #endregion

        #region constructor

        public CarteiraDiariaFuOpcViewModel(DataRow modelentity, double totFu)
            : base(modelEntity: modelentity)
        {
            _totFu = totFu;
        }

        #endregion

        #region properties


        
		public double VlPatrLiqTot
        {
            get { return (double) ModelEntity["SAC_CL_PATR_VL_PATRLIQTOT"]; }
        }

		public string FuAtvCd
        {
            get { return ModelEntity["FUATV_CD"].ToString(); }
        }

		public string FuVctCd
        {
            get { return ModelEntity["FUVCT_CD"].ToString(); }
        }
		public string RvcorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public string SgTipoOpc
        {
            get { return ModelEntity["SG_TIPOOPC"].ToString(); }
        }

		public double Qt
        {
            get { return (double) ModelEntity["QT"]; }
        }

		public double VlFuPosPreco
        {
            get { return (double)ModelEntity["SAC_FU_POS_VL_PRECO"]; }
        }

        public DateTime? DtFuVcto
        {
            get { return (DateTime?)ModelEntity["DT"]; }
        }

		public double VlVarPonto
        {
            get { return (double)ModelEntity["VL_VARPONTO"]; }
        }

        public double VlPrecoExerc
        {
            get { return (double)ModelEntity["VL_PRECO_EXERC"]; }
        }

        public double VlPrecoMerc
        {
            get { return (double)ModelEntity["VL_PRECOMERC"]; }
        }

        public double VlFuPosVlPrecoMerc
        {
            get { return (double)ModelEntity["SAC_FU_POS_VL_PRECOMERC"]; }
        }

        public string NmAtivo
        {
            get { return ModelEntity["NM"].ToString(); }
        }

		public string Ic
        {
            get { return ModelEntity["IC"].ToString(); }
        }
		public string CdBolsa
        {
            get { return ModelEntity["CD_BOLSA"].ToString(); }
        }
		public string DsBolsa
        {
            get { return ModelEntity["DS_BOLSA"].ToString(); }
        }
		public string NmAtivoIngles
        {
            get { return ModelEntity["NM_ATIVO_INGLES"].ToString(); }
        }

        public double VlcustoTotal
        {
            get { return Qt*VlFuPosPreco*VlVarPonto; }
        }

        public double Resultado
        {
            get { return VlPrecoMerc - VlcustoTotal; }
        }

        

        public double PcSobreFu
        {
            get
            {
                if (_totFu != 0)
                    return VlPrecoMerc/_totFu;

                return 0;
            }
        }

        public double PcSobrePl
        {
            get
            {
                if (VlPatrLiqTot != 0)
                    return VlPrecoMerc / VlPatrLiqTot;

                return 0;
            }
        }

        
        public string GrupoBolsa
        {

            get
            {
                return string.Format("{0} - {1}", CdBolsa.Trim(), DsBolsa.Trim());
            }
        }

        public string TipoPapel
        {
            get
            {
                return SgTipoOpc == "C" ? "Compra" : "Venda";
            }
        }

        #endregion
    }
}