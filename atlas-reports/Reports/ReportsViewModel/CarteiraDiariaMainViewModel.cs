﻿using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class CarteiraDiariaMainViewModel : BaseDataRowReportViewModel<DataRow>
    {

        #region constructor

        public CarteiraDiariaMainViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties


        #endregion
    }
}