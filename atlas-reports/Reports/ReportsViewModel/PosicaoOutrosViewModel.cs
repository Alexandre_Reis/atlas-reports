﻿using System;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ReportsViewModel
{
    public class PosicaoOutrosViewModel : BaseReportViewModel<PosicaoOutrosModel>
    {
        #region constructor

        public PosicaoOutrosViewModel(PosicaoOutrosModel modelentity)
            : base(modelEntity: modelentity)
        {
            
        }

        #endregion

        #region properties

        public DateTime? DtPosicao
        {
            get { return ModelEntity.DtPosicao; }
        }

        public string Fundo
        {
            get { return string.Format("{0} - {1}", ModelEntity.CdFundo, ModelEntity.Fundo); }
        }
        public double? Vlcota
        {
            get { return ModelEntity.Vlcota; }
        }
        public double? QtCotas
        {
            get { return ModelEntity.QtCotas; }
        }
        public double? QtBloqueada
        {
            get { return ModelEntity.QtBloqueada; }
        }
        public DateTime? DtAplicacao
        {
            get { return ModelEntity.DtAplicacao; }
        }
        public DateTime? DtUltimoAniv
        {
            get { return ModelEntity.DtUltimoAniv; }
        }
        public DateTime? DtproxAniv
        {
            get { return ModelEntity.DtproxAniv; }
        }
        public double? VlPrincipal
        {
            get { return ModelEntity.VlPrincipal; }
        }
        public double? VlResgEmit
        {
            get { return ModelEntity.VlResgEmit; }
        }
        public double? VlIrrf
        {
            get { return ModelEntity.VlIrrf; }
        }
        public double? VlIof
        {
            get { return ModelEntity.VlIof; }
        }
        public double? VlLiq
        {
            get { return ModelEntity.VlLiq; }
        }
        public double? VlTotal
        {
            get { return ModelEntity.VlTotal; }
        }
        public string SgLocalCustodia
        {
            get { return ModelEntity.SgLocalCustodia; }
        }

        #region calculate fields

        //public double ValorAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Valor : 0; }

        //}

        //public double ValorResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Valor : 0; }
        //}

        //public double ValorDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Valor : 0; }
        //}

        //public double ValorRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Valor : 0; }
        //}


        //public double CorretagemAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Corretagem : 0; }

        //}

        //public double CorretagemResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Corretagem : 0; }
        //}

        //public double CorretagemDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Corretagem : 0; }
        //}

        //public double CorretagemRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Corretagem : 0; }
        //}


        //public double EmolumentosAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Emolumentos : 0; }

        //}

        //public double EmolumentosResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Emolumentos : 0; }
        //}

        //public double EmolumentosDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Emolumentos : 0; }
        //}

        //public double EmolumentosRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Emolumentos : 0; }
        //}


        //public double IofAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Iof : 0; }

        //}

        //public double IofResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Iof : 0; }
        //}

        //public double IofDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Iof : 0; }
        //}

        //public double IofRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Iof : 0; }
        //}


        //public double IrrfAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Irrf : 0; }

        //}

        //public double IrrfResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Irrf : 0; }
        //}

        //public double IrrfDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Irrf : 0; }
        //}

        //public double IrrfRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Irrf : 0; }
        //}


        //public double ValorLiquidoAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? ValorLiquido : 0; }

        //}

        //public double ValorLiquidoResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? ValorLiquido : 0; }
        //}

        //public double ValorLiquidoDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? ValorLiquido : 0; }
        //}

        //public double ValorLiquidoRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? ValorLiquido : 0; }
        //}

        //public double QuantidadeAplicacoes
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "N" ? Quantidade : 0; }

        //}

        //public double QuantidadeResgates
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "N" ? Quantidade : 0; }
        //}

        //public double QuantidadeDeposito
        //{
        //    get { return IcAplicResg == "A" && IcTransf == "S" ? Quantidade : 0; }
        //}

        //public double QuantidadeRetirada
        //{
        //    get { return IcAplicResg != "A" && IcTransf == "S" ? Quantidade : 0; }
        //}

        #endregion

        #endregion
    }
}
