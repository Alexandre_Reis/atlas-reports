﻿using System;
using System.Data;
using Atlas.Reports.BaseViewModel;

namespace Atlas.Reports.ReportsViewModel
{
    public class PosicaoFuturoViewModel : BaseDataRowReportViewModel<DataRow>
    {
        #region constructor

        public PosicaoFuturoViewModel(DataRow modelentity)
            : base(modelEntity: modelentity)
        {

        }

        #endregion

        #region properties


        public DateTime? Dt
        {
            get { return (DateTime?) ModelEntity["DT"]; }
        }

        public string ClCliCd
        {
            get { return ModelEntity["CLCLI_CD"].ToString(); }
        }

        public string FuAtvCd
        {
            get { return ModelEntity["FUVCT_FUATV_CD"].ToString(); }
        }

        public string FuVctCd
        {
            get { return ModelEntity["FUVCT_CD"].ToString(); }
        }

        public DateTime? FuVctDt
        {
            get { return (DateTime?) ModelEntity["SAC_FU_VCTO_DT"]; }
        }

        public string RvCorCd
        {
            get { return ModelEntity["RVCOR_CD"].ToString(); }
        }

        public double VlPrecoExerc
        {
            get { return (double) ModelEntity["VL_PRECO_EXERC"]; }
        }

        public double Qt
        {
            get { return ModelEntity["QT"] == null ? 0 : (double) ModelEntity["QT"]; }
        }

        public double VlPreco
        {
            get { return (double) ModelEntity["VL_PRECO"]; }
        }

        public double VlPrecoInt
        {
            get { return (double) ModelEntity["VL_PRECO_INT"]; }
        }

        public double VlPrecoCli
        {
            get { return (double) ModelEntity["VL_PRECO_CLI"]; }
        }

        public double VlPrecoMerc
        {
            get { return ModelEntity["VL_PRECOMERC"] == null ? 0 : (double) ModelEntity["VL_PRECOMERC"]; }
        }

        public double VlAjusteALiqudar
        {
            get { return ModelEntity["VL_AJUSTE_A_LIQUIDAR"] == null ? 0 : (double)ModelEntity["VL_AJUSTE_A_LIQUIDAR"]; }
        }

        public double VlVarPonto
        {
            get { return ModelEntity["VL_VARPONTO"] == null ? 0 : (double) ModelEntity["VL_VARPONTO"]; }
        }

        public string Ic
        {
            get { return ModelEntity["IC"].ToString(); }
        }

        public DateTime? DtDiv
        {
            get
            {
                string dt = ModelEntity["DT_DIV"].ToString();
                if(string.IsNullOrEmpty(dt))
                    return null;

                return (DateTime?) ModelEntity["DT_DIV"];
                
            }
        }


        public double ValorDeMercado
        {
            get { return Ic == "TMO" || Ic == "SWP" ? VlAjusteALiqudar : VlPrecoMerc * Qt * VlVarPonto; }

        }

        #endregion
    }


}