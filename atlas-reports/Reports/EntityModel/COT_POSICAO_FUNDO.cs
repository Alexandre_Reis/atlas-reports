//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class COT_POSICAO_FUNDO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public COT_POSICAO_FUNDO()
        {
            this.COT_POSICAO_NOTA = new HashSet<COT_POSICAO_NOTA>();
        }
    
        public string CD_FUNDO { get; set; }
        public System.DateTime DT_POSICAO { get; set; }
        public string IC_AF_POSICAO { get; set; }
        public Nullable<double> VL_PATRIMONIO_ABERTURA { get; set; }
        public Nullable<double> VL_COTA_ABERTURA { get; set; }
        public Nullable<double> QT_COTAS_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_PF { get; set; }
        public Nullable<double> QT_COTAS_PJ { get; set; }
        public Nullable<double> VL_TAXA_ADMINISTRACAO { get; set; }
        public Nullable<double> VL_APLICACOES_TOTAL { get; set; }
        public Nullable<double> VL_APLICACOES_PF { get; set; }
        public Nullable<double> VL_APLICACOES_PJ { get; set; }
        public Nullable<double> VL_RESGATES_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_TOTAL_ANTERIOR { get; set; }
        public Nullable<double> VL_RESGATES_PF { get; set; }
        public Nullable<double> QT_COTAS_PF_ANTERIOR { get; set; }
        public Nullable<double> QT_COTAS_PJ_ANTERIOR { get; set; }
        public Nullable<double> VL_RESGATES_PJ { get; set; }
        public Nullable<double> QT_COTAS_APLICACOES_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_APLICACOES_PF { get; set; }
        public Nullable<double> QT_COTAS_APLICACOES_PJ { get; set; }
        public Nullable<double> QT_COTAS_RESGATES_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_RESGATES_PF { get; set; }
        public Nullable<double> VL_RESGATES_APLIC_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_RESGATES_PJ { get; set; }
        public Nullable<double> VL_RESGATES_APLIC_PF { get; set; }
        public Nullable<double> VL_RESGATES_APLIC_PJ { get; set; }
        public Nullable<int> NO_APLICACOES_TOTAL { get; set; }
        public Nullable<int> NO_RESGATES_TOTAL { get; set; }
        public Nullable<int> NO_COTISTAS_FECHAMENTO_TOTAL { get; set; }
        public Nullable<int> NO_COTISTAS_FECHAMENTO_PF { get; set; }
        public Nullable<int> NO_COTISTAS_FECHAMENTO_PJ { get; set; }
        public Nullable<double> VL_IOF_TOTAL { get; set; }
        public Nullable<double> VL_IOF_PF { get; set; }
        public Nullable<double> VL_IOF_PJ { get; set; }
        public Nullable<double> VL_IR_TOTAL { get; set; }
        public Nullable<double> VL_IR_PF { get; set; }
        public Nullable<double> VL_IR_PJ { get; set; }
        public Nullable<double> VL_RECEITA_SAQUE_CARENCIA { get; set; }
        public Nullable<int> NO_APLICACOES_PF { get; set; }
        public Nullable<double> VL_RECEITA_PFEE { get; set; }
        public Nullable<int> NO_APLICACOES_PJ { get; set; }
        public Nullable<double> VL_RECEITA_PENALTY { get; set; }
        public Nullable<double> VL_COTA_PRE_PFEE { get; set; }
        public Nullable<int> NO_RESGATES_PF { get; set; }
        public Nullable<double> VL_PATRIMONIO_PRE_PFEE { get; set; }
        public Nullable<double> VL_COTAS_EMITIR_FECHAMENTO { get; set; }
        public Nullable<int> NO_RESGATES_PJ { get; set; }
        public Nullable<double> VL_COTAS_RESGATAR_FECHAMENTO { get; set; }
        public Nullable<double> VL_PFEE_ACUMULADO { get; set; }
        public Nullable<double> VL_PFEE_RESGATADO { get; set; }
        public Nullable<double> VL_PFEE_CALCULADA_ATIVO { get; set; }
        public Nullable<double> VL_PATRIMONIO_FECHAMENTO_TOTAL { get; set; }
        public Nullable<double> VL_COTAS_EMITIR_ABERTURA { get; set; }
        public Nullable<double> VL_PATRIMONIO_FECHAMENTO_PF { get; set; }
        public Nullable<double> VL_COTAS_RESGATAR_ABERTURA { get; set; }
        public Nullable<double> VL_PATRIMONIO_FECHAMENTO_PJ { get; set; }
        public Nullable<double> QT_COTAS_FECHAMENTO_TOTAL { get; set; }
        public Nullable<double> QT_COTAS_FECHAMENTO_PF { get; set; }
        public Nullable<double> QT_COTAS_FECHAMENTO_PJ { get; set; }
        public Nullable<double> VL_COTA_FECHAMENTO { get; set; }
        public string IC_SN_REABERTO { get; set; }
        public Nullable<double> QT_COTAS_APLIC_PF_RETROATIVA { get; set; }
        public Nullable<double> QT_COTAS_APLIC_PJ_RETROATIVA { get; set; }
        public Nullable<double> VL_APLIC_PF_RETROATIVA { get; set; }
        public Nullable<double> VL_APLIC_PJ_RETROATIVA { get; set; }
        public Nullable<int> NO_APLIC_PF_RETROATIVA { get; set; }
        public Nullable<int> NO_APLIC_PJ_RETROATIVA { get; set; }
        public Nullable<double> QT_COTAS_APLIC_PF_TRANSF { get; set; }
        public Nullable<double> QT_COTAS_APLIC_PJ_TRANSF { get; set; }
        public Nullable<double> VL_APLIC_PF_TRANSF { get; set; }
        public Nullable<double> VL_APLIC_PJ_TRANSF { get; set; }
        public Nullable<int> NO_APLIC_PF_TRANSF { get; set; }
        public Nullable<int> NO_APLIC_PJ_TRANSF { get; set; }
        public Nullable<double> VL_COTA_PRE_AMORTIZACAO { get; set; }
        public Nullable<double> VL_AMORTIZACAO_TOTAL { get; set; }
        public Nullable<double> VL_AMORTIZACAO_PJ { get; set; }
        public Nullable<double> VL_AMORTIZACAO_PF { get; set; }
        public string CD_VERSAO { get; set; }
        public string CD_USUARIO { get; set; }
        public Nullable<System.DateTime> DT_PROCESSADO { get; set; }
        public Nullable<double> VL_COTA_AMORTIZACAO { get; set; }
    
        public virtual COT_FUNDO COT_FUNDO { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COT_POSICAO_NOTA> COT_POSICAO_NOTA { get; set; }
    }
}
