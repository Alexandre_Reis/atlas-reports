﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpf.Core.HandleDecorator;

namespace Atlas.Reports.EntityModel
{
    public partial class SacEntities
    {
        private static Common.Logging.ILog _logger = Common.Logging.LogManager.Adapter.GetLogger(typeof(SacEntities));

        public SacEntities(string connectString)
            : base(connectString)
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 200000;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
            Database.Log = s => _logger.Debug(s);
        }
    }
}
