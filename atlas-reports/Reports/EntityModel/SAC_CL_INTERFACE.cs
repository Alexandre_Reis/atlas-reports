//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SAC_CL_INTERFACE
    {
        public string CLCLI_CD { get; set; }
        public string CD_PLATAFORMA { get; set; }
        public string CD_GERENTE { get; set; }
        public string CD_AREA { get; set; }
        public Nullable<double> CD_SEQUENCIAL { get; set; }
        public string IC_ADMINISTRADA { get; set; }
        public string CD_AG_CONT_OPER { get; set; }
        public string CD_CC_CONT_OPER { get; set; }
        public string CD_DG_CONT_OPER { get; set; }
        public string CD_AG_CONT_INV { get; set; }
        public string CD_CC_CONT_INV { get; set; }
        public string CD_DG_CONT_INV { get; set; }
        public string CD_AG_CONT_TRIB { get; set; }
        public string CD_CC_CONT_TRIB { get; set; }
        public string CD_DG_CONT_TRIB { get; set; }
        public string CD_CCI { get; set; }
        public string CD_DG_CCI { get; set; }
        public string IC_REL_PADRAO { get; set; }
        public string IC_RISCO { get; set; }
        public string IC_TIPO_CPMF { get; set; }
        public string CD_PASSIVO1 { get; set; }
        public string CD_PASSIVO2 { get; set; }
        public string IC_TIPO_CLIENTE { get; set; }
        public string IC_TP_APUR { get; set; }
        public string CD_PRODUTO { get; set; }
        public string DS_PRODUTO { get; set; }
        public string SG_CATEGORIA { get; set; }
        public string CD_FI { get; set; }
        public string CD_AFI { get; set; }
        public string CD_CIF { get; set; }
        public string CD_SIGLA { get; set; }
        public string CD_BANCO_CONT_OPER { get; set; }
        public string CD_BANCO_CONT_INV { get; set; }
        public string CD_BANCO_CONT_TRIB { get; set; }
        public string IC_INTEGRA_HT { get; set; }
        public string IC_INTEGRA_RO { get; set; }
        public string IC_INTEGRA_E4 { get; set; }
        public string CD_CENTRO_CUSTO { get; set; }
        public string IC_D_COTA { get; set; }
        public string IC_INTEGRA_LC { get; set; }
        public string IC_PROCESSA_FY { get; set; }
        public string IC_EXPORTA_PAC_LC { get; set; }
        public string IC_INTEGRA_KX { get; set; }
        public string CD_BANCO_CONT_CETIP { get; set; }
        public string CD_AG_CONT_CETIP { get; set; }
        public string CD_CC_CONT_CETIP { get; set; }
        public string CD_DG_CONT_CETIP { get; set; }
        public string DS_PLANO { get; set; }
        public string DS_PATROCINADORA { get; set; }
        public string IC_REL_FLX_CAIXA { get; set; }
        public string CD_CARTMIC { get; set; }
        public string CD_CDLC { get; set; }
        public string IC_PROCESSA_BATCH { get; set; }
        public string IC_INTEGRA_HX { get; set; }
        public string IC_TXADM_LC { get; set; }
        public string IC_INTEGRA_LK { get; set; }
        public string IC_INTEGRA_L2 { get; set; }
        public string IC_INTEGRA_AY { get; set; }
        public string IC_CONTACORRENTE { get; set; }
        public string IC_LF00 { get; set; }
        public string IC_MC00 { get; set; }
        public string IC_AGENDAMENTO { get; set; }
        public string IC_MIS { get; set; }
        public string IC_TRIBUTACAO { get; set; }
        public string CD_LF { get; set; }
        public string IC_LF00_MOV { get; set; }
        public string IC_LF00_POS { get; set; }
        public string IC_INTEGRA_LD { get; set; }
        public string IC_FBSB { get; set; }
        public string IC_RDI { get; set; }
        public string CD_PERF_RDI { get; set; }
        public string IC_FLUXO_CAIXA { get; set; }
        public string COD_PERFIL_FLUXO_CX { get; set; }
        public string IC_CGI { get; set; }
        public Nullable<int> CD_EMPRESA { get; set; }
        public Nullable<int> CD_DEPENDENCIA { get; set; }
        public Nullable<int> CD_CARTEIRA { get; set; }
        public string IC_CETIP_SCF { get; set; }
        public string IC_INTEGRA_TR { get; set; }
        public string COD_PERFIL_PAPEL { get; set; }
        public string IC_LOG_LIBERADO { get; set; }
        public Nullable<int> ID_LOG_LIBERADO { get; set; }
        public string CD_RISKWATCH_BMF { get; set; }
        public string CODMOEDA { get; set; }
        public string CD_UNEGOC { get; set; }
        public string IC_INTERLIGACAO_GESTAO { get; set; }
        public string IC_INTERLIGACAO_A { get; set; }
        public string IC_INTERLIGACAO_B { get; set; }
        public string IC_INTERLIGACAO_C { get; set; }
        public string IC_INTEGRA_GR { get; set; }
        public string IC_IMPRIME_TIR { get; set; }
        public string CD_PERFIL_IDX_COMP { get; set; }
        public string IC_MENSAGEM_SPB { get; set; }
        public Nullable<int> COD_ISPB { get; set; }
        public string IC_CUSTODIA_INTERNACIONAL { get; set; }
        public string CD_FAIXARATING { get; set; }
        public string CD_PERFIL_AVL_EMIT { get; set; }
        public string IC_TVM { get; set; }
        public string IC_FILA_EXP_LHPAC { get; set; }
        public string SG_LIQMSG_SPB { get; set; }
        public string IC_LIQCC_INTERNA_SPB { get; set; }
        public string IC_SENSIB_CC_LEGADO_SPB { get; set; }
        public string IC_GERA_MSG_SEL1023 { get; set; }
    }
}
