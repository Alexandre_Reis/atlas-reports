//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class RV_MOV
    {
        public int ORDEM { get; set; }
        public System.DateTime DATAMOV { get; set; }
        public string CODCLI { get; set; }
        public string CODCOR { get; set; }
        public string CODOPER { get; set; }
        public string CODPAP { get; set; }
        public string PRACA { get; set; }
        public string COMPRAVENDA { get; set; }
        public Nullable<double> CORCLI { get; set; }
        public Nullable<double> CORINTEGRALBANCO { get; set; }
        public Nullable<double> CORRINTEGRALCOR { get; set; }
        public Nullable<System.DateTime> DATALIB { get; set; }
        public Nullable<double> DEVCORBANCO { get; set; }
        public Nullable<double> DEVCORCLI { get; set; }
        public string EVE_CODPAP { get; set; }
        public Nullable<System.DateTime> EVE_DATAEX { get; set; }
        public string FLAGCOR { get; set; }
        public string FLAGLIB { get; set; }
        public string FLAGLIBFIN { get; set; }
        public string IC_DAYTRADE { get; set; }
        public Nullable<double> P_CUSTO_BANCO { get; set; }
        public Nullable<double> P_CUSTO_CLI { get; set; }
        public Nullable<double> P_CUSTO_FISC { get; set; }
        public Nullable<double> P_CUSTO_HIST { get; set; }
        public Nullable<double> PERCDEVBANCOCLI { get; set; }
        public Nullable<double> PERCDEVCORBANCO { get; set; }
        public Nullable<double> PRECO { get; set; }
        public Nullable<double> QUANTIDADE { get; set; }
        public string TIPOMOV { get; set; }
        public Nullable<double> VALOR { get; set; }
        public Nullable<double> VL_EMOLUMENTO { get; set; }
        public Nullable<double> VL_TX_ANA { get; set; }
        public Nullable<double> VL_TX_ANA_CORRETORA { get; set; }
        public Nullable<double> VL_TX_OPCOES { get; set; }
        public Nullable<double> VL_QTD_BLOQ { get; set; }
        public string IC_IMPORTADA { get; set; }
        public string CD_ORDEM { get; set; }
        public Nullable<double> VL_SEQUENCIA { get; set; }
        public string CD_NEGOC { get; set; }
        public string CD_FILIAL { get; set; }
        public string CD_PRODUTO { get; set; }
        public string CD_CT_RESULTADO { get; set; }
        public string CD_USER_TRADER { get; set; }
        public string CD_LIVRO { get; set; }
        public string CD_CARTEIRA { get; set; }
        public string CD_ENTIDADE { get; set; }
        public string CD_PAPEL { get; set; }
        public string CD_PACOTE { get; set; }
        public string CD_ESTRATEGIA { get; set; }
        public string CD_CLIENTE_ING { get; set; }
        public string CD_CORR_ING { get; set; }
        public Nullable<double> VL_PERCENT_RISCO { get; set; }
        public string ST_OPER_ESPECIAL { get; set; }
        public Nullable<System.DateTime> DT_NEGOC { get; set; }
        public string CD_ENTIDADE_RISCO { get; set; }
        public string ST_RISCO_PAIS { get; set; }
        public string ST_RISCO_TRANSFER { get; set; }
        public string TP_OPERACAO { get; set; }
        public string CD_MOEDA { get; set; }
        public string CD_CORR { get; set; }
        public Nullable<double> VL_PRECO_LOTE { get; set; }
        public Nullable<double> VL_QTD_LOTE { get; set; }
        public string IC_EXPORTADA_IT { get; set; }
        public string NO_NOTA { get; set; }
        public string RVCOR_CD_AGENTE { get; set; }
        public Nullable<double> VL_EMOLUMENTO_AGENTE { get; set; }
        public Nullable<double> VL_TX_OPCOES_AGENTE { get; set; }
        public Nullable<double> VL_EMOLUMENTO_BANCO { get; set; }
        public Nullable<double> VL_TX_OPCOES_BANCO { get; set; }
        public Nullable<double> VL_COR_FIXA { get; set; }
        public Nullable<int> ID_MT { get; set; }
        public Nullable<int> ID_MT_SEQ_B { get; set; }
        public Nullable<double> VL_IR_DAYTRADE { get; set; }
        public string IC_IRRF_VIRTUAL { get; set; }
        public Nullable<decimal> ID_SCPRIV { get; set; }
        public string RVCOR_CD_FISICA { get; set; }
        public string CD_OPER_INTERNO { get; set; }
        public string IC_LIQ_FISICA { get; set; }
        public Nullable<System.DateTime> DT_INPUT { get; set; }
        public Nullable<System.DateTime> DT_PREVLIQ { get; set; }
        public string NO_OPBOLSA { get; set; }
        public string CD_SUBSEG_SPC { get; set; }
        public string SG_ALOCACAO { get; set; }
        public Nullable<int> ID_MOV_PLANO { get; set; }
        public string IC_LOG_LIBERADO { get; set; }
        public Nullable<int> ID_LOG_LIBERADO { get; set; }
        public string CD_HOTKEY_DC { get; set; }
        public string IC_TIPO_SEG { get; set; }
        public string IC_NETBOL { get; set; }
        public string IC_MOV_AUTOMATICO { get; set; }
        public Nullable<double> VL_EQUALIZACAO { get; set; }
        public Nullable<double> VL_EMOLUMENTO_FA { get; set; }
        public Nullable<double> VL_TX_REG_FA { get; set; }
        public Nullable<double> VL_TX_REG_FA_AGENTE { get; set; }
        public Nullable<double> VL_TX_LIQ_FA_AGENTE { get; set; }
        public string BALOCUST_CD_CODLCC { get; set; }
        public string TP_LIQUI { get; set; }
        public string BACLR_CD_CODCLR { get; set; }
        public string CD_MERCADO_EXT { get; set; }
        public Nullable<float> PC_DEVOLCORRBCO { get; set; }
        public Nullable<float> PC_DEVOLCORRCLI { get; set; }
        public Nullable<double> QT_EMPRESTIMO { get; set; }
        public Nullable<double> VL_RESULTADO { get; set; }
        public Nullable<double> VL_COR_LANC { get; set; }
        public string SG_TIPO_COR_LANC { get; set; }
        public Nullable<double> VL_EMOL_LANC { get; set; }
        public string SG_TIPO_EMOL_LANC { get; set; }
        public string CD_BOLSA { get; set; }
        public string IC_TRUNCA_ARREDONDA_COR_RV { get; set; }
        public string IC_TRUNCA_ARREDONDA_EMOL_LANC { get; set; }
        public Nullable<decimal> ID_CONTA { get; set; }
        public string IC_CONVERTE_COTAS { get; set; }
        public Nullable<double> VL_IRRF_ON { get; set; }
        public Nullable<double> VL_TX_SERVICO { get; set; }
        public Nullable<System.DateTime> DT_LIQ_FIS_SWIFT { get; set; }
        public Nullable<int> ID_ORDEM_ORIGEM { get; set; }
        public Nullable<double> QT_AUTO_EA { get; set; }
        public string IC_DAYTRADE_GANHO_LIQ { get; set; }
        public Nullable<System.DateTime> EVE_DT_PAGAMENTO { get; set; }
        public Nullable<System.DateTime> EVE_DT_CRED_RECIBO { get; set; }
        public Nullable<System.DateTime> EVE_DT_LIM_EMP { get; set; }
        public string CD_ESTRATEGIA_MOV { get; set; }
        public string IC_BAIXA_DIRSUB { get; set; }
        public string IC_AFTER_MARKET { get; set; }
        public Nullable<int> ID_ORDEM_FILHA { get; set; }
        public Nullable<double> ID_ORDEM_REV_MOV { get; set; }
        public Nullable<double> QT_ORIGINAL { get; set; }
        public Nullable<double> VL_ORIGINAL { get; set; }
        public string IC_MOV_DAYTRADE_AUT { get; set; }
        public string CD_NEGOCIO_BVP { get; set; }
        public Nullable<double> VL_PGTO_FRACAO { get; set; }
        public Nullable<double> VL_FRACAO { get; set; }
        public string CD_DISTRIBUICAO_CBLC { get; set; }
        public Nullable<int> NR_SEQUENCIA_CBLC { get; set; }
        public string SG_CONVERSAO { get; set; }
        public string CD_MOTIVO_TRANSF { get; set; }
        public Nullable<int> ID_PROV_TRANSF { get; set; }
        public string ID_MOTIVO { get; set; }
        public string IC_MOV_TERMO { get; set; }
        public Nullable<System.DateTime> DT_MOV_TERMO { get; set; }
        public Nullable<int> ID_ORDEM_MOV_TERMO { get; set; }
        public string IC_PROV_POS_VENDIDA { get; set; }
    
        public virtual CLI_BAS CLI_BAS { get; set; }
    }
}
