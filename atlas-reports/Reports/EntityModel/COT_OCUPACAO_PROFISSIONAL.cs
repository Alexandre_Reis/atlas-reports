//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class COT_OCUPACAO_PROFISSIONAL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public COT_OCUPACAO_PROFISSIONAL()
        {
            this.COT_CLIENTE = new HashSet<COT_CLIENTE>();
        }
    
        public string CD_OCUPACAO_PROFISSIONAL { get; set; }
        public string DS_OCUPACAO_PROFISSIONAL { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COT_CLIENTE> COT_CLIENTE { get; set; }
    }
}
