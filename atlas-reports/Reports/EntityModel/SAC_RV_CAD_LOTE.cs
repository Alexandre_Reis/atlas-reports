//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SAC_RV_CAD_LOTE
    {
        public string CD_PAPEL { get; set; }
        public System.DateTime DT_INICIO { get; set; }
        public System.DateTime DT_FIM { get; set; }
        public Nullable<double> VL_LOTE { get; set; }
        public string SG_ORIGEM { get; set; }
    }
}
