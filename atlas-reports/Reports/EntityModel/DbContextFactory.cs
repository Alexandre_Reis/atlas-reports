﻿
using Atlas.Link.Settings;
using Common.Logging;
using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;

namespace Atlas.Reports.EntityModel
{
    public static class DbContextFactory
    {
        #region Fields
        private static ApplicationConfig _settings = SettingsManager.Current.GetApplicationConfig();
        private static ILog _logger = LogManager.GetLogger("DbContextFactory");
        private static string _connectString;
        private static string _cotConnectString;
        private static string _adoConnectString;
        private static string _adoCotConnectString;

        #endregion

        public static string ConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectString))
                {
                    try
                    {
                        // Initialize the connection string builder for the
                        // underlying provider.
                        SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

                        var dbSettings = _settings.Databases.FirstOrDefault(t => t.Name.ToUpper() == "SAC");

                        if (dbSettings == null || string.IsNullOrEmpty(dbSettings.ServerName) || string.IsNullOrEmpty(dbSettings.DatabaseName))
                            return null;

                        // Set the properties for the data source.
                        sqlBuilder.DataSource = dbSettings.ServerName;
                        sqlBuilder.InitialCatalog = dbSettings.DatabaseName;
                        sqlBuilder.UserID = dbSettings.UseriD;
                        sqlBuilder.Password = dbSettings.Password;
                        sqlBuilder.MultipleActiveResultSets = true;
                        sqlBuilder.ApplicationName = "Atlas Reports";
                        // Build the SqlConnection connection string.
                        string providerString = sqlBuilder.ToString();

                        // Initialize the EntityConnectionStringBuilder.
                        EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

                        //Set the provider name.
                        entityBuilder.Provider = dbSettings.ProviderName;

                        // Set the provider-specific connection string.
                        entityBuilder.ProviderConnectionString = providerString;

                        // Set the Metadata location.
                        entityBuilder.Metadata = @"res://*/Reports.EntityModel.Sac.csdl|res://*/Reports.EntityModel.Sac.ssdl|res://*/Reports.EntityModel.Sac.msl";

                        _connectString = entityBuilder.ToString();

                    }
                    catch (Exception ex)
                    {
                        DXMessageBox.Show("Erro ao criar string de conexao, verifique o arquivo app.config.\n" + ex.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                        _logger.Error("DbContextFactory", ex);
                    }
                }

                return _connectString;
            }
        }

        public static string CotConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_cotConnectString))
                {
                    try
                    {

                        var dbSettings = _settings.Databases.FirstOrDefault(t => t.Name.ToUpper() == "COT");

                        if (dbSettings == null || string.IsNullOrEmpty(dbSettings.ServerName) || string.IsNullOrEmpty(dbSettings.DatabaseName))
                            return null;
                        
                        // Initialize the connection string builder for the
                        // underlying provider.
                        SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

                        // Set the properties for the data source.
                        sqlBuilder.DataSource = dbSettings.ServerName;
                        sqlBuilder.InitialCatalog = dbSettings.DatabaseName;
                        sqlBuilder.UserID = dbSettings.UseriD;
                        sqlBuilder.Password = dbSettings.Password;
                        sqlBuilder.MultipleActiveResultSets = true;
                        sqlBuilder.ApplicationName = "Atlas Reports";
                        // Build the SqlConnection connection string.
                        string providerString = sqlBuilder.ToString();

                        // Initialize the EntityConnectionStringBuilder.
                        EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

                        //Set the provider name.
                        entityBuilder.Provider = dbSettings.ProviderName;

                        // Set the provider-specific connection string.
                        entityBuilder.ProviderConnectionString = providerString;

                        // Set the Metadata location.
                        entityBuilder.Metadata = @"res://*/Reports.EntityModel.Cot.csdl|res://*/Reports.EntityModel.Cot.ssdl|res://*/Reports.EntityModel.Cot.msl";

                        _cotConnectString = entityBuilder.ToString();

                    }
                    catch (Exception ex)
                    {
                        DXMessageBox.Show("Erro ao criar string de conexao, verifique o arquivo app.config.\n" + ex.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                        _logger.Error("DbContextFactory", ex);
                    }

                }

                return _cotConnectString;
            }
        }

        public static string AdoConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_adoConnectString))
                {
                    try
                    {
                        var dbSettings = _settings.Databases.FirstOrDefault(t => t.Name.ToUpper() == "SAC");

                        if (dbSettings == null)
                            throw new Exception("Db settings to connect on SAC not found");

                        // Initialize the connection string builder for the
                        // underlying provider.
                        var sqlBuilder = new SqlConnectionStringBuilder();

                        // Set the properties for the data source.
                        sqlBuilder.DataSource = dbSettings.ServerName;
                        sqlBuilder.InitialCatalog = dbSettings.DatabaseName;
                        sqlBuilder.UserID = dbSettings.UseriD;
                        sqlBuilder.Password = dbSettings.Password;
                        sqlBuilder.MultipleActiveResultSets = true;
                        sqlBuilder.ApplicationName = "Atlas Reports";
                        // Build the SqlConnection connection string.
                        _adoConnectString = sqlBuilder.ToString();
                        
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("DbContextFactory", ex);
                    }
                }

                return _adoConnectString;
            }
        }
    }
}