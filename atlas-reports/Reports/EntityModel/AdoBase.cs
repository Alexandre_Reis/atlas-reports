﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.EntityModel
{
    public class AdoBase
    {
        public DataTable RunRawSqlQuerie(string query)
        {
            SqlConnection sqlConn = null;
            SqlCommand cmd = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            

            try
            {
                using (sqlConn = new SqlConnection(DbContextFactory.AdoConnectString))
                {
                    using (cmd = new SqlCommand(query, sqlConn))
                    {
                        using (da = new SqlDataAdapter(cmd))
                        {
                            using (dt = new DataTable())
                            {
                                cmd.CommandTimeout = 100000;
                                sqlConn.Open();
                                cmd.CommandType = System.Data.CommandType.Text;
                                da.Fill(dt);
                                return dt;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConn.State != System.Data.ConnectionState.Closed)
                    sqlConn.Close();

                if (cmd != null)
                    cmd.Dispose();
            }
        }
    }
}
