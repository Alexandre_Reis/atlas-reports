//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SAC_RF_TIPO
    {
        public SAC_RF_TIPO()
        {
            this.SAC_RF_LASTRO = new HashSet<SAC_RF_LASTRO>();
        }
    
        public string CD { get; set; }
        public string NM { get; set; }
        public string IC_PRE_POS { get; set; }
        public string IC_PUB_PRIV { get; set; }
        public string IC_JUROS { get; set; }
        public string SG_APROP_LED { get; set; }
        public string SG_APROP_UCD { get; set; }
        public string IC_ANIVER { get; set; }
        public Nullable<double> VL_BASE_JUROS { get; set; }
        public Nullable<double> VL_PU_EMISSAO { get; set; }
        public Nullable<double> VL_PU_VENC { get; set; }
        public string IC_FIXA_VAR { get; set; }
        public string SG_PAPDIV { get; set; }
        public string IDDIR_CD { get; set; }
        public string SG_BACEN { get; set; }
        public Nullable<int> DD_LIQ_FIN { get; set; }
        public string IC_TX_PERIODO { get; set; }
        public string IC_PAGA_IRRF { get; set; }
        public Nullable<int> DD_DEFASAGEM { get; set; }
        public string IC_JUROS_LIQ_FIN { get; set; }
        public string SG_BACEN2 { get; set; }
        public string SG_BACEN3 { get; set; }
        public string BACEN_CD { get; set; }
        public string IC_LIMITE_CREDITO { get; set; }
        public Nullable<double> VL_LIMITE_CREDITO { get; set; }
        public string CD_EMISSOR_PADRAO { get; set; }
        public string SG_CAIXA_PADRAO { get; set; }
        public Nullable<int> NO_DECIMAIS_QT { get; set; }
        public string IC_MTM_LE { get; set; }
        public Nullable<int> ID_TIPO_FERIADO { get; set; }
        public string SG_LOCAL_CUSTODIA { get; set; }
        public Nullable<int> DD_DEFASAGEM_CM { get; set; }
        public string SG_APROP_UCD_MTM { get; set; }
        public string SG_MTM_DEFAULT { get; set; }
        public string IC_DISPONIBILIDADE { get; set; }
        public string CD_SFR { get; set; }
        public string IC_ENQUADRAMENTO { get; set; }
        public string IC_TX_APURACAO { get; set; }
        public Nullable<double> VL_BASE_APURACAO { get; set; }
        public string IC_DIA_APURACAO { get; set; }
        public string IC_PU_APURACAO { get; set; }
        public string CD_SUSEP { get; set; }
        public string IC_OBRIGA_CADASTRO_EVENTOS { get; set; }
        public string IC_CALCULO_PADRONIZADO_MTM { get; set; }
        public string IC_CALCULO_PADRONIZADO { get; set; }
        public Nullable<double> VL_BASE_JUROS_MTM { get; set; }
        public string IC_LOG_LIBERADO { get; set; }
        public Nullable<int> ID_LOG_LIBERADO { get; set; }
        public string CD_ENQUADRA_SPC { get; set; }
        public string CD_SUBSEG_SPC { get; set; }
        public Nullable<System.DateTime> DT_REGISTRO { get; set; }
        public string IC_TRUNCA_PU { get; set; }
        public Nullable<int> NO_DECIMAIS_PU { get; set; }
        public string NM_TRADUZIDO { get; set; }
        public string CD_PAPEL_SND { get; set; }
        public Nullable<int> CODTLF { get; set; }
        public string SG_CLASSIF_ATIVO { get; set; }
        public string IC_EST_OPER_TMO { get; set; }
        public string IC_ATIVO_INATIVO { get; set; }
        public string IC_ORIGEM { get; set; }
        public string IDDIR_CD_CM { get; set; }
        public Nullable<int> DD_LIQ_VENC { get; set; }
        public string CD_BOLSA_NEG { get; set; }
        public string CD_BOLSA_ORG { get; set; }
        public string CD_BOLSA_LIQ { get; set; }
        public string SG_TIPO_DC { get; set; }
        public string SG_VF_TRUNCATE_ROUND { get; set; }
        public string SG_PU_TRUNCATE_ROUND { get; set; }
        public string SG_QT_TRUNCATE_ROUND { get; set; }
        public string SG_IDX_TRUNCATE_ROUND { get; set; }
        public string SG_JRS_TRUNCATE_ROUND { get; set; }
        public Nullable<decimal> NO_DECIMAIS_INDEX { get; set; }
        public Nullable<decimal> NO_DECIMAIS_JUROS { get; set; }
        public string IC_PROC_SFLUXO { get; set; }
        public string CD_BOLSA { get; set; }
        public Nullable<int> NO_DECIMAIS_VNA { get; set; }
        public string IC_TRUNCA_VNA { get; set; }
        public string IC_CREDITO_VENCIDO { get; set; }
        public string SG_NIVEL { get; set; }
    
        public virtual ICollection<SAC_RF_LASTRO> SAC_RF_LASTRO { get; set; }
    }
}
