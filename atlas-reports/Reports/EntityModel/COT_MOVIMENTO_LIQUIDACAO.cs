//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class COT_MOVIMENTO_LIQUIDACAO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public COT_MOVIMENTO_LIQUIDACAO()
        {
            this.COT_MOVIMENTO = new HashSet<COT_MOVIMENTO>();
        }
    
        public string CD_LIQUIDACAO { get; set; }
        public string DS_LIQUIDACAO { get; set; }
        public string IC_SN_MOSTRAR_BANCO { get; set; }
        public Nullable<int> NO_DIAS_SENSIBILIZACAO { get; set; }
        public Nullable<int> ID_TLF { get; set; }
        public string CD_TIPO_LIQUIDACAO { get; set; }
        public string IC_SN_FIXAR_TIPO_SAIDA { get; set; }
        public Nullable<int> ID_ORDEM { get; set; }
        public string CD_TIPO_MOVIMENTO { get; set; }
        public Nullable<int> ID_CONTA_CCI { get; set; }
        public string IC_SN_INTEGRA_SPB { get; set; }
        public string CD_MENSAGEM { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<COT_MOVIMENTO> COT_MOVIMENTO { get; set; }
    }
}
