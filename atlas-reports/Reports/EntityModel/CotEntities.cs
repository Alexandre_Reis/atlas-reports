﻿using System.Data.Entity.Infrastructure;

namespace Atlas.Reports.EntityModel
{
    public partial class CotEntities
    {
        private static Common.Logging.ILog _logger = Common.Logging.LogManager.Adapter.GetLogger(typeof(CotEntities));

        public CotEntities(string connectString)
            : base(connectString)
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 200000;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
            Database.Log = s => _logger.Debug(s);
        }
    }
}
