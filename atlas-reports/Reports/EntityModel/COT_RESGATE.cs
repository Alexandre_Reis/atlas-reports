//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atlas.Reports.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class COT_RESGATE
    {
        public int ID_NOTA { get; set; }
        public int ID_NOTA_APLICACAO { get; set; }
        public Nullable<System.DateTime> DT_RESGATE { get; set; }
        public Nullable<System.DateTime> DT_APLICACAO { get; set; }
        public Nullable<double> QT_COTAS { get; set; }
        public Nullable<double> VL_COTA_APLICACAO { get; set; }
        public Nullable<double> VL_APLICACAO { get; set; }
        public Nullable<double> VL_CUSTO_MEDIO { get; set; }
        public Nullable<double> VL_CORRIGIDO { get; set; }
        public Nullable<double> VL_RECEITA_SAQUE_CARENCIA { get; set; }
        public Nullable<double> VL_IOF { get; set; }
        public Nullable<double> VL_IOF_APLICACAO { get; set; }
        public Nullable<double> VL_IOF_VIRTUAL { get; set; }
        public Nullable<double> VL_BRUTO_RESGATE { get; set; }
        public Nullable<double> VL_IR { get; set; }
        public Nullable<double> VL_RENDIMENTO_RESGATE_IR { get; set; }
        public Nullable<double> VL_PENALTY_FEE { get; set; }
        public Nullable<double> PC_ALIQUOTA_IR { get; set; }
        public Nullable<double> VL_RENDIMENTO_COMPENSADO { get; set; }
        public Nullable<double> VL_LIQUIDO_RESGATE { get; set; }
        public Nullable<double> VL_PERFORMANCE { get; set; }
        public Nullable<double> VL_PERFORMANCE_ATIVO { get; set; }
        public Nullable<double> VL_PIP { get; set; }
        public Nullable<double> VL_IR_PIP { get; set; }
        public Nullable<double> VL_DEVOLUCAO_TAXA_ADMIN { get; set; }
        public Nullable<double> VL_TAXA_ADMINISTRACAO { get; set; }
        public Nullable<double> VL_DEVOLUCAO_PERFORMANCE { get; set; }
        public Nullable<System.DateTime> DT_COTA_UTILIZADA { get; set; }
        public Nullable<double> VL_COTA_UTILIZADA { get; set; }
        public Nullable<double> VL_RENDIMENTO_TRIBUTADO { get; set; }
        public Nullable<double> VL_RENDIMENTO_ISENTO { get; set; }
        public Nullable<double> PC_ALIQ_1 { get; set; }
        public Nullable<double> VL_REND_1 { get; set; }
        public Nullable<double> PC_ALIQ_2 { get; set; }
        public Nullable<double> VL_REND_2 { get; set; }
        public Nullable<double> VL_REND_1_COMPENSADO { get; set; }
        public Nullable<double> VL_REND_2_COMPENSADO { get; set; }
        public Nullable<double> VL_REND_ULTIMO_COME_COTAS { get; set; }
        public Nullable<double> VL_REND_PRIM_COME_COTAS { get; set; }
        public Nullable<double> VL_REND_SEGU_COME_COTAS { get; set; }
        public Nullable<double> VL_REND_TERC_COME_COTAS { get; set; }
        public Nullable<decimal> VL_DESENQ_RENDIMENTO { get; set; }
        public Nullable<decimal> VL_DESENQ_RENDIMENTO_COMPL { get; set; }
        public string CD_DESENQ { get; set; }
        public Nullable<decimal> VL_DESENQ_RENDIMENTO_1 { get; set; }
        public Nullable<decimal> VL_DESENQ_RENDIMENTO_COMPL_1 { get; set; }
        public string CD_DESENQ_1 { get; set; }
        public Nullable<double> VL_IR_CONTA_ORDEM { get; set; }
        public Nullable<double> VL_IOF_CONTA_ORDEM { get; set; }
        public Nullable<decimal> VL_CUSTO_CONTABIL { get; set; }
        public Nullable<decimal> VL_AGIO_DESAGIO { get; set; }
        public Nullable<int> QT_DIAS_IOF { get; set; }
        public Nullable<decimal> VL_ALIQUOTA_IOF { get; set; }
        public string IC_PV_TIPO_IOF { get; set; }
    
        public virtual COT_MOVIMENTO COT_MOVIMENTO { get; set; }
        public virtual COT_MOVIMENTO COT_MOVIMENTO1 { get; set; }
    }
}
