﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class MovimentoOutrosModel : BaseReportModel
    {
        public DateTime? Data { get; set; }
        public string Fundo { get; set; }
        public DateTime? LiquidacaoFinanceira { get; set; }
        public DateTime? LiquidacaoFisica { get; set; }
        public double? Vl { get; set; }
        public double? QtCotas { get; set; }
        public string IcAplicacaoResgate { get; set; }
        public DateTime? DataAplicacao { get; set; }
        public double? VlIrrf { get; set; }
        public double? VlIof { get; set; }
        public string SgIofResgate { get; set; }
        public string IcLiquidezDiaria { get; set; }
        public string IcTransferencia { get; set; }
        public double? VlEmol { get; set; }
        public double? VlCor { get; set; }
    }
}
