﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class BaseReportModel
    {
        public DateTime Startdate { get; set; }

        public DateTime EndDate { get; set; }

        public string CodCli { get; set; }

        public string ClientName { get; set; }

    }
}
