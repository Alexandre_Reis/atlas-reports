﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraCharts;

namespace Atlas.Reports.Model
{
    public class PosicaoOutrosModel : BaseReportModel
    {
        public DateTime? DtPosicao { get; set; }
        public string CdFundo { get; set; }
        public string Fundo { get; set; }
        public double? Vlcota { get; set; }
        public double? QtCotas { get; set; }
        public double? QtBloqueada { get; set; }
        public DateTime? DtAplicacao { get; set; }
        public DateTime? DtUltimoAniv { get; set; }
        public DateTime? DtproxAniv { get; set; }
        public double? VlPrincipal { get; set; }
        public double? VlResgEmit { get; set; }
        public double? VlIrrf { get; set; }
        public double? VlIof { get; set; }
        public double? VlLiq { get; set; }
        public double? VlTotal { get; set; }
        public string SgLocalCustodia { get; set; }

    }
}
