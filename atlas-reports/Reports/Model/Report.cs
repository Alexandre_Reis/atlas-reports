﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class Report
    {
        public string ReportName { get; set; }
        public Enums.Enums.Reports ReportEnum { get; set; }
        
    }
}
