﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class MovimentoCotModel : BaseReportModel
    {
        public DateTime? Data { get; set; }
        public string Fundo { get; set; }
        public DateTime? LiquidacaoFinanceira { get; set; }
        public DateTime? LiquidacaoFisica { get; set; }
        public double? VlBruto { get; set; }
        public double? VlLiquido { get; set; }
        public double? VlCota { get; set; }
        public double? QtCotas { get; set; }
        public string IcAplicacaoResgate { get; set; }
        public DateTime? DataAplicacao { get; set; }
        public double? VlIrrf { get; set; }
        public double? VlIof { get; set; }
        public string SgIofResgate { get; set; }
        public string CdFundo { get; set; }
        
    }
}
