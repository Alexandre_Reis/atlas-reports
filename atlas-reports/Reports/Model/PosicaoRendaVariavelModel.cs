﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class PosicaoRendaVariavelModel : BaseReportModel
    {
        public DateTime? Data { get; set; }
        public string Papel { get; set; }
        public string Isin { get; set; }
        public string Corretora { get; set; }
        public string Praca { get; set; }
        public double? QtdeDisp { get; set; }
        public double? QtdeBloq { get; set; }
        public double? PrecoHistorico { get; set; }
        public double? PrecoFiscal { get; set; }
        public double? PrecoMercMedio { get; set; }
        public string TipoRv { get; set; }
        public double? PrecoMercFech { get; set; }
        public double? LoteCot { get; set; }
        public double? Irrf { get; set; }
        public double? VlMercadoLiquido { get; set; }
        public string codPap { get; set; }

    }

}
