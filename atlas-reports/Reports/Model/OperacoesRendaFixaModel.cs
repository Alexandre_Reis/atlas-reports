﻿using System;

namespace Atlas.Reports.Model
{
    public class OperacoesRendaFixaModel : BaseReportModel
    {
        public DateTime? DtAquisicao { get; set; }
        public DateTime? Data { get; set; }
        public string CodOperacao { get; set; }
        public string SgOperacao { get; set; }
        public double? IdRegVendaTermo { get; set; }
        public string IcRegVendaTermo { get; set; }
        public DateTime? DtVencimento { get; set; }
        public string IcOperTmo { get; set; }
        public string IcVendaTermo { get; set; }
        public string TipoTitulo { get; set; }
        public string CodLastro { get; set; }
        public string CodCetipSelic { get; set; }
        public string CodEmissor { get; set; }
        public DateTime? DtEmissao { get; set; }
        public string CdIndex { get; set; }
        public double? VlJurosOper { get; set; }
        public double? Quantidade { get; set; }
        public double? VlPuemPrinc { get; set; }
        public double? PuOperacao { get; set; }
        public double? RfPosPuMtm { get; set; }
        public double? RfPosPu { get; set; }
        public double? VlPuemCm { get; set; }
        public double? PuAbertura { get; set; }
        public double? PuAq { get; set; }
        public double? PuAquisicao { get; set; }
        public double? VlJuros { get; set; }
        public double? VlLp { get; set; }
        public double? VlPuCurva { get; set; }
        public double? VlPremio { get; set; }
        public double? VlAgio { get; set; }
        public double? VlPuAgio { get; set; }
        public double? VlFatorAgio { get; set; }
        public double? VlIrrf { get; set; }
        public double? VlIof { get; set; }
        public double? VlBruto { get; set; }
        public string IcIrrfVirtual { get; set; }
        public string IcIofResgateVirtual{ get; set; }
        public string Clearing { get; set; }
    }
    
}
