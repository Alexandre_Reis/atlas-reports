﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Reports.Model
{
    public class MovimentoRendaVariavelModel : BaseReportModel
    {
        public DateTime? DataMov { get; set; }
        public DateTime? DataLiq { get; set; }
        public string Moeda{ get; set; }
        public string CodPap { get; set; }
        public string CodCor { get; set; }
        public string RvCorCdFisica { get; set; }
        public string Isin { get; set; }
        public string Praca { get; set; }
        public string TipoMov { get; set; }
        public double? Quantidade { get; set; }
        public double? Preco { get; set; }
        public double? Valor { get; set; }
        public double? Equalizacao { get; set; }
        public double? Corretagem { get; set; }
        public double? Taxas { get; set; }
        public double? TaxaServico { get; set; }
        public double? Total { get; set; }
        public string CompraVenda { get; set; }
        public string SgTipo { get; set; }
        public string DayTrade { get; set; }
        public string CadPapTipo { get; set; }
        public string FlagCor { get; set; }
        public double? VlEmolumento { get; set; }
        public double? VlEmolumentoAgente { get; set; }
        public double? VlTxOpcoesAgente { get; set; }
        public double? VlTxOpcoes { get; set; }
        public double? VlEmolumentoFa { get; set; }
        public double? VlTxRegFa { get; set; }
        public double? VlTxRegFaAgente { get; set; }
        public double? VlTxLiqFaAgente { get; set; }
        public string CodBvsp { get; set; }

    }

}
