﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Atlas.Reports.Windows
{
    /// <summary>
    /// Interaction logic for WaitProgressWindow.xaml
    /// </summary>
    public partial class WaitProgressWindow : Window
    {
        public WaitProgressWindow()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty PropertyTypeProperty = DependencyProperty.Register(
            "TitleProgress", typeof(String), typeof(WaitProgressWindow), new PropertyMetadata(default(String)));

        public String TitleProgress
        {
            get { return (String) GetValue(PropertyTypeProperty); }
            set { SetValue(PropertyTypeProperty, value); }
        }
    }
}
