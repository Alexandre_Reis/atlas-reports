﻿using System.Windows;
using Atlas.Link.Messages;
using Atlas.Link.Support;
using Atlas.Reports.EntityModel;
using DevExpress.Mvvm;

namespace Atlas.Reports.BaseViewModel
{
    public class BaseSacViewModel : NotifyUiBase
    {
        protected SacEntities Db;

        private void HandleCommand(CommandMessage action)
        {
            if (isCurrentView)
            {
                switch (action.Command)
                {
                    case CommandType.Insert:
                        break;
                    case CommandType.Edit:
                        break;
                    case CommandType.Delete:
                        DeleteCurrent();
                        break;
                    case CommandType.Commit:
                        CommitUpdates();
                        break;
                    case CommandType.Refresh:
                        RefreshData();
                        break;
                    default:
                        break;
                }
            }
        }
        private Visibility throbberVisible = Visibility.Visible;
        public Visibility ThrobberVisible
        {
            get { return throbberVisible; }
            set
            {
                throbberVisible = value;
                RaisePropertyChanged();
            }
        }
        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                RaisePropertyChanged();
            }
        }

        private void CommitUpdates()
        {
        }

        private void DeleteCurrent()
        {
        }

        private void RefreshData()
        {
            GetData();
            Messenger.Default.Send(new UserMessage { Message = "Data Refreshed" });
        }

        protected virtual void GetData()
        {
        }

        protected BaseSacViewModel()
        {
            if (!string.IsNullOrEmpty(DbContextFactory.ConnectString))
                Db = new SacEntities(DbContextFactory.ConnectString);

            //GetData();
            //Messenger.Default.Register<CommandMessage>(this, (action) => HandleCommand(action));
            //Messenger.Default.Register<NavigateMessage>(this, (action) => CurrentUserControl(action));
        }

        private bool isCurrentView = false;
        private void CurrentUserControl(NavigateMessage nm)
        {
            if (this.GetType() == nm.ViewModelType)
            {
                isCurrentView = true;
            }
            else
            {
                isCurrentView = false;
            }
        }
    }
}
