﻿using System;
using Atlas.Link.Support;

namespace Atlas.Reports.BaseViewModel
{
    public class BaseViewModel : NotifyUiBase
    {
        private bool isNew = true;
        public bool IsNew
        {
            get { return isNew; }
            set
            {
                isNew = value;
                RaisePropertyChanged();
            }
        }
        private bool isSelected = false;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                RaisePropertyChanged();
            }
        }
        private bool isDeleted = false;

        public bool IsDeleted
        {
            get { return isDeleted; }
            set
            {
                isDeleted = value;
                RaisePropertyChanged();
            }
        }


        #region Methods
        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <param name="propName">Name of the property.</param>
        /// <returns></returns>
        public static object GetPropValue(object src, string propName )
        {
            var teste = src.GetType().GetProperty(propName).GetValue(src, null) ?? ""; 
            Console.WriteLine(teste);
            return src.GetType().GetProperty(propName).GetValue(src, null) ?? "";
        }

        #endregion
        
    }
}
