﻿using System;
using Atlas.Link.Support;
using Atlas.Reports.Model;

namespace Atlas.Reports.BaseViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseReportViewModel<TEntity> : NotifyUiBase where TEntity : BaseReportModel
    {
        protected TEntity ModelEntity { get; private set; }

        #region constructor

        public BaseReportViewModel(TEntity modelEntity)
        {
            ModelEntity = modelEntity;
        }

        #endregion

        #region Properties

        public DateTime Startdate { get { return ModelEntity.Startdate; } }

        public DateTime EndDate { get { return ModelEntity.EndDate; } }

        public string CodCli { get { return ModelEntity.CodCli.Trim(); } }

        public string ClientName { get { return ModelEntity.ClientName.Trim(); } }

        public string IssueString
        {
            get { return string.Format("Data de Emissao: {0:dd/MM/yyyy HH:mm}", DateTime.Now); }
        }

        public string ClientNameAndCodeString
        {
            get { return string.Format("{0} - {1}", ClientName, CodCli); }
        }

        public string RangeDateString
        {
            get { return string.Format("{0:dd/MM/yyyy} a {1:dd/MM/yyyy}", Startdate, EndDate); }
        }


        private bool isNew = true;
        public bool IsNew
        {
            get { return isNew; }
            set
            {
                isNew = value;
                RaisePropertyChanged();
            }
        }
        private bool isSelected = false;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                RaisePropertyChanged();
            }
        }
        private bool isDeleted = false;

        public bool IsDeleted
        {
            get { return isDeleted; }
            set
            {
                isDeleted = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <param name="propName">Name of the property.</param>
        /// <returns></returns>
        public static object GetPropValue(object src, string propName )
        {
            var teste = src.GetType().GetProperty(propName).GetValue(src, null) ?? ""; 
            Console.WriteLine(teste);
            return src.GetType().GetProperty(propName).GetValue(src, null) ?? "";
        }

        #endregion

        
    }
}
