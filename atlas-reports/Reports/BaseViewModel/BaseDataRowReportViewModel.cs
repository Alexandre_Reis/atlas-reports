﻿using System;
using System.Data;
using Atlas.Link.Support;

namespace Atlas.Reports.BaseViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseDataRowReportViewModel<TEntity> : NotifyUiBase where TEntity : DataRow
    {
        protected TEntity ModelEntity { get; private set; }

        #region constructor

        public BaseDataRowReportViewModel(TEntity modelEntity)
        {
            ModelEntity = modelEntity;
        }

        #endregion

        #region Properties

        public DateTime? Startdate { get { return (DateTime?)ModelEntity["STARTDATE"]; } }

        public DateTime? EndDate { get { return (DateTime?)ModelEntity["ENDDATE"]; } }

        public string CodCli { get { return ModelEntity["CODCLI"].ToString().Trim();  } }

        public string ClientName { get { return ModelEntity["CLIENTNAME"].ToString().Trim(); } }

        public string IssueString
        {
            get { return string.Format("Data de Emissao: {0:dd/MM/yyyy HH:mm}", DateTime.Now); }
        }

        public string ClientNameAndCodeString
        {
            get { return string.Format("{0} - {1}", ClientName, CodCli); }
        }

        public string RangeDateString
        {
            get { return string.Format("{0:dd/MM/yyyy} a {1:dd/MM/yyyy}", Startdate, EndDate); }
        }


        private bool _isNew = true;
        public bool IsNew
        {
            get { return _isNew; }
            set
            {
                _isNew = value;
                RaisePropertyChanged();
            }
        }
        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaisePropertyChanged();
            }
        }
        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="src">The source.</param>
        /// <param name="propName">Name of the property.</param>
        /// <returns></returns>
        public static object GetPropValue(object src, string propName )
        {
            var teste = src.GetType().GetProperty(propName).GetValue(src, null) ?? ""; 
            Console.WriteLine(teste);
            return src.GetType().GetProperty(propName).GetValue(src, null) ?? "";
        }


        public double ToDouble(object value)
        {
            return value == null ? 0 : (double) value;
        }

        #endregion

        
    }
}
