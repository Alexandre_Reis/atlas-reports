﻿using System.Windows;
using Atlas.Link.Messages;
using Atlas.Link.Support;
using Atlas.Reports.EntityModel;
using DevExpress.Mvvm;

namespace Atlas.Reports.BaseViewModel
{
    public class BaseCotViewModel :  NotifyUiBase
    {
        protected CotEntities Db;

        private void HandleCommand(CommandMessage action)
        {
            if (isCurrentView)
            {
                switch (action.Command)
                {
                    case CommandType.Insert:
                        break;
                    case CommandType.Edit:
                        break;
                    case CommandType.Delete:
                        DeleteCurrent();
                        break;
                    case CommandType.Commit:
                        CommitUpdates();
                        break;
                    case CommandType.Refresh:
                        RefreshData();
                        break;
                    default:
                        break;
                }
            }
        }
        private Visibility throbberVisible = Visibility.Visible;
        public Visibility ThrobberVisible
        {
            get { return throbberVisible; }
            set
            {
                throbberVisible = value;
                RaisePropertyChanged();
            }
        }
        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set 
            { 
                errorMessage = value;
                RaisePropertyChanged();
            }
        }

        private void CommitUpdates()
        {
        }

        private void DeleteCurrent()
        {
        }

        private void RefreshData()
        {
            GetData();
            Messenger.Default.Send(new UserMessage {Message="Data Refreshed" });
        }

        protected virtual void GetData()
        {
        }

        protected BaseCotViewModel()
        {
            if(!string.IsNullOrEmpty(DbContextFactory.CotConnectString))
                Db = new CotEntities(DbContextFactory.CotConnectString);

            //GetData();
            //Messenger.Default.Register<CommandMessage>(this, (action) => HandleCommand(action));
            //Messenger.Default.Register<NavigateMessage>(this, (action) => CurrentUserControl(action));
        }

        private bool isCurrentView = false;
        private void CurrentUserControl(NavigateMessage nm)
        {
            if (this.GetType() == nm.ViewModelType)
            {
                isCurrentView = true;
            }
            else
            {
                isCurrentView = false;
            }
        }
    }
}
