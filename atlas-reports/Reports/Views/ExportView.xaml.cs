﻿using System;
using System.Windows.Controls;
using Atlas.Reports.ExportViewModel;
using DevExpress.Xpf.Core.HandleDecorator;
using DevExpress.Xpf.Grid;

namespace Atlas.Reports.Views
{
    /// <summary>
    /// Interaction logic for ExportView.xaml
    /// </summary>
    public partial class ExportView 
    {
        public ExportView(BaseExportViewModel viewModel)
        {
            InitializeComponent();

            this.DataContext = viewModel;

        }

        #region Members

        public GridControl GetGridControl()
        {
            return GridControlExport;
        }

        public TableView GetTableView()
        {
            GridControlExport.UpdateLayout();
            TableViewExport.UpdateLayout();
            return TableViewExport;
        }

        #endregion
    }
}
