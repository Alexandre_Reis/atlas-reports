﻿using Common.Logging;
using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Windows;
using DevExpress.Xpf.Core;

namespace Atlas.Ecd.DataAccess
{
    public static class DbContextFactory
    {
        private static ILog _logger = LogManager.GetLogger("DbContextFactory");
        private static string _connectString;

        public static string ConnectString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectString))
                {
                    try
                    {
                        // Specify the provider name, server and database.
                        string providerName = "System.Data.SqlClient";
                        string serverName = "";
                        string databaseName = "";

                        // Initialize the connection string builder for the
                        // underlying provider.
                        SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

                        // Set the properties for the data source.
                        sqlBuilder.DataSource = serverName;
                        sqlBuilder.InitialCatalog = databaseName;
                        sqlBuilder.UserID = "";
                        sqlBuilder.Password = "";
                        sqlBuilder.MultipleActiveResultSets = true;
                        // Build the SqlConnection connection string.
                        string providerString = sqlBuilder.ToString();

                        // Initialize the EntityConnectionStringBuilder.
                        EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

                        //Set the provider name.
                        entityBuilder.Provider = providerName;

                        // Set the provider-specific connection string.
                        entityBuilder.ProviderConnectionString = providerString;

                        // Set the Metadata location.
                        entityBuilder.Metadata = @"res://*/Ecd.Model.EcdModel.csdl|res://*/Ecd.Model.EcdModel.ssdl|res://*/Ecd.Model.EcdModel.msl";
                        
                        _connectString = entityBuilder.ToString();

                        //Context = new EcdEntitie(ConnectString);

                    }
                    catch (Exception ex)
                    {
                        DXMessageBox.Show("Erro ao criar string de conexao, consulto o arquivo de log.", "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                        _logger.Error("DbContextFactory", ex);
                    }

                }

                return _connectString;
            }
        }
    }
}

