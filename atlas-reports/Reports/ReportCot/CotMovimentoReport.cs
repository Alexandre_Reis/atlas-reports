﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.Report;
using Atlas.Reports.ReportsViewModel;

namespace Atlas.Reports.ReportCot
{
    public partial class CotMovimentoReport : BaseReport
    {
        public CotMovimentoReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            if (reportType == Enums.Enums.ReportType.Report)
            {
                var list = new List<CotMovimentoViewModel>();

                using (var db = new CotEntities(DbContextFactory.CotConnectString))
                {
                    var result = (from movimento in db.COT_MOVIMENTO.Include("COT_COTISTA").Include("COT_COTISTA.COT_CLIENTE").Include("COT_FUNDO").Include("COT_FUNDO.COT_CLIENTE")
                                  where clients.Contains(movimento.CD_COTISTA) &&
                                        movimento.DT_MOVIMENTO >= startDate && movimento.DT_MOVIMENTO <= endDate 
                                       
                                  select new MovimentoCotModel()
                                  {
                                      EndDate = endDate,
                                      Startdate = startDate,
                                      CodCli = movimento.CD_COTISTA,
                                      ClientName = movimento.COT_COTISTA.COT_CLIENTE.NM_CLIENTE,
                                      Data = movimento.DT_MOVIMENTO,
                                      Fundo = movimento.COT_FUNDO.COT_CLIENTE.NM_CLIENTE,
                                      LiquidacaoFinanceira = movimento.DT_LIQUIDACAO_FINANCEIRA,
                                      LiquidacaoFisica = movimento.DT_LIQUIDACAO_FISICA,
                                      QtCotas = movimento.QT_COTAS,
                                      IcAplicacaoResgate = movimento.COT_MOVIMENTO_TIPO.DS_TIPO,
                                      VlIrrf = movimento.VL_IR,
                                      VlIof = movimento.VL_IOF,
                                      VlBruto = movimento.VL_BRUTO,
                                      VlLiquido = movimento.VL_LIQUIDO,
                                      CdFundo = movimento.CD_FUNDO,
                                      VlCota = movimento.VL_COTA_MOVIMENTACAO
                                      
                                  }).ToList();

                    result.ForEach(t => list.Add(new CotMovimentoViewModel(t)));

                }

                this.DataSource = list;
            }

            else
            {
                try
                {
                    //var carteiras = string.Join("','", clients.ToArray());
                    //string querie = string.Format(Queries.QuerieFiles.MovimentoOutros, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    //InitializeComponent();

                    //AdoBase ado = new AdoBase();
                    //var dataTable = ado.RunRawSqlQuerie(querie);

                    //Export(dataTable, folder);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

        }

    }
}
