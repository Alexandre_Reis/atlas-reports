﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.Report;
using Atlas.Reports.ReportsViewModel;

namespace Atlas.Reports.ReportCot
{
    public partial class CotPosicaoReport : BaseReport
    {
        public CotPosicaoReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            try
            {
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    var list = new List<CotPosicaoViewModel>();

                    using (var db = new CotEntities(DbContextFactory.CotConnectString))
                    {
                        var result = (from posicao in db.COT_POSICAO_NOTA_HISTORICO
                                      join fundo in db.COT_FUNDO.Include("COT_CLIENTE") on posicao.CD_FUNDO equals fundo.CD_FUNDO
                                      join cotista in db.COT_COTISTA.Include("COT_CLIENTE") on posicao.CD_COTISTA equals cotista.CD_COTISTA
                                      where clients.Contains(posicao.CD_COTISTA) &&
                                            posicao.DT_POSICAO >= startDate && posicao.DT_POSICAO <= endDate

                                      select new CotPosicaoModel()
                                      {
                                          EndDate = endDate,
                                          Startdate = startDate,
                                          CodCli = posicao.CD_COTISTA,
                                          ClientName = cotista.COT_CLIENTE.NM_CLIENTE,
                                          DtPosicao = posicao.DT_POSICAO,
                                          Fundo = fundo.COT_CLIENTE.NM_CLIENTE,
                                          QtCotas = posicao.QT_COTAS,
                                          VlIrrf = posicao.VL_IR,
                                          VlIof = posicao.VL_IOF,
                                          VlBruto = posicao.VL_BRUTO,
                                          VlLiq = posicao.VL_RESGATE,
                                          CdFundo = posicao.CD_FUNDO,
                                          Nota = posicao.ID_NOTA ,
                                          DtAplicacao = posicao.DT_APLICACAO 

                                      }).ToList();

                        result.ForEach(t => list.Add(new CotPosicaoViewModel(t)));
                    }

                    this.DataSource = list;
                }

                else
                {
                    //var carteiras = string.Join("','", clients.ToArray());
                    //string querie = string.Format(Queries.QuerieFiles.MovimentoOutros, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    //InitializeComponent();

                    //AdoBase ado = new AdoBase();
                    //var dataTable = ado.RunRawSqlQuerie(querie);

                    //Export(dataTable, folder);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}