﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Atlas.Reports.Enums
{
    public class Enums
    {
        public enum ReportType
        {
            Report,
            Excel
        }

        public enum ReportsCot
        {
            [Description("Movimento")]
            Movimento,

            [Description("Posição")]
            Posicao
        }

        public enum Reports
        {
            [Description("Carteira Diaria")]
            CarteiraDiaria,

            [Description("Demonstrativo de Caixa")]
            DemoCaixa,

            [Description("Rentabilidade Patrimonial")]
            RentabPatrimonial,

            [Description("Outros Fundos - Movimento")]
            MovimentoOutrosFundos,

            [Description("Outros Fundos - Posição")]
            PosicaoOutrosFundos,

            [Description("Renda Variável - Movimento")]
            MovimentoRendaVariavel,

            [Description("Renda Variável - Posição")]
            PosicaoRendaVariavel,

            [Description("Renda Fixa - Operações")]
            OperacoesRendaFixa,

            [Description("Renda Fixa - Posição")]
            PosicaoRendaFixa,

            [Description("Futuros - Movimento")]
            MovimentoFuturos,

            [Description("Futuros - Posição")]
            PosicaoFuturos,

            [Description("Swap - Movimento")]
            MovimentoSwap,

            [Description("Swap - Posição")]
            PosicaoSwap
        }

        public static IEnumerable<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            // Can't use generic type constraints on value types,
            // so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);
            List<T> enumValList = new List<T>(enumValArray.Length);

            foreach (int val in enumValArray)
            {
                enumValList.Add((T)Enum.Parse(enumType, val.ToString()));
            }

            return enumValList;
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return attributes[0].Description;

            return value.ToString();
        }
    }
}
