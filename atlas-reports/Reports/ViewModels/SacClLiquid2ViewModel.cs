﻿using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class SacClLiquid2ViewModel : BaseViewModel.BaseViewModel
    {
        #region Properties

        public SAC_CL_LIQUID2 SacClLiquid2 { get; set; }

        public string ClcliCd
        {
            get { return SacClLiquid2.CLCLI_CD; }
        }


        public string TipoProcessamentoCota
        {
            get
            {
                return SacClLiquid2.SG_TIPOCOTA == "A" ? "Abertura" : "Fechamento";
            }
        }

        public string ArredondaTrunca
        {
            get
            {
                return SacClLiquid2.IC_TRUNCAR_COTA == "N" ? "Arredondar" : "Truncar";
            }
        }

        public string DiaSemana
        {
            get
            {
                return SacClLiquid2.IC_PARAM_FIXO == "S" ? "Sim" : "Não";
            }
        }

        public string CalculoQtdeAutomatico
        {
            get
            {
                switch (SacClLiquid2.SG_CONV_QTD_COTAS)
                {
                case "S":
                        return "Sim";
                case "N":
                        return "Não";
                case "F":
                        return "Sim - Fechamento";

                }

                return SacClLiquid2.SG_CONV_QTD_COTAS;
            }
        }

        public string SolicitacaoDe
        {
            get
            {
                switch (SacClLiquid2.SG_DIA_SOLICITACAO_INICIO)
                {
                    case 2:
                    return "Segunda";
                    case 3:
                    return "Terça";
                    case 4:
                    return "Quarta";
                    case 5:
                    return "Quinta";
                    case 6:
                    return "Sexta";
                    
                }
                return SacClLiquid2.SG_DIA_SOLICITACAO_INICIO.ToString();
            }
        }

        public string SolicitacaoAte
        {
            get
            {
                switch (SacClLiquid2.SG_DIA_SOLICITACAO_FIM)
                {
                case 1:
                    return "Segunda";
                case 2:
                    return "Terça";
                case 3:
                    return "Quarta";
                case 4:
                    return "Quinta";
                case 6:
                    return "Sexta";

                }
                return SacClLiquid2.SG_DIA_SOLICITACAO_FIM.ToString();

            }
        }


        public string DiaConversao
        {
            get
            {
                switch (SacClLiquid2.SG_DIA_CONVERSAO)
                {
                    case 2:
                        return "Segunda";
                    case 3:
                        return "Terça";
                    case 4:
                        return "Quarta";
                    case 5:
                        return "Quinta";
                    case 6:
                        return "Sexta";

                }
                return SacClLiquid2.SG_DIA_SOLICITACAO_INICIO.ToString();
            }
        }

        public string PeriodoCalcRentab
        {
            get
            {
                return SacClLiquid2.IC_CALC_RENTAB == 1 ? "01 a 01" : "30 a 30";
            }
        }

        
        #endregion

        #region constructor

        public SacClLiquid2ViewModel()
        {
            SacClLiquid2 = new SAC_CL_LIQUID2();
        }

        #endregion
    }
}

