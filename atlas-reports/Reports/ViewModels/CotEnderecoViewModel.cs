﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class CotEnderecoViewModel : BaseViewModel.BaseViewModel
    {
        #region Properties
        public COT_ENDERECO CotEndereco { get; set; }

        public string TipoEndereco
        {
            get { return CotEndereco.COT_TIPO_ENDERECO.DS_TIPO_ENDERECO; }
        }

        public string Logradouro
        {
            get { return CotEndereco.DS_LOGRADOURO; }
        }

        public string Bairro
        {
            get { return CotEndereco.NM_BAIRRO; }
        }
        public string Cidade
        {
            get { return CotEndereco.NM_CIDADE; }
        }
        public string Uf
        {
            get { return CotEndereco.SG_UF; }
        }
        public string Cep
        {
            get { return CotEndereco.NR_CEP; }
        }
        public string CxPostal
        {
            get { return CotEndereco.NR_CAIXA_POSTAL; }
        }
        public string Telefone
        {
            get { return CotEndereco.NR_TELEFONE; }
        }
        public string Ramal
        {
            get { return CotEndereco.NR_RAMAL; }
        }
        public string Fax
        {
            get { return CotEndereco.NR_FAX; }
        }
        public string Email
        {
            get { return CotEndereco.DS_EMAIL; }
        }
        public string Telex
        {
            get { return CotEndereco.NR_TELEX; }
        }
        public string Estrangeiro
        {
            get { return CotEndereco.IC_SN_ESTRANGEIRO; }
        }
        public string Pais
        {
            get { return CotEndereco.DS_PAIS; }
        }
        public string EstadoEstrangeiro
        {
            get { return CotEndereco.DS_ESTADO_ESTRANGEIRO; }
        }
        public string ZipCode
        {
            get { return CotEndereco.DS_ZIP_CODE; }
        }
        public string SeComprovado
        {
            get { return CotEndereco.IC_SN_COMPROVADO; }
        }

        #endregion

        #region constructor

        public CotEnderecoViewModel()
        {
            CotEndereco = new COT_ENDERECO();
        }

        #endregion
    }
}
