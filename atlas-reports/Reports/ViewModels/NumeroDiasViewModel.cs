﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class NumeroDiasViewModel
    {
        private int _local = 1;

        public DateTime BusinessDay(DateTime date, int count)
        {

            using (var db = new SacEntities(DbContextFactory.ConnectString))
            {

                var result = (from nrdias in db.SAC_BA_NUMERO_DIAS
                              where nrdias.DT == date &&
                                    nrdias.ID_LOCAL == _local
                              select new
                                     {
                                         ddBanco = nrdias.DD_BANCO,
                                         icUtilBanco = nrdias.IC_UTIL_BANCO

                                     }).FirstOrDefault();
                if (result == null)
                    return date;

                if (count == 0)
                {
                    if (result.icUtilBanco == "S")
                        return date;
                    else
                    {
                        count = result.ddBanco + 1;

                        var resultdate = (from nrdias in db.SAC_BA_NUMERO_DIAS
                                          where nrdias.DD_BANCO == count &&
                                                nrdias.ID_LOCAL == _local &&
                                                nrdias.IC_UTIL_BANCO == "S"
                                          select new
                                                 {
                                                     Date = nrdias.DT
                                                 }).FirstOrDefault();
                        if (resultdate == null)
                            return date;

                        return resultdate.Date;

                    }
                }

                int finalCount = count == -1 && result.icUtilBanco == "N" ? result.ddBanco : result.ddBanco + count;

                var resultRet = (from nrdias in db.SAC_BA_NUMERO_DIAS
                                 where nrdias.DD_BANCO == finalCount &&
                                        nrdias.ID_LOCAL == _local &&
                                        nrdias.IC_UTIL_BANCO == "S"
                                  select new
                                  {
                                      Date = nrdias.DT
                                  }).FirstOrDefault();
                if (resultRet == null)
                    return date;

                return resultRet.Date;
            }
        }
    }
}
