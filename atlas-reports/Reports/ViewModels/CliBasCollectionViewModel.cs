﻿using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Atlas.Link;
using Atlas.Link.Settings;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Report;
using DevExpress.Data.Helpers;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Editors.Helpers;
using DevExpress.Xpf.Printing;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Core;
using Application = System.Windows.Application;

namespace Atlas.Reports.ViewModels
{
    public class CliBasCollectionViewModel : BaseSacViewModel
    {
        #region fields
        private readonly ObservableCollection<CliBasViewModel> _selectClients = new ObservableCollection<CliBasViewModel>();
        private CliBasViewModel _selectedCliBas { get; set; }
        #endregion

        #region Properties
        public ObservableCollection<CliBasViewModel> SelectClients { get { return this._selectClients; } }
        public ObservableCollection<CliBasViewModel> CliBasList { get; set; }
        public ObservableCollection<SacBaScriptViewModel> ScriptList { get; set; }
        public DelegateCommand<object> DeleteSelectedRowsCommand { get; private set; }
        public ObservableCollection<ReportViewModel> Reports { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public CliBasViewModel SelectedCliBas
        {
            get { return _selectedCliBas; }
            set
            {
                _selectedCliBas = value;
                RaisePropertyChanged("SelectedCliBas");
            }
        }

        #endregion

        #region get the data

        protected override void GetData()
        {
            var cliBasVmList = new ObservableCollection<CliBasViewModel>();
            cliBasVmList = Cache.EntityCache.InstanceSac().CliBasList();

            if (cliBasVmList == null || cliBasVmList.Count == 0)
            {
                Console.WriteLine("erro");
                DXMessageBox.Show("Não foi retornado nenhum cliente, verifique arquivo o app.config\nTag <database name=\"SAC\"", "Falha ao ler cadastro de clientes", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //var clibasList = (from p in Db.CLI_BAS orderby p.NOME select p).ToList();
            //foreach (var item in clibasList)
            //{
            //    cliBasVmList.Add(new CliBasViewModel { IsNew = false, CliBas = item });
            //}

            CliBasList = cliBasVmList;

            var sacBaScript = new SacBaScriptCollectionViewModel();
            ScriptList = sacBaScript.SacBaScriptdistinctList;

            RaisePropertyChanged("CliBasList");
            RaisePropertyChanged("SacBaScript");
        }

        #endregion

        #region Constructor

        public CliBasCollectionViewModel()
            : base()
        {

            StartDate = DateTime.UtcNow.Date;
            EndDate = DateTime.UtcNow.Date;

            Reports = new ObservableCollection<ReportViewModel>();
            var result = Enums.Enums.EnumToList<Enums.Enums.Reports>();

            result.ForEach( x=> Reports.Add(new ReportViewModel(x)));
            
            GetData();
        }


        #endregion

        #region Commands

        [Command]
        public void Print(object parameter)
        {
            var parm = parameter.ToString();
            var reportType = parm == "Print" ? Enums.Enums.ReportType.Report : Enums.Enums.ReportType.Excel;
            string folder = string.Empty;

            List<string> clients = new List<string>();
            SelectClients.ForEach(x => clients.Add(x.CliBas.CODCLI));

            if (!Validate(clients))
                return;

            if (reportType == Enums.Enums.ReportType.Excel)
            {
                ApplicationConfig appConfig = SettingsManager.Current.GetApplicationConfig();
                
                var dialog = new FolderBrowserDialog();
                dialog.SelectedPath = appConfig.XlsFolder;

                var result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                folder = dialog.SelectedPath;
                appConfig.XlsFolder = folder;
                SettingsManager.Current.SetApplicationConfig(appConfig);

            }


            var mainReport = new XtraReport();
            mainReport.ExportOptions.Xls.ExportMode = XlsExportMode.SingleFile;

            mainReport.CreateDocument(false);

            int totalRows = 0;
            var nrDias = new NumeroDiasViewModel();

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.CarteiraDiaria && t.IsSelected))
            {
                foreach (var client in clients)
                {
                    for (DateTime date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))
                    {
                        var cdReport = new CarteiraDiariaMainReportcs(client, date, reportType, folder);

                        if (cdReport.RowCount > 0 && reportType == Enums.Enums.ReportType.Report)
                        {
                            cdReport.CreateDocument(false);
                            mainReport.Pages.AddRange(cdReport.Pages);
                            totalRows += cdReport.RowCount;
                        }
                    }
                }

            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.DemoCaixa && t.IsSelected))
            {
                var demoCxReport = new DemoCaixaReport(clients, StartDate, EndDate, reportType, folder);

                if (reportType == Enums.Enums.ReportType.Report)
                {
                    demoCxReport.CreateDocument(false);
                    mainReport.Pages.AddRange(demoCxReport.Pages);
                    totalRows += demoCxReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.RentabPatrimonial && t.IsSelected))
            {
                var rentabPatriReport = new RentabPatrimonialReport(clients, StartDate, EndDate, reportType, folder);

                if (reportType == Enums.Enums.ReportType.Report)
                {
                    rentabPatriReport.CreateDocument(false);
                    mainReport.Pages.AddRange(rentabPatriReport.Pages);
                    totalRows += rentabPatriReport.RowCount;
                }
            }


            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.PosicaoOutrosFundos && t.IsSelected))
            {
                var posOutrosReport = new PosicaoOutrosReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    posOutrosReport.CreateDocument(false);
                    mainReport.Pages.AddRange(posOutrosReport.Pages);

                    totalRows += posOutrosReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.MovimentoOutrosFundos && t.IsSelected))
            {
                var movOutrosReport = new MovimentoOutrosReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    movOutrosReport.CreateDocument(false);
                    mainReport.Pages.AddRange(movOutrosReport.Pages);

                    totalRows += movOutrosReport.RowCount;
                }
            }


            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.MovimentoRendaVariavel && t.IsSelected))
            {
                var moRvReport = new MovimentoRendaVariavelReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    moRvReport.CreateDocument(false);
                    mainReport.Pages.AddRange(moRvReport.Pages);

                    totalRows += moRvReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.PosicaoRendaVariavel && t.IsSelected))
            {
                var posRvReport = new PosicaoRendaVariavelReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    posRvReport.CreateDocument(false);
                    mainReport.Pages.AddRange(posRvReport.Pages);

                    totalRows += posRvReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.OperacoesRendaFixa && t.IsSelected))
            {
                var operRfReport = new OperacoesRendaFixaReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    operRfReport.CreateDocument(false);
                    mainReport.Pages.AddRange(operRfReport.Pages);

                    totalRows += operRfReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.PosicaoRendaFixa && t.IsSelected))
            {
                var posRfReport = new PosicaoRendaFixaReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    posRfReport.CreateDocument(false);
                    mainReport.Pages.AddRange(posRfReport.Pages);

                    totalRows += posRfReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.MovimentoFuturos && t.IsSelected))
            {
                var movFuReport = new MovimentoFuturosReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    movFuReport.CreateDocument(false);
                    mainReport.Pages.AddRange(movFuReport.Pages);

                    totalRows += movFuReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.PosicaoFuturos && t.IsSelected))
            {
                var posFuReport = new PosicaoFuturoReport(clients, StartDate, EndDate, reportType, folder);

                if (reportType == Enums.Enums.ReportType.Report)
                {
                    posFuReport.CreateDocument(false);
                    mainReport.Pages.AddRange(posFuReport.Pages);

                    totalRows += posFuReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.MovimentoSwap && t.IsSelected))
            {
                var movSwReport = new MovimentoSwapReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    movSwReport.CreateDocument(false);
                    mainReport.Pages.AddRange(movSwReport.Pages);

                    totalRows += movSwReport.RowCount;
                }
            }

            if (Reports.Exists(t => t.ReportEnum == Enums.Enums.Reports.PosicaoSwap && t.IsSelected))
            {
                var posSwReport = new PosicaoSwapReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    posSwReport.CreateDocument(false);
                    mainReport.Pages.AddRange(posSwReport.Pages);

                    totalRows += posSwReport.RowCount;
                }
            }

            if (reportType == Enums.Enums.ReportType.Report)
            {
                if (totalRows == 0)
                {
                    MessageBoxService.Show("Não ha dados para imprimir.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                var mainWindow = Application.Current.Windows
                    .OfType<Window>().FirstOrDefault(x => x.GetType().Name == "ApplicationMainWindow");

                var model = new XtraReportPreviewModel(mainReport);

                var window = new DocumentPreviewWindow()
                             {
                                 Model = model,
                                 WindowState = WindowState.Maximized,
                                 ShowIcon = true,
                                 ShowActivated = true,
                                 ShowInTaskbar = true,
                                 Owner = mainWindow
                             };

                window.ShowDialog();
            }

            else
            {
                var result = MessageBoxService.Show("Deseja abrir o diretorio destino?", "Exportacao XLS", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    Process.Start(folder);
                }

            }
        }

        #endregion

        #region Methods

        private bool Validate(List<string> clients)
        {
            if (clients.Count == 0)
            {
                MessageBoxService.Show("Selecione ao menos um cliente.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (StartDate > EndDate)
            {
                MessageBoxService.Show("Periodo Inválido.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (!Reports.Exists(t => t.IsSelected))
            {
                MessageBoxService.Show("Selecione ao menos um relatório.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return true;
        }
        #endregion
    }
}
