﻿using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class SacFiCadastroViewModel : Atlas.Reports.BaseViewModel.BaseViewModel
    {
        #region Properties

        public SAC_FI_CADASTRO SacFiCadastro { get; set; }

        
        #endregion

        #region constructor

        public SacFiCadastroViewModel()
        {
            SacFiCadastro = new SAC_FI_CADASTRO();
        }

        #endregion
    }
}

