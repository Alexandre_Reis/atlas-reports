﻿using System.Collections.ObjectModel;
using System.Linq;
using System;
using System.Diagnostics;
using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class CotCotistaViewModel : BaseViewModel.BaseViewModel
    {
        #region Properties

        public ObservableCollection<CotEnderecoViewModel> CotEnderecos { get; set; }
        public ObservableCollection<CotContaCorrenteViewModel> ContaCorrentes { get; set; }

        public COT_COTISTA CotCotista { get; set; }

        public string Nome
        {
            get { return CotCotista.COT_CLIENTE.NM_CLIENTE; }
        }

        public string TipoCotista
        {
            get
            {
                return CotCotista.COT_TIPO_COTISTA.DS_TIPO_COTISTA;
            }
        }

        public string CdCotista
        {
            get { return CotCotista.CD_COTISTA; }
        }

        public string CdDistribuidor
        {
            get { return CotCotista.CD_DISTRIBUIDOR; }
        }

        public string CdOperador
        {
            get { return CotCotista.CD_OPERADOR; }
        }

        public string CdCliente
        {
            get { return CotCotista.CD_CLIENTE; }
        }

        public string CriterioResgate
        {
            get
            {
                switch (CotCotista.CD_CRITERIO_RESGATE)
                {
                    case "F":
                        return "FIFO";
                    case "0":
                        return "0";
                    case "I":
                        return "LIFO";

                    default:
                        return "";
                }
            }
        }

        public string Titularidade
        {
            get { return ""; }
        }

        public string SeAtivo
        {
            get { return CotCotista.IC_SN_ATIVO == "S" ? "Sim" : "Não"; }
        }

        public string SeInvestidorQualificado
        {
            get { return CotCotista.IC_SN_INV_QUALIFICADO == "S" ? "Sim" : "Não"; }
        }

        public DateTime? DataInclusao
        {
            get { return CotCotista.DT_INCLUSAO; }
        }

        public string CdPerfilInvestimento
        {
            get { return CotCotista.CD_PERFIL_INVEST; }
        }

        public string ClassificacaoCvm
        {
            get { return CotCotista.COT_COTISTA_CLASSIFICACAO.DS_CLASSIFICACAO; }
        }

        public string CoTitular
        {
            get { return ""; }
        }

        public string TipoPessoa
        {
            get { return CotCotista.COT_CLIENTE.IC_FJ_PESSOA == "F" ? "Fisica" : "Juridica"; }
        }

        public String CpfCnpj
        {
            get
            {
                switch (CotCotista.COT_CLIENTE.IC_FJ_PESSOA)
                {
                    case "J":
                        return Convert.ToUInt64(CotCotista.COT_CLIENTE.NO_CGC).ToString(@"00\.000\.000\/0000\-00");
                    case "F":
                        return Convert.ToUInt64(CotCotista.COT_CLIENTE.NO_CPF).ToString(@"000\.000\.000\-00");

                    default:
                    return "";
                }
            }
        }

        public string SeIsento
        {
            get { return "Não"; }
        }

        public decimal AliquotaIr
        {
            get { return 0; }
        }
        
        #endregion

        #region constructor

        public CotCotistaViewModel()
        {
            CotCotista = new COT_COTISTA();
        }

        #endregion
    }
}

