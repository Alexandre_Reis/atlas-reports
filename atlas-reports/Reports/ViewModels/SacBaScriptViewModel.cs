﻿using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class SacBaScriptViewModel : Atlas.Reports.BaseViewModel.BaseViewModel
    {
        #region Properties

        public SAC_BA_SCRIPT SacBaScript { get; set; }

        #endregion

        #region constructor

        public SacBaScriptViewModel()
        {
            SacBaScript = new SAC_BA_SCRIPT();
        }

        #endregion
    }
}

