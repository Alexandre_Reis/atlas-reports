﻿using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
   public  class CotContaCorrenteViewModel : BaseViewModel.BaseViewModel
    {
        #region Properties
        public COT_CONTA_EXTERNA CotContaCorrente { get; set; }

       public string Banco
       {
            get { return CotContaCorrente.ID_BANCO.ToString(); }
       }

       public string Agencia
       {
           get { return CotContaCorrente.CD_AGENCIA_EXTERNA; }
       }

        public string Titularidade
        {
            get { return CotContaCorrente.DS_TITULARIDADE; }
        }

        public string Contacorrente
       {
            get { return string.IsNullOrEmpty(CotContaCorrente.DV_CONTA_EXTERNA) ? CotContaCorrente.CD_CONTA_EXTERNA : string.Format("{0}-{1}", CotContaCorrente.CD_CONTA_EXTERNA.Trim(), CotContaCorrente.DV_CONTA_EXTERNA.Trim()); }
       }

        #endregion
    }
}
