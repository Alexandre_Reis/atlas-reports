﻿
using Atlas.Link.Support;

namespace Atlas.Reports.ViewModels
{
    public class ReportViewModel : NotifyUiBase
    {
        private Enums.Enums.Reports _reportEnum;
        private Enums.Enums.ReportsCot _reportEnumCot;

        public ReportViewModel(Enums.Enums.Reports reportEnum)
        {
            _reportEnum = reportEnum;
        }

        public ReportViewModel(Enums.Enums.ReportsCot reportEnum)
        {
            _reportEnumCot = reportEnum;
        }

        public string ReportName
        {
            get { return Enums.Enums.GetEnumDescription(_reportEnum); }
        }

        public Enums.Enums.Reports ReportEnum
        {
            get { return _reportEnum; }
        }

        public string ReportNameCot
        {
            get { return Enums.Enums.GetEnumDescription(_reportEnumCot); }
        }

        public Enums.Enums.ReportsCot ReportEnumCot
        {
            get { return _reportEnumCot; }
        }

        public bool IsSelected { get; set; }
    }
}
