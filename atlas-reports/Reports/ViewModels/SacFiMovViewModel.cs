﻿using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class SacFiMovViewModel :  Atlas.Reports.BaseViewModel.BaseViewModel
    {
        #region Properties

        public SAC_FI_MOV SacFiMov { get; set; }

        
        #endregion

        #region constructor

        public SacFiMovViewModel()
        {
            SacFiMov = new SAC_FI_MOV();
        }

        #endregion
    }
}

