﻿using System.Linq;
using System.Collections.ObjectModel;
using System.Data.Entity;
using Atlas.Link;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Model;

namespace Atlas.Reports.ViewModels
{
    public class SacBaScriptCollectionViewModel : BaseSacViewModel
    {
        #region Properties

        public SacBaScriptViewModel SelectedSacBaScript { get; set; }
        public ObservableCollection<SacBaScriptViewModel> SacBaScriptList { get; set; }
        public ObservableCollection<SacBaScriptViewModel> SacBaScriptdistinctList { get; set; }

        #endregion

        #region Load the data
        protected override void GetData()
        {
            var sacBaScriptVmList = new ObservableCollection<SacBaScriptViewModel>();

            var sacBaScriptList = (from p in Db.SAC_BA_SCRIPT orderby p.CD select p).ToList();

            foreach (var item in sacBaScriptList)
            {
                sacBaScriptVmList.Add(new SacBaScriptViewModel { IsNew = false, SacBaScript = item });

                if (!SacBaScriptdistinctList.Exists(t => t.SacBaScript.CD == item.CD))
                {
                    SacBaScriptdistinctList.Add(new SacBaScriptViewModel { IsNew = false, SacBaScript = item });
                }

            }

            SacBaScriptList = sacBaScriptVmList;

            RaisePropertyChanged("SacBaScriptList");
            RaisePropertyChanged("SacBaScriptdistinctList");
            
        }

        #endregion

        #region Constructor

        public SacBaScriptCollectionViewModel()
            : base()
        {
            SacBaScriptdistinctList = new ObservableCollection<SacBaScriptViewModel>();

            GetData();
        }

        #endregion

    }
}
