﻿using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Atlas.Link;
using Atlas.Link.Settings;
using Atlas.Reports.BaseViewModel;
using Atlas.Reports.Report;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Editors.Helpers;
using DevExpress.Xpf.Printing;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using Atlas.Reports.ReportCot;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Core;
using Application = System.Windows.Application;

namespace Atlas.Reports.ViewModels
{
    public class CotCotistaCollectionViewModel : BaseCotViewModel
    {
        #region fields
        private readonly ObservableCollection<CotCotistaViewModel> _SelectCotistas = new ObservableCollection<CotCotistaViewModel>();
        private CotCotistaViewModel _selectedCotista { get; set; }
        #endregion

        #region Properties
        public ObservableCollection<CotCotistaViewModel> SelectCotistas { get { return this._SelectCotistas; } }
        public ObservableCollection<CotCotistaViewModel> CotistaList { get; set; }
        public ObservableCollection<SacBaScriptViewModel> ScriptList { get; set; }
        public DelegateCommand<object> DeleteSelectedRowsCommand { get; private set; }
        public ObservableCollection<ReportViewModel> Reports { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public CotCotistaViewModel SelectedCotCotista
        {
            get { return _selectedCotista; }
            set
            {
                _selectedCotista = value;
                RaisePropertyChanged("SelectedCotCotista");
            }
        }

        #endregion

        #region get the data

        protected override void GetData()
        {
            try
            {
                var cliBasVmList = new ObservableCollection<CotCotistaViewModel>();
                cliBasVmList = Cache.EntityCache.InstanceCot().CotCotistaList();

                if (cliBasVmList == null || cliBasVmList.Count == 0)
                {
                    Console.WriteLine("erro");
                    DXMessageBox.Show("Não foi retornado nenhum cotista, verifique arquivo o app.config\nTag <database name=\"COT\"", "Falha ao ler cadastro de cotistas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                //var CotistaList = (from p in Db.CLI_BAS orderby p.NOME select p).ToList();
                //foreach (var item in CotistaList)
                //{
                //    cliBasVmList.Add(new CliBasViewModel { IsNew = false, CliBas = item });
                //}

                CotistaList = cliBasVmList;

                //var sacBaScript = new SacBaScriptCollectionViewModel();
                ScriptList = null;

                RaisePropertyChanged("CotistaList");
                RaisePropertyChanged("SacBaScript");

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        #endregion

        #region Constructor

        public CotCotistaCollectionViewModel()
            : base()
        {

            StartDate = DateTime.UtcNow.Date;
            EndDate = DateTime.UtcNow.Date;

            Reports = new ObservableCollection<ReportViewModel>();
            var result = Enums.Enums.EnumToList<Enums.Enums.ReportsCot>();

            result.ForEach(x => Reports.Add(new ReportViewModel(x)));

            GetData();
        }


        #endregion

        #region Commands

        [Command]
        public void Print(object parameter)
        {
            var parm = parameter.ToString();
            var reportType = parm == "Print" ? Enums.Enums.ReportType.Report : Enums.Enums.ReportType.Excel;
            string folder = string.Empty;

            List<string> clients = new List<string>();
            SelectCotistas.ForEach(x => clients.Add(x.CdCotista));

            if (!Validate(clients))
                return;

            if (reportType == Enums.Enums.ReportType.Excel)
            {
                ApplicationConfig appConfig = SettingsManager.Current.GetApplicationConfig();

                var dialog = new FolderBrowserDialog();
                dialog.SelectedPath = appConfig.XlsFolder;

                var result = dialog.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;

                folder = dialog.SelectedPath;
                appConfig.XlsFolder = folder;
                SettingsManager.Current.SetApplicationConfig(appConfig);

            }


            var mainReport = new XtraReport();
            mainReport.ExportOptions.Xls.ExportMode = XlsExportMode.SingleFile;

            mainReport.CreateDocument(false);

            int totalRows = 0;
            var nrDias = new NumeroDiasViewModel();

            if (Reports.Exists(t => t.ReportEnumCot == Enums.Enums.ReportsCot.Movimento && t.IsSelected))
            {
                var movOutrosReport = new CotMovimentoReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    movOutrosReport.CreateDocument(false);
                    mainReport.Pages.AddRange(movOutrosReport.Pages);

                    totalRows += movOutrosReport.RowCount;
                }

            }
            if (Reports.Exists(t => t.ReportEnumCot == Enums.Enums.ReportsCot.Posicao && t.IsSelected))
            {
                var movOutrosReport = new CotPosicaoReport(clients, StartDate, EndDate, reportType, folder);
                if (reportType == Enums.Enums.ReportType.Report)
                {
                    movOutrosReport.CreateDocument(false);
                    mainReport.Pages.AddRange(movOutrosReport.Pages);

                    totalRows += movOutrosReport.RowCount;
                }

            }

            if (reportType == Enums.Enums.ReportType.Report)
            {
                if (totalRows == 0)
                {
                    MessageBoxService.Show("Não ha dados para imprimir.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                var mainWindow = Application.Current.Windows
                    .OfType<Window>().FirstOrDefault(x => x.GetType().Name == "ApplicationMainWindow");

                var model = new XtraReportPreviewModel(mainReport);

                var window = new DocumentPreviewWindow()
                {
                    Model = model,
                    WindowState = WindowState.Maximized,
                    ShowIcon = true,
                    ShowActivated = true,
                    ShowInTaskbar = true,
                    Owner = mainWindow
                };

                window.ShowDialog();
            }

            else
            {
                var result = MessageBoxService.Show("Deseja abrir o diretorio destino?", "Exportacao XLS", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    Process.Start(folder);
                }

            }
        }

        #endregion

        #region Methods

        private bool Validate(List<string> clients)
        {
            if (clients.Count == 0)
            {
                MessageBoxService.Show("Selecione ao menos um cliente.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (StartDate > EndDate)
            {
                MessageBoxService.Show("Periodo Inválido.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (!Reports.Exists(t => t.IsSelected))
            {
                MessageBoxService.Show("Selecione ao menos um relatório.", "Relatórios", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return true;
        }
        #endregion
    }
}
