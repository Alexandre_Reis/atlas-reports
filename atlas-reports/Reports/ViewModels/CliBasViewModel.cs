﻿using System.Collections.ObjectModel;
using System.Linq;
using System;
using Atlas.Reports.EntityModel;

namespace Atlas.Reports.ViewModels
{
    public class CliBasViewModel : BaseViewModel.BaseViewModel
    {
        #region Properties

        public ObservableCollection<SacClLiquid2ViewModel> SacClLiquid2 { get; set; } 

        public CLI_BAS CliBas { get; set; }

        public string Tipo
        {
            get
            {
                switch (CliBas.TIPO.Trim())
                {
                case "J":
                        return "Pessoa Juridica";
                case "P":
                        return "Fundação";
                case "S":
                        return "Seguradora";
                case "X":
                        return "Consolidação";
                case "M":
                    return "Fundo Mutuo";
                default:
                    return CliBas.TIPO;
                }
            }
        }

        public string CodCli
        {
            get { return CliBas.CODCLI; }
        }

        public String CpfCnpj
        {
            get
            {
                string sId = CliBas.CGC ?? "0";

                sId = sId.Replace(".", "").Replace("-", "").Replace("/", "").Replace(" ", "");

                switch (CliBas.TIPO)
                {
                case "M":
                case "J":
                case "S":
                case "X":
                    return Convert.ToUInt64(sId).ToString(@"00\.000\.000\/0000\-00");
                case "P":
                    return Convert.ToUInt64(sId).ToString(@"000\.000\.000\-00");

                default:
                    return CliBas.CGC;
                }
            }
        }

        public string IcCarteiraPropria
        {
            get
            {
                switch (CliBas.IC_CARTEIRA_PROPRIA)
                {
                case "F":
                    return "Fundo";
                case "A":
                    return "Administrada";
                case "P":
                    return "Própria";
                case "T":
                    return "Clientes de Terceiros";
                case "R":
                    return "Clientes Próprios";
                }

                return CliBas.IC_CARTEIRA_PROPRIA;
            }
        }

        public string Nivel
        {
            get
            {
                switch (CliBas.NIVEL)
                {
                    case "1":
                        return "Cliente filho";
                    case "2":
                        return "Cliente mãe";
                    case "3":
                        return "Cliente mãe c/movimento";
                }

                return CliBas.NIVEL;
            }
        }

        public string ColaMovimento
        {
            get
            {
                switch (CliBas.IC_MAEMOVIMENTO)
                {
                    case "S":
                        return "Sim";
                    case "N":
                        return "Não";
                }

                return CliBas.IC_MAEMOVIMENTO;
            }
        }


        public string TipoCota
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_CL_LIQUID2
                                      where p.CLCLI_CD == CliBas.CODCLI &&
                                            p.DT_VALIDADE_FIM == null
                                      select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.SG_TIPOCOTA == "A" ? "Abertura" : "Fechamento";
                    }

                    return "";

                }
            }
        }

        public string ClassifBacen
        {
            get
            {
                switch (CliBas.CD_BACEN_PESP500)
                {
                case "FICE":
                    return "Fdo Invest.- Cap. Estrang.";
                case "FCCI":
                    return "Fdo Conv Cap Estrang-Incentiv";
                case "FCCN":
                    return "Fdo ConvCapEstrang-NãoIncen";
                case "FTVM":
                    return "Fdo Invest. em Tit. Val. Mobil.";
                }
                return CliBas.CD_BACEN_PESP500;
            }
        }

        public string DescSocio
        {
            get
            {
                switch (CliBas.SOCIOBMF)
                {
                case "B":
                    return "BM&F";
                case "C":
                    return "CETIP";
                case "T":
                    return "Todos";
                case "N":
                    return "Nenhum";
                }
                return CliBas.SOCIOBMF;
            }
        }

        public string IcDiacot
        {
            get
            {
                return CliBas.IC_DIACOT == 0 ? "RV/FU em D+0" : "RV em D-1";
            }
        }

        public string BolsaParaPreco
        {
            get
            {


                switch (CliBas.IC_RV_COTACAO)
                {
                    case "S":
                        return "BOVESPA";
                    case "R":
                        return "BVRJ";
                    case "M":
                        return "Maior Volume";
                }
                return CliBas.IC_RV_COTACAO;
            }
        }

        public string ClienteNaoResidente
        {
            get
            {
                switch (CliBas.SG_RESIDENTE)
                {
                    case "N":
                        return "Não";
                    case "P":
                        return "Carteira Própria";
                    case "C":
                        return "Carteira Coletiva";
                    case "G":
                        return "Passageiro";
                }
                return CliBas.SG_RESIDENTE;
            }
        }
        public string TipoRv
        {
            get
            {
                return CliBas.TIPORV == "M" ? "RV pela Média" : "RV pelo Fechamento";
            }
        }

        public string ClienteEncerrado
        {
            get
            {
                return CliBas.FLAGDEL == 1 ? "Sim" : "Não";
            }
        }

        public string ColaPosicao
        {
            get
            {
                return CliBas.IC_COLAGEM_PROC == "S" ? "Sim" : "Não";
            }
        }

        public string CcPropria
        {
            get
            {
                return CliBas.IC_CONTA_PROPRIA_SELIC == "S" ? "Sim" : "Não";
            }
        }

        public string UtilizaCci
        {
            get
            {
                return CliBas.IC_UTILIZA_CCI == "S" ? "Sim" : "Não";
            }
        }
        public string ContaAplicacao
        {
            get { return string.Format("{0} {1} {2}", string.IsNullOrEmpty(CliBas.CD_AGENCIA_1) ? "" : string.Format("Agência: {0}", CliBas.CD_AGENCIA_1),
                                                                    string.IsNullOrEmpty(CliBas.NO_CC_1) ? "" : string.Format("Conta Corrente: {0}", CliBas.NO_CC_1),
                                                                    string.IsNullOrEmpty(CliBas.CD_DV_1) ? "" : string.Format("Digito: {0}", CliBas.CD_DV_1));
            }
        }

        public string ContaResgate
        {
            get
            {
                return string.Format("{0} {1} {2}", string.IsNullOrEmpty(CliBas.CD_AGENCIA_2) ? "" : string.Format("Agência: {0}", CliBas.CD_AGENCIA_2),
                                                                  string.IsNullOrEmpty(CliBas.NO_CC_2) ? "" : string.Format("Conta Corrente: {0}", CliBas.NO_CC_2),
                                                                  string.IsNullOrEmpty(CliBas.CD_DV_2) ? "" : string.Format("Digito: {0}", CliBas.CD_DV_2));
            }
        }

        public string ContaTributada
        {
            get
            {
                return string.Format("{0} {1} {2}", string.IsNullOrEmpty(CliBas.CD_AGENCIA_3) ? "" : string.Format("Agência: {0}", CliBas.CD_AGENCIA_3),
                                                                  string.IsNullOrEmpty(CliBas.NO_CC_3) ? "" : string.Format("Conta Corrente: {0}", CliBas.NO_CC_3),
                                                                  string.IsNullOrEmpty(CliBas.CD_DV_3) ? "" : string.Format("Digito: {0}", CliBas.CD_DV_3));
            }
        }

        public string ContaIsenta
        {
            get
            {
                return string.Format("{0} {1} {2}", string.IsNullOrEmpty(CliBas.CD_AGENCIA_4) ? "" : string.Format("Agência: {0}", CliBas.CD_AGENCIA_4),
                                                                  string.IsNullOrEmpty(CliBas.NO_CC_4) ? "" : string.Format("Conta Corrente: {0}", CliBas.NO_CC_4),
                                                                  string.IsNullOrEmpty(CliBas.CD_DV_4) ? "" : string.Format("Digito: {0}", CliBas.CD_DV_4));
            }
        }

        public string FormaApropriacao
        {
            get
            {
                return CliBas.LINEXP == "L" ? "Linear" : "Exponencial";
            }
        }

        public string CalculaPatrimonioAte
        {
            get
            {
                return CliBas.CALCPATUDC == "S" ? "o último dia corrido" : "o último dia útil";
            }
        }

        public string PlcBma
        {
            get
            {
                return CliBas.IC_PLC == "S" ? "Sim" : "Não";
            }
        }

        public string ContagemDias
        {
            get
            {
                return CliBas.UTCOR == "U" ? "Dias Úteis" : "Dias Corridos";
            }
        }

        public string Gestor
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_ADMIN
                                  where p.CD == CliBas.CODADM
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.NM;
                    }

                    return CliBas.CODADM;
                }
            }

        }

        public string LocalFeriado
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_LOCAIS
                                  where p.ID == CliBas.ID_LOCAL
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.DS;
                    }

                    // ReSharper disable once SpecifyACultureInStringConversionExplicitly
                    return CliBas.ID_LOCAL.ToString();
                }
            }

        }

        public string Administrador
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_ADMINISTRACAO
                                  where p.CD_ADMINISTRACAO == CliBas.ICATUFININVEST
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return string.Format("{0} {1}", result.NM_ADMINISTRACAO, result.CD_ADMINISTRACAO);
                    }

                    return CliBas.ICATUFININVEST;
                }
            }

        }

        public string Custodiante
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.BAS_CAD_CONTROL
                                  where p.CD == CliBas.CD_CONTROL
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.DESCRICAO;
                    }

                    return CliBas.CD_CONTROL;
                }
            }

        }

        public string ControlDoGestor
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_GRUPO_ECONOMICO
                                  where p.CD == CliBas.CD_GRUPO_ECONOMICO
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.NM;
                    }

                    return CliBas.CD_GRUPO_ECONOMICO;
                }
            }
        }

        public string BancoLiquidante
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.BAS_CAD_IF
                                  where p.CODINST == CliBas.CD_BCO_LIQUIDANTE
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.NOMEINST;
                    }

                    return CliBas.CD_BCO_LIQUIDANTE;
                }
            }
        }


        public string ResponsavelRf
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_RESPONSAVEL_GESTAO
                                  where p.CD_RESPONSAVEL == CliBas.CD_RESPONSAVEL_RF
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.NM_AGENTE;
                    }

                    return CliBas.CD_RESPONSAVEL_RF;
                }
            }
        }

        public string ResponsavelRv
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_RESPONSAVEL_GESTAO
                                  where p.CD_RESPONSAVEL == CliBas.CD_RESPONSAVEL_RV
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.NM_AGENTE;
                    }

                    return CliBas.CD_RESPONSAVEL_RV;
                }
            }
        }

        public string TipoContaBmf
        {
            get
            {
                switch (CliBas.CD_TIPO_CONTA)
                {
                    case "1":
                        return "Fundos";
                    case "2":
                        return "Carteira";
                    case "3":
                        return "CC5";
                    case "4":
                        return "DTVM";
                }
                return CliBas.CD_TIPO_CONTA;
            }
        }

        public string CalculaEmolumento
        {
            get
            {
                return CliBas.IC_DESCONTO_EMOLUMENTO == "S" ? "Fundos" : "Outros";
            }
        }

        public string ShortPapelAVista
        {
            get
            {
                return CliBas.IC_SN_PERMITIR_SHORT_RV == "S" ? "Sim" : "Não";
            }
        }

        public string RegistroOfereta
        {
            get
            {
                return CliBas.IC_REGISTRO_OFERTA == "S" ? "Sim" : "Não";
            }
        }

        public string ProventosEmpAcoes
        {
            get
            {
                return CliBas.IC_PROVENTOS_EA == "S" ? "Sim" : "Não";
            }
        }
        public string ProventosEmpAcoesTsr
        {
            get
            {
                return CliBas.IC_TES_PROV_EA == "A" ? "Analítico" : "Consolidado";
            }
        }
        public string EmprestimoAcoes
        {
            get
            {
                switch (CliBas.SG_PARTICIPANTE_EMPR_ACOES)
                {
                    case "N":
                        return "Não opera";
                    case "T":
                        return "Tomador";
                    case "D":
                        return "Doador";
                    case "A":
                        return "Tom./Doador";
                }
                return CliBas.SG_PARTICIPANTE_EMPR_ACOES;
            }
        }

        public string DayTradeAutomatico
        {
            get
            {
                switch (CliBas.SG_DT_AUTOMATICO.Trim())
                {
                    case "N":
                        return "Não";
                    case "RV":
                        return "RV";
                    case "FU":
                        return "FU";
                    case "S":
                        return "RV/FU";
                }
                return CliBas.SG_DT_AUTOMATICO;
            }
        }

        public string PucustoPubolsa
        {
            get
            {
                return CliBas.IC_RV_CUSTO == "S" ? "Sim" : "Não";
            }
        }

        public String Mes1
        {
            get
            {
                if (CliBas.MM_RV_CUSTO_1 == 0 ||CliBas.MM_RV_CUSTO_1 == null )
                    return "";

                int month = CliBas.MM_RV_CUSTO_1.GetValueOrDefault();

                if (System.Globalization.DateTimeFormatInfo.CurrentInfo != null)
                    return System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(month);

                return "";
            }
        }

        public String Mes2
        {
            get
            {
                if (CliBas.MM_RV_CUSTO_2 == 0 || CliBas.MM_RV_CUSTO_2 == null)
                    return "";

                int month = CliBas.MM_RV_CUSTO_2.GetValueOrDefault();

                if (System.Globalization.DateTimeFormatInfo.CurrentInfo != null)
                    return System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(month);

                return "";
            }
        }

        public String NotaCorretTsr
        {
            get
            {

                switch (CliBas.SG_FATURA_RV)
                {
                    case "U":
                        return "UNICA";
                    case "C":
                        return "CORRETORA";
                    case "1":
                        return "UNICA - 3 LANCAM.";
                    case "2":
                        return "CORRETORA - 3 LANCAM.";
                    case "3":
                        return "DEFAULT DO SISTEMA";
                }
                return CliBas.SG_FATURA_RV;
            }
        }

        public string ImpactoPlD1
        {
            get
            {
                return CliBas.IC_BANCO_CENTRAL == "S" ? "Sim" : "Não";
            }
        }

        public String TemRefaturamento
        {
            get
            {

                switch (CliBas.REFATURAMENTO)
                {
                    case "S":
                        return "RV / Futuros";
                    case "R":
                        return "Renda Variável";
                    case "F":
                        return "Futuros";
                    case "N":
                        return "Não";
                }
                return CliBas.REFATURAMENTO;
            }
        }
        public string FormaRefaturamento
        {
            get
            {
                return CliBas.SG_REFATURAMENTO == "N" ? "Sobre Nota" : "Sobre Corretora";
            }
        }

        public string ResgateFifo
        {
            get
            {
                return CliBas.IC_FIFO == "S" ? "Sim" : "Não";
            }
        }

        public string PuMedioRf
        {
            get
            {
                return CliBas.IC_CUSTO_MEDIO_RF == "S" ? "Sim" : "Não";
            }
        }

        public string DefasagemRf
        {
            get
            {
                return CliBas.IC_DEFASAGEM == "S" ? "Sim" : "Não";
            }
        }


        public String PerfilDaCarteira
        {
            get
            {

                switch (CliBas.SG_NEG_VENC)
                {
                    case "N":
                        return "Negociação";
                    case "V":
                        return "Vencimento";
                    case "M":
                        return "Mista";
                }
                return CliBas.SG_NEG_VENC;
            }
        }

        public String AlteraStatusPos
        {
            get
            {

                switch (CliBas.SG_ALTERACAO_NEG_VENC.Trim())
                {
                    case "TD":
                        return "Todos";
                    case "RF":
                        return "Renda Fixa";
                    case "RFSW":
                        return "RF e  SW-Associado";
                }
                return CliBas.SG_ALTERACAO_NEG_VENC;
            }
        }

        public string VendeTitulos
        {
            get
            {
                return CliBas.IC_NEG_VENC == "S" ? "Sim" : "Não";
            }
        }

        public string EnquadramentoBase
        {
            get
            {
                switch (CliBas.IC_PLENQ)
                {
                    case "P0":
                        return "PL em D+0";
                    case "P1":
                        return "PL em D-1";
                    case "A0":
                        return "Total Ativos D+0";
                    case "A1":
                        return "Total Ativos D-1";
                }
                return CliBas.IC_PLENQ;
            }
        }

        public string FormaCalculo
        {
            get
            {
                return CliBas.IC_NOTA_APLICACAO == "S" ? "Aberta por nota" : "Consolidada no fundo";
            }
        }
        public string UtilizaCotaOutrosFundos
        {
            get
            {
                return CliBas.IC_SN_ULTIMA_COTA_FI == "S" ? "Sim" : "Não";
            }
        }
        public string NegCotaMercado
        {
            get
            {
                return CliBas.IC_NEG_FDO_MERC == "S" ? "Sim" : "Não";
            }
        }

        public string CriterioParaResgate
        {
            get
            {

                switch (CliBas.SG_TIPO_PRIORIZ_NOTAS)
                {
                    case "P":
                        return "FIFO/Peps";
                    case "A":
                        return "Melhor Aniversário";
                    case "Q":
                        return "Menor Qtde. Cotas";
                    case "I":
                        return "Menor Imposto";
                    case "F":
                        return "Usa do Fundo";
                }
                return CliBas.SG_TIPO_PRIORIZ_NOTAS;
            }
        }

        public string ZeraSldTsr
        {
            get
            {

                switch (CliBas.IC_ZERA_SALDO_TSR)
                {
                    case "S":
                        return "Sim";
                    case "A":
                        return "Sim - Proc.";
                    case "N":
                        return "Não";
                    
                }
                return CliBas.IC_ZERA_SALDO_TSR;
            }
        }

        public string ZeraSldTsrCc
        {
            get
            {

                switch (CliBas.IC_ZERA_SALDO_TSR_CC)
                {
                    case "S":
                        return "Sim";
                    case "A":
                        return "Sim - Proc.";
                    case "N":
                        return "Não";

                }
                return CliBas.IC_ZERA_SALDO_TSR_CC;
            }
        }

        public string CaixaCriterio
        {
            get
            {

                switch (CliBas.SG_CRITERIO_ZERAGEM_B0)
                {
                    case "N":
                        return "Não zera saldo de CC ou CCI";
                    case "I":
                        return "Zera saldo de CCI";
                    case "C":
                        return "Zera saldo de CC e CCI";

                }
                return CliBas.SG_CRITERIO_ZERAGEM_B0;
            }
        }

        public string Tradushistorico
        {
            get
            {

                switch (CliBas.IC_TRADUZ_SN)
                {
                    case "D":
                        return "Default do Sistema";
                    case "N":
                        return "Não";
                    case "S":
                        return "Sim";

                }
                return CliBas.IC_TRADUZ_SN;
            }
        }

        public string ImpactaPl
        {
            get
            {
                return CliBas.IC_PATR_DIV == "S" ? "Sim" : "Não";
            }
        }

        public string CustoNaAcao
        {
            get
            {
                return CliBas.IC_DEDUZ_CUSTO_DIVIDENDO == "S" ? "Sim" : "Não";
            }
        }


        public string GeraEventos
        {
            get
            {

                switch (CliBas.IC_PROCESSA_CTB)
                {
                    case "S":
                        return "Sim";
                    case "N":
                        return "Não";
                    case "V":
                        return "FAQ Virtual";

                }
                return CliBas.IC_PROCESSA_CTB;
            }
        }

        public string Regime
        {
            get
            {
                return CliBas.IC_REGIME_CTB == "X" ? "Caixa" : "Competência";
            }
        }

        public string SegmentoCtb
        {
            get
            {

                switch (CliBas.SG_SEGMENTO_CTB)
                {
                    case "CA":
                        return "Carteira Administrada";
                    case "CT":
                        return "Carteira Cliente de Terceiros";
                    case "CP":
                        return "Carteira Cliente Próprio";
                    case "CC":
                        return "Carteira Coligada";
                    case "CF":
                        return "Carteira de Fundos";
                    case "CX":
                        return "Cart.de Fundo de Inverstimento - Exterior (FIEX)";
                    case "RF":
                        return "Carteira de Fundo de Investimento - RF";
                    case "RV":
                        return "Carteira de Fundo de Investimento - RV";
                    case "CI":
                        return "Carteira Institucional";
                    case "PB":
                        return "Carteira Private Bank";
                    case "CR":
                        return "Carteira Própria";
                }
                return CliBas.SG_SEGMENTO_CTB;
            }
        }


        public string ConsistirMargem
        {
            get
            {
                switch (CliBas.IC_MGCONCOB)
                {
                    case "D":
                        return "Default ";
                    case "N":
                        return "Não";
                    case "S":
                        return "Sim";

                }
                return CliBas.IC_MGCONCOB;
            }
        }
        public string ConsistirCobFin
        {
            get
            {
                switch (CliBas.IC_MGGACOB)
                {
                    case "D":
                        return "Default ";
                    case "N":
                        return "Não";
                    case "S":
                        return "Sim";

                }
                return CliBas.IC_MGGACOB;
            }
        }

        public string GeraisTipo
        {
            get
            {
                switch (CliBas.CD_TIPO)
                {
                    case "S":
                        return "Seguros";
                    case "P":
                        return "Previdência";
                    case "C":
                        return "Capitalização";

                }
                return CliBas.CD_TIPO;
            }
        }

        public string LimiteCredito
        {
            get
            {
                return CliBas.IC_LIMITE_CREDITO == "S" ? "Sim" : "Não";
            }
        }
        public string FixaValorCota
        {
            get
            {
                return CliBas.IC_FIXA_VL_COTA == "S" ? "Sim" : "Não";
            }
        }

        public string ClassificacaoFundo
        {
            get
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from p in db.SAC_BA_TIPO_CLASSIFICACAO
                                  where p.CD_TIPO_CLASSIFICACAO == CliBas.CD_TIPO_CLASSIFICACAO
                                  select p).FirstOrDefault();

                    if (result != null)
                    {
                        return result.DS_TIPO_CLASSIFICACAO;
                    }

                    return CliBas.CD_TIPO_CLASSIFICACAO;
                }
            }
        }

        public string GeraMsgSpb
        {
            get
            {
                return CliBas.IC_MSG_SPB == "S" ? "Sim" : "Não";
            }
        }

        public string BenchMark1
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CliBas.INDEX1))
                    return "";

                return string.Format("{0}. Tx %AA: {1:P} {2}", CliBas.INDEX1.Trim(), PcAdicIndex1, IcTaxaCdiBenchmark1);

            }
        }


        public string BenchMark2
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CliBas.INDEX2))
                    return "";

                return string.Format("{0}. Tx %AA: {1:P} {2}", CliBas.INDEX2.Trim(), PcAdicIndex2, IcTaxaCdiBenchmark2);
                
            }
        }

        public string BenchMark3
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CliBas.INDEX3))
                    return "";

                return string.Format("{0}. Tx %AA: {1:P} {2}", CliBas.INDEX3.Trim(), PcAdicIndex3, IcTaxaCdiBenchmark3);

            }
        }

        public string BenchMark4
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CliBas.IDDIR_CD_COMP4))
                    return "";

                return string.Format("{0}. Tx %AA: {1:P} {2}", CliBas.IDDIR_CD_COMP4.Trim(), PcAdicIndex4, IcTaxaCdiBenchmark4);

            }
        }

        public string IcTaxaCdiBenchmark1
        {
            get
            {
                switch (CliBas.IC_TAXA_CDI)
                {
                    case "D":
                        return "Diária";
                    case "P":
                        return "Período";

                }

                return "";
            }
        }


        public string IcTaxaCdiBenchmark2
        {
            get
            {
                switch (CliBas.IC_TAXA_CDI2)
                {
                    case "D":
                        return "Diária";
                    case "P":
                        return "Período";

                }

                return "";
            }
        }

        public string IcTaxaCdiBenchmark3
        {
            get
            {
                switch (CliBas.IC_TAXA_CDI3)
                {
                    case "D":
                        return "Diária";
                    case "P":
                        return "Período";

                }

                return "";
            }
        }

        public string IcTaxaCdiBenchmark4
        {
            get
            {
                switch (CliBas.IC_TAXA_CDI4)
                {
                    case "D":
                        return "Diária";
                    case "P":
                        return "Período";

                }

                return "";
            }
        }

        public double PcAdicIndex1
        {
            get
            {
                if (CliBas.PC_ADIC_INDEX1 == null)
                    return 0;

                return CliBas.PC_ADIC_INDEX1.GetValueOrDefault();
            }
        }

        public double PcAdicIndex2
        {
            get
            {
                if (CliBas.PC_ADIC_INDEX2 == null)
                    return 0;

                return CliBas.PC_ADIC_INDEX2.GetValueOrDefault();
            }
        }

        public double PcAdicIndex3
        {
            get
            {
                if (CliBas.PC_ADIC_INDEX3 == null)
                    return 0;

                return CliBas.PC_ADIC_INDEX3.GetValueOrDefault();
            }
        }

        public double PcAdicIndex4
        {
            get
            {
                if (CliBas.PC_ADIC_INDEX4 == null)
                    return 0;

                return CliBas.PC_ADIC_INDEX4.GetValueOrDefault();
            }
        }
        #endregion

        #region constructor

        public CliBasViewModel()
        {
            CliBas = new CLI_BAS();
        }

        #endregion
    }
}

