﻿using System.Data;

namespace Atlas.Reports.Interface
{
    public interface IReport
    {
        void Print(DataTable dataTable);

        void Export(DataTable dataTable, string folder);
    }
}
