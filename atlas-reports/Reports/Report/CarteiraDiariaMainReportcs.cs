﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Atlas.Reports.EntityModel;
using Atlas.Reports.ExportViewModel;
using Atlas.Reports.ReportsViewModel;
using Atlas.Reports.Views;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class CarteiraDiariaMainReportcs : XtraReport
    {

        #region Fields

        private string _client;
        private DateTime _date;
        private string _folder;

        #endregion

        public CarteiraDiariaMainReportcs(string client, DateTime date, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            _folder = folder;
            _client = client;
            _date = date;

            string querie = string.Format(Queries.QuerieFiles.CarteiraDiaria, client, date.ToString("yyyyMMdd"), date.ToString("yyyyMMdd"));

            AdoBase ado = new AdoBase();
            var dataTable = ado.RunRawSqlQuerie(querie);

            List<DataRow> dtList = dataTable.AsEnumerable().ToList();

            if (dtList.Count == 0)
                return;

            if (reportType == Enums.Enums.ReportType.Report)
                Print(dtList);

            if (reportType == Enums.Enums.ReportType.Excel)
                Export(dataTable);
        }

        #region Utility Methods

        private List<DataRow> GetDataRows (string querie)
        {
            string sqlQuerie = string.Format(querie,_client, _date.ToString("yyyyMMdd"), _date.ToString("yyyyMMdd"));

            var ado = new AdoBase();
            var dataTable = ado.RunRawSqlQuerie(sqlQuerie);

            return dataTable.AsEnumerable().ToList();
        }

        private void Export(DataTable datatable)
        {
            Print(datatable.AsEnumerable().ToList());
            this.ExportToXlsx(Path.Combine(_folder, string.Format("{0}_{1}.XLSX", "CarteiraDiaria", DateTime.Now.ToString("yyyyMMddHHmmssfff"))), new XlsxExportOptions() { ExportMode = XlsxExportMode.SingleFile, TextExportMode = TextExportMode.Text, ShowGridLines = false});
        }

        private void Print(IEnumerable<DataRow> dtList)
        {
            var list = new List<CarteiraDiariaMainViewModel>();
            
            foreach (var dataRow in dtList)
            {
                list.Add(new CarteiraDiariaMainViewModel(dataRow));
            }

            DataSource = list;

            // RV
            var rvList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaRv);
            var totRv = rvList.Sum(t => t.Field<double>("SAC_CL_PATRRV_VL_MERC"));
            var listRv = rvList.Select(dataRow => new CarteiraDiariaRvViewModel(dataRow, totRv)).ToList();
            subRv.ReportSource.DataSource = listRv;


            // OPC
            var opcList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaOpc);
            var totOpc = opcList.Sum(t => t.Field<double>("VL_MERC"));
            var listOpc = opcList.Select(dataRow => new CarteiraDiariaOpcViewModel(dataRow, totOpc)).ToList();
            subOpc.ReportSource.DataSource = listOpc;

            // OPCFLEX
            var opcFlexList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaOpcFlex);
            var totOpcFlex = opcFlexList.Sum(t => t.Field<double>("SAC_CL_PATRRV_VL_MERC_LIQUIDO"));
            var listOpcFlex = opcFlexList.Select(dataRow => new CarteiraDiariaOpcFlexViewModel(dataRow, totOpcFlex)).ToList();
            subOpcFlex.ReportSource.DataSource = listOpcFlex;


            // FU
            var fuList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaFu);
            var totFu = fuList.Sum(t => t.Field<double>("VL_PRECOMERC"));
            var listFu = fuList.Select(dataRow => new CarteiraDiariaFuViewModel(dataRow, totFu)).ToList();
            subFu.ReportSource.DataSource = listFu;


            // FUOPC
            var fuOpcList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaFuOpc);
            var totFuOpc = fuOpcList.Sum(t => t.Field<double>("VL_PRECOMERC"));
            var listFuOpc = fuOpcList.Select(dataRow => new CarteiraDiariaFuOpcViewModel(dataRow, totFuOpc)).ToList();
            subFuOpc.ReportSource.DataSource = listFuOpc;


            // FI
            var fiList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaFi);
            var totFi = fiList.Sum(t => t.Field<double>("VL_FINANCEIRO_LIQ"));
            var listFi = fiList.Select(dataRow => new CarteiraDiariaFiViewModel(dataRow, totFi)).ToList();
            subFi.ReportSource.DataSource = listFi;


            // RF
            var rfList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaRf);
            var totRf = rfList.Sum(t => t.Field<double>("SAC_CL_PATRRF_VL_FIN_LIQUIDO"));
            var listRf = rfList.Select(dataRow => new CarteiraDiariaRfViewModel(dataRow, totRf)).ToList();
            subRf.ReportSource.DataSource = listRf;

            // RFCOMP
            var rfcList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaCompromissadas);
            var totRfc = rfcList.Sum(t => t.Field<double>("SAC_CL_PATRRF_VL_FIN_LIQUIDO"));
            var listRfc = rfcList.Select(dataRow => new CarteiraDiariaRfCompViewModel(dataRow, totRfc)).ToList();
            subRfComp.ReportSource.DataSource = listRfc;

            // SW
            var swList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaSwap);
            var totSw = swList.Sum(t => t.Field<double>("VL_APROP_LIQUIDO"));
            var listSw = swList.Select(dataRow => new CarteiraDiariaSwViewModel(dataRow, totSw)).ToList();
            subSw.ReportSource.DataSource = listSw;

            // CPR
            var cprList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaCpr);
            var totCpr = cprList.Sum(t => t.Field<double>("VALOR"));
            var listCpr = cprList.Select(dataRow => new CarteiraDiariaCprViewModel(dataRow, totCpr)).ToList();
            subCpr.ReportSource.DataSource = listCpr;

            // TSR
            var tsrList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaTsr);
            var totTsr = tsrList.Sum(t => t.Field<double>("VALOR"));
            var listTsr = tsrList.Select(dataRow => new CarteiraDiariaTsrViewModel(dataRow, totTsr)).ToList();
            subTsr.ReportSource.DataSource = listTsr;

            // PL
            var plList = GetDataRows(Queries.QuerieFiles.CarteiraDiariaRentab);
            var listPl = plList.Select(dataRow => new CarteiraDiariaRentabViewModel(dataRow)).ToList();
            subPl.ReportSource.DataSource = listPl;
        }

        #endregion

    }

    
}



