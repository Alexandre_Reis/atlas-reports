﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Documents;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class PosicaoRendaVariavelReport : BaseReport
    {
        public PosicaoRendaVariavelReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            if (reportType == Enums.Enums.ReportType.Report)
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var list = new List<PosicaoRendaVariavelViewModel>();

                    var result = (from cliBas in db.CLI_BAS
                                  join rvPos in db.RV_POS on cliBas.CODCLI equals rvPos.CODCLI
                                  join codPap in db.RV_CADPAP on rvPos.CODPAP equals codPap.CODPAP
                                  join loteCot in db.SAC_RV_CAD_LOTE on rvPos.CODPAP equals loteCot.CD_PAPEL
                                  join rvCusto in db.SAC_RV_CUSTOS on new {CodCli = cliBas.CODCLI, DataPos = rvPos.DATAPOS, CodPap = rvPos.CODPAP} equals new {CodCli = rvCusto.CLCLI_CD, DataPos = rvCusto.DT, CodPap = rvCusto.RVPAP_CD}
                                  where clients.Contains(cliBas.CODCLI) &&
                                        rvPos.DATAPOS >= startDate && rvPos.DATAPOS <= endDate &&
                                        (loteCot.DT_INICIO <= rvPos.DATAPOS && loteCot.DT_FIM >= rvPos.DATAPOS) &&
                                        rvCusto.IC_POSICAO == "C" &&
                                        (rvPos.QTDBLOQ + rvPos.QTDDISP >= 0)

                                  select new PosicaoRendaVariavelModel()
                                         {
                                             EndDate = endDate,
                                             Startdate = startDate,
                                             CodCli = cliBas.CODCLI,
                                             ClientName = cliBas.NOME,
                                             Data = rvPos.DATAPOS,
                                             Papel = codPap.NOME,
                                             Isin = codPap.ISIN,
                                             Corretora = rvPos.CODCOR,
                                             Praca = rvPos.PRACA,
                                             QtdeDisp = rvPos.QTDDISP,
                                             QtdeBloq = rvPos.QTDBLOQ,
                                             PrecoHistorico = rvCusto.VL_PRECO_HIST,
                                             PrecoFiscal = rvCusto.VL_PRECO_FISC,
                                             PrecoMercMedio = rvCusto.VL_PRECO_MERCMEDIO,
                                             TipoRv = cliBas.TIPORV,
                                             PrecoMercFech = rvCusto.VL_PRECO_MERCFECH,
                                             LoteCot = loteCot.VL_LOTE,
                                             Irrf = rvPos.VL_IRRF,
                                             VlMercadoLiquido = rvPos.VL_MERC_LIQUIDO,
                                             codPap = codPap.CODPAP

                                         }).ToList();

                    result.ForEach(x => list.Add(new PosicaoRendaVariavelViewModel(x)));


                    result = (from cliBas in db.CLI_BAS
                              join rvPos in db.RV_POS on cliBas.CODCLI equals rvPos.CODCLI
                              join codPap in db.RV_CADPAP on rvPos.CODPAP equals codPap.CODPAP
                              join loteCot in db.SAC_RV_CAD_LOTE on rvPos.CODPAP equals loteCot.CD_PAPEL
                              join rvCusto in db.SAC_RV_CUSTOS on new {CodCli = cliBas.CODCLI, DataPos = rvPos.DATAPOS, CodPap = rvPos.CODPAP} equals new {CodCli = rvCusto.CLCLI_CD, DataPos = rvCusto.DT, CodPap = rvCusto.RVPAP_CD}
                              where clients.Contains(cliBas.CODCLI) &&
                                    rvPos.DATAPOS >= startDate && rvPos.DATAPOS <= endDate &&
                                    (loteCot.DT_INICIO <= rvPos.DATAPOS && loteCot.DT_FIM >= rvPos.DATAPOS) &&
                                    rvCusto.IC_POSICAO == "V" &&
                                    (rvPos.QTDBLOQ + rvPos.QTDDISP < 0)

                              select new PosicaoRendaVariavelModel()
                                     {
                                         EndDate = endDate,
                                         Startdate = startDate,
                                         CodCli = cliBas.CODCLI,
                                         ClientName = cliBas.NOME,
                                         Data = rvPos.DATAPOS,
                                         Papel = codPap.NOME,
                                         Isin = codPap.ISIN,
                                         Corretora = rvPos.CODCOR,
                                         Praca = rvPos.PRACA,
                                         QtdeDisp = rvPos.QTDDISP,
                                         QtdeBloq = rvPos.QTDBLOQ,
                                         PrecoHistorico = rvCusto.VL_PRECO_HIST,
                                         PrecoFiscal = rvCusto.VL_PRECO_FISC,
                                         PrecoMercMedio = rvCusto.VL_PRECO_MERCMEDIO,
                                         TipoRv = cliBas.TIPORV,
                                         PrecoMercFech = rvCusto.VL_PRECO_MERCFECH,
                                         LoteCot = loteCot.VL_LOTE,
                                         Irrf = rvPos.VL_IRRF,
                                         VlMercadoLiquido = rvPos.VL_MERC_LIQUIDO,
                                         codPap = codPap.CODPAP

                                     }).ToList();

                    result.ForEach(x => list.Add(new PosicaoRendaVariavelViewModel(x)));

                    this.DataSource = list;
                }
            }
            else
            {
                try
                {
                    var carteiras = string.Join("','", clients.ToArray());
                    string querie = string.Format(Queries.QuerieFiles.PosicaoRendaVariavel, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    InitializeComponent();

                    AdoBase ado = new AdoBase();
                    var dataTable = ado.RunRawSqlQuerie(querie);

                    Export(dataTable, folder);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
