﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;

namespace Atlas.Reports.Report
{
    public partial class OperacoesRendaFixaReport : BaseReport
    {
        public OperacoesRendaFixaReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            if (reportType == Enums.Enums.ReportType.Report)
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var list = new List<OperacoesRendaFixaViewModel>();

                    var result = (from cliBas in db.CLI_BAS
                                  join rfOper in db.SAC_RF_OPERACAO on cliBas.CODCLI equals rfOper.CLCLI_CD
                                  join rfLastro in db.SAC_RF_LASTRO on rfOper.RFLAS_CD equals rfLastro.CD
                                  join rfTipo in db.SAC_RF_TIPO on rfLastro.RFTP_CD equals rfTipo.CD
                                  join rfMov in db.SAC_RF_MOV on rfOper.CD equals rfMov.RFOP_CD
                                  join rfPos in db.SAC_RF_POS on new {rfopcd = rfMov.RFOP_CD, dt = rfMov.DT} equals new {rfopcd = rfPos.RFOP_CD, dt = rfPos.DT}
                                  from rfMem in db.SAC_RF_MEMORIA_IRRF.Where(rfMem => rfMem.DT == rfMov.DT && rfMem.RFOP_CD == rfMov.RFOP_CD).DefaultIfEmpty() // left outer join
                                  where clients.Contains(cliBas.CODCLI) &&
                                        rfMov.DT >= startDate && rfMov.DT <= endDate

                                  select new OperacoesRendaFixaModel()
                                         {
                                             EndDate = endDate,
                                             Startdate = startDate,
                                             CodCli = cliBas.CODCLI,
                                             ClientName = cliBas.NOME,
                                             DtAquisicao = rfPos.DT_AQUISICAO,
                                             Data = rfMov.DT,
                                             CodOperacao = rfMov.RFOP_CD,
                                             SgOperacao = rfMov.SG_OPERACAO,
                                             IdRegVendaTermo = rfMov.ID_REG_VENDA_TERMO,
                                             IcRegVendaTermo = rfMov.IC_REG_VENDA_TERMO,
                                             DtVencimento = rfLastro.DT_VENCIMENTO,
                                             IcOperTmo = rfMov.IC_REG_VENDA_TERMO == "S" ? "S" : (rfOper.IC_OPER_TMO ?? "N"),
                                             IcVendaTermo = rfMov.IC_VENDA_TERMO,
                                             TipoTitulo = rfLastro.RFTP_CD,
                                             CodLastro = rfLastro.CD,
                                             CodCetipSelic = rfLastro.CD_CETIP_SELIC,
                                             CodEmissor = rfLastro.BAINS_CD,
                                             DtEmissao = rfLastro.DT_EMISSAO,
                                             CdIndex = rfLastro.IDDIR_CD,
                                             VlJurosOper = rfOper.VL_JUROSOPER,
                                             Quantidade = rfMov.QT,
                                             VlPuemPrinc = rfPos.VL_PUEM_PRINC,
                                             PuOperacao = 0,
                                             RfPosPuMtm = rfPos.VL_PU_AJUSTE_MTM,
                                             RfPosPu = rfPos.VL_PU,
                                             VlPuemCm = rfPos.VL_PUEM_CM,
                                             PuAbertura = 0,
                                             PuAq = (rfMem.PU_AQ ?? 0),
                                             PuAquisicao = rfPos.VL_PU_AQUISICAO,
                                             VlJuros = rfPos.VL_JUROS,
                                             VlLp = 0,
                                             VlPuCurva = rfPos.VL_PU_CURVA,
                                             VlPremio = rfPos.VL_PREMIO,
                                             VlAgio = rfPos.VL_AGIO,
                                             VlPuAgio = rfPos.VL_PU_AGIO,
                                             VlFatorAgio = rfPos.VL_FATOR_AGIO,
                                             VlIrrf = rfMov.VL_IRRF,
                                             VlIof = rfMov.VL_IOF,
                                             VlBruto = rfMov.VL_BRUTO,
                                             IcIrrfVirtual = rfMov.IC_IRRF_VIRTUAL,
                                             IcIofResgateVirtual = rfMov.IC_IOF_RESGATE_VIRTUAL,
                                             Clearing = rfMov.CODCLR
                                         }).ToList();



                    result.ForEach(x => list.Add(new OperacoesRendaFixaViewModel(x)));

                    this.DataSource = list;
                }
            }
            else
            {
                try
                {
                    var carteiras = string.Join("','", clients.ToArray());
                    string querie = string.Format(Queries.QuerieFiles.OperacoesRendaFixa, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    InitializeComponent();

                    AdoBase ado = new AdoBase();
                    var dataTable = ado.RunRawSqlQuerie(querie);

                    Export(dataTable, folder);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

    }
}




