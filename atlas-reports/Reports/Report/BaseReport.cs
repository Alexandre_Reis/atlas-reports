﻿using System;
using System.IO;
using Atlas.Reports.ExportViewModel;
using Atlas.Reports.Interface;
using Atlas.Reports.Views;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public class BaseReport : XtraReport, IReport
    {
        void IReport.Print(System.Data.DataTable dataTable)
        {

        }

        public void Export(System.Data.DataTable dataTable, string folder)
        {
            var viewModel = new BaseExportViewModel(dataTable);
            var view = new ExportView(viewModel);

            TableView tableView = view.GetTableView();
            view.InitializeComponent();
            tableView.UpdateLayout();
            view.Show();
            view.Close();
            tableView.ExportToXlsx(Path.Combine(folder, string.Format("{0}_{1}.XLSX", this.DisplayName, DateTime.Now.ToString("yyyyMMddHHmmssfff"))), new XlsxExportOptions() { ExportMode = XlsxExportMode.SingleFile, TextExportMode = TextExportMode.Text, ShowGridLines = false });
            
        }
    }
}
