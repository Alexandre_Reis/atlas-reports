﻿namespace Atlas.Reports.Report
{
    partial class CarteiraDiariaMainReportcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.subOpcFlex = new DevExpress.XtraReports.UI.XRSubreport();
            this.subPl = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTsr = new DevExpress.XtraReports.UI.XRSubreport();
            this.subCpr = new DevExpress.XtraReports.UI.XRSubreport();
            this.subSw = new DevExpress.XtraReports.UI.XRSubreport();
            this.subRfComp = new DevExpress.XtraReports.UI.XRSubreport();
            this.subRf = new DevExpress.XtraReports.UI.XRSubreport();
            this.subFi = new DevExpress.XtraReports.UI.XRSubreport();
            this.subFuOpc = new DevExpress.XtraReports.UI.XRSubreport();
            this.subFu = new DevExpress.XtraReports.UI.XRSubreport();
            this.subOpc = new DevExpress.XtraReports.UI.XRSubreport();
            this.subRv = new DevExpress.XtraReports.UI.XRSubreport();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.subOpcFlex,
            this.subPl,
            this.subTsr,
            this.subCpr,
            this.subSw,
            this.subRfComp,
            this.subRf,
            this.subFi,
            this.subFuOpc,
            this.subFu,
            this.subOpc,
            this.subRv});
            this.Detail.HeightF = 371.8749F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel1});
            this.TopMargin.HeightF = 102.7083F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Startdate", "Data da Posição: {0:dd/MM/yyyy}")});
            this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 52.25002F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(383.5633F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ClientNameAndCodeString")});
            this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 75.25002F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(621.875F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "xrLabel2";
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "IssueString")});
            this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 29.25002F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(383.5633F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "xrLabel3";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(592.7083F, 29.25002F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Relatório de Carteira Diária";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrLine2});
            this.PageFooter.HeightF = 100F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "Pagina {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(969.6782F, 67.00001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(172.8958F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 50F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1132.574F, 12.5F);
            // 
            // subOpcFlex
            // 
            this.subOpcFlex.CanShrink = true;
            this.subOpcFlex.LocationFloat = new DevExpress.Utils.PointFloat(0F, 66.5F);
            this.subOpcFlex.Name = "subOpcFlex";
            this.subOpcFlex.ReportSource = new Atlas.Reports.Report.CdOpcFlexReport();
            this.subOpcFlex.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subPl
            // 
            this.subPl.CanShrink = true;
            this.subPl.LocationFloat = new DevExpress.Utils.PointFloat(0F, 336.7499F);
            this.subPl.Name = "subPl";
            this.subPl.ReportSource = new Atlas.Reports.Report.CdRentabReport();
            this.subPl.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subTsr
            // 
            this.subTsr.CanShrink = true;
            this.subTsr.LocationFloat = new DevExpress.Utils.PointFloat(0F, 305.9999F);
            this.subTsr.Name = "subTsr";
            this.subTsr.ReportSource = new Atlas.Reports.Report.CdTsrReport();
            this.subTsr.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subCpr
            // 
            this.subCpr.CanShrink = true;
            this.subCpr.LocationFloat = new DevExpress.Utils.PointFloat(0F, 275.2916F);
            this.subCpr.Name = "subCpr";
            this.subCpr.ReportSource = new Atlas.Reports.Report.CdCprReport();
            this.subCpr.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subSw
            // 
            this.subSw.CanShrink = true;
            this.subSw.LocationFloat = new DevExpress.Utils.PointFloat(0F, 247.1666F);
            this.subSw.Name = "subSw";
            this.subSw.ReportSource = new Atlas.Reports.Report.CdSwReport();
            this.subSw.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subRfComp
            // 
            this.subRfComp.CanShrink = true;
            this.subRfComp.LocationFloat = new DevExpress.Utils.PointFloat(0F, 216.1666F);
            this.subRfComp.Name = "subRfComp";
            this.subRfComp.ReportSource = new Atlas.Reports.Report.CdRfCompReport();
            this.subRfComp.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subRf
            // 
            this.subRf.CanShrink = true;
            this.subRf.LocationFloat = new DevExpress.Utils.PointFloat(0F, 185F);
            this.subRf.Name = "subRf";
            this.subRf.ReportSource = new Atlas.Reports.Report.CdRfReport();
            this.subRf.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subFi
            // 
            this.subFi.CanShrink = true;
            this.subFi.LocationFloat = new DevExpress.Utils.PointFloat(0F, 154F);
            this.subFi.Name = "subFi";
            this.subFi.ReportSource = new Atlas.Reports.Report.CdFiReport();
            this.subFi.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subFuOpc
            // 
            this.subFuOpc.CanShrink = true;
            this.subFuOpc.LocationFloat = new DevExpress.Utils.PointFloat(0F, 124F);
            this.subFuOpc.Name = "subFuOpc";
            this.subFuOpc.ReportSource = new Atlas.Reports.Report.CdFuOpcReport();
            this.subFuOpc.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subFu
            // 
            this.subFu.CanShrink = true;
            this.subFu.LocationFloat = new DevExpress.Utils.PointFloat(0F, 95F);
            this.subFu.Name = "subFu";
            this.subFu.ReportSource = new Atlas.Reports.Report.CdFuReport();
            this.subFu.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subOpc
            // 
            this.subOpc.CanShrink = true;
            this.subOpc.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37.5F);
            this.subOpc.Name = "subOpc";
            this.subOpc.ReportSource = new Atlas.Reports.Report.CdOpcReport();
            this.subOpc.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // subRv
            // 
            this.subRv.CanShrink = true;
            this.subRv.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.subRv.Name = "subRv";
            this.subRv.ReportSource = new Atlas.Reports.Report.CdRvReport();
            this.subRv.SizeF = new System.Drawing.SizeF(1149F, 23F);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(Atlas.Reports.ReportsViewModel.CarteiraDiariaMainViewModel);
            // 
            // CarteiraDiariaMainReportcs
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter});
            this.DataSource = this.bindingSource1;
            this.Font = new System.Drawing.Font("Verdana", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.HorizontalContentSplitting = DevExpress.XtraPrinting.HorizontalContentSplitting.Smart;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(10, 10, 103, 10);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "14.2";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRSubreport subRv;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRSubreport subOpc;
        private DevExpress.XtraReports.UI.XRSubreport subFu;
        private DevExpress.XtraReports.UI.XRSubreport subFuOpc;
        private DevExpress.XtraReports.UI.XRSubreport subFi;
        private DevExpress.XtraReports.UI.XRSubreport subRf;
        private DevExpress.XtraReports.UI.XRSubreport subRfComp;
        private DevExpress.XtraReports.UI.XRSubreport subSw;
        private DevExpress.XtraReports.UI.XRSubreport subCpr;
        private DevExpress.XtraReports.UI.XRSubreport subTsr;
        private DevExpress.XtraReports.UI.XRSubreport subPl;
        private DevExpress.XtraReports.UI.XRSubreport subOpcFlex;
    }
}
