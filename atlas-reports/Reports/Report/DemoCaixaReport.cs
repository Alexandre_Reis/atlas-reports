﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using Atlas.Reports.EntityModel;
using Atlas.Reports.ExportViewModel;
using Atlas.Reports.Interface;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;
using Atlas.Reports.Views;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class DemoCaixaReport : BaseReport
    {

        public DemoCaixaReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            var carteiras = string.Join("','", clients.ToArray());
            string querie = string.Format(Queries.QuerieFiles.DemoCaixaAdm, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
            InitializeComponent();

            AdoBase ado = new AdoBase();
            var dataTable = ado.RunRawSqlQuerie(querie);

            if (reportType == Enums.Enums.ReportType.Report)
                Print(dataTable);

            if (reportType == Enums.Enums.ReportType.Excel)
                Export(dataTable, folder);

        }

        public void Print(DataTable dataTable)
        {
            var list = new List<DemoCaixaViewModel>();
            List<DataRow> dtList = dataTable.AsEnumerable().ToList();

            foreach (var dataRow in dtList)
            {
                list.Add(new DemoCaixaViewModel(dataRow));
            }

            this.DataSource = list;
        }

    }
}
