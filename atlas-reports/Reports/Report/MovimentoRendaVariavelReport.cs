﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Documents;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class MovimentoRendaVariavelReport : BaseReport
    {
        public MovimentoRendaVariavelReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            if (reportType == Enums.Enums.ReportType.Report)
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from cliBas in db.CLI_BAS
                                  join rvMov in db.RV_MOV on cliBas.CODCLI equals rvMov.CODCLI
                                  join codPap in db.RV_CADPAP on rvMov.CODPAP equals codPap.CODPAP
                                  join moeda in db.BAS_MOEDA on cliBas.MOEDA equals moeda.CODMOEDA
                                  where clients.Contains(cliBas.CODCLI) &&
                                        rvMov.DATAMOV >= startDate && rvMov.DATAMOV <= endDate &&
                                        rvMov.TIPOMOV != "4" &&
                                        ((rvMov.TIPOMOV != "1") || (rvMov.TIPOMOV == "1" && rvMov.COMPRAVENDA != "V" && rvMov.PRECO != 0))
                                  select new MovimentoRendaVariavelModel()
                                         {
                                             EndDate = endDate,
                                             Startdate = startDate,
                                             CodCli = cliBas.CODCLI,
                                             ClientName = cliBas.NOME,
                                             DataMov = rvMov.DATAMOV,
                                             DataLiq = rvMov.DATALIB,
                                             Moeda = moeda.NOMEMOEDA,
                                             CodPap = codPap.CODPAP,
                                             CodCor = rvMov.CODCOR,
                                             RvCorCdFisica = rvMov.RVCOR_CD_FISICA,
                                             Isin = codPap.ISIN,
                                             Praca = rvMov.PRACA,
                                             TipoMov = rvMov.TIPOMOV,
                                             Quantidade = rvMov.QUANTIDADE,
                                             Preco = rvMov.PRECO,
                                             Valor = rvMov.VALOR,
                                             Equalizacao = rvMov.VL_EQUALIZACAO,
                                             Corretagem = rvMov.CORCLI,
                                             Taxas = 0,
                                             TaxaServico = rvMov.VL_TX_SERVICO,
                                             Total = 0,
                                             CompraVenda = rvMov.COMPRAVENDA,
                                             SgTipo = "",
                                             DayTrade = rvMov.IC_DAYTRADE,
                                             CadPapTipo = codPap.TIPO,
                                             FlagCor = rvMov.FLAGCOR,
                                             VlEmolumento = rvMov.VL_EMOLUMENTO,
                                             VlEmolumentoAgente = rvMov.VL_EMOLUMENTO_AGENTE,
                                             VlTxOpcoesAgente = rvMov.VL_TX_OPCOES_AGENTE,
                                             VlTxOpcoes = rvMov.VL_TX_OPCOES,
                                             VlEmolumentoFa = rvMov.VL_EMOLUMENTO_FA,
                                             VlTxRegFa = rvMov.VL_TX_REG_FA,
                                             VlTxRegFaAgente = rvMov.VL_TX_REG_FA_AGENTE,
                                             VlTxLiqFaAgente = rvMov.VL_TX_LIQ_FA_AGENTE,
                                             CodBvsp = cliBas.CODBVSP
                                         }).ToList();

                    var list = new List<MovimentoRendaVariavelViewModel>();

                    result.ForEach(x => list.Add(new MovimentoRendaVariavelViewModel(x)));

                    this.DataSource = list;
                }
            }
            else
            {
                try
                {
                    var carteiras = string.Join("','", clients.ToArray());
                    string querie = string.Format(Queries.QuerieFiles.MovimentoRendaVariavel, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    InitializeComponent();

                    AdoBase ado = new AdoBase();
                    var dataTable = ado.RunRawSqlQuerie(querie);

                    Export(dataTable, folder);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

        }

    }
}