﻿using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Atlas.Reports.Report
{
    public partial class CarteiraDiariaRvReport : MyXtraReport
    {
        public CarteiraDiariaRvReport(List<String> clients, DateTime startDate, DateTime endDate)
        {
            InitializeComponent();

            var list = new List<MovimentoOutrosViewModel>();

            using (var db = new SacEntities(DbContextFactory.ConnectString))
            {
                var result = (from cliBas in db.CLI_BAS
                              join sacFiMov in db.SAC_FI_MOV on cliBas.CODCLI equals sacFiMov.CLCLI_CD
                              join sacfiCad in db.SAC_FI_CADASTRO on sacFiMov.FICAD_CD equals sacfiCad.CD
                              where clients.Contains(cliBas.CODCLI) &&
                                    sacFiMov.DT >= startDate && sacFiMov.DT <= endDate &&
                                    sacFiMov.IC_APLICACAO_RESGATE != "N" &&
                                    cliBas.TIPO != "I" &&
                                    cliBas.TIPO != "L"
                              select new MovimentoOutrosModel()
                                     {
                                         EndDate = endDate,
                                         Startdate = startDate,
                                         CodCli = cliBas.CODCLI,
                                         ClientName = cliBas.NOME,
                                         Data = sacFiMov.DT,
                                         Fundo = sacfiCad.CD,
                                         LiquidacaoFinanceira = sacFiMov.DT_LIQUID_FIN,
                                         LiquidacaoFisica = sacFiMov.DT_LIQUID_FIS,
                                         Vl = sacFiMov.VL,
                                         QtCotas = sacFiMov.QT_COTAS,
                                         IcAplicacaoResgate = sacFiMov.IC_APLICACAO_RESGATE,
                                         DataAplicacao = sacFiMov.DT_APLICACAO,
                                         VlIrrf = sacFiMov.VL_IRRF,
                                         VlIof = sacFiMov.VL_IOF,
                                         SgIofResgate = cliBas.SG_IOF_RESGATE,
                                         IcLiquidezDiaria = sacfiCad.IC_LIQUIDEZ_DIARIA,
                                         IcTransferencia = sacFiMov.IC_TRANSFERENCIA,
                                         VlEmol = 0,
                                         VlCor = 0
                                     }).ToList();

                result.ForEach(x => list.Add(new MovimentoOutrosViewModel(x)));

                result = (from cliBas in db.CLI_BAS
                              join sacFiMov in db.SAC_FI_MOV on cliBas.CODCLI equals sacFiMov.CLCLI_CD
                              join sacfiCad in db.SAC_FI_CADASTRO on sacFiMov.FICAD_CD equals sacfiCad.CD
                              where clients.Contains(cliBas.CODCLI) &&
                                    sacFiMov.DT >= startDate && sacFiMov.DT <= endDate &&
                                    sacFiMov.IC_APLICACAO_RESGATE != "N" &&
                                    (cliBas.TIPO == "I" || cliBas.TIPO == "L")
                              select new MovimentoOutrosModel()
                              {
                                  EndDate = endDate,
                                  Startdate = startDate,
                                  CodCli = cliBas.CODCLI,
                                  ClientName = cliBas.NOME,
                                  Data = sacFiMov.DT,
                                  Fundo = sacfiCad.CD,
                                  LiquidacaoFinanceira = sacFiMov.DT_LIQUID_FIN,
                                  LiquidacaoFisica = sacFiMov.DT_LIQUID_FIS,
                                  Vl = sacFiMov.VL,
                                  QtCotas = sacFiMov.QT_COTAS,
                                  IcAplicacaoResgate = sacFiMov.IC_APLICACAO_RESGATE,
                                  DataAplicacao = sacFiMov.DT_APLICACAO,
                                  VlIrrf = 0,
                                  VlIof = sacFiMov.VL_IOF,
                                  SgIofResgate = cliBas.SG_IOF_RESGATE,
                                  IcLiquidezDiaria = sacfiCad.IC_LIQUIDEZ_DIARIA,
                                  IcTransferencia = sacFiMov.IC_TRANSFERENCIA,
                                  VlEmol = 0,
                                  VlCor = 0
                              }).ToList();

                result.ForEach(x => list.Add(new MovimentoOutrosViewModel(x)));

                result = (from cliBas in db.CLI_BAS
                          join sacFiMov in db.SAC_FI_MOV on cliBas.CODCLI equals sacFiMov.CLCLI_CD
                          join sacfiCad in db.SAC_FI_CADASTRO on sacFiMov.FICAD_CD equals sacfiCad.CD
                          where clients.Contains(cliBas.CODCLI) &&
                                sacFiMov.DT >= startDate && sacFiMov.DT <= endDate &&
                                sacFiMov.IC_APLICACAO_RESGATE == "N" &&
                                    cliBas.TIPO != "I" &&
                                    cliBas.TIPO != "L"
                          select new MovimentoOutrosModel()
                          {
                              EndDate = endDate,
                              Startdate = startDate,
                              CodCli = cliBas.CODCLI,
                              ClientName = cliBas.NOME,
                              Data = sacFiMov.DT,
                              Fundo = sacfiCad.CD,
                              LiquidacaoFinanceira = sacFiMov.DT_LIQUID_FIN,
                              LiquidacaoFisica = sacFiMov.DT_LIQUID_FIS,
                              Vl = sacFiMov.VL,
                              QtCotas = sacFiMov.QT_COTAS,
                              IcAplicacaoResgate = sacFiMov.IC_APLICACAO_RESGATE,
                              DataAplicacao = sacFiMov.DT_APLICACAO,
                              VlIrrf = sacFiMov.VL_IRRF,
                              VlIof = sacFiMov.VL_IOF,
                              SgIofResgate = cliBas.SG_IOF_RESGATE,
                              IcLiquidezDiaria = sacfiCad.IC_LIQUIDEZ_DIARIA,
                              IcTransferencia = sacFiMov.IC_TRANSFERENCIA,
                              VlEmol = 0,
                              VlCor = 0
                          }).ToList();

                result.ForEach(x => list.Add(new MovimentoOutrosViewModel(x)));
            }

            this.DataSource = list;
        }

    }

}
