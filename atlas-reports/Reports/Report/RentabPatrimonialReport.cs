﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Documents;
using Atlas.Reports.EntityModel;
using Atlas.Reports.ReportsViewModel;

namespace Atlas.Reports.Report
{
    public partial class RentabPatrimonialReport : BaseReport
    {
        public RentabPatrimonialReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();
            var carteiras = string.Join("','", clients.ToArray());
            string querie = string.Format(Queries.QuerieFiles.MapaEvolucaoCotas, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
            var ado = new AdoBase();
            var dtTable = ado.RunRawSqlQuerie(querie);

            if (reportType == Enums.Enums.ReportType.Report)
                Print(dtTable);

            if (reportType == Enums.Enums.ReportType.Excel)
                Export(dtTable, folder);

        }

        public void Print(DataTable dataTable)
        {
            var list = new List<RentabPatrimonialViewModel>();
            List<DataRow> dtList = dataTable.AsEnumerable().ToList();

            foreach (var dataRow in dtList)
            {
                list.Add(new RentabPatrimonialViewModel(dataRow));
            }

            this.DataSource = list;
        }
    }
}
