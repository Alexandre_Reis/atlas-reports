﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Documents;
using Atlas.Reports.ReportsViewModel;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class CdRentabReport : DevExpress.XtraReports.UI.XtraReport
    {
        public CdRentabReport()
        {
            InitializeComponent();

            BeforePrint += CdRentabReport_BeforePrint;

        }

        void CdRentabReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            List<CarteiraDiariaRentabViewModel> dtSource = DataSource as List<CarteiraDiariaRentabViewModel>;

            if (dtSource != null && dtSource.Count > 0)
            {
                if(string.IsNullOrEmpty(dtSource[0].IddirCdComp1))
                    xrTableRowidx1.Visible = false;

                if (string.IsNullOrEmpty(dtSource[0].IddirCdComp2))
                    xrTableRowidx2.Visible = false;

                if (string.IsNullOrEmpty(dtSource[0].IddirCdComp3))
                    xrTableRowidx3.Visible = false;

                if (string.IsNullOrEmpty(dtSource[0].IddirCdComp4))
                    xrTableRowidx4.Visible = false;
            }
        }

    }
}
