﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using Atlas.Reports.ReportsViewModel;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Atlas.Reports.Report
{
    public partial class PosicaoOutrosReport : BaseReport
    {
        public PosicaoOutrosReport(List<String> clients, DateTime startDate, DateTime endDate, Enums.Enums.ReportType reportType, string folder)
        {
            InitializeComponent();

            if (reportType == Enums.Enums.ReportType.Report)
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var result = (from cliBas in db.CLI_BAS
                                  join sacFiPos in db.SAC_FI_POS on cliBas.CODCLI equals sacFiPos.CLCLI_CD
                                  join sacfiCad in db.SAC_FI_CADASTRO on sacFiPos.FICAD_CD equals sacfiCad.CD
                                  where clients.Contains(cliBas.CODCLI) &&
                                        sacFiPos.DT >= startDate && sacFiPos.DT <= endDate
                                  select new PosicaoOutrosModel()
                                         {
                                             EndDate = endDate,
                                             Startdate = startDate,
                                             CodCli = cliBas.CODCLI,
                                             ClientName = cliBas.NOME,
                                             DtPosicao = sacFiPos.DT,
                                             CdFundo = sacfiCad.CD,
                                             Fundo = sacfiCad.NM,
                                             Vlcota = sacFiPos.VL_COTA,
                                             QtCotas = sacFiPos.QT_COTAS,
                                             QtBloqueada = sacFiPos.QT_BLOQUEADA,
                                             DtAplicacao = sacFiPos.DT_APLICACAO,
                                             DtUltimoAniv = sacFiPos.DT_ULTIMO_ANIV,
                                             DtproxAniv = sacFiPos.DT_PROX_ANIV,
                                             VlPrincipal = sacFiPos.VL_PRINCIPAL,
                                             VlResgEmit = sacFiPos.VL_RESG_EMIT,
                                             VlIrrf = sacFiPos.VL_IRRF,
                                             VlIof = sacFiPos.VL_IOF,
                                             VlLiq = sacFiPos.VL_LIQ,
                                             VlTotal = sacFiPos.VL_TOTAL,
                                             SgLocalCustodia = sacfiCad.SG_LOCAL_CUSTODIA
                                         }).ToList();


                    var list = new List<PosicaoOutrosViewModel>();

                    result.ForEach(x => list.Add(new PosicaoOutrosViewModel(x)));

                    this.DataSource = list;
                }
            }
            else
            {
                try
                {
                    var carteiras = string.Join("','", clients.ToArray());
                    string querie = string.Format(Queries.QuerieFiles.PosicaoOutros, carteiras, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    InitializeComponent();

                    AdoBase ado = new AdoBase();
                    var dataTable = ado.RunRawSqlQuerie(querie);

                    Export(dataTable, folder);
                }
                catch (Exception ex)
                {
                    throw;
                }
                
            }

        }

        public void Print(DataTable dataTable)
        {
            
        }

    }
}
