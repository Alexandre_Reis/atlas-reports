﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace Atlas.Ecd.Wpf.Converter
{
    public class AccountDefaultConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { (bool)value, true };
        }
    }
}
