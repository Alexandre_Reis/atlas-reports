﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using Atlas.Reports.EntityModel;
using Atlas.Reports.ViewModels;
using System.Linq;
using System.Windows;
using Common.Logging;
using DevExpress.Mvvm;
using DevExpress.Xpf.Core;


namespace Atlas.Cache
{
    public class EntityCache
    {
        #region static Fields
        
        private static ILog _logger = LogManager.GetLogger("EntityCache");
        private static EntityCache _cacheSac;
        private static EntityCache _cacheCot;

        private static ObservableCollection<CliBasViewModel> _listCliBas;
        private static ObservableCollection<SacClLiquid2ViewModel> _listLiquid2;
        private static ObservableCollection<CotCotistaViewModel> _listCotista;
        private static List<COT_BANCO> _listBancos;

        #endregion

        #region constructor

        private EntityCache()
        {

        }

        #endregion

        public static EntityCache InstanceSac()
        {
            if (_cacheSac == null)
            {
                _cacheSac = new EntityCache();
                InitialiseAllSac();
            }
            return _cacheSac;
        }

        public static EntityCache InstanceCot()
        {
            if (_cacheCot == null)
            {
                _cacheCot = new EntityCache();
                InitialiseAllCot();
            }
            return _cacheCot;
        }

        #region Initializes

        private static void InitialiseAllSac()
        {
            InitialiseLiquid2();
            InitialiseCliBas();
        }

        private static void InitialiseAllCot()
        {
            InitialiseCotCotista();
        }

        private static void InitialiseCliBas()
        {
            _listCliBas = new ObservableCollection<CliBasViewModel>();

            if (string.IsNullOrEmpty(DbContextFactory.ConnectString))
                return;

            try
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var clibasList = (from p in db.CLI_BAS
                                      orderby p.NOME
                                      select p).ToList();
                    foreach (var item in clibasList)
                    {
                        var sacClLIquid2List = (from p in _listLiquid2
                                                where p.ClcliCd == item.CODCLI
                                                select p).ToList();

                        var list = new ObservableCollection<SacClLiquid2ViewModel>();

                        sacClLIquid2List.ForEach(list.Add);

                        _listCliBas.Add(new CliBasViewModel { IsNew = false, CliBas = item, SacClLiquid2 = list });
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                _logger.Error("DoSaveCommand", ex);

                _logger.ErrorFormat("{0}{1}Validation errors:{1}{2}", ex, Environment.NewLine, ex.EntityValidationErrors.Select(e =>

                    string.Join(Environment.NewLine, e.ValidationErrors.Select(v => string.Format("{0} - {1}", v.PropertyName, v.ErrorMessage)))));

                string errorMessage = string.Empty;

                foreach (var failure in ex.EntityValidationErrors)
                {
                    foreach (var error in failure.ValidationErrors)
                    {
                        errorMessage += error.PropertyName + " " + error.ErrorMessage + "\n";

                    }
                }
                DXMessageBox.Show(errorMessage, "Erro ao Ler clientes", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message+"\n"+ex.InnerException.Message, "Erro ao Ler clientes", MessageBoxButton.OK, MessageBoxImage.Error);
                _logger.Error("DoSaveCommand", ex);
            }
        }

        private static void InitialiseCotCotista()
        {
            _listCotista = new ObservableCollection<CotCotistaViewModel>();
            if (string.IsNullOrEmpty(DbContextFactory.CotConnectString))
                return;

            try
            {
                using (var db = new CotEntities(DbContextFactory.CotConnectString))
                {
                    var dt = new DateTime(2014, 05, 06);
                    _listBancos = db.COT_BANCO.ToList();
                    var enderecos = db.COT_ENDERECO.Include(t => t.COT_TIPO_ENDERECO).ToList();
                    var contas = db.COT_CONTA_EXTERNA.ToList();

                    var cotistas = db.COT_COTISTA
                        .Include(cotista => cotista.COT_CLIENTE)
                        .Include(cotista => cotista.COT_TIPO_COTISTA)
                        .Include(cotista => cotista.COT_COTISTA_CLASSIFICACAO)
                        .OrderBy(cotista => cotista.COT_CLIENTE.NM_CLIENTE)
                        .ToList();

                    cotistas.ForEach(t =>
                                     {
                                         var cotEnderecos = new ObservableCollection<CotEnderecoViewModel>();
                                         var cotContas = new ObservableCollection<CotContaCorrenteViewModel>();

                                         enderecos.Where(e => e.CD_CLIENTE == t.CD_CLIENTE).ToList().ForEach(d => cotEnderecos.Add(new CotEnderecoViewModel
                                         {
                                             CotEndereco = d
                                         }));
                                         contas.Where(e => e.CD_COTISTA == t.CD_COTISTA).ToList().ForEach(c => cotContas.Add(new CotContaCorrenteViewModel
                                         {
                                             CotContaCorrente = c
                                         }));

                                         _listCotista.Add(new CotCotistaViewModel
                                         {
                                             CotCotista = t,
                                             IsNew = false,
                                             CotEnderecos = cotEnderecos,
                                             ContaCorrentes = cotContas

                                         });

                                     });
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                _logger.Error("DoSaveCommand", ex);

                _logger.ErrorFormat("{0}{1}Validation errors:{1}{2}", ex, Environment.NewLine, ex.EntityValidationErrors.Select(e =>

                    string.Join(Environment.NewLine, e.ValidationErrors.Select(v => string.Format("{0} - {1}", v.PropertyName, v.ErrorMessage)))));

                string errorMessage = string.Empty;

                foreach (var failure in ex.EntityValidationErrors)
                {
                    foreach (var error in failure.ValidationErrors)
                    {
                        errorMessage += error.PropertyName + " " + error.ErrorMessage + "\n";

                    }
                }
                DXMessageBox.Show(errorMessage, "Erro ao Ler cotistas", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message + "\n" + ex.InnerException.Message, "Erro ao Ler cotistas", MessageBoxButton.OK, MessageBoxImage.Error);
                _logger.Error("DoSaveCommand", ex);
            }
        }

        private static void InitialiseLiquid2()
        {
            _listLiquid2 = new ObservableCollection<SacClLiquid2ViewModel>();

            if (string.IsNullOrEmpty(DbContextFactory.ConnectString))
                return;
            try
            {
                using (var db = new SacEntities(DbContextFactory.ConnectString))
                {
                    var liquid2List = (from p in db.SAC_CL_LIQUID2
                                       select p).ToList();

                    foreach (var item in liquid2List)
                    {
                        _listLiquid2.Add(new SacClLiquid2ViewModel { IsNew = false, SacClLiquid2 = item });

                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                _logger.Error("DoSaveCommand", ex);

                _logger.ErrorFormat("{0}{1}Validation errors:{1}{2}", ex, Environment.NewLine, ex.EntityValidationErrors.Select(e =>

                    string.Join(Environment.NewLine, e.ValidationErrors.Select(v => string.Format("{0} - {1}", v.PropertyName, v.ErrorMessage)))));

                string errorMessage = string.Empty;

                foreach (var failure in ex.EntityValidationErrors)
                {
                    foreach (var error in failure.ValidationErrors)
                    {
                        errorMessage += error.PropertyName + " " + error.ErrorMessage + "\n";

                    }
                }
                DXMessageBox.Show(errorMessage, "Erro ao Ler clientes", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            catch (Exception ex)
            {
                DXMessageBox.Show(ex.Message + "\n" + ex.InnerException.Message, "Erro ao Ler clientes", MessageBoxButton.OK, MessageBoxImage.Error);
                _logger.Error("DoSaveCommand", ex);
            }
        }

        #endregion

        #region Methods properties
        public List<COT_BANCO> BancoList()
        {
            return _listBancos;
        }
        public ObservableCollection<CliBasViewModel> CliBasList()
        {
            return _listCliBas;
        }
        public ObservableCollection<CotCotistaViewModel> CotCotistaList()
        {
            return _listCotista;
        }
        #endregion
    }
}
