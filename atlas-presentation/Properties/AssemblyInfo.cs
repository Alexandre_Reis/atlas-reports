﻿using System.Reflection;
using System.Runtime.InteropServices;

/*--------------------------------------------------------------------
  Assembly project attributes
--------------------------------------------------------------------*/

[assembly: AssemblyTitle("atlas-presentation")]
[assembly: AssemblyDescription("atlas-presentation")]
[assembly: ComVisible(false)]
[assembly: Guid("cd3aed6f-f488-4105-9b47-ebba728fa5eb")]

/*--------------------------------------------------------------------
  Assembly product attributes
--------------------------------------------------------------------*/

[assembly: AssemblyVersion("4.0.0.1")]     // CLR Binding Version (API version: modified manually)
[assembly: AssemblyFileVersion("1.0.0.0")] // Win32 File version (modified by build system)