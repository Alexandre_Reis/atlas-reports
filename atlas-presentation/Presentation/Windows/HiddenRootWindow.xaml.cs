﻿using System.Windows;

namespace Atlas.Presentation.Windows
{
    /// <summary>
    /// The MainWindow from the WPF application perspective.
    /// </summary>
    public partial class HiddenRootWindow : Window
    {
        public HiddenRootWindow()
        {
            InitializeComponent();
        }
    }
}
