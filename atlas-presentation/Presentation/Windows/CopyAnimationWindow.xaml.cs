﻿using System;
using System.Windows;

namespace Atlas.Presentation.Windows
{
    /// <summary>
    /// Interaction logic for CopyAnimationWindow.xaml
    /// </summary>
    public partial class CopyAnimationWindow : Window
    {
        public static void ShowAnimationWindow()
        {
            CopyAnimationWindow window = new CopyAnimationWindow();
            window.ShowDialog();
        }

        public CopyAnimationWindow()
        {
            InitializeComponent();
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            Close();
        }
    }
}