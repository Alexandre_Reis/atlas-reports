﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Atlas.Link;
using Atlas.Link.Localization;
using Atlas.Link.Presentation;
using Atlas.Link.Settings;
using Atlas.Link.Views;
using Atlas.Link.Wpf.Control;
using Atlas.Presentation.ViewModels;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Core.Native;
using DevExpress.Xpf.Docking;
using DevExpress.XtraRichEdit.Layout;

namespace Atlas.Presentation.Windows
{
    public partial class ApplicationMainWindow : DXWindow
    {
        private static readonly Common.Logging.ILog Logger = Common.Logging.LogManager.Adapter.GetLogger(typeof(ApplicationMainWindow));

        #region Constructors

        public ApplicationMainWindow()
        {
            InitializeComponent();
        }

        #endregion

    }
}