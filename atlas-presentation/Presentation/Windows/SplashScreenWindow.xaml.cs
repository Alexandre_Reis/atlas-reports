﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;

namespace Atlas.Presentation.Windows
{
    /// <summary>
    /// Interaction logic for SplashScreenWindow.xaml
    /// </summary>
    public partial class SplashScreenWindow : Window, ISplashScreen
    {
        public SplashScreenWindow()
        {
            this.Visibility = System.Diagnostics.Debugger.IsAttached ? Visibility.Hidden : Visibility.Visible;
            InitializeComponent();
            this.board.Completed += OnAnimationCompleted;
        }

        #region Event Handlers

        private void OnAnimationCompleted(object sender, EventArgs e)
        {
            this.board.Completed -= OnAnimationCompleted;
            this.Close();
        }

        #endregion

        void ISplashScreen.CloseSplashScreen()
        {
            this.board.Begin(this);
        }

        void ISplashScreen.Progress(double value)
        {
            
        }

        void ISplashScreen.SetProgressState(bool isIndeterminate)
        {
        }
    }
}
