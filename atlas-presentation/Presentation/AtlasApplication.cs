﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Atlas.Link;
using Atlas.Link.Localization;
using Atlas.Link.Presentation;
using Atlas.Link.Settings;
using Atlas.Link.Views;
using Atlas.Link.Wpf.Control;
using Atlas.Presentation.Windows;
using Common.Logging;
using DevExpress.Xpf.Core;
using NLog;
using Application = System.Windows.Application;
using LogManager = Common.Logging.LogManager;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace Atlas.Presentation
{
    /// <summary>
    /// The base Application functionality for Atlas shell runtimes.
    /// </summary>
    public abstract class AtlasApplication : Application, IPresentationManager
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AtlasApplication" /> class.
        /// </summary>
        protected AtlasApplication()
        {
            // initialize logging
            _logger = LogManager.Adapter.GetLogger(GetType());

            // initialize logging
            _logger.Info("Application starting");
            _logger.InfoFormat("Executable Path = {0}", AppEnvironment.RuntimeExe);
            _logger.InfoFormat("Compile Version = {0}", AppEnvironment.CompileVersion);
            _logger.InfoFormat("Product Version = {0}", AppEnvironment.ProductVersion);
            _logger.InfoFormat("App Data Path = {0}", AppEnvironment.AppDataPath);
            _logger.InfoFormat("User Data Path = {0}", AppEnvironment.UserDataPath);

            // set runtime state
            this.State = PresentationState.Initializing;

            // override default behaviour
            this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            // hook into application unhandled-exception event
            this.DispatcherUnhandledException += AtlasApplication_DispatcherUnhandledException;

            // Load resources dictionaries            
            this.Resources.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("/AtlasLink.Core;component/Wpf/Resources/Styles/AtlasStylesDictionary.xaml", UriKind.Relative) });
            this.Resources.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("/AtlasLink.Core;component/Wpf/Resources/Templates/EditorsCustomTemplates.xaml", UriKind.Relative) });
            this.Resources.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("/AtlasLink.Core;component/Wpf/Resources/Templates/AtlasTemplatesDictionary.xaml", UriKind.Relative) });
        }

        #endregion

        #region Fields

        private Window _mainWindow;
        private readonly ILog _logger;

        #endregion

        #region Properties

        protected ILog Logger
        {
            get { return _logger; }
        }

        #endregion

        #region IPresentationManager Members

        public ImageSource DefaultIcon { get; private set; }

        #region Application Management

        private void InitializeThemes(ApplicationConfig appConfig)
        {
            IList<ViewTheme> themeList = GetThemes();
            ViewTheme themeDefault = themeList.FirstOrDefault(x => x.ID == "HybridApp");

            if (themeDefault != null)
                CurrentTheme = themeDefault;
            else if (themeList.Count > 0)
                CurrentTheme = themeList[0];

            PresentationManager.Current.CurrentTheme = themeDefault;
        }
        private void InitializeCultures(ApplicationConfig appConfig)
        {
            // define a fallback default culture
            var cultureDefault = new CultureInfo("en-GB");

            foreach (var language in TranslationManager.Instance.Languages)
            {
                if (language.CultureKey == appConfig.UiLocale)
                {
                    // set the default to the configured default
                    cultureDefault = language.Culture;
                    break;
                }
            }

            TranslationManager.Instance.CurrentLanguage = cultureDefault;
        }

        /// <summary>
        /// Gets the state of the presentation layer.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public PresentationState State { get; private set; }

        /// <summary>
        /// Starts the presentation manager (UI).
        /// </summary>
        public void Start()
        {
            if (State != PresentationState.Initializing)
                throw new Exception("An invalid attempt was made to start the PresentationManager more than once");

            // get a reference to the application settings
            ApplicationConfig appConfig = SettingsManager.Current.GetApplicationConfig();

            InitializeThemes(appConfig);
            InitializeCultures(appConfig);

            // create a hidden main window
            this.MainWindow = new HiddenRootWindow();
            this.MainWindow.Hide();

            // set presentation layer state
            this.State = PresentationState.Authenticating;

            // perform authentication in the background
            //IAuthenticationProvider authProvider = AuthenticationManager.Provider;
            //var waitHandle = authProvider.BeginAuthenticate();

            // create the "visual main" window
            _mainWindow = new ApplicationMainWindow();
            _mainWindow.Closed += (s, e) => Stop(); // stop the presentation layer when the main visual gets closed
            this.DefaultIcon = _mainWindow.Icon;

            // wait for the authentication to complete
            //waitHandle.WaitOne(); // will block this thread until authentication completes

            //// determine the result of authentication
            //if (authProvider.Principal.IsAuthenticated)
            //{
            //    // update the current thread principal
            //    Thread.CurrentPrincipal = authProvider.Principal;

                // set presentation layer state
                this.State = PresentationState.Running;

                // display the main visual window
                _mainWindow.Show();

                //IMPORTANT: run the application (standard WPF Application Run() command invocation)
                Run(); // will block this thread until the Stop() method is called
            //}
            //else
            //{
            //    Stop();
            //}
        }
        /// <summary>
        /// Stops the presentation manager (UI).
        /// </summary>
        public void Stop()
        {
            Logger.Info("Application shutting down");

            // set presentation layer state
            this.State = PresentationState.Stopping;

            // close open child windows gracefully
            //TODO...

            // standard application termination
            this.Shutdown();

            // set presentation layer state
            this.State = PresentationState.Stopped;

            Logger.Info("Application stopped");
        }

        #endregion

        #region Window Management

        /// <summary>
        /// Gets the dispatcher for the main window.
        /// </summary>
        /// <value>
        /// The dispatcher.
        /// </value>
        public new Dispatcher Dispatcher
        {
            get { return _mainWindow.Dispatcher; }
        }

        /// <summary>
        /// Gets the open child windows.
        /// </summary>
        /// <value>
        /// The open child windows.
        /// </value>
        public IList<Object> OpenChildWindows
        {
            get { throw new NotImplementedException(); }
        }

        
        #endregion

        #region Dialogs

        public Boolean MessageBoxYesNo(LocalizedString text, params Object[] args)
        {
            return (DXMessageBox.Show(LocalizedString.Format(text, args), StringConst.Caption.Confirm.Text, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes);
        }
        public Boolean MessageBoxYesNo(LocalizedString text, string key, params Object[] args)
        {
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsShowDialog userSetdialog = userSettings.ShowDialogs.FirstOrDefault(x => x.Key == key);

            if (userSetdialog == null)
            {
                userSetdialog = new UserSettingsShowDialog { Key = key, ShowDialog = true };
                userSettings.ShowDialogs.Add(userSetdialog);
            }

            if (userSetdialog.ShowDialog)
            {
                var msgBoxInfo = new MessageBoxExInfo
                {
                    Text = LocalizedString.Format(text, args),
                    Title = StringConst.Caption.Confirm.Text,
                    OptionCheckText = StringConst.Caption.DontShowMessageAgain.Text,
                    Icon = MessageBoxExIcons.Question,
                    Buttons = MessageBoxExButtons.YesNo,
                    UseOptionCheck = true,
                    OptionCheckValue = false
                };

                var result = MessageBoxEx.Show(msgBoxInfo);

                userSetdialog.ShowDialog = !msgBoxInfo.OptionCheckValue ?? true;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);

                return result == "Yes";
            }

            return true;
        }

        public Boolean MessageBoxOkCancel(LocalizedString text, params Object[] args)
        {
            return (DXMessageBox.Show(LocalizedString.Format(text, args), StringConst.Caption.Confirm.Text, MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK);
        }

        public Boolean MessageBoxOkCancel(LocalizedString text, string key, params Object[] args)
        {
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsShowDialog userSetdialog = userSettings.ShowDialogs.FirstOrDefault(x => x.Key == key);

            if (userSetdialog == null)
            {
                userSetdialog = new UserSettingsShowDialog { Key = key, ShowDialog = true };
                userSettings.ShowDialogs.Add(userSetdialog);
            }

            if (userSetdialog.ShowDialog)
            {
                var msgBoxInfo = new MessageBoxExInfo
                {
                    Text = LocalizedString.Format(text, args),
                    Title = StringConst.Caption.Confirm.Text,
                    OptionCheckText = StringConst.Caption.DontShowMessageAgain.Text,
                    Icon = MessageBoxExIcons.Question,
                    Buttons = MessageBoxExButtons.OKCancel,
                    UseOptionCheck = true,
                    OptionCheckValue = false
                };

                String result = MessageBoxEx.Show(msgBoxInfo);

                userSetdialog.ShowDialog = !msgBoxInfo.OptionCheckValue ?? true;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);

                return result == "OK";
            }

            return true;
        }

        public void MessageBoxInfo(string text, params Object[] args)
        {
            DXMessageBox.Show(String.Format(text, args), StringConst.Caption.Information.Text, MessageBoxButton.OK, MessageBoxImage.Information);
        }
        public void MessageBoxInfo(LocalizedString text, params Object[] args)
        {
            MessageBoxInfo(text.Text, args);
        }
        public void MessageBoxInfo(LocalizedString text, string key, params Object[] args)
        {
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsShowDialog userSetdialog = userSettings.ShowDialogs.FirstOrDefault(x => x.Key == key);

            if (userSetdialog == null)
            {
                userSetdialog = new UserSettingsShowDialog { Key = key, ShowDialog = true };
                userSettings.ShowDialogs.Add(userSetdialog);
            }

            if (userSetdialog.ShowDialog)
            {
                var msgBoxInfo = new MessageBoxExInfo
                {
                    Text = LocalizedString.Format(text, args),
                    Title = StringConst.Caption.Confirm.Text,
                    OptionCheckText = StringConst.Caption.DontShowMessageAgain.Text,
                    Icon = MessageBoxExIcons.Information,
                    Buttons = MessageBoxExButtons.OKCancel,
                    UseOptionCheck = true,
                    OptionCheckValue = false
                };

                MessageBoxEx.Show(msgBoxInfo);

                userSetdialog.ShowDialog = !msgBoxInfo.OptionCheckValue ?? true;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);
            }
        }

        public void MessageBoxWarn(LocalizedString text, params Object[] args)
        {
            DXMessageBox.Show(LocalizedString.Format(text, args), StringConst.Caption.Warning.Text, MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        public void MessageBoxWarn(LocalizedString text, string key, params Object[] args)
        {
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsShowDialog userSetdialog = userSettings.ShowDialogs.FirstOrDefault(x => x.Key == key);

            if (userSetdialog == null)
            {
                userSetdialog = new UserSettingsShowDialog { Key = key, ShowDialog = true };
                userSettings.ShowDialogs.Add(userSetdialog);
            }

            if (userSetdialog.ShowDialog)
            {
                var msgBoxInfo = new MessageBoxExInfo
                {
                    Text = LocalizedString.Format(text, args),
                    Title = StringConst.Caption.Warning.Text,
                    OptionCheckText = StringConst.Caption.DontShowMessageAgain.Text,
                    Icon = MessageBoxExIcons.Warning,
                    Buttons = MessageBoxExButtons.OK,
                    UseOptionCheck = true,
                    OptionCheckValue = false
                };

                MessageBoxEx.Show(msgBoxInfo);

                userSetdialog.ShowDialog = !msgBoxInfo.OptionCheckValue ?? true;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);
            }

        }

        public void MessageBoxError(LocalizedString text, params Object[] args)
        {
            DXMessageBox.Show(LocalizedString.Format(text, args), StringConst.Caption.Error.Text, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void MessageBoxError(LocalizedString text, string key, params Object[] args)
        {
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsShowDialog userSetdialog = userSettings.ShowDialogs.FirstOrDefault(x => x.Key == key);

            if (userSetdialog == null)
            {
                userSetdialog = new UserSettingsShowDialog { Key = key, ShowDialog = true };
                userSettings.ShowDialogs.Add(userSetdialog);
            }

            if (userSetdialog.ShowDialog)
            {
                var msgBoxInfo = new MessageBoxExInfo
                {
                    Text = LocalizedString.Format(text, args),
                    Title = StringConst.Caption.Error.Text,
                    OptionCheckText = StringConst.Caption.DontShowMessageAgain.Text,
                    Icon = MessageBoxExIcons.Error,
                    Buttons = MessageBoxExButtons.OK,
                    UseOptionCheck = true,
                    OptionCheckValue = false
                };

                MessageBoxEx.Show(msgBoxInfo);

                userSetdialog.ShowDialog = !msgBoxInfo.OptionCheckValue ?? true;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);
            }
        }

        public Boolean CustomMessageBox(MessageBoxExInfo msgBoxInfo)
        {
            msgBoxInfo.Buttons = new List<MessageBoxExButton>
            {
                new MessageBoxExButton("Yes",StringConst.Data.Yes.Text),
                new MessageBoxExButton("No",StringConst.Data.No.Text),
            };

            ImageSource img = Imaging.CreateBitmapSourceFromHIcon(SystemIcons.Question.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            msgBoxInfo.Icon = img;
            var result = MessageBoxEx.Show(msgBoxInfo);
            return result == "Yes";

        }

        public Boolean FileOpenDialog(LocalizedString title, String defaultExt, String filter, ref String fileName)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = title.Text;
            dlg.FileName = fileName;
            dlg.DefaultExt = defaultExt;
            dlg.Filter = filter;

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                fileName = dlg.FileName;
                return true;
            }

            return false;
        }

        public Boolean DirectoryOpenDialog(LocalizedString description, ref String selectedPath, string key)
        {
            // get persisted directory from user settings
            UserSettingsConfig userSettings = SettingsManager.Current.GetUserSettingsConfig();
            UserSettingsDirectory usetSetdir = userSettings.DialogDirectories.FirstOrDefault(x => x.Key == key);
            if (usetSetdir == null)
            {
                usetSetdir = new UserSettingsDirectory { Key = key, Value = "" };
                userSettings.DialogDirectories.Add(usetSetdir);
            }

            var dlg = new FolderBrowserDialog
            {
                SelectedPath = usetSetdir.Value, 
                Description = description.Text
            };

            // Show open file dialog box
            DialogResult result = dlg.ShowDialog();

            if (result.ToString() == "OK")
            {
                selectedPath = dlg.SelectedPath;
                usetSetdir.Value = dlg.SelectedPath;
                SettingsManager.Current.SetUserSettingsConfig(userSettings);
                return true;
            }

            return false;
        }

        public Boolean FileSaveDialog(LocalizedString title, String defaultExt, String filter, ref String fileName)
        {
            var dlg = new SaveFileDialog
            {
                Title = title.Text,
                FileName = fileName,
                DefaultExt = defaultExt,
                Filter = filter
            };

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                fileName = dlg.FileName;
                return true;
            }
            
            return false;
        }

        public void WaitDialog(WaitDialogType dialogType)
        {
            switch (dialogType)
            {
                case WaitDialogType.Spin:
                    CopyAnimationWindow.ShowAnimationWindow();
                    break;
                case WaitDialogType.Clipboard:
                    CopyAnimationWindow.ShowAnimationWindow();
                    break;
            }
        }

        #endregion

        #region Service Handling

        /// <summary>
        /// Gets a list of available styles.
        /// </summary>
        /// <returns></returns>
        public IList<ViewTheme> GetThemes()
        {
            if (_themes == null)
            {

             

                _themes = new List<ViewTheme>();

                foreach (var theme in Theme.Themes)
                {
                    _themes.Add(new ViewTheme { ID = theme.Name, Name = new LocalizedLiteralString("Theme." + theme.Name, theme.FullName) });
                }
            }

            return _themes;
        }
        private IList<ViewTheme> _themes;

        /// <summary>
        /// Gets and sets the active theme
        /// </summary>
        /// <value>
        /// The current theme.
        /// </value>
        public ViewTheme CurrentTheme
        {
            get { return _currentTheme; }
            set
            {
                if (value != null)
                    ThemeManager.ApplicationThemeName = value.ID;
            }
        }
        private ViewTheme _currentTheme;

        /// <summary>
        /// Sets the audit trail.
        /// </summary>
        /// <param name="key">The key.</param>
        public void SetAuditTrail(String key)
        {
            EventHandler<AuditTrailEventArgs> handler = HandleAuditTrailRequest;

            if (handler != null)
                handler(this, new AuditTrailEventArgs(key));
        }

        /// <summary>
        /// Allows Audit Trail service handlers to respond to requests.
        /// </summary>
        public event EventHandler<AuditTrailEventArgs> HandleAuditTrailRequest;

        /// <summary>
        /// Writes to the application log.
        /// </summary>
        /// <param name="logData">The log data.</param>
        public void WriteApplicationLog(ApplicationLogData logData)
        {
            EventHandler<ApplicationLogEventArgs> handler = HandleApplicationLogRequest;

            if (handler != null)
                handler(this, new ApplicationLogEventArgs(logData));
        }

        /// <summary>
        /// Allows Application Log service handlers to respond to requests.
        /// </summary>
        public event EventHandler<ApplicationLogEventArgs> HandleApplicationLogRequest;

        #endregion

        #endregion

        #region Application Event Overrides

        private void AtlasApplication_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                e.Handled = true;
                Logger.Error(String.Format("Unhandled application exception on thread {0}", e.Dispatcher.Thread.ManagedThreadId), e.Exception);
            }
            catch (Exception ex)
            {
                Console.WriteLine("AtlasBusApp_DispatcherUnhandledException: " + ex.ToString());
            }
        }

        #endregion
    }
}