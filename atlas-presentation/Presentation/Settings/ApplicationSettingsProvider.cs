﻿using System;
using System.Collections.Generic;
using Atlas.Link;
using Atlas.Link.Settings;

namespace Atlas.Presentation.Settings
{
    /// <summary>
    /// Concrete implementation of application settings.
    /// </summary>
    public class ApplicationSettingsProvider : Atlas.Link.Settings.IApplicationSettings
    {
        internal static String ApplicationConfigName = "application.config";
        internal static String MainFormConfigName = "main-form.config";
        internal static String UserSettingsConfigName = "user-settings.config";

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingsProvider" /> class.
        /// </summary>
        public ApplicationSettingsProvider()
        {
            _settingsStore = ServiceLocator.Get<ISettingsStorageProvider>();
        }

        #endregion

        #region Fields

        private Atlas.Link.Settings.ISettingsStorageProvider _settingsStore;

        #endregion

        #region Application Configuration

        private Atlas.Link.Settings.ApplicationConfig _applicationConfig = null;
        private Atlas.Link.Settings.UserSettingsConfig _userSettingsConfig = null;

        /// <summary>
        /// Determines whether the application configuration file exists.
        /// </summary>
        /// <value>
        /// The application configuration exists.
        /// </value>
        public Boolean ApplicationConfigExists
        {
            get { return _settingsStore.AppSettingsExists(ApplicationConfigName); }
        }

        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <returns></returns>
        public Atlas.Link.Settings.ApplicationConfig GetApplicationConfig()
        {
            if (_applicationConfig == null)
            {
                try
                {
                    if (!ApplicationConfigExists)
                        SetApplicationConfig(new ApplicationConfig()
                        {
                            UiLocale = "en-GB",
                            XlsFolder = "C:\temp",
                            Databases = new List<DbConfig>
                                      {
                                          new DbConfig { Name="SAC", DatabaseName = "database", Password = "password", ProviderName = "System.Data.SqlClient", ServerName = "servername",  UseriD = "userid"},
                                          new DbConfig { Name="COT", DatabaseName = "database", Password = "password", ProviderName = "System.Data.SqlClient", ServerName = "servername",  UseriD = "userid"}
                                      }
                        });

                    // attempt to get the configuration instance from storage
                    _applicationConfig = _settingsStore.ReadAppSettings<Atlas.Link.Settings.ApplicationConfig>(ApplicationConfigName);
                }
                catch (Exception)
                {
                    throw new RuntimeException("Error.ApplicationConfigRead", "Unable to read the application config file: {0}", ApplicationConfigName);
                }

                if (_applicationConfig == null)
                {
                    throw new RuntimeException("Error.ApplicationConfigMissing", "Unable to find the application config file: {0}", ApplicationConfigName);
                }
            }

            // return a clone of the Config Settings in case any code attempts a global modification
            return Atlas.Link.Model.Utils.Clone(_applicationConfig);
        }
        /// <summary>
        /// Saves the application configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public void SetApplicationConfig(Atlas.Link.Settings.ApplicationConfig config)
        {
            // cache copy in memory
            _applicationConfig = config;

            // save to disk
            _settingsStore.SaveAppSettings<Atlas.Link.Settings.ApplicationConfig>(ApplicationConfigName, config);
        }

        #endregion

        #region User Settings

        public Atlas.Link.Settings.UserSettingsConfig GetUserSettingsConfig()
        {
            if (_userSettingsConfig == null)
            {
                // attempt to get the configuration instance from storage
                _userSettingsConfig = _settingsStore.ReadAppSettings<Atlas.Link.Settings.UserSettingsConfig>(UserSettingsConfigName);

                if (_userSettingsConfig == null)
                {
                    List<Atlas.Link.Settings.UserSettingsDirectory> dir = new List<Atlas.Link.Settings.UserSettingsDirectory>();
                    _userSettingsConfig = new Atlas.Link.Settings.UserSettingsConfig();
                    _userSettingsConfig.DialogDirectories = dir;
                    // persist the new values
                    _settingsStore.SaveAppSettings<Atlas.Link.Settings.UserSettingsConfig>(UserSettingsConfigName, _userSettingsConfig);
                }

            }

            // return a clone of the Config Settings in case any code attempts a global modification
            return Atlas.Link.Model.Utils.Clone(_userSettingsConfig);
        }

        public void SetUserSettingsConfig(Atlas.Link.Settings.UserSettingsConfig config)
        {
            // cache copy in memory
            _userSettingsConfig = config;

            // save to disk
            _settingsStore.SaveAppSettings<Atlas.Link.Settings.UserSettingsConfig>(UserSettingsConfigName, config);
        }

        #endregion

        #region Main Form Configuration

        /// <summary>
        /// Gets the main form configuration.
        /// </summary>
        /// <returns></returns>
        public Atlas.Link.Settings.MainFormConfig GetMainFormConfig()
        {
            return new Atlas.Link.Settings.MainFormConfig();
        }
        /// <summary>
        /// Saves the main form configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public void SetMainFormConfig(Atlas.Link.Settings.MainFormConfig config)
        {
            //
        }

        #endregion

        #region View Configuration

        /// <summary>
        /// Gets the view configuration.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public Atlas.Link.Settings.ViewConfig GetViewConfig(String viewName)
        {
            return new Atlas.Link.Settings.ViewConfig();
        }
        /// <summary>
        /// Saves the view configuration.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="config">The configuration.</param>
        public void SetViewConfig(String viewName, Atlas.Link.Settings.ViewConfig config)
        {
            //
        }

        #endregion
    }
}