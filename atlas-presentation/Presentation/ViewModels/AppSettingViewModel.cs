﻿using System;
using System.Collections.Generic;
using Atlas.Link.Localization;
using Atlas.Link.Presentation;
using Atlas.Link.Settings;


namespace Atlas.Presentation.ViewModels
{
    public class AppSettingViewModel 
    {
        #region Construction

        public AppSettingViewModel()
        {
            this.SaveCommand = new Apex.MVVM.Command(ExecuteSaveCommand, false);

            if (this.ApplicationConfig == null)
            {
                ApplicationConfig = SettingsManager.Current.GetApplicationConfig();
            }

        }

        #endregion

        #region fields
        private ApplicationConfig _applicationConfig;
        #endregion

        #region Binding Properties
        public ApplicationConfig ApplicationConfig
        {
            get
            {
                return _applicationConfig;
            }
            set
            {
                _applicationConfig = value;
            }
        }



        #endregion

        #region Bindable Lookup Collections

        public IEnumerable<TranslationLanguage> UILocaleList
        {
            get { return Atlas.Link.Localization.TranslationManager.Instance.Languages; }
        }

        public IList<ViewTheme> UIThemeList
        {
            get { return PresentationManager.Current.GetThemes(); }
        }

        #endregion

        #region Binding Command

        public Apex.MVVM.Command SaveCommand { get; private set; }

        protected virtual void ExecuteSaveCommand()
        {
            SettingsManager.Current.SetApplicationConfig(ApplicationConfig);
            
        }

        #endregion

        #region Overrides
        
        //TODO: re-implemnt validation 

        //protected override void DoValidation(string propertyName)
        //{

        //    base.ClearPropertyErrors(propertyName);

        //    if (propertyName.Equals("HostName", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        if (String.IsNullOrWhiteSpace(this.HostName))
        //            base.SetPropertyError(propertyName, Localization.StringConst.Validation.PropertyRequiredError);
        //        else if (this.HostName.Trim().IndexOf(' ') > 0)
        //            base.SetPropertyError(propertyName, Localization.StringConst.Validation.SpacesNotAllowedError);
        //    }

        //    else if (propertyName.Equals("HostPort", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        if (this.HostPort == null || this.HostPort == 0)
        //            base.SetPropertyError(propertyName, Localization.StringConst.Validation.PropertyRequiredError);
        //    }

        //    //else if (propertyName.Equals("UITheme", StringComparison.InvariantCultureIgnoreCase))
        //    //{
        //    //    if (String.IsNullOrWhiteSpace(this.UITheme))
        //    //        base.SetPropertyError(propertyName, Localization.StringConst.Validation.PropertyRequiredError);
        //    //}

        //    //else if (propertyName.Equals("UILocale", StringComparison.InvariantCultureIgnoreCase))
        //    //{
        //    //    if (String.IsNullOrWhiteSpace(this.UILocale))
        //    //        base.SetPropertyError(propertyName, Localization.StringConst.Validation.PropertyRequiredError);
        //    //}
        //}

        

        #endregion

    }
}
