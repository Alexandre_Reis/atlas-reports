﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Atlas.Reports.EntityModel;
using Atlas.Reports.Model;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using DevExpress.Utils;

namespace  Atlas.Presentation.ViewModels
{
    public class ApplicationMainViewModel
    {
        private bool loaded = false;
        private List<ModuleViewModelBase> modules;

        public ApplicationMainViewModel()
        {
            modules = new List<ModuleViewModelBase>();
            modules.Add(ViewModelSource.Create(() => new ModuleViewModel<CLI_BAS, CLI_BAS>("Reports", "ReportsView", null, this)).SetIcon("Reports").SetColor(0x00, 0x87, 0x9C).Do(x => x.GroupHeader = "REPORTS"));

            SelectedModule = modules.First(m => m.DocumentType == "ReportsView");
            if (!this.IsInDesignMode())
                InitializeInRuntime();
        }

        protected virtual void InitializeInRuntime()
        {
            foreach (ModuleViewModelBase module in Modules)
                module.Initialize();
        }

        [Command]
        public void OnLoaded()
        {
            loaded = true;
            OnSelectedModuleChanged(null);
        }


        public IEnumerable<ModuleViewModelBase> Modules
        {
            get { return modules; }
        }

        public virtual ModuleViewModelBase SelectedModule { get; set; }

        //[ServiceProperty(Key = "FinishModuleChangingDispatcherService"), Required]
        protected virtual IDispatcherService FinishModuleChangingDispatcherService
        {
            get { return null; }
        }

        protected virtual void OnSelectedModuleChanged(ModuleViewModelBase oldModule)
        {
            if (!loaded) return;
            ModuleChangingInProgress = true;
            try
            {
                if (SelectedModule != null)
                    SelectedModule.Show();
            }
            finally
            {
                FinishModuleChangingDispatcherService.BeginInvoke(() => ModuleChangingInProgress = false);
            }
        }

        public virtual bool ModuleChangingInProgress { get; set; }

        protected virtual void OnModuleChangingInProgressChanged()
        {
            if (!ModuleChangingInProgress)
                Application.Current.Windows.Cast<Window>().LastOrDefault().Do(w => w.Activate());
        }
    }

    public class ModuleViewModelBase
    {
        protected object parentViewModel;

        public ModuleViewModelBase(string title, string documentType, object parentViewModel)
        {
            this.SetParentViewModel(parentViewModel);
            this.parentViewModel = parentViewModel;
            DocumentType = documentType;
            Title = title;
        }

        public virtual void Initialize()
        {
        }

        public string DocumentType { get; set; }
        public string Title { get; set; }
        public virtual IDocument Document { get; set; }
        protected object Parameter { get; set; }
        public virtual Color Color { get; set; }
        public virtual ImageSource Icon { get; set; }
        public virtual string GroupHeader { get; set; }

        public void Show()
        {
            if (Document == null)
            {
                Document = DocumentManagerService.CreateDocument(this.DocumentType, null, Parameter, this.parentViewModel);
                Document.Title = Title ;
                Document.DestroyOnClose = false;
            }
            Document.Show();
            OnDocumentShowed();
        }

        protected virtual void OnDocumentShowed()
        {
        }

        protected virtual IDocumentManagerService DocumentManagerService
        {
            get { return null; }
        }
    }


    public class ModuleViewModel<TEntity, TFilterTreeModelSettings> : ModuleViewModelBase where TFilterTreeModelSettings : new()
    {
        private string staticFilterName;

        public ModuleViewModel(string title, string viewName, string staticFilterName, object parentViewModel) : base(title, viewName, parentViewModel)
        {
            this.staticFilterName = staticFilterName;
        }

        public override void Initialize()
        {
            base.Initialize();
            
        }

        
        public ModuleViewModel<TEntity, TFilterTreeModelSettings> SetIcon(string iconName)
        {
            //Icon = new BitmapImage(AssemblyHelper.GetResourceUri(typeof (ModuleViewModel<TEntity, TFilterTreeModelSettings>).Assembly, string.Format("Images/Menu/{0}.png", iconName)));
            return this;
        }

        public ModuleViewModel<TEntity, TFilterTreeModelSettings> SetColor(byte r, byte g, byte b)
        {
            Color = Color.FromRgb(r, g, b);
            return this;
        }

        protected override void OnDocumentShowed()
        {
            //var viewModel = Document.Content as ISupportFiltering<TEntity>;
            //if (viewModel != null)
            //{
            //    FilterTreeViewModel.ChangeViewModel(viewModel);
            //}
        }

        
    }

    
}
