﻿namespace Atlas.Presentation.ViewModels
{
    /// <summary>
    /// View model for Log Configuration
    /// </summary>
    public class LogConfigurationViewModel 
    {
        private Atlas.Link.Logging.LogConfiguration _logConfiguration;
        /// <summary>
        /// Gets or sets the log configuration.
        /// </summary>
        /// <value>
        /// The log configuration.
        /// </value>
        public Atlas.Link.Logging.LogConfiguration LogConfiguration
        {
            get { return _logConfiguration; }
            set
            {
                if (value != _logConfiguration)
                {
                    _logConfiguration = value;
                    
                }
            }
        }

        /// <summary>
        /// Gets the save command.
        /// </summary>
        /// <value>
        /// The save command.
        /// </value>
        public Apex.MVVM.Command SaveCommand { get; private set; }

        /// <summary>
        /// Executes the save command.
        /// </summary>
        protected virtual void ExecuteSaveCommand()
        {
            var atlasNLogFactory = Common.Logging.LogManager.Adapter as Atlas.Link.Logging.AtlasNLogFactory;
            if (atlasNLogFactory != null)
            {
                atlasNLogFactory.SaveConfiguration();
            }

            
        }
        

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LogConfigurationViewModel"/> class.
        /// </summary>
        public LogConfigurationViewModel()
        {
            SaveCommand = new Apex.MVVM.Command(ExecuteSaveCommand, false);

            LogConfiguration = Atlas.Link.Logging.AtlasNLogFactory.LogConfiguration;
        }

        #endregion

        
    }
}