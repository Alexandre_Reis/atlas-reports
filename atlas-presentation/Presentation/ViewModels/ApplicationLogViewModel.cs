﻿using System;
using System.Collections.ObjectModel;
using Atlas.Link.Presentation;
using System.Windows;

namespace Atlas.Presentation.ViewModels
{
    public class ApplicationLogViewModel
    {
        #region Construction

        public ApplicationLogViewModel()
        {
            _itemData = new ObservableCollection<ApplicationLogData>();
            PresentationManager.Current.HandleApplicationLogRequest += new EventHandler<ApplicationLogEventArgs>(Current_HandleApplicationLogRequest);
        }
        #endregion

        #region Thread method add log info
        private void AddLog(ApplicationLogData info)
        {
            _itemData.Insert(0,info);
        }
        #endregion

        #region Fields

        private ObservableCollection<ApplicationLogData> _itemData;
        
        #endregion

        #region Properties

        public ObservableCollection<ApplicationLogData> AppLogData
        {
            get { return _itemData; }
        }

        #endregion

        #region Events
        void Current_HandleApplicationLogRequest(object sender, ApplicationLogEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(new Action<ApplicationLogData>(AddLog), e.LogData);
        }
        #endregion
    }
}